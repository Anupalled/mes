package pages.affinityDashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;

public class LoginPage {
	WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="username")	
	public  WebElement userName;
	
	@FindBy(id="password")	
	public  WebElement password;
	
	@FindBy(css="button[class='mas-btn mas-btn-primary']")	
	public  WebElement continueButton;
	
	@FindBy(css="a[class='site-logo-wrapper']")	
	public  WebElement homeIcon;	
	
	public void login(String usrname, String pass) throws InterruptedException {
		setInput(userName, usrname);
		setInput(password, pass);
		System.out.println("Clicking continue button...");
		clickElement(continueButton);
		System.out.println("Waiting for page to load...");
		waitForElementToDisappear(By.cssSelector("div[class='mas-loading-indicator']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		System.out.println("Page  loaded...");
	}
}
