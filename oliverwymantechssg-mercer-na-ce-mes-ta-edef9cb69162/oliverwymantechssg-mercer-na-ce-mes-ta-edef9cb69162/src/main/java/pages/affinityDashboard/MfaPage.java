package pages.affinityDashboard;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class MfaPage extends InitTests {
	WebDriver driver;
	static String Code;
	String outlookurl = "https://apac1mail.mmc.com/OWA/";
	static String browser = InitTests.BROWSER_TYPE;

	public MfaPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//h2[contains(text(),'almost there')]")
	public WebElement mfapageheader;

	@FindBy(xpath = "//span[text()='Other']")
	public WebElement othermail;

	
	@FindBy(css = "input[id*='okta-signin-username']")
	public static WebElement outlookusername;

	@FindBy(css = "input[id='okta-signin-password']")
	public static WebElement outlookPassword;

	@FindBy(xpath = "//*[@id='okta-signin-submit']")
	public WebElement signInBtn;

	@FindBy(xpath = "//label[contains(text(),'Email ru*****@mercer.com')]")
	public WebElement selectcontact;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	public WebElement sendContBtn;

	@FindBy(css = "input[id='verificationCode']")
	public WebElement enterCodeTxt;

	@FindBy(css = "input[name*='proceed']")
	public WebElement confirmCodeBtn;

	@FindBy(xpath = "//*[@class='ms-Button-icon _1IolX_6rKX93IRLO1O_oCP SiBv31IfzVFl2WodUyTbP _1XmKWz3L5NcheRkx38wxTL _3o738zmfzs1fXK1kxpiX5 _1OPSsVxfk_GWTnwo-KIMYX flipForRtl icon-90']")
	public WebElement Mfa_InboxFolder;

	@FindBy(xpath = "//span[@class='_16aPYHYIOVxzNYTA4BGnwS _1XmKWz3L5NcheRkx38wxTL _3FwRpWCSns-2PgbiDhEErG' and contains(text(),'MFA')]")
	public WebElement mfa_Folder;

	@FindBy(xpath = "//span[contains(text(),'Verification code')]")
	//@FindBy(xpath = "(//div[@class='_1hHMVrN7VV4d6Ylz-FsMuP _3BM_8P4ZFTWBjM1l53p2-M'])[1]")
	public WebElement Mfa_Mail;

	@FindBy(xpath = "(//div[contains(string(),'Administrative bypass of MFA Service')])[8]")
	public WebElement errorMessage;

	@FindBy(xpath = "(//a[@id='mfa-choose-another-method'])[1]")
	public WebElement resendOTP;
	
	//@FindBy(xpath = "(//div[@class='_1hHMVrN7VV4d6Ylz-FsMuP _3BM_8P4ZFTWBjM1l53p2-M'])[1]")
	@FindBy(xpath = "//span[contains(text(),'Verification code')]")
	public WebElement firstMessage;
	
	@FindBy(xpath = "//div[contains(text(),'Your verification code is')]")
	public WebElement verifiationCodeText;
	
	@FindBy(xpath = "//label[contains(text(),'Email au*****@mercer.com')]")
	public WebElement verifiationMethod1;
	
	@FindBy(xpath = "//label[contains(text(),'Email an*****@mercer.com')]")
	public WebElement verifiationMethods;
	
	@FindBy(css = "button[class='mas-btn mas-btn-primary']")
	public WebElement verifiationContinue;
	
	@FindBy(xpath = "//span[contains(text(),'Delete')]")
	public WebElement deleteMFAMailButton;
	//ms-Button-label label-93
	//ms-Button-flexContainer flexContainer-45
	@FindBy(id = "verificationCode")
	public WebElement verifiationCodeInput;
	
	@FindBy(css = "button[class='mas-btn mas-btn-primary']")
	public WebElement verifiationCodeContinue;
	
	@FindBy(xpath = "//label[contains(text(),'Email au*****@mercer.com')]")
	public WebElement verifiationMethod2;
	
	@FindBy(xpath = "//*[@id=\"app\"]/div/div[2]/div[1]/div/div/div/div[1]/div[1]/div/button/span/i")
	public WebElement Burgerbutton;
		

	public void fetchMFACodeFromOutlookEmail(String url,String MFAEmailId, String MFAEmailPassword ) throws InterruptedException {
		
			
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(url);
		waitForElementToDisplay(outlookusername);
		outlookusername.click();
		outlookusername.clear();
		// outlookemailid.sendKeys(MFAEmailId);
		setInput(outlookusername, MFAEmailId);
		waitForElementToDisplay(outlookPassword);
		outlookPassword.click();
		outlookPassword.clear();
		setInput(outlookPassword, MFAEmailPassword);
		clickElement(signInBtn);
		Thread.sleep(20000);
		clickElement(othermail);
		Thread.sleep(6000);
		/*
		clickElement(Burgerbutton);
		waitForElementToDisplay(Mfa_InboxFolder);
		clickElementUsingJavaScript(driver, Mfa_InboxFolder);
		clickElementUsingJavaScript(driver, mfa_Folder);
*/
		scrollToElement(driver, Mfa_Mail);
		clickElementUsingJavaScript(driver, Mfa_Mail);

		Code = Mfa_Mail.getText();
		Code = Code.substring(27, 33);
		Thread.sleep(3000);
		clickElement(deleteMFAMailButton);
		System.out.println(Code);
		driver.close();
		switchToWindowByTitle("Mercer BenefitsCentral", driver);
		/*
		 * if(browser.contains("HEADLESSGC")) { waitForElementToDisplay(masAppMode);
		 * clickElement(masAppMode); clickElement(verificationcode); Code =
		 * selectmfa.getText(); Code = Code.substring(27, 33); driver.close();
		 * switchToWindowByTitle("iBenefit Center",driver); }
		 */
	}
	
	
	public void NavigateToOutlookAndGetMFA(WebDriver driver,String[] outlookDetails) throws InterruptedException {
		
		clickElement(verifiationMethod1);
		scrollToElement(driver, verifiationContinue);
		clickElement(verifiationContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		((JavascriptExecutor) driver).executeScript("window.open()","_blank");
		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(outlookDetails[0]);
		Thread.sleep(60000);
		setInput(outlookusername, outlookDetails[1]);
		Thread.sleep(20000);
		setInput(outlookPassword, outlookDetails[2]);
		Thread.sleep(30000);
		clickElement(signInBtn);
		Thread.sleep(5000);
		driver.manage().window().maximize();
		Thread.sleep(20000);
		clickElement(othermail);
		Thread.sleep(6000);
		
		/*clickElement(Burgerbutton);
		waitForElementToDisplay(Mfa_InboxFolder);
		clickElementUsingJavaScript(driver, Mfa_InboxFolder);
		Thread.sleep(5000);
		clickElementUsingJavaScript(driver, mfa_Folder);
		Thread.sleep(6000);
		*/
		clickElement(firstMessage);
		String code = verifiationCodeText.getText();
		code = code.substring(27, 33);
		Thread.sleep(6000);
		clickElement(deleteMFAMailButton);
		System.out.println(Code);
		//clickElement(deleteMFAMailButton);
		driver.close();
		switchToWindowByTitle("MES Dashboard", driver);
		setInput(verifiationCodeInput, code);
		clickElement(verifiationCodeContinue);
		//waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	}
public void NavigateToOutlookAndGetMFACIT(WebDriver driver,String[] outlookDetails) throws InterruptedException {
		
		clickElement(verifiationMethods);
		scrollToElement(driver, verifiationContinue);
		clickElement(verifiationContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		((JavascriptExecutor) driver).executeScript("window.open()","_blank");
		
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(outlookDetails[0]);
		Thread.sleep(60000);
		setInput(outlookusername, outlookDetails[1]);
		Thread.sleep(20000);
		setInput(outlookPassword, outlookDetails[2]);
		Thread.sleep(30000);
		clickElement(signInBtn);
		Thread.sleep(5000);
		driver.manage().window().maximize();
		Thread.sleep(20000);
		/*clickElement(othermail);
		Thread.sleep(6000);*/
		/*
		clickElement(Burgerbutton);
		waitForElementToDisplay(Mfa_InboxFolder);
		clickElementUsingJavaScript(driver, Mfa_InboxFolder);
		Thread.sleep(5000);
		clickElementUsingJavaScript(driver, mfa_Folder);
		Thread.sleep(6000);
		*/
		clickElement(firstMessage);
		String code = verifiationCodeText.getText();
		code = code.substring(27, 33);
		Thread.sleep(6000);
		clickElement(deleteMFAMailButton);
		System.out.println(Code);
		//clickElement(deleteMFAMailButton);
		driver.close();
		switchToWindowByTitle("MES Dashboard", driver);
		setInput(verifiationCodeInput, code);
		clickElement(verifiationCodeContinue);
		//waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	}
	
public void NavigateToOutlookAndGetMFABroker(WebDriver driver,String[] outlookDetails) throws InterruptedException {
		
		clickElement(verifiationMethod2);
		clickElement(verifiationContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(outlookDetails[0]);
		Thread.sleep(60000);
		setInput(outlookusername, outlookDetails[1]);
		Thread.sleep(20000);
		setInput(outlookPassword, outlookDetails[2]);
		Thread.sleep(20000);
		clickElement(signInBtn);
		driver.manage().window().maximize();
		Thread.sleep(6000);
		clickElement(othermail);
		Thread.sleep(6000);
		/*clickElement(Burgerbutton);
		Thread.sleep(6000);
		waitForElementToDisplay(Mfa_InboxFolder);
		clickElementUsingJavaScript(driver, Mfa_InboxFolder);
		Thread.sleep(5000);
		clickElementUsingJavaScript(driver, mfa_Folder); */
		Thread.sleep(6000);
		waitForElementToDisplay(firstMessage);
		clickElement(firstMessage);
		String code = verifiationCodeText.getText();
		code = code.substring(27, 33);
		Thread.sleep(6000);
		clickElement(deleteMFAMailButton);
		System.out.println(Code);
		//clickElement(deleteMFAMailButton);
		driver.close();
		switchToWindowByTitle("MES Dashboard", driver);
		setInput(verifiationCodeInput, code);
		clickElement(verifiationCodeContinue);
		//waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	}

}
