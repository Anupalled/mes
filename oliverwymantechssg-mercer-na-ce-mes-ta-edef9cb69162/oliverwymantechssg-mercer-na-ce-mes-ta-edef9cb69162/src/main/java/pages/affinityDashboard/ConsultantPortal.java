package pages.affinityDashboard;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindowWithURL;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class ConsultantPortal {
	WebDriver driver;
	public ConsultantPortal(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	Select select;
	
	@FindBy(xpath="//*[@icon='menu']")	
	public  WebElement Menu;
	
	@FindBy(css="a[class='site-logo-wrapper']")	
	public  WebElement homeIcon;	
	
	@FindBy(css="a[data-e2e-name='add-new-client']")	
	public  WebElement addNewClient;

	@FindBy(xpath="(//a[contains(text(),'Users')])[1]")	
	public  WebElement users;

	@FindBy(xpath="//a[contains(text(),'MES Users')]")	
	public  WebElement mesUsers;

	@FindBy(xpath="//h1[contains(text(),'MES Users')]")	
	public  WebElement mesUsersHeader;

	@FindBy(css="a[ng-click='addUser()']")	
	public  WebElement addNewUser;

	@FindBy(xpath="//input[@name='firstName']")	
	public  WebElement firstName;

	@FindBy(xpath="//input[@name='lastName']")	
	public  WebElement lastName;

	@FindBy(css="input[name='addressLine1']")	
	public  WebElement addressLine1;

	@FindBy(css="input[name='addressLine2']")	
	public  WebElement addressLine2;

	@FindBy(css="input[name='city']")	
	public  WebElement city;

	@FindBy(css="input[name='zip']")	
	public  WebElement zip;

	@FindBy(css="input[name='phone']")	
	public  WebElement phone;

	@FindBy(css="input[name='email']")	
	public  WebElement emailAddress;

	@FindBy(css="select[name='state']")	
	public  WebElement state;

	@FindBy(css="a[ng-click='save()']")	
	public  WebElement addUserSaveButton;
	
	@FindBy(css="input[ng-model='searchString']")	
	public  WebElement searchBox;
	
	@FindBy(css="a[ng-click='search()']")	
	public  WebElement searchButton;
	
	@FindBy(xpath="//h4[contains(text(),'Aurojit Das')]")	
	public  WebElement searchResultAuro;
	
	@FindBy(css="div[ng-click='toggleUserState(user)']")	
	public  WebElement deleteOrDeactivateButton;
	
	@FindBy(css="a[ng-click='confirm()']")	
	public  WebElement confirmButton;
	
	
	@FindBy(xpath="//a[contains(text(),'Remove')]")	
	public  WebElement removeButton;
	
	@FindBy(css="div[ng-show='operationCompleted']")	
	public  WebElement deleteMesage;
	
	@FindBy(xpath="//h4[@class='ng-binding']")	
	public  WebElement firstUserSearchResult;
	
	@FindBy(css="span[class='ng-binding user-status active']")	
	public  WebElement userStatusActive;
	
	@FindBy(css="span[class='ng-binding user-status inactive']")	
	public  WebElement userStatusInactive;	
	
	@FindBy(css="div[ng-click='editUser(user.id)']")	
	public  WebElement editUserButton;
	
	@FindBy(css="i[class='evo-icon-site-user-block']")	
	public  WebElement deactivateUserButtonEditpage;
	
	@FindBy(css="select[ng-model='roleModel']")	
	public  WebElement userRoleSelect;
	
	@FindBy(css="div[ng-click='editAssignment(user)']")	
	public  WebElement editUserAssosiation;
	
	@FindBy(css="select[ng-options='option as option.clientName for option in associations']")	
	public  WebElement userAssosiationAssignmentSelect;
	
	@FindBy(xpath="//a[contains(text(),'Save')]")	
	public  WebElement userAssosiationAssignmentSave;
	
	@FindBy(css="div[ng-click='removeAssignment(user)']")	
	public  WebElement removeUserAssosiatioon;
	
	@FindBy(css="div[ng-show='operationCompleted']")	
	public  WebElement removeUserAssosiationMessage;
	
	@FindBy(xpath="//a[text()='Associations']")	
	public  WebElement associationTab;
	
	@FindBy(xpath="//a[text()='!!!QA NAM (Headhunters)!!!!']")	
	public  WebElement qaNamAssoc;
	
	@FindBy(xpath="(//span[@class='icon-wrap']//i)[2]")	
	public  WebElement affinityDashboardProxy;
	
	@FindBy(xpath="//span[text()='Documents']")	
	public  WebElement docTab;
	//a[text()=' Documents'] 

	@FindBy(xpath="(//button[@type='button'])[1]")	
	public  WebElement documnetUploadButton;
	
	@FindBy(xpath="//input[@type='file']")	
	public  WebElement fileUpload;
	
	@FindBy(xpath="//button[text()=' Confirm ']")	
	public  WebElement confirmButtons;
	
	@FindBy(xpath="//a[text()='Doc1.xlsx']")	
	public  WebElement docVerification;
	
	@FindBy(xpath="//h4[contains(text(),'Shreyash Kapadi')]")	
	public  WebElement searchResultBroker;
	
	@FindBy(xpath="//a[text()='!!!QA NAM (Headhunters)!!!!']")	
	public  WebElement qaNamAssoc2;
	


	public void navigateToMesUsers(WebDriver driver) throws InterruptedException {
		
		Thread.sleep(1000);
		hoverAndClickOnElement(driver, users, mesUsers);
		waitForPageLoad();
	}

	public void addNewUser(WebDriver driver) throws InterruptedException {

		clickElement(addNewUser);
		waitForPageLoad();
		waitForElementToDisplay(userRoleSelect);
		select = new Select(userRoleSelect);
		select.selectByVisibleText("Association User");		
		setInput(firstName, "Aurojit");
		setInput(lastName, "Das");
		setInput(addressLine1, "Line 1");
		setInput(addressLine2, "Line 2");
		setInput(city, "XYZ");
		setInput(zip, "12345");
		setInput(phone, "1234567890");
		setInput(emailAddress, "aurojit.das123@mercer.com");
		select = new Select(state);
		select.selectByIndex(7);
		clickElement(addUserSaveButton);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForPageLoad();		

	}
	
	public void searchUser(WebDriver driver,String stringToSearch) throws InterruptedException {
		
		setInput(searchBox, stringToSearch);
		clickElement(searchButton);
		System.out.println("User search finished.");
		
		
	}
	
	
	public void deleteUser(WebDriver driver) throws InterruptedException {
		
		clickElement(deleteOrDeactivateButton);
		clickElement(confirmButton);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForPageLoad();
		
	}
	
	public void deactivateOrReactivateUser(WebDriver driver) throws InterruptedException {
		clickElement(deleteOrDeactivateButton);
		clickElement(confirmButton);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForPageLoad();		
		
	}	
	
	public void deactivateUserusingEditButton(WebDriver driver) throws InterruptedException {
		clickElement(editUserButton);
		waitForPageLoad();
		clickElement(deactivateUserButtonEditpage);
		clickElement(confirmButton);
		Thread.sleep(2000);
		//waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForPageLoad();
		
	}
	
	public void assignAssosiation(WebDriver driver,String userAssosiation) throws InterruptedException {
		String userAssosiationtrimmed = "!HSA-GK";
		clickElement(editUserAssosiation);
		waitForElementToDisplay(userAssosiationAssignmentSelect);
		select = new Select(userAssosiationAssignmentSelect);
		select.selectByVisibleText(userAssosiationtrimmed);
		clickElement(userAssosiationAssignmentSave);
		//waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForPageLoad();
		
		
	}
	
	public void removeAssosiation(WebDriver driver) throws InterruptedException {
		
		clickElement(removeUserAssosiatioon);
		clickElement(removeButton);
		waitForPageLoad();
		
		
	}
	
	
	
	public void waitForPageLoad() {
		System.out.println("Waiting for page to load..");		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
		System.out.println("Page Loaded..");
	}
	
	public void docUpload_Consultant() throws InterruptedException {
		String userdir = System.getProperty("user.dir");
		clickElement(associationTab);
		clickElement(qaNamAssoc);
		waitForPageLoad();
		clickElement(affinityDashboardProxy);
		
		switchToWindowWithURL("https://dashboard-qa.merceraffinity.com/dashboard",driver);
		Thread.sleep(3000);
		waitForElementToDisplay(Menu);
		clickElement(Menu);
		clickElement(docTab);
		clickElement(documnetUploadButton);
		fileUpload.sendKeys(userdir+"\\src\\main\\resources\\testdata\\Doc1.xlsx");
		clickElement(confirmButtons);
		waitForElementToDisplay(docVerification);
		

	}
	
	public void proxyToAffinityDashboard() throws InterruptedException {
		
		clickElement(associationTab);
		clickElement(qaNamAssoc);
		waitForPageLoad();
		clickElement(affinityDashboardProxy);
		Thread.sleep(3000);
		switchToWindowWithURL("https://dashboard-qa.merceraffinity.com/dashboard",driver);
	}
	
	public void waitForConsultantPortalHome() {
		System.out.println("Waiting for Consultant Portal to load..");		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		waitForElementToDisplay(addNewClient);		
		System.out.println("Page Loaded..");
	}
	

}
