package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;
import static driverfactory.Driver.selEleByVisbleText;
import static driverfactory.Driver.waitForPageLoad;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import com.aventstack.extentreports.ExtentTest;
import com.gargoylesoftware.htmlunit.javascript.host.Screen;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import driverfactory.Driver;
import utilities.ExcelReader;

public class CensusData {
	static WebDriver driver;
	public static String content;
	
	@FindBy(xpath = "//h1[text()=' Census Data']")
	public static WebElement censusdataHeader;
	
	@FindBy(xpath = "//button[@data-name='Add Employee']")
	public static WebElement addEmployee;
	
	//@FindBy(xpath = "//input[@data-name='firstName']")
	
	@FindBy(xpath = "//input[@ng-reflect-name='firstName']") 
	public static WebElement firstName;
	
    //@FindBy(xpath = "//input[@data-name='lastName']")
	@FindBy(xpath = "//input[@ng-reflect-name='lastName']") 
	public static WebElement lastName;
	
	@FindBy(xpath = "//mercer-icon[@icon='date_range']")
	public static WebElement datePicker;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[21]//a")
	public static WebElement currentDate;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[20]//a")
	public static WebElement currentDate1;
	
	@FindBy(xpath = "//select[@class='mos-c-calendar__selectors-year ng-untouched ng-pristine ng-valid']")
	public static WebElement planYear;
	
	//@FindBy(xpath = "//select[@data-name='genderId']")
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[1]") 
	public static WebElement gender;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[2]") 
	public static WebElement genderd;
	
	//@FindBy(xpath = "//input[@data-name='address1']")
	@FindBy(xpath = "//input[@ng-reflect-name='address1']") 
	public static WebElement address;
	
	//@FindBy(xpath = "//input[@data-name='city']")
	@FindBy(xpath = "//input[@ng-reflect-name='city']")
	public static WebElement city;
	
	
	//@FindBy(xpath = "//select[@data-name='stateId']")
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[2]") 
	public static WebElement state;
	
	//@FindBy(xpath = "//input[@data-name='zip']")
	@FindBy(xpath = "//input[@ng-reflect-name='zip']")
	public static WebElement zipCode;
	
	//@FindBy(xpath = "//select[@data-name='employmentStatusId']")
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[3]") 
	public static WebElement empStatusID;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[2]") 
	public static WebElement AffempStatusID;
	
	//@FindBy(xpath = "//select[@data-name='medicalCoverageId']")
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[4]") 
	public static WebElement mediCovgID;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[5]") 
	public static WebElement denCovgID;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[6]") 
	public static WebElement visCovgID;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[3]") 
	public static WebElement affmediCovgID;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[4]") 
	public static WebElement affdenCovgID;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[5]") 
	public static WebElement affvisCovgID;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[6]") 
	public static WebElement afflifCovgID;
	
	@FindBy(xpath = "//button[@dataname='Save Employee']")
	public static WebElement saveEmp;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	
	@FindBy(xpath = "//a[@data-name='Edit record']")
	public static WebElement editRecord;
	
	@FindBy(xpath = "//a[@data-name='Add Child Employee']")
	public static WebElement addDependent;
	
	@FindBy(xpath = "(//select[@ng-reflect-name='first criteria'])[1]")
	public static WebElement dependentDropDown;  
	
	@FindBy(xpath = "//button[@data-name='Save And Add Dependent']")
	public static WebElement saveAddDependent;  
	
	@FindBy(xpath = "//div[@class='mos-c-alert__content']")
	public static WebElement alertmsg;
	
	@FindBy(xpath = "//button[text()='Cancel']")
	public static WebElement cancelButton;
	
	@FindBy(xpath = "//div[text()=' Such census record already exists ']")
	public static WebElement recordExistsAlert;
	
	@FindBy(xpath = "//div[@class='mos-u-hint-text mos-u-hint-text--invalid ng-star-inserted']")
	public static WebElement empAgeAlertMsg;
	
	@FindBy(xpath = "//input[@type='file']")
	public static WebElement file;
	
	@FindBy(xpath = "//button[@data-name='Upload a File']")
	public static WebElement uploadFileIcon;
	
	@FindBy(xpath = "//li[@data-id='CensusData']//a")
	public static WebElement censusDataTab;
	
	@FindBy(xpath = "//button[@data-name='Confirm']")
	public static WebElement confirmButton;
	
	@FindBy(xpath = "//span[text()='File upload failed.']")
	public static WebElement fileErrorMsg;
	
	@FindBy(xpath = "//span[text()='File uploaded, but contains errors.']")
	public static WebElement fileErrorMsg1;
	
	
	
	@FindBy(xpath = "//button[@data-name='Close upload file dialog']")
	public static WebElement closeUploadFile;
	
	@FindBy(xpath = "//select[@data-name='employmentStatusId']")
	public static WebElement statusID;
	
	@FindBy(xpath = "//input[@ng-reflect-name='salary']")
	public static WebElement salary;
	
	@FindBy(xpath = "//select[@data-name='visionCoverageId']")
	public static WebElement visioncvg;
	
	
	@FindBy(xpath = "//select[@data-name='dentalCoverageId']")
	public static WebElement dentalcvg;
	
	@FindBy(xpath = "//select[@data-name='lifeCoverageId']")
	public static WebElement lifecvg;
	
	@FindBy(xpath = "//div[text()=' You have successfully uploaded a file! ']")
	public static WebElement successMsg;
	
	
	@FindBy(xpath = "(//div[@class='mos-u-display--inline-block'])[1]")
	public static WebElement getFirstEmployee;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[1]")
	public static WebElement getFirstDOB;
	
	@FindBy(xpath = "(//div[@class='mos-u-display--inline-block'])[2]")
	public static WebElement getSecondEmp;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[6]")
	public static WebElement getSecondDOB;
	
	@FindBy(xpath = "(//div[@class='mos-u-display--inline-block'])[3]")
	public static WebElement getThirdEmp;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[11]")
	public static WebElement getThirdDOB;
	
	@FindBy(xpath = "//button[@data-name='Export to Excel']")
	public static WebElement excelExport;
	
	@FindBy(xpath = "//div[@class='error-sign']")
	public static WebElement errorsign;
	
	@FindBy(xpath = "//div[@class='mos-c-alert__content']")
	public static WebElement alertMsg;
	
	@FindBy(xpath = "//h4[text()='Who is this information for?']")
	public static WebElement WhoIsInfoTitle;
	
	@FindBy(xpath = "//div[@class='content']")
	public static WebElement WhoIsInfoDescp;
	
	
	
			
	
	public static String getemp1;
	public static String getDOB1;
	public static String getemp2;
	public static String getDOB2;
	public static String getemp3;
	public static String getDOB3;
	
	
	
	public CensusData(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public static void setCensusData(String censusdata) throws InterruptedException, IOException {
		clickElement(addEmployee);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(censusdata)) {
				
					cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec1= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec1);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					
					clickElement(currentDate);
					
					
					cell = (Cell) coliterator.next();
					String gen = cell.getStringCellValue();
					String s15 = gen.trim();
					Driver.selEleByVisbleText(gender, s15);
					
					cell = (Cell) coliterator.next();
					String ss1 = cell.getStringCellValue();
					String ss2 = ss1.trim();
					setInput(address, ss2);
					
					cell = (Cell) coliterator.next();
					String cityvalue = cell.getStringCellValue();
					String citystring = cityvalue.trim();
					setInput(city, citystring);

					cell = (Cell) coliterator.next();
					String state2value = cell.getStringCellValue();
					String state2 = state2value.trim();
					Driver.selEleByVisbleText(state, state2);
					
					cell = (Cell) coliterator.next();
					int Exe1= (int) cell.getNumericCellValue();
					String zipcode = Integer.toString(Exe1);
					String Exec_ind = zipcode.trim();
					setInput(zipCode, Exec_ind);
					
					cell = (Cell) coliterator.next();
					String empstatus = cell.getStringCellValue();
					String ss11 = empstatus.trim();
					Driver.selEleByVisbleText(empStatusID, ss11);
					
					cell = (Cell) coliterator.next();
					String medcovg = cell.getStringCellValue();
					String ss12 = medcovg.trim();
					Driver.selEleByVisbleText(mediCovgID, ss12);
					
					
					clickElement(saveEmp);
					
					
					
				
	}
        }
	}
	public static void setCensusData_AllSavers(String censusdata) throws InterruptedException, IOException {
		clickElement(addEmployee);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(censusdata)) {
				
					cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec1= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec1);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					
					clickElement(currentDate);
					
					
					cell = (Cell) coliterator.next();
					String gen = cell.getStringCellValue();
					String s15 = gen.trim();
					Driver.selEleByVisbleText(gender, s15);
					
					
					
					cell = (Cell) coliterator.next();
					String empstatus = cell.getStringCellValue();
					String ss11 = empstatus.trim();
					Driver.selEleByVisbleText(AffempStatusID, ss11);
					
					cell = (Cell) coliterator.next();
					int Exe1= (int) cell.getNumericCellValue();
					String salary1 = Integer.toString(Exe1);
					String Exec_ind = salary1.trim();
					setInput(salary, Exec_ind);
					
					cell = (Cell) coliterator.next();
					String medcovg = cell.getStringCellValue();
					String ss12 = medcovg.trim();
					Driver.selEleByVisbleText(affmediCovgID, ss12);
					
					cell = (Cell) coliterator.next();
					String visioncovg = cell.getStringCellValue();
					String s112 = visioncovg.trim();
					Driver.selEleByVisbleText(affvisCovgID, s112);
					
					cell = (Cell) coliterator.next();
					String dentalcovg = cell.getStringCellValue();
					String s212 = dentalcovg.trim();
					Driver.selEleByVisbleText(affdenCovgID, s212);
					
					cell = (Cell) coliterator.next();
					String lifecovg = cell.getStringCellValue();
					String s213 = lifecovg.trim();
					Driver.selEleByVisbleText(afflifCovgID, s213);
					
					
					clickElement(saveEmp);
					
					
					
				
	}
        }
	}
	
	public static void selectDependents(String dependentdata) throws InterruptedException, IOException {
		Thread.sleep(3000);
		clickElement(addDependent);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(dependentdata)) {
				Thread.sleep(3000);
				cell = (Cell) coliterator.next();
				String data6 = cell.getStringCellValue();
				String s136 = data6.trim();
					
				Driver.selEleByVisbleText(dependentDropDown, s136);
				
					cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec2= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec2);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					
					clickElement(currentDate1);
					
					
					clickElement(saveEmp);
						
					
				
				
	}
        }
	}
	
	public static void selectSecondDependents(String dependentdata) throws InterruptedException, IOException {
		clickElement(addDependent);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(dependentdata)) {
				
				cell = (Cell) coliterator.next();
				String data6 = cell.getStringCellValue();
				String s136 = data6.trim();
					Driver.selEleByVisbleText(dependentDropDown, s136);
				
					cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec2= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec2);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					
					clickElement(currentDate1);
					clickElement(saveEmp);
					
						String Str = "You can’t add more than one spouse record for employee"; // based on string text match I want to select
						if (Str.equals(alertmsg.getText())) {
						System.out.println("Please enter the values");
						
					}
					
					clickElement(cancelButton);
				
				
	}
        }
	}
	
	
	public static void setNegCensusData(String censusdata) throws InterruptedException, IOException {
		clickElement(addEmployee);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(censusdata)) {
				
					cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec1= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec1);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					Thread.sleep(3000);
					clickElement(currentDate1);
					
					
					cell = (Cell) coliterator.next();
					String gen = cell.getStringCellValue();
					String s15 = gen.trim();
					Driver.selEleByVisbleText(gender, s15);
					
					cell = (Cell) coliterator.next();
					String ss1 = cell.getStringCellValue();
					String ss2 = ss1.trim();
					setInput(address, ss2);
					
					cell = (Cell) coliterator.next();
					String cityvalue = cell.getStringCellValue();
					String citystring = cityvalue.trim();
					setInput(city, citystring);

					cell = (Cell) coliterator.next();
					String state2value = cell.getStringCellValue();
					String state2 = state2value.trim();
					Driver.selEleByVisbleText(state, state2);
					
					cell = (Cell) coliterator.next();
					int Exe1= (int) cell.getNumericCellValue();
					String zipcode = Integer.toString(Exe1);
					String Exec_ind = zipcode.trim();
					setInput(zipCode, Exec_ind);
					
					cell = (Cell) coliterator.next();
					String empstatus = cell.getStringCellValue();
					String ss11 = empstatus.trim();
					Driver.selEleByVisbleText(empStatusID, ss11);
					
					cell = (Cell) coliterator.next();
					String medcovg = cell.getStringCellValue();
					String ss12 = medcovg.trim();
					Driver.selEleByVisbleText(mediCovgID, ss12);
					
					clickElement(saveEmp);
					String Str1 = "Such census record already exists"; // based on string text match I want to select
					if (Str1.equals(recordExistsAlert.getText())) {
					System.out.println("Please enter the values");
					clickElement(cancelButton);
				}
					else {
						System.out.println("jdfhg");
					}
					
					
	}
        }
	}
	
	public void uploadValidFiles() throws InterruptedException
	{
		
		clickElement(uploadFileIcon);
		Thread.sleep(3000);
		String uploadvalidfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\Allsavers_Census.xlsx";
		file.sendKeys(uploadvalidfile);
		clickElement(confirmButton);
		Thread.sleep(10000);
		closeUploadFile.click();
		
	}
	
	public void uploadInvalidFiles() throws InterruptedException
	{
		
		clickElement(uploadFileIcon);
		Thread.sleep(3000);
		//String uploadinvalidfile = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" +
				//File.separator + "testdata" + File.separator + "QT census data invalid.xlsx";

		String uploadinvalidfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\QT census data invalid.xlsx";
		file.sendKeys(uploadinvalidfile);
		clickElement(confirmButton);
		waitForElementToDisplay(CensusData.fileErrorMsg);
		clickElement(closeUploadFile);
		
	}
	
	public void uploadValidFiles_ahp() throws InterruptedException
	{
		
		clickElement(uploadFileIcon);
		String uploadinvalidfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\QT Valid Data_ahp.xlsx";
		file.sendKeys(uploadinvalidfile);
		clickElement(confirmButton);
		Thread.sleep(20000);
		waitForPageLoad(driver);
		Thread.sleep(20000);
		clickElement(closeUploadFile);
		
	}
	public void uploadValidFiles_allSavers() throws InterruptedException
	{
		
		clickElement(uploadFileIcon);
		Thread.sleep(5000);
		String uploadinvalidfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\Allsavers_Census.xlsx";
		Thread.sleep(5000);
		file.sendKeys(uploadinvalidfile);
		clickElement(confirmButton);
		Thread.sleep(10000);
		waitForPageLoad(driver);
		Thread.sleep(20000);
		clickElement(closeUploadFile);
		
	}
	public void uploadValidFiles_Medica() throws InterruptedException
	{
		
		clickElement(uploadFileIcon);
		String uploadinvalidfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\ONLY_MEDICAL_employees.xlsx";
		file.sendKeys(uploadinvalidfile);
		clickElement(confirmButton);
		Thread.sleep(10000);
		waitForPageLoad(driver);
		Thread.sleep(10000);
		clickElement(closeUploadFile);
		
	}
	
	public void uploadInvalidFiles_ahp() throws InterruptedException
	{
		
		clickElement(uploadFileIcon);
		Thread.sleep(5000);
		String uploadinvalidfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\QT InValid Data_ahp.xlsx";
		Thread.sleep(5000);
		file.sendKeys(uploadinvalidfile);
		clickElement(confirmButton);
		//waitForElementToDisplay(CensusData.fileErrorMsg1);
		Thread.sleep(20000);
		clickElement(closeUploadFile);
		
	}
	
	public static void affinitySetCensusData(String censusdata) throws InterruptedException, IOException {
		Thread.sleep(5000);
		waitForPageLoad(driver);
		clickElement(addEmployee);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(censusdata)) {
				
				
				cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec1= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec1);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					
					clickElement(currentDate1);
					
					
					cell = (Cell) coliterator.next();
					String gen = cell.getStringCellValue();
					String ss15 = gen.trim();
					Driver.selEleByVisbleText(gender, ss15);
					
					cell = (Cell) coliterator.next();
					String ss1 = cell.getStringCellValue();
					String ss2 = ss1.trim();
					Driver.selEleByVisbleText(AffempStatusID, ss2);
					
					cell = (Cell) coliterator.next();
					int Exe1= (int) cell.getNumericCellValue();
					String salary1 = Integer.toString(Exe1);
					String Exec_ind = salary1.trim();
					setInput(salary, Exec_ind);
					
					cell = (Cell) coliterator.next();
					String medcovg = cell.getStringCellValue();
					String ss12 = medcovg.trim();
					Driver.selEleByVisbleText(affmediCovgID, ss12);
					
					cell = (Cell) coliterator.next();
					String visioncovg = cell.getStringCellValue();
					String s112 = visioncovg.trim();
					Driver.selEleByVisbleText(affvisCovgID, s112);
					
					cell = (Cell) coliterator.next();
					String dentalcovg = cell.getStringCellValue();
					String s212 = dentalcovg.trim();
					Driver.selEleByVisbleText(affdenCovgID, s212);
					
					cell = (Cell) coliterator.next();
					String lifecovg = cell.getStringCellValue();
					String s213 = lifecovg.trim();
					Driver.selEleByVisbleText(afflifCovgID, s213);
					
	
					
					clickElement(saveEmp);
					
	}
        }
	}
	
	
	public static void affinitySetNegCensusData(String censusdata) throws InterruptedException, IOException {
		clickElement(addEmployee);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(censusdata)) {
				
					cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec1= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec1);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					Thread.sleep(3000);
					clickElement(currentDate);
					
					
					cell = (Cell) coliterator.next();
					String gen = cell.getStringCellValue();
					String s15 = gen.trim();
					Driver.selEleByVisbleText(gender, s15);
					
					cell = (Cell) coliterator.next();
					String ss1 = cell.getStringCellValue();
					String ss2 = ss1.trim();
					Driver.selEleByVisbleText(AffempStatusID, ss2);
					
					cell = (Cell) coliterator.next();
					int Exe1= (int) cell.getNumericCellValue();
					String salary1 = Integer.toString(Exe1);
					String Exec_ind = salary1.trim();
					setInput(salary, Exec_ind);
					
					
					cell = (Cell) coliterator.next();
					String medcovg = cell.getStringCellValue();
					String ss12 = medcovg.trim();
					Driver.selEleByVisbleText(affmediCovgID, ss12);
					
					cell = (Cell) coliterator.next();
					String visioncovg = cell.getStringCellValue();
					String s112 = visioncovg.trim();
					Driver.selEleByVisbleText(affvisCovgID, s112);
					
					cell = (Cell) coliterator.next();
					String dentalcovg = cell.getStringCellValue();
					String s212 = dentalcovg.trim();
					Driver.selEleByVisbleText(affdenCovgID, s212);
					
					cell = (Cell) coliterator.next();
					String lifecovg = cell.getStringCellValue();
					String s312 = lifecovg.trim();
					Driver.selEleByVisbleText(afflifCovgID, s312);
					
					clickElement(saveEmp);
					String Str1 = "Such census record already exists"; // based on string text match I want to select
					if (Str1.equals(recordExistsAlert.getText())) {
					System.out.println("Please enter the values");
					clickElement(cancelButton);
				}
					else {
						System.out.println("jdfhg");
					}
					
					
	}
        }
	}
	
	public static void selectDependents_ahp(String dependentdata) throws InterruptedException, IOException {
		clickElement(addDependent);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("censusdata");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(dependentdata)) {
				Thread.sleep(5000);
				cell = (Cell) coliterator.next();
				String data6 = cell.getStringCellValue();
				String s136 = data6.trim();
					Driver.selEleByVisbleText(dependentDropDown, s136);
				
					cell = (Cell) coliterator.next();
					String data1 = cell.getStringCellValue();
					String s13 = data1.trim();
					setInput(firstName, s13);
					
					cell = (Cell) coliterator.next();
					String data2 = cell.getStringCellValue();
					String s14 = data1.trim();
					setInput(lastName, s14);
					
					clickElement(datePicker);

					cell = (Cell) coliterator.next();
					int Exec2= (int) cell.getNumericCellValue();
					String planyear = Integer.toString(Exec2);
					String py = planyear.trim();
					Driver.selEleByVisbleText(planYear, py);
					
					
					clickElement(currentDate);
					
					
					cell = (Cell) coliterator.next();
					String gen = cell.getStringCellValue();
					String s15 = gen.trim();
					Driver.selEleByVisbleText(genderd, s15);
					
					cell = (Cell) coliterator.next();
					int Exe1= (int) cell.getNumericCellValue();
					String zipcode = Integer.toString(Exe1);
					String Exec_ind = zipcode.trim();
					setInput(zipCode, Exec_ind);
					
					clickElement(saveEmp);
					
					String Str = "You can’t add more than one spouse record for employee"; // based on string text match I want to select
					if (Str.equals(alertmsg.getText())) {
					System.out.println("Please enter the values");
					clickElement(cancelButton);
					
				}
				
				
				
	}
        }
	}
	
	
	public static void fetchEmpData() {
		getemp1=getFirstEmployee.getText();
		System.out.println(getemp1);
		
		getDOB1=getFirstDOB.getText();
		System.out.println(getDOB1);
		
		getemp2=getSecondEmp.getText();
		System.out.println(getemp2);
		
		getDOB2=getSecondDOB.getText();
		System.out.println(getDOB2);
		
		getemp3=getThirdEmp.getText();
		System.out.println(getemp3);
		
		getDOB3=getThirdDOB.getText();
		System.out.println(getDOB3);
		
	}
	
	public static void downloadFile() {
        try {
        	//Setting clipboard with file location
        	clickElement(excelExport);
         
            //native key strokes for CTRL, V and ENTER keys
        	//Screen s=new Screen();
        	//String uploadinvalidfile = System.getProperty("user.dir") + "\\src\\main\\resources\\WindowScreens\\Savebuuton.PNG";
        	//s.find(uploadinvalidfile);
        	
        	//s.click(uploadinvalidfile);
       
        } 
        catch (Exception exp) {
        	exp.printStackTrace();
        }
    }
	
	public static void downloadFile1() throws InterruptedException, AWTException {
		Thread.sleep(5000);
		clickElement(excelExport);
		 Robot robot = new Robot();
		 robot.keyPress(KeyEvent.VK_TAB);	
         Thread.sleep(2000);	
         robot.keyPress(KeyEvent.VK_TAB);	
         Thread.sleep(2000);	
         robot.keyPress(KeyEvent.VK_TAB);	
         Thread.sleep(2000);	
         robot.keyPress(KeyEvent.VK_ENTER);
         System.out.println("working");
		 
	}
	public void invalidData() throws InterruptedException {
		clickElement(continueButton);
	}
	
	public void getContent() {
		content=WhoIsInfoDescp.getText();
		System.out.println(content);
	}
	
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

