package pages.MES;

import static driverfactory.Driver.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.ExcelReader;

public class WorkSheetPage {
	
	WebDriver driver;
	
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[4]")
	public static WebElement annualPremium;
	
	@FindBy(xpath = "//button[@data-name='save changes']")
	public static WebElement saveChanges;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//h1[text()=' Worksheet']")
	public static WebElement workSheetHeader;
	
	@FindBy(xpath = "//div[@data-name='DENTAL']")
	public static WebElement dentalTab;
	
	@FindBy(xpath = "//div[@data-name='VISION']")
	public static WebElement visionTab;
	
	@FindBy(xpath = "//div[@data-name='BLIFE']")
	public static WebElement lifeTab;
	
	@FindBy(xpath = "//div[@class='section half ng-star-inserted']")
	public static WebElement getEmpDetails;
	
	@FindBy(xpath = "//div[@data-name='MEDICAL']")
	public static WebElement medicalTab;
	
	//input[@data-name='currency input']
	
	public WorkSheetPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void workSheet() throws IOException, InterruptedException {
		
		waitForElementToDisplay(workSheetHeader);
		
		
		String annual = annualPremium.getText();
		String ahd = annual.substring(1,4);
		System.out.println("annual: "+ahd);
		
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("memberrates");
		
		
		Cell row1=mySheet.getRow(0).getCell(0); 
		int cellValue=(int) row1.getNumericCellValue();
		String zipcode1 = Integer.toString(cellValue);
		String s23 = zipcode1.trim();
		System.out.println("sgds :"+s23);
		
	
		if(ahd.equals(s23)) {
			System.out.println("pass");
		}
		
		clickElement(saveChanges);
		waitForLoadingPage();
		clickElement(dentalTab);
		waitForLoadingPage();
		clickElement(saveChanges);
		waitForLoadingPage();
		clickElement(visionTab);
		waitForLoadingPage();
		clickElement(saveChanges);
		waitForLoadingPage();
		clickElement(lifeTab);
		waitForLoadingPage();
		clickElement(saveChanges);
		waitForLoadingPage();
		clickElement(continueButton);
		
	
	}
	
	public void worksheetAffinity() throws InterruptedException {
		
		
		String annual = getEmpDetails.getText();
		System.out.println(annual);
		clickElement(medicalTab);
		clickElement(saveChanges);
	
		Thread.sleep(20000);
		waitForElementToDisplay(dentalTab);
		Thread.sleep(10000);
		clickElement(dentalTab);
		Thread.sleep(10000);
		clickElement(saveChanges);
		
		Thread.sleep(20000);
		waitForElementToDisplay(visionTab);
		Thread.sleep(10000);
		clickElement(visionTab);
		Thread.sleep(10000);
		clickElement(saveChanges);
		
		Thread.sleep(20000);
		waitForElementToDisplay(lifeTab);
		Thread.sleep(10000);
		clickElement(lifeTab);
		Thread.sleep(10000);
		clickElement(saveChanges);
		
		Thread.sleep(20000);
		waitForElementToDisplay(continueButton);
		Thread.sleep(10000);
		clickElement(continueButton);
	}
	
public void worksheetAhp() throws InterruptedException {
		
		
		String annual = getEmpDetails.getText();
		System.out.println(annual);
		
		
		clickElement(medicalTab);
		Thread.sleep(5000);
		clickElement(saveChanges);
		
	
		Thread.sleep(10000);
		waitForElementToDisplay(dentalTab);
		clickElement(dentalTab);
		waitForElementToDisplay(saveChanges);
		clickElement(saveChanges);
		Thread.sleep(10000);
		waitForElementToDisplay(visionTab);
		clickElement(visionTab);
		waitForElementToDisplay(saveChanges);
		clickElement(saveChanges);
		
		Thread.sleep(10000);
		waitForElementToDisplay(lifeTab);
		clickElement(lifeTab);
		waitForElementToDisplay(saveChanges);
		clickElement(saveChanges);
		
		Thread.sleep(10000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
	}

public void worksheetMedicaAhp() throws InterruptedException {
	
	
	String annual = getEmpDetails.getText();
	System.out.println(annual);
	clickElement(medicalTab);
	clickElement(saveChanges);
	Thread.sleep(30000);
	waitForElementToDisplay(continueButton);
	clickElement(continueButton);
}


	
	public void clickSave() throws InterruptedException {
		Thread.sleep(5000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
	}
	public void empContribution() throws InterruptedException {
		
		for(int i=1;i<5;i++) {
		WebElement e =driver.findElement(By.xpath("(//input[@data-name='currency input'])["+i+"]"));
		setInput(e,"12");
	}
	}
		public void empContribution_ahp() throws InterruptedException {
			
			for(int i=1;i<5;i++) {
			WebElement e =driver.findElement(By.xpath("(//input[@data-name='currency input'])["+i+"]"));
			setInput(e,"20");
		}
	}	
		
		public void waitForLoadingPage() throws InterruptedException {
			System.out.println("Waiting for Loading Page...");
			Thread.sleep(1000);
			waitForElementToDisappear(By.cssSelector("div[class='mos-c-preloader__loading-content']"));
			System.out.println("page loaded...");
			
		}
}
