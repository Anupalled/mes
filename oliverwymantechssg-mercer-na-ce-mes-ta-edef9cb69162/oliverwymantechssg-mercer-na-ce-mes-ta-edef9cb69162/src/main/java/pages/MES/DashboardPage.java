package pages.MES;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import static driverfactory.Driver.waitForPageLoad;
import static regression.ActiveOffSystem.tests.TC3_AffinityDashboard.test;

import utilities.DatabaseUtils;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static verify.SoftAssertions.verifyElementTextContains;
import static driverfactory.Driver.clickElementUsingJavaScript;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DashboardPage {
	WebDriver driver;
	public static String time;
	
	@FindBy(xpath = "//h3[contains(text(),'Request a Quote')]")
	public static WebElement quotingHeader;
	
	@FindBy(xpath = "//h3[text()='Initiate Renewal Process']")
	public static WebElement RenewalHeader;
	
	@FindBy(xpath = "//a[@class='mos-c-button--md mos-t-button--info mos-c-button']")
	public static WebElement requestQuoteButton;
	
	@FindBy(xpath = "//a[@data-name='go to Application']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//a[text()='Show Me My Quote']")
	public static WebElement myQuotesButton;
	
	@FindBy(xpath = "//h3[text()='Continue / Review Quote']")
	public static WebElement reviewQuoteHeader;
	
	@FindBy(xpath = "//div[@class='columns shrink mos-c-header__bar-logo']//a")
	public static WebElement homeIcon;
	
	@FindBy(xpath = "//a[@data-name='go to Application']")
	public static WebElement completeSetUp;
	
	@FindBy(xpath = "//h1[text()[normalize-space()='Quoting Engine']]")
	public static WebElement quotingEngineTitle;
	
	
	@FindBy(xpath = "//a[text()='Tips to obtain a quote']")
	public static WebElement FAQLink;
	
	@FindBy(xpath = "//div[@class='mos-c-avatar']//img")
	public static WebElement securityLink;
	
	@FindBy(xpath = "//a[text()='Security Settings']")
	public static WebElement selectSecuritySetting;
	
	@FindBy(xpath = "//input[@id='password']")
	public static WebElement inputPassword;
	
	@FindBy(xpath = "//button[text()='Continue']")
	public static WebElement continueButton1;
	
	@FindBy(id = "password") 
	public static WebElement password;
	
	@FindBy(xpath = "//h2[text()='Please Enter Your Current Password']") 
	public static WebElement currentPassword;
	
	@FindBy(xpath = "//h2[text()='Contact Information']") 
	public static WebElement contactInfoTitle;
	
	@FindBy(xpath = "//a[text()='Add more contact methods']") 
	public static WebElement addContact;
	
	@FindBy(xpath = "//input[@id='email']") 
	public static WebElement addEmail;
	
	@FindBy(xpath = "//button[text()='Add contact']") 
	public static WebElement addContactButton;
	
	@FindBy(xpath = "//button[@class='mas-btn mas-btn-primary']") 
	public static WebElement updateContact;
	
	@FindBy(xpath = "//a[text()='Update Password']") 
	public static WebElement updatePassword;
	
	@FindBy(xpath = "(//input[@type='password'])[1]") 
	public static WebElement currentpwd;
	
	@FindBy(xpath = "(//input[@type='password'])[2]") 
	public static WebElement password1;
	
	@FindBy(xpath = "(//input[@type='password'])[3]") 
	public static WebElement confirmPwd;
	
	@FindBy(xpath = "//button[text()='Submit']") 
	public static WebElement submit;
	
	@FindBy(xpath = "//div[@class='mas-message mas-message-success']") 
	public static WebElement successMsg;
	
	@FindBy(xpath = "//a[text()='Back']") 
	public static WebElement backButton;
	
	@FindBy(xpath = "//a[text()='www.csahealthplans.com']") 
	public static WebElement link1;
	
	@FindBy(xpath = "//a[text()='Request Your Quote!']") 
	public static WebElement link2;
	
	@FindBy(xpath = "//h1[text()=' Profile ']")
	public static WebElement profileHeader1;
	
	@FindBy(xpath = "//a[text()='Tips to obtain a quote']")
	public static WebElement FAQ;
	
	@FindBy(xpath = "(//span[@class='mas-icon mas-icon-remove'])[1]")
	public static WebElement deleteEmail;
	
	@FindBy(xpath = "//div[@class='mas-message mas-message-success']")
	public static WebElement successmsg;
	
	@FindBy(xpath = "//h3[text()='Congratulations! Your set-up is now complete!']")
	public static WebElement setupCompletionMsg;
	
	@FindBy(xpath = "//a[text()='View Your Benefits Offering']")
	public static WebElement benefitOfferingButton;
	
	@FindBy(xpath = "//a[text()='Logout']")
	public static WebElement logout;
	
	@FindBy(xpath = "//p[text()='Log in to your existing account.']")
	public static WebElement loginPage;
	
	@FindBy(xpath = "//button[text()=' 				Close 			']")
	public static WebElement closeButton;
	
	@FindBy(xpath = "//strong[text()='8 AM to 6 PM ET Monday-Friday ']")
	public static WebElement workTimings;
	
	@FindBy(xpath = "//a[text()='website']")
	public static WebElement QAMedicaLink;
	
	@FindBy(xpath = "//a[text()='Store']")
	public static WebElement storeLink;
	
	@FindBy(xpath = "//a[text()='Edit Account Structure']")
	public static WebElement editAccountStructure;
	
	@FindBy(xpath = "//div[contains(text(),'Please contact us or UHC with any questions regarding ongoing changes.')]")
	public static WebElement msgVerify;
	
	@FindBy(xpath = "//div[contains(text(),' Please contact us to begin the process.')]")
	public static WebElement msgVerifyPY;
	
	@FindBy(xpath = "//a[@class='mos-c-button--md mos-t-button--info mos-c-button ng-star-inserted']")
	public static WebElement editClient;
	
	@FindBy(xpath = "//a[text()='View Company Set Up']")
	public static WebElement viewCompanyButton;
	
	@FindBy(xpath = "//button[@type='button']")
	public static WebElement disabledbutton;
	
	@FindBy(xpath = "//div[contains(text(),' is up for renewal with UHC. Please contact us to begin the process.')]")
	public static WebElement planYearFinishedMsg_QT;
	
	@FindBy(xpath = "//a[contains(text(),'View Your Benefits Offering')]")
	public static WebElement benefitOffering_Button;
	
	
	@FindBy(xpath = "//h3[text()='Contact Us']")
	public static WebElement contactUsTitle;
	
	@FindBy(xpath = "//h3[text()='Active (Off System)']")
	public static WebElement activeoffSystemTitle;
	
	
	@FindBy(xpath = "//li[contains(text(),'Initiate Quoting Process')]")
	public static WebElement quotingProcess;
	
	@FindBy(xpath = "//li[contains(text(),'Submit Initial Quote Request')]")
	public static WebElement quoteRequest;
	
	
	@FindBy(xpath = "//li[contains(text(),'Review Quote')]")
	public static WebElement reviewQuote;
	
	@FindBy(xpath = "//li[contains(text(),'Sign Up!')]")
	public static WebElement signUp;
	
	@FindBy(xpath = "//div[contains(text(),'directly with UHC. Please contact us or UHC with any questions regarding ongoing changes.')]")
	public static WebElement planNotFinished_QT;
	
	
		
	 public static String signupTool_AssociationID;
	 public static String getSignUpToolUrl;
	
	public DashboardPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void submitQuoteRequest(WebElement e) throws InterruptedException
	{
		//waitForElementToDisplay(quotingHeader);
		clickElement(e);
	}
	public void requestQuote() throws InterruptedException {
		Thread.sleep(5000);
		clickElement(continueButton);
	}
	public void myQuotes() throws InterruptedException {
		waitForPageLoad(driver);
		waitForElementToDisplay(myQuotesButton);
		clickElement(continueButton);
	}
	public void clickOnDashboard() throws InterruptedException {
		Thread.sleep(5000);
		clickElement(homeIcon);
	}
	
	

			
		public void fetchHyperlinks_DB() throws ClassNotFoundException, SQLException, InterruptedException {
		int columnCount = 12;
		Thread.sleep(5000);
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
		ResultSet myres = getResultSet(mycon, "select * from assoc_supported_details where association_id='32954'");
		while (myres.next()) {
			for (int i = 1; i <= columnCount; i++) {
				String t = myres.getString(i);
				System.out.println(t);
			}
		}

	}
		
		
		public void updateEmailID(String passwordTxt) throws InterruptedException {
			Thread.sleep(5000);
			clickElement(securityLink);
			clickElement(selectSecuritySetting);
			switchToWindowByTitle("Wood Products Manufacturers Association auth-qa.mercer.com",driver);
			waitForElementToDisplay(currentPassword);
			clickElement(password);
			Thread.sleep(5000);
			setInput(password,passwordTxt);
			clickElement(continueButton1);
			
			waitForElementToDisplay(contactInfoTitle);
			clickElement(addContact);
			getAlphaNumericStrings();
			clickElement(addContactButton);
			clickElement(updateContact);
			clickElement(deleteEmail);
			clickElement(updateContact);
			waitForElementToDisplay(DashboardPage.successmsg);
			Thread.sleep(3000);
			waitForElementToDisplay(closeButton);
			clickElementUsingJavaScript(driver,closeButton);
			switchToWindowByTitle("Mercer Associations",driver);
		}
		
		public void updatePassword() throws InterruptedException {
			clickElement(updatePassword);
			setInput(currentpwd,"fFhofHIBHIk0EZSDIJDogw==");
			setInput(password1,"qmHgir0EGJV0tKEhNRYLrg==");
			setInput(confirmPwd,"qmHgir0EGJV0tKEhNRYLrg==");
			clickElement(submit);
			driver.close();
			switchToWindowByTitle("Mercer Associations",driver);
		}
		
		public void verfiyHyperlinks() throws InterruptedException
		{
			Thread.sleep(5000);
			clickElement(link1);
			switchToWindowByTitle("CSA - Home",driver);
			driver.close();
			switchToWindowByTitle("Mercer Associations",driver);
			
			clickElement(link2);
			waitForElementToDisplay(profileHeader1);
			
			
		}
		
		public void getAlphaNumericStrings() throws InterruptedException {
		String randval = RandomStringUtils.randomAlphabetic(4).toLowerCase();
			System.out.println(randval);
			String email=randval.concat("@gmail.com");
			setInput(addEmail,email);
		
		}
		
		public static void FAQLink() {
	        try {
	        	//Setting clipboard with file location
	        	clickElement(FAQ);
	         
	            //native key strokes for CTRL, V and ENTER keys
	            Robot robot = new Robot();
		
	            Thread.sleep(3000);
	            robot.keyRelease(KeyEvent.VK_CONTROL);
	            robot.keyPress(KeyEvent.VK_ENTER);
	            robot.keyRelease(KeyEvent.VK_ENTER);
	            Thread.sleep(3000);
	            robot.keyPress(KeyEvent.VK_TAB);
	            robot.keyRelease(KeyEvent.VK_TAB);
	            robot.keyPress(KeyEvent.VK_TAB);
	            robot.keyRelease(KeyEvent.VK_TAB);
	            robot.keyPress(KeyEvent.VK_TAB);
	            robot.keyRelease(KeyEvent.VK_TAB);
	            robot.keyPress(KeyEvent.VK_TAB);
	            robot.keyRelease(KeyEvent.VK_TAB);
	            robot.keyPress(KeyEvent.VK_ENTER);
	            robot.keyRelease(KeyEvent.VK_ENTER);
	        } catch (Exception exp) {
	        	exp.printStackTrace();
	        }
	    }
		
		public void setUpComplete() throws InterruptedException, AWTException {
			waitForElementToDisplay(completeSetUp);
			clickElement(completeSetUp);
			Thread.sleep(3000);
			switchToWindowByTitle("Mercer Marketplace 365+ HR Admin Portal auth-qa.mercer.com",driver);
			Thread.sleep(3000);
			driver.close();
			switchToWindowByTitle("Mercer Associations",driver);
			clickElement(benefitOfferingButton);
			
			Robot robot = new Robot();
			Thread.sleep(3000);
			robot.keyPress(KeyEvent.VK_ALT);
		     Thread.sleep(1000);
		     robot.keyPress(KeyEvent.VK_S);
		     robot.keyRelease(KeyEvent.VK_ALT);
		     robot.keyRelease(KeyEvent.VK_S);
		     
		     clickElement(securityLink);
		     clickElement(logout);
		     
         
		}
		
		public void getAssociationID_SignUpTool() {
			getSignUpToolUrl = driver.getCurrentUrl();
	        System.out.println(getSignUpToolUrl);
	        
	        signupTool_AssociationID=getSignUpToolUrl.substring(50,55);
	       System.out.println(signupTool_AssociationID);
		}
		public void NavigateAssoc() throws InterruptedException {
			 time=workTimings.getText();
			 clickElement(QAMedicaLink);
			 switchToWindowByTitle("Google",driver);
			
			 
		}
		public void verifyButton() throws InterruptedException {
			Thread.sleep(5000);
			waitForElementToDisplay(disabledbutton);
			String button=disabledbutton.getAttribute("disabled");
			
			Assert.assertTrue(driver.findElement(By.xpath("//button[@type='button']")).isEnabled(), "OK button is disabled.");
		}
		public void dateTime() {
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm");
		 
		 //get current date time with Date()
		 Date date = new Date();
		 
		 // Now format the date
		 String date1= dateFormat.format(date);
		 
		 // Print the Date
		 System.out.println("Current date and time is " +date1);
		}
		
}
