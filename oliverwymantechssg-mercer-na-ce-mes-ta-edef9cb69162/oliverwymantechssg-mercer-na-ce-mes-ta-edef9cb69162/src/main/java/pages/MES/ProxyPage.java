package pages.MES;

import static driverfactory.Driver.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static pages.MES.ValidationPage.company_id;
import static pages.MES.ValidationPage.signup_url;
import static regression.Medica_ahp.tests.TC4_SignUpTool.test;
import static regression.Medica_ahp.tests.TC4_SignUpTool.softAssert;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.IOException;

public class ProxyPage {
	 WebDriver driver;
	
	
	@FindBy(xpath = "(//a[text()='Test Employee'])[1]")
	public static WebElement testEmp ;
	
	@FindBy(xpath = "//a[text()='View as Employee']")
	public static WebElement viewEmp ;
	
	@FindBy(xpath = "//a[text()='View as Administrator']")
	public static WebElement viewAdmin ;
	
	@FindBy(xpath = "(//tr[@class='ng-scope'])[1]//td[1]//a")
	public static WebElement selectUser ;
	
	@FindBy(xpath = "//a[text()='Profile']")
	public static WebElement profileTab ;
	
	@FindBy(xpath = "(//span[@class='data-value ng-binding'])[1]")
	public static WebElement firstName ;
	
	@FindBy(xpath = "(//span[@class='data-value ng-binding'])[3]")
	public static WebElement lastName ;
	
	
	@FindBy(xpath = "(//a[@class='button button-action round evo-button small text-center ng-binding'])[3]")
	public static WebElement viewEmpDetails ;
	
	@FindBy(xpath = "//h2[text()='Search for an employee to test']")
	public static WebElement empHeader ;
	
	@FindBy(xpath = "(//span[text()='GET STARTED'])[2]")
	public static WebElement getStartedButton ;
	
	@FindBy(xpath = "//h1[text()='Build Your Profile']")
	public static WebElement profilembc ;
	
	
	@FindBy(xpath = "(//button[@class='btn btn-primary'])[2]")
	public static WebElement nextButton ;
	
	@FindBy(xpath = "//span[text()='Menu']")
	public static WebElement menuButton ;
	
	@FindBy(xpath = "(//a[@class='ng-binding'])[1]")
	public static WebElement profileButton ;
	
	@FindBy(xpath = "//h1[text()='Personal Information']")
	public static WebElement personalInfoHeader ;
	
	@FindBy(xpath = "//a[text()='End Proxy Session']")
	public static WebElement endProxy ;
	
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//h2[text()='Terms and Conditions Agreement']")
	public static WebElement termsConditionHeader;
	
	@FindBy(xpath = "//span[text()='Accept']")
	public static WebElement acceptButton;
	
	@FindBy(xpath = "//button[text()='Access Testing Portal']")
	public static WebElement testingPortal ;
	
	@FindBy(xpath = "//h2[text()='Contact Information']")
	public static WebElement contactInfoHeader ;
	
	@FindBy(xpath = "//button[text()='Save and continue']")
	public static WebElement saveContinueButton ;
	
	@FindBy(xpath = "//h1[text()='2020 Open Enrollment']")
	public static WebElement openEnrollHeader ;
	
	@FindBy(xpath = "//a[text()='Open Enrollment']")
	public static WebElement openenrollSubTab ;
	
	@FindBy(xpath = "//p[text()='Welcome!']")
	public static WebElement welcomeTitle ;
	
	@FindBy(xpath = "//button[text()='Start Open Enrollment']")
	public static WebElement startOpenEnrollButton ;
	
	@FindBy(xpath = "//h1[text()='Who's Covered']")
	public static WebElement whosCoveredTitle ;
	
	@FindBy(xpath = "(//div[@ng-repeat='category in categoriesToShow']//button[@type='button'][@role='checkbox'])[1]")
	public static WebElement selectMedical;
	
	@FindBy(xpath = "//span[text()='Continue']")
	public static WebElement continueButton1;
	
	@FindBy(xpath = "//h1[contains(text(),'My Information')]")
	public static WebElement myInfoTitle;
	
	@FindBy(xpath = "(//span[text()='Do you currently use Tobacco Products?']/..//button[contains(text(),'No')])[1]")
	public static WebElement tobaccoProductsNo;
	
	@FindBy(xpath = "(//span[text()='Have you used Tobacco Products in the last 12 months?']/..//button[contains(text(),'No')])[1]")
	public static WebElement tobaccoProductsNo1;
	
	@FindBy(xpath = "(//span[text()='Have you completed your Biometric Screening?']/..//button[contains(text(),'Yes')])[1]")
	public static WebElement biometricYes;
	
	@FindBy(xpath = "(//span[text()='Have you completed the Personal Health Assessment?']/..//button[contains(text(),'Yes')])[1]")
	public static WebElement personalHealthYes;
	
	@FindBy(xpath = "(//input[@name='field'])[1]")
	public static WebElement workNumber;
	
	@FindBy(xpath = "(//input[@name='field'])[2]")
	public static WebElement mobileNumber;
	
	@FindBy(xpath = "//button[contains(text(),'Update')]")
	public static WebElement updateButton;
	
	@FindBy(xpath = "//h1[text()='Medical Insurance']")
	public static WebElement medicalInsuranceTitle;
	
	@FindBy(xpath = "(//span[text()='Next'])[2]")
	public static WebElement nextbutton;
	
	@FindBy(xpath = "//h1[text()='Dental Insurance']")
	public static WebElement dentalInsuranceTitle;
	
	
	@FindBy(xpath = "//h1[text()='Vision Insurance']")
	public static WebElement visionInsurance;
	
	@FindBy(xpath = "//h1[text()='Spending Accounts']")
	public static WebElement spendingAccountTitle ;
	
	@FindBy(xpath = "//input[@name='amountEntered']")
	public static WebElement enterAmount;
	
	
	@FindBy(xpath = "//button[text()='Add to Cart']")
	public static WebElement addToCart;
	
	@FindBy(xpath = "//h4[text()='HSA Eligibility Questionnaire']")
	public static WebElement HSAQuestionnarieTitle;
	
	
	
	
	@FindBy(xpath = "(//button[text()='Continue'])[1]")
	public static WebElement continueButton2;
	
	
	@FindBy(xpath = "//button[text()='Approve']")
	public static WebElement approveButton;//2 times
	
	@FindBy(xpath = "//h1[text()='Life Insurance']")
	public static WebElement lifeInsurance;
	
	@FindBy(xpath = "//h1[text()='Review Your Cart']")
	public static WebElement reviewYourCartTitle;
	
	@FindBy(xpath = "//button[text()='Yes, I accept.']")
	public static WebElement yesAcceptCheckBox;
	
	@FindBy(xpath = "(//span[text()='Check out'])[2]")
	public static WebElement checkOut;
	
	@FindBy(xpath = "//span[contains(text(),'Continue')]")
	public static WebElement continueInfo;
	
	
	@FindBy(xpath = "//button[text()='Save and continue']")
	public static WebElement saveandContinueButton;
	
	
	
	
	public ProxyPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void proxyUser1() throws InterruptedException {
		waitForElementToDisplay(testingPortal);
		clickElementUsingJavaScript(driver,testingPortal);
		System.out.println(driver.getWindowHandles().size());
		
		String url1="https://hr-qaf.mercermarketplace365plus.com/HrPortal/#/hr/";
		String url2="/2019/testingEmployees";
		String auth_url=url1.concat(company_id).concat(url2);
		System.out.println(auth_url);
		//https://mes-qa.mercermarketplace365plus.com/62291/signup/62542/2021/validation
	switchToWindowWithURL("https://mes-qa.mercermarketplace365plus.com/api/signup/validation/proxyToTest?companyId=34057&planYear=2019",driver);
	switchToWindowWithURL("https://auth-qa.mercer.com/api/proxy?applicationUrl=MMX365PLUS/HR&wctx=ru%3D%2FHrPortal%2F%23%2Fhr%2F34057%2F2019%2FtestingEmployees",driver);
	switchToWindowWithURL(auth_url,driver);
	Thread.sleep(3000);
	waitForElementToDisplay(empHeader);
	Thread.sleep(5000);
	waitForElementToDisplay(selectUser);
	clickElementUsingJavaScript(driver,selectUser);
	
	waitForElementToDisplay(profileTab);

	
}
	public void clickEmp() throws InterruptedException {
		waitForElementToDisplay(testingPortal);
		testingPortal.click();
		Thread.sleep(6000);
		
		switchToWindowWithURL("https://hr-qaf.mercermarketplace365plus.com/HrPortal/#/hr/52961/2021/testing/testingEmployees",driver);
		/*Thread.sleep(5000);
		clickElement(viewEmpDetails);
		Thread.sleep(5000);
		clickElement(viewEmp);
		Thread.sleep(5000);
		switchToWindowWithURL("https://authoring-qaf.mercermarketplace365plus.com/viewtnc",driver);
		
		Thread.sleep(5000);
		
		waitForElementToDisplay(acceptButton);
		clickElementUsingJavaScript(driver,acceptButton);
		Thread.sleep(5000);
		scrollToElement(driver,getStartedButton);
		waitForElementToDisplay(getStartedButton);
		clickElementUsingJavaScript(driver,getStartedButton);
	

		
		waitForElementToDisplay(profilembc);
		clickElementUsingJavaScript(driver,nextButton);
		waitForElementToDisplay(menuButton);
		clickElementUsingJavaScript(driver,menuButton);
		waitForElementToDisplay(profileButton);
		clickElementUsingJavaScript(driver,profileButton);
		
		waitForElementToDisplay(personalInfoHeader);
		
		//clickElementUsingJavaScript(driver,continueButton);
*/		
	}
	
	public void clickAdmin() throws InterruptedException {
		String currentURL=driver.getCurrentUrl();
		System.out.println(currentURL);
		clickElement(viewEmpDetails);
		clickElementUsingJavaScript(driver,viewAdmin);
		switchToWindowWithURL("https://authoring-qaf.mercermarketplace365plus.com/dashboard",driver);
		
		waitForElementToDisplay(termsConditionHeader);
		clickElementUsingJavaScript(driver,acceptButton);
		
		waitForElementToDisplay(getStartedButton);
		clickElementUsingJavaScript(driver,getStartedButton);
		
		waitForElementToDisplay(profilembc);
		clickElementUsingJavaScript(driver,nextButton);
		clickElementUsingJavaScript(driver,menuButton);
		clickElementUsingJavaScript(driver,profileButton);
		
		waitForElementToDisplay(personalInfoHeader);
		driver.close();
		switchToWindowWithURL(currentURL,driver);
		driver.close();
		Thread.sleep(5000);
		switchToWindowWithURL(signup_url,driver);
		clickElementUsingJavaScript(driver,continueButton);
			
	}
	public void proxyMBC() throws InterruptedException, IOException {
		Thread.sleep(20000);
		waitForElementToDisplay(getStartedButton);
		clickElement(getStartedButton);

		clickElement(saveContinueButton);
		waitForElementToDisplay(openEnrollHeader);
		
		clickElement(startOpenEnrollButton);
		
		Thread.sleep(3000);
		clickElement(selectMedical);
		clickElement(continueButton1);
		waitForElementToDisplay(myInfoTitle);
		
		clickElement(tobaccoProductsNo);
		clickElement(tobaccoProductsNo1);
		clickElement(biometricYes);
		clickElement(personalHealthYes);
		setInput(workNumber, "8967543219");
		setInput(mobileNumber, "8967543219");
		clickElement(updateButton);
		Thread.sleep(10000);
		clickElement(continueInfo);
	}
	
	public void medicaproxyMBC() throws InterruptedException, IOException {
		Thread.sleep(20000);
		waitForElementToDisplay(getStartedButton);
		clickElement(getStartedButton);
		waitForElementToDisplay(saveandContinueButton);
		clickElement(saveandContinueButton);
		Thread.sleep(3000);
		waitForElementToDisplay(openEnrollHeader);
		clickElement(startOpenEnrollButton);
		Thread.sleep(3000);
		clickElement(continueButton1);
		waitForElementToDisplay(myInfoTitle);
		
		clickElement(tobaccoProductsNo);
		clickElement(tobaccoProductsNo1);
		clickElement(biometricYes);
		clickElement(personalHealthYes);
		setInput(workNumber, "8967543219");
		setInput(mobileNumber, "8967543219");
		clickElement(updateButton);
		Thread.sleep(10000);
		clickElement(continueInfo);
	}
	
	public void chooseBenefits() throws IOException, InterruptedException {
		waitForElementToDisplay(medicalInsuranceTitle);
		//verifyElementTextContains(driver,medicalInsuranceTitle, "Medical Insurance","Medical Insurance pass","Medical Insurance fail", test,softAssert);
		clickElement(nextbutton);
		waitForElementToDisplay(dentalInsuranceTitle);
		//verifyElementTextContains(driver,dentalInsuranceTitle, "Dental Insurance","Dental Insurance pass","Dental Insurance fail", test,softAssert);
		clickElement(nextbutton);
		waitForElementToDisplay(visionInsurance);
		//verifyElementTextContains(driver,visionInsurance, "Vision Insurance","vision Insurance pass","vision Insurance fail", test,softAssert);
		clickElement(nextbutton);
		waitForElementToDisplay(spendingAccountTitle);
		//verifyElementTextContains(driver,spendingAccountTitle, "Spending Accounts","Spending Accounts pass","Spending Accounts fail", test,softAssert);
		setInput(enterAmount, "567");
		clickElement(addToCart);
		waitForElementToDisplay(HSAQuestionnarieTitle);
		//verifyElementTextContains(driver,HSAQuestionnarieTitle, "HSA Eligibility Questionnaire","HSA Eligibility Questionnaire pass","HSA Eligibility Questionnairefail", test,softAssert);
		for(int i=1;i<7;i++) {
		WebElement nobutton=driver.findElement(By.xpath("(//button[text()='No'])["+i+"]"));
			nobutton.click();
		}	
			
		clickElement(continueButton2);
		clickElement(approveButton);
		clickElement(approveButton);
		clickElement(nextbutton);
		waitForElementToDisplay(lifeInsurance);
		//verifyElementTextContains(driver,lifeInsurance, "Life Insurance","Life Insurance pass","Life Insurance fail", test,softAssert);
		clickElement(nextbutton);
	}
	
	public void yourCart() throws IOException, InterruptedException {
		Thread.sleep(7000);
		waitForElementToDisplay(reviewYourCartTitle);
		//verifyElementTextContains(driver,reviewYourCartTitle, "Review Your Cart","Review Your Cart pass","Review Your Cart fail", test,softAssert);
		clickElement(yesAcceptCheckBox);
		clickElement(checkOut);
		Thread.sleep(6000);
	}
	
}
