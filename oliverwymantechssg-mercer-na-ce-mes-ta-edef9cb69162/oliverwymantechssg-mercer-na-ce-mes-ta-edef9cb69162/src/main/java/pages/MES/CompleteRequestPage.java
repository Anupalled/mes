package pages.MES;

import static pages.MES.ValidationPage.company_id;
import static pages.MES.ValidationPage.signup_url;
import static verify.SoftAssertions.verifyElementTextContains;

import java.util.List;

import static pages.MES.DashboardPage.signupTool_AssociationID;
import static driverfactory.Driver.*;
import static pages.MES.DashboardPage.getSignUpToolUrl;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class CompleteRequestPage {
	static WebDriver driver;
	 String currentURL ;
	 public static String AssociationID;
	
	 String currenturl;
	@FindBy(css = "label[data-name='Accept']")
	public static WebElement acceptCheckbox;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//h1[text()=' Review Quote Request']")
	public static WebElement reviewQuoteHeader;
	
	@FindBy(xpath = "//div[contains(@class,'ahp-content-inner ahp-content-row complete-request-wrap')]//div[text()='pzmo']")
	public static WebElement org1;
	
	@FindBy(xpath = "(//div[contains(@class,'ahp-content-inner ahp-content-row complete-request-wrap')]//div[@class='data-value'])[1]")
	public static WebElement org;
	
	@FindBy(xpath = "(//div[@class='field-view-wrap ng-star-inserted']//div[text()='5'])[1]")
	public static WebElement AvgEmp;
	
	
	@FindBy(xpath = "(//div[@class='data-value'])[3]")
	public static WebElement sic;
	
	@FindBy(xpath = "(//div[@class='data-value'])[4]")
	public static WebElement sic2;
	
	@FindBy(xpath = "(//div[@class='data-value'])[5]")
	public static WebElement tax;
	
	@FindBy(xpath = "(//div[@class='data-value'])[9]")
	public static WebElement address;
	
	@FindBy(xpath = "(//div[@class='data-value'])[10]")
	public static WebElement city;
	
	@FindBy(xpath = "(//div[@class='data-value'])[12]")
	public static WebElement zipcode;
	
	@FindBy(xpath = "(//div[@class='data-value'])[6]")
	public static WebElement DOC;
	
	@FindBy(xpath = "(//div[@class='data-value'])[7]")
	public static WebElement startDate;
	
	@FindBy(xpath = "(//div[@class='data-value'])[8]")
	public static WebElement endDate;
	
	@FindBy(xpath = "(//div[@class='data-value'])[13]")
	public static WebElement name;
			
	@FindBy(xpath = "(//div[@class='data-value'])[14]")
	public static WebElement phoneNum;	
	
	@FindBy(xpath = "(//div[@class='data-value'])[15]")
	public static WebElement emailId;		
	
	@FindBy(xpath = "(//div[@class='data-value'])[16]")
	public static WebElement addressLine;		
	
	@FindBy(xpath = "(//div[@class='data-value'])[17]")
	public static WebElement cityName;
	
	@FindBy(xpath = "(//div[@class='data-value'])[18]")
	public static WebElement stateName;
					
	@FindBy(xpath = "(//div[@class='data-value'])[19]")
	public static WebElement zipCode;
	
	@FindBy(xpath = "(//div[@class='data-value'])[20]")
	public static WebElement avgEmployee;
	
	@FindBy(xpath = "(//div[@class='data-value'])[21]")
	public static WebElement eligibleEmp;
	
	@FindBy(xpath = "(//div[@class='data-value'])[22]")
	public static WebElement coverageOffered; //--not required
	
	@FindBy(xpath = "(//div[@class='data-value'])[23]")
	public static WebElement totalEnrolledEmp;
	
	@FindBy(xpath = "(//div[@class='data-value'])[24]")
	public static WebElement totalValidWavier;
	
	@FindBy(xpath = "(//div[@class='data-value'])[25]")
	public static WebElement currentCarrier;
	
	@FindBy(xpath = "(//div[@class='data-value'])[26]")
	public static WebElement planDesignACR;//--not required
	
	@FindBy(xpath = "(//div[@class='data-value'])[27]")
	public static WebElement planDesignASO;//--not required
	
	@FindBy(xpath = "(//div[@class='data-value'])[28]")
	public static WebElement planDesign;//--not required
			
	@FindBy(xpath = "//li[@data-id='CompleteRequest']//a")
	public static WebElement completeRequestTab;	
	
	@FindBy(xpath = "(//div[@class='mos-u-display--inline-block'])[1]")
	public static WebElement getFirstEmployee1;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[1]")
	public static WebElement getFirstDOB1;
	
	@FindBy(xpath = "(//div[@class='mos-u-display--inline-block'])[2]")
	public static WebElement getSecondEmp1;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[6]")
	public static WebElement getSecondDOB1;
	
	@FindBy(xpath = "(//div[@class='mos-u-display--inline-block'])[3]")
	public static WebElement getThirdEmp1;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[11]")
	public static WebElement getThridDOB1;
	
	
	@FindBy(xpath = "(//div[@data-tooltip='Proxy to Member Company Configuration'])[1]")
	public static WebElement memberCompanyConfig1;
	
	
	@FindBy(xpath = "//div[@class='action ng-scope']")
	public static WebElement memberCompanyConfig;
	
	@FindBy(xpath = "//a[@data-name='go to Application']")
	public static WebElement mercerBenefitAdminPortal;
	
	@FindBy(xpath = "//i[contains(@class,'evo-icon-site-view-coverage')]//parent::a//parent::div")
	public static WebElement hrPortal;
	
	@FindBy(xpath = "//a[text()='Testing']")
	public static WebElement testingTab;
	
	@FindBy(xpath = "//h2[text()='Search for an employee to test']")
	public static WebElement empHeader ;
	
	@FindBy(xpath = "(//a[text()='Test Employee'])[1]")
	public static WebElement testEmp ;
	
	@FindBy(xpath = "//a[text()='View as Employee']")
	public static WebElement viewEmp ;
	
	@FindBy(xpath = "//div[@class='mes-card-actions']//a[2]")
	public static WebElement editClientSetUp ;
	
	
	@FindBy(xpath = "//h2[text()='Terms and Conditions Agreement']")
	public static WebElement termsConditionHeader;
	
	@FindBy(xpath = "//span[text()='Accept']")
	public static WebElement acceptButton;
	
	
	@FindBy(xpath = "//a[text()='GET STARTED']")
	public static WebElement getStartedButton ;
	
	@FindBy(xpath = "//h1[text()='Build Your Profile']")
	public static WebElement profilembc ;
	
	
	@FindBy(xpath = "(//button[@class='btn btn-primary'])[2]")
	public static WebElement nextButton ;
	
	@FindBy(xpath = "//span[text()='Menu']")
	public static WebElement menuButton ;
	
	@FindBy(xpath = "(//a[@class='ng-binding'])[1]")
	public static WebElement profileButton ;
	
	@FindBy(xpath = "//h1[text()='Personal Information']")
	public static WebElement personalInfoHeader ;
	
	@FindBy(xpath = "(//li[@class='enabled completed ng-star-inserted'])[12]//a")
	public static WebElement confirmationTab;
	
	@FindBy(xpath = "(//div[@class='side-action-item'])[2]")
	public static WebElement affinityDashboard;
	
	@FindBy(xpath = "//h1[@class='header-lead']")
	public static WebElement affinityDashboardHeader;
	
	@FindBy(css = "h1[data-name='page title']")
	public static WebElement reviewQuoteRequest_Title;
	
	@FindBy(xpath = "//h4[text()='Medical']")
	public static WebElement medical;
	
	@FindBy(xpath = "//h4[text()='Dental']")
	public static WebElement dental;
	
	@FindBy(xpath = "//h4[text()='Vision']")
	public static WebElement vision;
	
	@FindBy(xpath = "//h4[text()='Basic Life / AD&D']")
	public static WebElement basiclife;
	
		
	public static String values;
	public static String getOrgName1;
	public static String sicvalue;
	public static String getsic2;
	public static String taxidval;
	public static String getAddress1;
	public static String getCity1;
	public static String getZipCode1;
	public static String getDOC;
	public static String getstartDate;
	public static String getendDate;
	public static String getName;
	public static String getphoneNum;
	public static String getEmailId;
	public static String getAddressLine;
	public static String getCityName;
	public static String getStateName;
	public static String getZipCode;
	public static String getAvgEmployee;
	public static String getEligibleEmp;
	public static String getCoverageOffered;
	public static String getTotalEnrolledEmp;
	public static String getTotalValidWavier;
	public static String getCurrentCarrier;
	public static String getPlanDesignACR;
	public static String getPlanDesignASO;
	public static String getPlanDesign;
	public static String getemployee1;
	public static String getDOB4;
	public static String getemployee2;
	public static String getDOB5;
	public static String getemployee3;
	public static String getDOB6;
	public static String url_cp;
	
	
	public CompleteRequestPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void completeRequest() throws InterruptedException {
		
		Thread.sleep(5000);
		waitForElementToDisplay(acceptCheckbox);
		System.out.println("before click");
		Thread.sleep(10000);
		scrollToElement(driver,acceptCheckbox);
		Thread.sleep(10000);
		clickElementUsingJavaScript(driver,acceptCheckbox);	
		clickElementUsingJavaScript(driver,continueButton);	
		
	
		
		currentURL = driver.getCurrentUrl();
        System.out.println(currentURL);
        
        AssociationID=currentURL.substring(44, 49);
        System.out.println(AssociationID);
        Thread.sleep(6000);

	}
	
	public void completeRequestAffinity() throws InterruptedException {
		
		Thread.sleep(10000);
		scrollToElement(driver,acceptCheckbox);
		Thread.sleep(7000);
		//acceptCheckbox.click();
		clickElementUsingJavaScript(driver,acceptCheckbox);	
		clickElementUsingJavaScript(driver,continueButton);	
		waitForPageLoad(driver);
		 
	}
	
	public void getURL() throws InterruptedException {
		
		currenturl = driver.getCurrentUrl();
        System.out.println(currenturl);
        String associd=currenturl.substring(0,79);        
        AssociationID= currenturl.substring(79,84);
        System.out.println(associd+AssociationID);
        String actualurl = associd.concat(AssociationID).concat("/2019");
       // String actualurl = associd.concat("26582").concat("/2019");
        System.out.println(actualurl);
        driver.get(actualurl);
        
        Thread.sleep(5000);
        
        driver.navigate().refresh();
	}
	public static String fetchOrgData() {
		getOrgName1=org.getText();
		System.out.println(getOrgName1);
		return getOrgName1;
	}
	
	public static String fetchsic() {
		String getsic=sic.getText();
		sicvalue=getsic.substring(0, 29);
		System.out.println(sicvalue);
		return sicvalue;
	}
	
	
	public static String fetchsic2() {
		getsic2=sic2.getText();
		System.out.println(getsic2);
		return getsic2;
	}
	
	public static String fetchTaxId() {
		String getTaxId1=tax.getText();
		taxidval=getTaxId1.substring(3,8);
		System.out.println(taxidval);
		return taxidval;
	}
	
	public static String fetchAddress() {
		getAddress1=address.getText().trim();
		System.out.println(getAddress1);
		return getAddress1;
	}
	
	public static String fetchCityName() {
		getCity1=city.getText();
		System.out.println(getCity1);
		return getCity1;
	}
	
	public static String fetchZipCode() {
		getZipCode1=zipcode.getText();
		System.out.println(getZipCode1);
		return getZipCode1;
	}
	
	public static String fetchDOC() {
		getDOC=DOC.getText();
		System.out.println(getDOC);
		return getDOC;
	}
	
	public static String fetchStartDate() {
		getstartDate=startDate.getText();
		System.out.println(getstartDate);
		return getstartDate;
	}
	
	public static String fetchEndDate() {
		getendDate=endDate.getText();
		System.out.println(getendDate);
		return getendDate;
	}
	
	public static String fetchName() {
		getName=name.getText();
		System.out.println(getName);
		return getName;
	}
	
	public static String fetchPhoneNum() {
		getphoneNum=phoneNum.getText();
		System.out.println(getphoneNum);
		return getphoneNum;
	}
	
	public static String fetchEmailId() {
		getEmailId=emailId.getText();
		System.out.println(getEmailId);
		return getEmailId;
	}
	
	public static String fetchAddressLine() {
		getAddressLine=addressLine.getText();
		System.out.println(getAddressLine);
		return getAddressLine;
	}
	
	
	public static String fetchCity() {
		getCityName=cityName.getText();
		System.out.println(getCityName);
		return getCityName;
	}
	
	public static String fetchStateName() {
		getStateName=stateName.getText();
		System.out.println(getStateName);
		return getStateName;
	}
	
	public static String fetchZipCode1() {
		 getZipCode=zipCode.getText();
		System.out.println(getZipCode);
		return getZipCode;
	}
	
	public static String fetchAvgEmp() {
		getAvgEmployee=avgEmployee.getText();
		System.out.println(getAvgEmployee);
		return getAvgEmployee;
	}
	
	public static String fetchEligibleEmp() {
		getEligibleEmp=eligibleEmp.getText();
		System.out.println(getEligibleEmp);
		return getEligibleEmp;
	}
	
	public static String fetchCovgOffered() {
		getCoverageOffered=coverageOffered.getText();
		System.out.println(getCoverageOffered);
		return getCoverageOffered;
	}
	
	public static String fetchTotalEmp() {
		getTotalEnrolledEmp=totalEnrolledEmp.getText();
		System.out.println(getTotalEnrolledEmp);
		return getTotalEnrolledEmp;
	}
	
	public static String fetchTotalWavier() {
		 getTotalValidWavier=totalValidWavier.getText();
		System.out.println(getTotalValidWavier);
		return getTotalValidWavier;
	}
	
	public static String fetchCurrentCarrier() {
		 getCurrentCarrier=currentCarrier.getText();
		System.out.println(getCurrentCarrier);
		return getCurrentCarrier;
	}
	
	public static void fetchCensusData() {
		getemployee1=getFirstEmployee1.getText();
		System.out.println(getemployee1);
		
		getDOB4=getFirstDOB1.getText();
		System.out.println(getDOB4);
		
		getemployee2=getSecondEmp1.getText();
		System.out.println(getemployee2);
		
		getDOB5=getSecondDOB1.getText();
		System.out.println(getDOB5);
		
		
	}
	

	public void switchRequestPage(WebElement e) throws InterruptedException {
		clickElement(e);
	}
	
	public void proxyMemberCompany() throws InterruptedException {
		Thread.sleep(5000);
		switchToWindowWithURL(url_cp,driver);
		Thread.sleep(3000);
		System.out.println(getSignUpToolUrl);
		clickElementUsingJavaScript(driver,memberCompanyConfig);
		switchToWindowWithURL(getSignUpToolUrl,driver);
		System.out.println("pass");
		waitForElementToDisplay(editClientSetUp);
		clickElementUsingJavaScript(driver,editClientSetUp);
		
	}
	
	public void getCurrentURL() {
		url_cp = driver.getCurrentUrl();
        System.out.println(url_cp);
        
       
	}
	
	public void proxyHRPortal() throws InterruptedException {
		
		String url1="https://consultant-qaf.mercermarketplace365plus.com/hrPortal#/hr/";
		String url2="/2020/dashboard";		
		signupTool_AssociationID="26909";
		String HRProxyURL=url1.concat(signupTool_AssociationID).concat(url2);
		System.out.println(HRProxyURL);		
		driver.get(HRProxyURL);
		waitForElementToDisplay(hrPortal);
		clickElement(hrPortal);
		Thread.sleep(5000);
		
		System.out.println(driver.getWindowHandles().size());
		switchToWindowWithURL(HRProxyURL,driver);
		waitForElementToDisplay(testingTab);
		clickElement(testingTab);
		
		waitForElementToDisplay(empHeader);
		Thread.sleep(5000);
		waitForElementToDisplay(testEmp);
		clickElementUsingJavaScript(driver,testEmp);
		clickElement(viewEmp);
		Thread.sleep(10000);
		switchToWindowWithURL("https://authoring-qaf.mercermarketplace365plus.com/viewtnc",driver);
		//switchToWindowWithURL("https://authoring-qaf.mercermarketplace365plus.com/dashboard",driver);
		
		Thread.sleep(10000);
		waitForElementToDisplay(termsConditionHeader);
		Thread.sleep(5000);
		clickElementUsingJavaScript(driver,acceptButton);
		
		waitForElementToDisplay(getStartedButton);
		clickElementUsingJavaScript(driver,getStartedButton);
		
		waitForElementToDisplay(profilembc);
		clickElementUsingJavaScript(driver,nextButton);
		waitForElementToDisplay(menuButton);
		clickElementUsingJavaScript(driver,menuButton);
		waitForElementToDisplay(profileButton);
		clickElementUsingJavaScript(driver,profileButton);
		
		waitForElementToDisplay(personalInfoHeader);
		System.out.println("sjdgf");
		driver.close();
		Thread.sleep(3000);
		switchToWindowWithURL(HRProxyURL,driver);
		driver.close();
		Thread.sleep(5000);
		switchToWindowWithURL(url_cp,driver);
		
	}
	
	public void proxyAffinityDashboard() throws InterruptedException {
		Thread.sleep(5000);
		switchToWindowWithURL(url_cp,driver);
		waitForElementToDisplay(affinityDashboard);
		clickElement(affinityDashboard);
		Thread.sleep(8000);
		switchToWindowByTitle("Affinity 365+ Dashboard - Google Chorme",driver);
	}
	
	

}