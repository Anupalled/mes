package pages.MES;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

public class PlanSelectionPage {
WebDriver driver;
	@FindBy(xpath = "//h3[text()='Medical']")
	public static WebElement MedicalPlan;
	
	@FindBy(xpath = "//span[@class='icon-toggle-arrow']")
	public static WebElement Arrowdown; 
	
	@FindBy(xpath = "//a[text()='Plan Selection']")
	public static WebElement planSelectionHeader;
	
	@FindBy(xpath = "//button[text()='Save & Generate']")
	public static WebElement SavenGen;

	@FindBy(xpath = "(//label[contains(text(),'UnitedHealthcare - $6,300 Deductible 100% HSA')])[1]")
	public static WebElement healthcare;
	

	@FindBy(xpath = "//label[text()='UnitedHealthcare - $3,000 Deductible (Family Style) 100% HSA ']")
	public static WebElement healthcareHSA;
	

	@FindBy(xpath = "(//span[@class='icon-toggle-arrow'])[1]")
	public static WebElement toggle;
	

	@FindBy(xpath = "//label[text()='UnitedHealthcare - $2,000 Passive PPO with Ortho; Contributory']")
	public static WebElement healthcareOrtho;
	
	@FindBy(xpath = "//label[text()='UnitedHealthcare - $1,000 Passive PPO; Contributory']")
	public static WebElement healthcarePPO;
	
	@FindBy(xpath = "//label[text()='UnitedHealthcare - $10/$25 Copay; $150 Allowance; Contributory']")
	public static WebElement vision;
	
	
	@FindBy(xpath = "	//label[text()='UnitedHealthcare - $10/$25 Copay; $150 Allowance; Employee Paid']")
	public static WebElement healthcareEmployeePaid;
	
	
	@FindBy(xpath = "//label[text()='UnitedHealthcare - $25,000']")
	public static WebElement lifeInsurance;
	
	
	@FindBy(xpath = "//h3[text()='Vision']")
	public static WebElement VisionPlan;
	
	@FindBy(xpath = "//h3[text()='Dental']")
	public static WebElement DentalPlan;
	
	@FindBy(xpath = "//h3[text()='Basic Employee Life/AD&D Insurance']")
	public static WebElement LifePlan;
	
	@FindBy(xpath = "//label[text()='Medica - $500 Deductible 75% PPO-(Medica Choice PP)']")
	public static WebElement medicaOption1;
	
	@FindBy(xpath = "//label[text()='Medica - $500 Deductible 75% PPO-(VantagePlus)']")
	public static WebElement medicaOption2;
	
	@FindBy(xpath = "//label[text()='UnitedHealthcare - $1,500 Ded 80% EPO CORE Essential Network HSA']")
	public static WebElement allSavers1;
	
	

	public PlanSelectionPage(WebDriver driver) {
	this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
			
			
			
	public void selectPlans() throws InterruptedException {
		waitForElementToDisplay(planSelectionHeader);
		//waitForElementToDisplay(healthcare);
		clickElement(planSelectionHeader);
		waitForElementToDisplay(MedicalPlan);
		clickElement(Arrowdown);
		clickElement(healthcare);
		clickElement(healthcareHSA);
		clickElement(SavenGen);
		Thread.sleep(3000);
		waitForElementToDisplay(DentalPlan);
		clickElement(toggle);
		clickElement(healthcareOrtho);
		clickElement(healthcarePPO);
		clickElement(SavenGen);
		Thread.sleep(3000);
		waitForElementToDisplay(VisionPlan);
		clickElement(toggle);
		clickElement(vision);
		clickElement(healthcareEmployeePaid);
		clickElement(SavenGen);
		Thread.sleep(3000);
		waitForElementToDisplay(LifePlan);
		clickElement(toggle);
		clickElement(lifeInsurance);
		clickElement(SavenGen);
		Thread.sleep(3000);
	}
			
			
			
	public void selectMedicaPlans() throws InterruptedException {
		
		waitForElementToDisplay(planSelectionHeader);
		clickElement(planSelectionHeader);
		waitForElementToDisplay(MedicalPlan);
		clickElement(toggle);
		clickElement(medicaOption1);
		clickElement(medicaOption2);
		clickElement(SavenGen);
		Thread.sleep(3000);
	}	
	
	public void selectallSaversPlans() throws InterruptedException {
		waitForElementToDisplay(planSelectionHeader);
		clickElement(planSelectionHeader);
		waitForElementToDisplay(MedicalPlan);
		clickElement(toggle);	
		clickElement(allSavers1);
		clickElement(SavenGen);
	}	
			
			
			
			
		
}
