package pages.MES;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.RowId;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import static driverfactory.Driver.*;
import static driverfactory.Driver.waitForElementsToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.SearchContext;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;



import utilities.DatabaseUtils;
import driverfactory.Driver;
import utilities.DatabaseUtils;
import utilities.ExcelReader;

public class ProfilePage1 {
	private static XSSFWorkbook wb;
public static String getdate;
	@FindBy(xpath = "//h1[text()=' Profile']")
	public static WebElement profileHeader1;

	@FindBy(xpath = "//input[@data-name='name']")
	public static WebElement organisation;

	@FindBy(xpath = "//select[@data-name='industryClass']")
	public static WebElement SIC;

	@FindBy(xpath = "//select[@data-name='industryCategory']")
	public static WebElement SIC2;

	@FindBy(xpath = "//input[@data-name='fedId']")
	public static WebElement taxId;

	@FindBy(xpath = "//input[@data-name='address1']")
	public static WebElement address1;

	@FindBy(xpath = "//input[@data-name='city']")
	public static WebElement city;

	@FindBy(xpath = "//select[@data-name='stateId']")
	public static WebElement state;

	@FindBy(xpath = "//input[@data-name='zip']")
	public static WebElement zipCode;

	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;

	@FindBy(css = "input[data-name='name']")
	public static WebElement org;
	//input[@data-name='name']
	@FindBy(xpath = "//input[@data-name='firstName']")
	public static WebElement firstName;

	@FindBy(xpath = "//input[@data-name='lastName']")
	public static WebElement lastName;
 
	@FindBy(xpath = "//input[@data-name='phone']")
	public static WebElement phone;

	@FindBy(xpath = "//input[@data-name='email']")
	public static WebElement email;

	@FindBy(xpath = "(//mercer-icon[@icon='date_range'])[1]")
	public static WebElement dateOfCov;
	
	@FindBy(xpath = "(//div[@class='column shrink'])[1]//mercer-icon")
	public static WebElement dateOfCov1;
	
	@FindBy(xpath = "(//a[@class='mos-c-calendar__day'])[1]")
	public static WebElement date1_ahp;
	
	

	@FindBy(xpath = "(//a[contains(text(),'1')])[2]")
	public static WebElement date1;

	@FindBy(xpath = "(//mercer-icon[@icon='date_range'])[2]")
	public static WebElement datepicker;
	
	@FindBy(xpath = "(//div[@class='column shrink'])[2]")
	public static WebElement datepicker2;

	
	@FindBy(xpath = "//a[text()='\"+getdate+\"']")
	public static WebElement date2;
	
	
	
	@FindBy(xpath = "//a[text()='26']")
	public static WebElement date2_ahp;
	
	

	@FindBy(xpath = "(//input[@type='text'])[1]")
	public static WebElement avgEmployee;

	@FindBy(xpath = "(//input[@type='text'])[3]")
	public static WebElement eligiEmployee;
	
	@FindBy(xpath = "(//input[@type='text'])[2]")
	public static WebElement totalEligiEmployee;
	
	@FindBy(xpath = "(//input[@type='text'])[4]")
	public static WebElement empAppliedCovg;
	
	@FindBy(xpath = "(//input[@type='text'])[5]")
	public static WebElement ValidWai;
	
	@FindBy(xpath = "(//input[@type='text'])[6]")
	public static WebElement EnrolledEmployee;

	@FindBy(xpath = "//button[@data-name='Previous']")
	public static WebElement previousButton;

	@FindBy(xpath = "(//div[@data-name='companyAnswer']//label[text()='Yes'])[1]")
	public static WebElement offerCovgYes1;

//	@FindBy(xpath = "//input[@data-id='profile-question-4']")
	//public static WebElement enrolledEmp;

// @FindBy(xpath = "//input[@data-id='profile-question-5']")
//	public static WebElement validWaiver;

	@FindBy(xpath = "//input[@data-id='profile-question-6']")
	public static WebElement currentCarrier;

	@FindBy(xpath = "(//div[@data-name='companyAnswer']//label[text()='Yes'])[2]")
	public static WebElement plan1;

	@FindBy(xpath = "(//div[@data-name='companyAnswer']//label[text()='Yes'])[3]")
	public static WebElement plan2;

	@FindBy(xpath = "(//div[@data-name='companyAnswer']//label[text()='Yes'])[4]")
	public static WebElement plan3;

	@FindBy(xpath = "//button[@icon='keyboard_arrow_right']")
	public static WebElement arrowRight;

	@FindBy(xpath = "//h3[text()='Tell us about yourself']")
	public static WebElement profileHeader2;
	

	@FindBy(xpath = "//h3[text()='Tell us when you want coverage']")
	public static WebElement profileHeader3;

	@FindBy(xpath = "//h3[text()='We have a few questions about your group and existing coverage.']")
	public static WebElement profileHeader4;

	@FindBy(xpath = "//div[@class='mos-u-hint-text mos-u-hint-text--invalid ng-star-inserted']")
	public static List<WebElement> valueRequiredErrorMsg;
	
	@FindBy(xpath = "//span[@class='tooltip-text']")
	public static WebElement toolTip;
	
	@FindBy(xpath = "//div[@class='mos-u-hint-text mos-u-hint-text--invalid ng-star-inserted']")
	public static List<WebElement> alphaNumValue;
	
	@FindBy(xpath = "//strong[text()='Same as Organization Address']")
	public static WebElement uncheckSameOrg;
	
	@FindBy(xpath = "//input[@data-name='address1']")
	public static WebElement address;
	
	@FindBy(xpath = "//li[@data-id='Profile']//a")
	public static WebElement profile;
	
	@FindBy(xpath = "//li[@data-id='QuoteDocuments']//a")
	public static WebElement quoteDocument;
	
	
	@FindBy(xpath = "//div[@class='section half']")
	public static List<WebElement> getProfileData;
	
	@FindBy(xpath = "//input[@class='ng-untouched ng-pristine ng-valid']")
	public static WebElement orgname;
	
	
	
	@FindBy(xpath = "(//span[@class='ng-star-inserted'])[1]")
	public static WebElement dateOfCovg;
	
	@FindBy(xpath = "(//div[@class='mos-c-datepicker__input-date'])[2]")
	public static WebElement startdate;
	
	@FindBy(xpath = "(//span[@class='ng-star-inserted'])[5]")
	public static WebElement enddate;
	
	@FindBy(xpath = "//h1[text()=' Quote Review & Plan Selection']")
	public static WebElement planSelectionTitle;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted']//a)[4]")
	public static WebElement dates1;
	
	@FindBy(xpath = "//select")
	public static WebElement currentCarrierDrpdown;
	
	@FindBy(xpath = "//tbody//td")
	public static WebElement datepicker_new;
	
	@FindBy(xpath = "//input[@data-name='Please Specify the Name of Your Current Carrier']")
	public static WebElement currentcarrier;
	
	
	
	public static String value;
	public static String getOrgName_PP;
	public static String getSIC_PP;
	public static String getSIC2_PP;
	public static String gettaxID_PP;
	public static String getaddress_PP;
	public static String getcity_PP;
	public static String getzipCode_PP;
	public static String getFirstName_PP;
	public static String getPhoneNum_PP;
	public static String getEmailId_PP;
	public static String getdateOfCovg_PP;
	public static String getstartdate_PP;
	public static String getenddate_PP;
	public static String avgEmp_PP;
	public static String eligiEmp_PP;
	public static String enrollemp1_PP;
	public static String validwavier1_PP;
	public static String currentcarrier1_PP;
	static Select select;
	
	public static String sic1;

	public static WebDriver driver;

	public ProfilePage1(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public static void setOrgName(String Index) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook1 = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook1.getSheet("profiledata1");
		Iterator<Row> rowIterator = mySheet.iterator();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);

			if (s23.equals(Index)) {

				cell = (Cell) coliterator.next();
				String sic1value = cell.getStringCellValue();
				String sic1 = sic1value.trim();
				Driver.selEleByVisbleText(SIC, sic1);
				


				cell = (Cell) coliterator.next();
				String sic2value = cell.getStringCellValue();
				String sic22 = sic2value.trim();
				System.out.println(sic22);
				Driver.selEleByVisbleText(SIC2, sic22);

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String taxid = s1s.trim();
				setInput(taxId, taxid);

				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String getaddress = ss1.trim();
				setInput(address1, getaddress);

				cell = (Cell) coliterator.next();
				String cityvalue = cell.getStringCellValue();
				String getCityName1 = cityvalue.trim();
				setInput(city, getCityName1);

				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String getstate = state2value.trim();
				Driver.selEleByVisbleText(state, getstate);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String sss = Integer.toString(Exec1);
				String  getzipCode1= sss.trim();
				setInput(zipCode, getzipCode1);
				Thread.sleep(3000);
			
			}

		}
	}
	
	public static void setOrgNameCIT(String Index) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook1 = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook1.getSheet("profiledata1");
		Iterator<Row> rowIterator = mySheet.iterator();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);

			if (s23.equals(Index)) {

				cell = (Cell) coliterator.next();
				String sic1value = cell.getStringCellValue();
				String sic1 = sic1value.trim();
				Driver.selEleByVisbleText(SIC, sic1);
				

				cell = (Cell) coliterator.next();
				String sic2value = cell.getStringCellValue();
				String sic22 = sic2value.trim();
				Driver.selEleByVisbleText(SIC2, sic22);

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String taxid = s1s.trim();
				setInput(taxId, taxid);

				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String getaddress = ss1.trim();
				setInput(address1, getaddress);

				cell = (Cell) coliterator.next();
				String cityvalue = cell.getStringCellValue();
				String getCityName1 = cityvalue.trim();
				setInput(city, getCityName1);

				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String getstate = state2value.trim();
				Driver.selEleByVisbleText(state, getstate);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String sss = Integer.toString(Exec1);
				String  getzipCode1= sss.trim();
				setInput(zipCode, getzipCode1);
				Thread.sleep(3000);
			
			}

		}
	}
	
	public static void setOrgNameAffinity(String Affinity) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook1 = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook1.getSheet("profiledataAffinity");
		Iterator<Row> rowIterator = mySheet.iterator();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			
			if (s23.equals(Affinity)) {
				
				cell = (Cell) coliterator.next();
				String sic1value = cell.getStringCellValue();
				String sic1 = sic1value.trim();
				Driver.selEleByVisbleText(SIC, sic1);
				
				
				clickElement(SIC2);
				cell = (Cell) coliterator.next();
				String sic2value = cell.getStringCellValue();
				String sic22 = sic2value.trim();
				Driver.selEleByVisbleText(SIC2, sic22);

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String taxid = s1s.trim();
				setInput(taxId, taxid);

				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String getaddress = ss1.trim();
				setInput(address1, getaddress);

				cell = (Cell) coliterator.next();
				String cityvalue = cell.getStringCellValue();
				String getCityName1 = cityvalue.trim();
				setInput(city, getCityName1);

				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String getstate = state2value.trim();
				Driver.selEleByVisbleText(state, getstate);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String sss = Integer.toString(Exec1);
				String  getzipCode1= sss.trim();
				setInput(zipCode, getzipCode1);
				
			
			}

		}
	}
	
	public static void setKnockOutState_Affinity(String Index) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook1 = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook1.getSheet("knockoutstate1");
		Iterator<Row> rowIterator = mySheet.iterator();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			
			if (s23.equals(Index)) {
				
				cell = (Cell) coliterator.next();
				String sic1value = cell.getStringCellValue();
				String sic1 = sic1value.trim();
				Driver.selEleByVisbleText(SIC, sic1);
				
				
				clickElement(SIC2);
				cell = (Cell) coliterator.next();
				String sic2value = cell.getStringCellValue();
				String sic22 = sic2value.trim();
				Driver.selEleByVisbleText(SIC2, sic22);

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String taxid = s1s.trim();
				setInput(taxId, taxid);

				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String getaddress = ss1.trim();
				setInput(address1, getaddress);

				cell = (Cell) coliterator.next();
				String cityvalue = cell.getStringCellValue();
				String getCityName1 = cityvalue.trim();
				setInput(city, getCityName1);

				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String getstate = state2value.trim();
				Driver.selEleByVisbleText(state, getstate);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String sss = Integer.toString(Exec1);
				String  getzipCode1= sss.trim();
				setInput(zipCode, getzipCode1);
				
			
			}

		}
	}

	public void selectContinue() throws InterruptedException {
		waitForElementToDisplay(continueButton);
		Thread.sleep(10000);
		clickElementUsingJavaScript(driver,continueButton);
		Thread.sleep(5000);
	}
	
	public static void switchProfile1(WebElement e) throws InterruptedException {
		clickElement(e);
	}
	

	public static void setProfileData(String data) throws IOException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("profiledata2");
		Iterator<Row> rowIterator = mySheet.iterator();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(data)) {

				cell = (Cell) coliterator.next();
				String data1 = cell.getStringCellValue();
				String getFirstName = data1.trim();
				setInput(firstName, getFirstName);
				
		

				cell = (Cell) coliterator.next();
				String data2 = cell.getStringCellValue();
				String lastname = data1.trim();
				setInput(lastName, lastname);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String s15 = Integer.toString(Exec1);
				String getPhoneNum = s15.trim();
				setInput(phone, getPhoneNum);

				cell = (Cell) coliterator.next();
				String data3 = cell.getStringCellValue();
				String getemailid = data3.trim();
				setInput(email, getemailid);
				

			}
		}
	}
	
	
	public void setCoverage() throws InterruptedException {
		Thread.sleep(5000);
		dateOfCov.click();
		clickElement(date1);
		clickElement(datepicker);
		Thread.sleep(7000);
		List<String> date=new ArrayList <String>();
		List<String> alldate=new ArrayList <String>();
		List<String> wrongdate =new ArrayList <String>();
		
		for(int j=1;j<=4;j++) {
			wrongdate.add(String.valueOf(j));
		}
		for(int i=1;i<=31;i++) {
			alldate.add(String.valueOf(i));
		}
		
		List<WebElement> allLinks = driver.findElements(By.xpath("//td[(@class='mos-c-calendar--disabled ng-star-inserted')]"));
		
		for(WebElement ele:allLinks) {
			
	String dates=ele.getText();
	date.add(dates);
	System.out.println("dates :"+ dates);
		
		}
	alldate.removeAll(date);
	alldate.removeAll(wrongdate);
	WebElement e =  driver.findElement(By.xpath("//a[text()='"+alldate.get(1)+"']"));
	System.out.println(e.getText());
	clickElement(e);
	}
	
public void setcovAllSaver() throws InterruptedException {
	Thread.sleep(5000);
	dateOfCov.click();
	clickElement(date1);
	clickElement(datepicker);
	Thread.sleep(3000);
	clickElement(date2_ahp);
	
}
	
	
	public void setCoverage_ahp() throws InterruptedException {
		waitForPageLoad(driver);
		clickElement(dateOfCov1);
		//clickElement(arrowRight);
		clickElement(date1_ahp);
		clickElement(datepicker2);
		Thread.sleep(3000);
		clickElement(date2_ahp);
		clickElement(continueButton);
		   
	}
	public void setCov() throws InterruptedException {
		Thread.sleep(5000);
		dateOfCov.click();
		clickElement(date1);
		clickElement(datepicker);
		Thread.sleep(7000);
		List<String> date=new ArrayList <String>();
		List<String> alldate=new ArrayList <String>();
		List<String> wrongdate =new ArrayList <String>();
		
		for(int i=1;i<=4;i++) {
			wrongdate.add(String.valueOf(i));
		}
		for(int i=1;i<=31;i++) {
			alldate.add(String.valueOf(i));
		}
		
		List<WebElement> allLinks = driver.findElements(By.xpath("//td[(@class='mos-c-calendar--disabled')]"));
		
		for(WebElement ele:allLinks) {
			
	String dates=ele.getText();
	date.add(dates);
	
		
		}
		
	alldate.removeAll(date);
	alldate.removeAll(wrongdate);
	
	Thread.sleep(5000);
	WebElement e =  driver.findElement(By.xpath("//a[text()='"+alldate.get(0)+"']"));
	System.out.println(e.getText());
	clickElement(e);
	}
	

	public static void setCoverage(String coverage) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("profiledata4");
		Iterator<Row> rowIterator = mySheet.iterator();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(coverage)) {

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String avgEmp = s1s.trim();
				setInput(avgEmployee, avgEmp);

							
				cell = (Cell) coliterator.next();
				int Exec2 = (int) cell.getNumericCellValue();
				String s2s = Integer.toString(Exec2);
				String totemp1 = s2s.trim();
				setInput(totalEligiEmployee, totemp1);

				cell = (Cell) coliterator.next();
				int Exec3 = (int) cell.getNumericCellValue();
				String s3s = Integer.toString(Exec3);
				String eligiEmp = s3s.trim();
				setInput(eligiEmployee, eligiEmp);
				
				cell = (Cell) coliterator.next();
				int Exec4 = (int) cell.getNumericCellValue();
				String s4s = Integer.toString(Exec4);
				String validwavier1 = s4s.trim();
				setInput(ValidWai, validwavier1);
				
				cell = (Cell) coliterator.next();
				int Exec5 = (int) cell.getNumericCellValue();
				String s5s = Integer.toString(Exec5);
				String empAppCvg = s5s.trim();
				setInput(empAppliedCovg,empAppCvg);
				
				Thread.sleep(2000);
				clickElement(offerCovgYes1);
				
				cell = (Cell) coliterator.next();
				int Exec6 = (int) cell.getNumericCellValue();
				String s6s = Integer.toString(Exec6);
				String enrollemp1 = s6s.trim();
				setInput(EnrolledEmployee, enrollemp1);
				select = new Select(currentCarrierDrpdown);
				select.selectByVisibleText("UnitedHealthcare");

			
				clickElement(plan1);
				clickElement(plan2);
				clickElement(plan3);

			}
		}
	}
	

	public static void setCoverage_AllSavers(String coverage) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("profiledata4");
		Iterator<Row> rowIterator = mySheet.iterator();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(coverage)) {

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String avgEmp = s1s.trim();
				setInput(avgEmployee, avgEmp);

							
				cell = (Cell) coliterator.next();
				int Exec2 = (int) cell.getNumericCellValue();
				String s2s = Integer.toString(Exec2);
				String totemp1 = s2s.trim();
				setInput(totalEligiEmployee, totemp1);

				cell = (Cell) coliterator.next();
				int Exec3 = (int) cell.getNumericCellValue();
				String s3s = Integer.toString(Exec3);
				String eligiEmp = s3s.trim();
				setInput(eligiEmployee, eligiEmp);
				
				
				cell = (Cell) coliterator.next();
				int Exec5 = (int) cell.getNumericCellValue();
				String s5s = Integer.toString(Exec5);
				String empAppCvg = s5s.trim();
				setInput(empAppliedCovg,empAppCvg);
				
				Thread.sleep(2000);
				clickElement(offerCovgYes1);
				
				cell = (Cell) coliterator.next();
				int Exec6 = (int) cell.getNumericCellValue();
				String s6s = Integer.toString(Exec6);
				String enrollemp1 = s6s.trim();
				setInput(ValidWai, enrollemp1);
				
				select = new Select(currentCarrierDrpdown);
				select.selectByVisibleText("UnitedHealthcare");

			}
		}
	}
	
	public static void setCoverageMedica(String coverage) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("profiledata4");
		Iterator<Row> rowIterator = mySheet.iterator();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(coverage)) {

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String avgEmp = s1s.trim();
				setInput(avgEmployee, avgEmp);

							
				cell = (Cell) coliterator.next();
				int Exec2 = (int) cell.getNumericCellValue();
				String s2s = Integer.toString(Exec2);
				String totemp1 = s2s.trim();
				setInput(totalEligiEmployee, totemp1);

				cell = (Cell) coliterator.next();
				int Exec3 = (int) cell.getNumericCellValue();
				String s3s = Integer.toString(Exec3);
				String eligiEmp = s3s.trim();
				setInput(eligiEmployee, eligiEmp);
				
				cell = (Cell) coliterator.next();
				int Exec4 = (int) cell.getNumericCellValue();
				String s4s = Integer.toString(Exec4);
				String validwavier1 = s4s.trim();
				setInput(ValidWai, validwavier1);
				
				cell = (Cell) coliterator.next();
				int Exec5 = (int) cell.getNumericCellValue();
				String s5s = Integer.toString(Exec5);
				String empAppCvg = s5s.trim();
				setInput(empAppliedCovg,empAppCvg);
				
				Thread.sleep(2000);
				clickElement(offerCovgYes1);
				
				cell = (Cell) coliterator.next();
				int Exec6 = (int) cell.getNumericCellValue();
				String s6s = Integer.toString(Exec6);
				String enrollemp1 = s6s.trim();
				setInput(EnrolledEmployee, enrollemp1);
				
				select = new Select(currentCarrierDrpdown);
				select.selectByVisibleText("Other");
				
				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String setcarrier = ss1.trim();
				setInput(currentcarrier, setcarrier);
				
			}
		}
	}

	
	public static void setCoverageNegFlow(String coverage) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("profiledata4");
		Iterator<Row> rowIterator = mySheet.iterator();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(coverage)) {

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String s13 = s1s.trim();
				setInput(avgEmployee, s13);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String s2s = Integer.toString(Exec1);
				String s14 = s2s.trim();
				setInput(eligiEmployee, s14);

				cell = (Cell) coliterator.next();
				int Exec2 = (int) cell.getNumericCellValue();
				String s3s = Integer.toString(Exec2);
				String s15 = s3s.trim();
				setInput(totalEligiEmployee, s15);

				
				cell = (Cell) coliterator.next();
				int Exec3 = (int) cell.getNumericCellValue();
				String s4s = Integer.toString(Exec3);
				String s16 = s4s.trim();
				setInput(empAppliedCovg, s16);

				

			}
		}
	}
	
	public static void setCoveragePosFlow(String coverage) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("profiledata4");
		Iterator<Row> rowIterator = mySheet.iterator();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(coverage)) {

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String s13 = s1s.trim();
				setInput(avgEmployee, s13);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String s2s = Integer.toString(Exec1);
				String s14 = s2s.trim();
				setInput(eligiEmployee, s14);

				cell = (Cell) coliterator.next();
				int Exec2 = (int) cell.getNumericCellValue();
				String s3s = Integer.toString(Exec2);
				String s15 = s3s.trim();
				setInput(totalEligiEmployee, s15);

				
				cell = (Cell) coliterator.next();
				int Exec3 = (int) cell.getNumericCellValue();
				String s4s = Integer.toString(Exec3);
				String s16 = s4s.trim();
				setInput(empAppliedCovg, s16);
				
				/*cell = (Cell) coliterator.next();
				int Exec4 = (int) cell.getNumericCellValue();
				String s5s = Integer.toString(Exec4);
				String s17 = s5s.trim();
				setInput(ValidWai, s17);
*/
				
				
			}
		}
	}

	public void selectPrevious() throws InterruptedException {

		clickElement(previousButton);

	}

	public  void getOrganisationName() throws InterruptedException {
		
		waitForElementToDisplay(org);
		value = RandomStringUtils.randomAlphabetic(5).toLowerCase();
		clickElement(org);
		setInput(org,value);
		System.out.println(value);
		
	}
	public void getOrg() throws InterruptedException {
		waitForElementToDisplay(org);
		setInput(org, "pzmohg");
	}

	public void fetchCompanyDetails_DB() throws ClassNotFoundException, SQLException {
		int columnCount = 12;
		Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
		ResultSet myres = getResultSet(mycon,"select * from company_plan_detail where company_id='33254'");
		while (myres.next()) {
			for (int i = 1; i <= columnCount; i++) {
				String t = myres.getString(i);
				System.out.println(t);
			}
		}

	}
	
	public void fetchToolTip() throws ClassNotFoundException, SQLException {
		int columnCount = 12;
		Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
		ResultSet myres = getResultSet(mycon,"select * from ASSOC_GROUPS_QUESTION");
		while (myres.next()) {
			for (int i = 1; i <= columnCount; i++) {
				String t = myres.getString(i);
				System.out.println(t);
			}
		}

	}
	
	


	public void verifyValueRequiredMsg() {
		waitForElementsToDisplay(valueRequiredErrorMsg);
		int value=valueRequiredErrorMsg.size();
		System.out.println(value);
		for (WebElement option : valueRequiredErrorMsg) {

			String Str = "Value is required"; // based on string text match I want to select
			if (Str.equals(option.getText())) {
			System.out.println("Please enter the values");
			}
		}

	}
	
	public void negativeFlowPP1() throws InterruptedException {
		Thread.sleep(3000);
		setInput(taxId, "sfdfd");
		setInput(zipCode, "sfdfd");
		clickElement(continueButton);
		//waitForElementsToDisplay(alphaNumValue);
			String str1="Tax ID should consist of 9 digits and should be numeric only"; 
			String str2="Zip code should consist of 5 digits and should be numeric only"; 
			if (str1.equals(taxId.getText())) {
			System.out.println("Please enter tax id value");
			}
			if (str1.equals(zipCode.getText())) {
			System.out.println("Please enter zip code value");
			}
		
	}
	
	public static void sameOrg(String org) throws IOException, InterruptedException {
		
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");

		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("unCheckData");
		Iterator<Row> rowIterator = mySheet.iterator();

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(org)) {

				cell = (Cell) coliterator.next();
				String data1 = cell.getStringCellValue();
				String s13 = data1.trim();
				setInput(address, s13);

				cell = (Cell) coliterator.next();
				String cityvalue = cell.getStringCellValue();
				String citystring = cityvalue.trim();
				setInput(city, citystring);

				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String state2 = state2value.trim();
				Driver.selEleByVisbleText(state, state2);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String zipcode = Integer.toString(Exec1);
				String Exec_ind = zipcode.trim();
				setInput(zipCode, Exec_ind);

			}
		}
	}
	
	public void checkbox() throws InterruptedException {
		waitForElementToDisplay(uncheckSameOrg);
		clickElement(uncheckSameOrg);
	}
	
	public static String fetchPPOrg() {
		waitForElementToDisplay(organisation);
		getOrgName_PP=organisation.getAttribute("value");
		return getOrgName_PP;
		}
	
	public static String fetchPPsic() {
		waitForElementToDisplay(SIC);
	getSIC_PP=SIC.getAttribute("value");
	return getSIC_PP;
	}
	
	public static String fetchPPsic2() {
		waitForElementToDisplay(SIC2);
		getSIC2_PP=SIC2.getText();
		System.out.println(getSIC2_PP);
		return getSIC2_PP;
		}
	
	public static String fetchPPTaxID() {
		waitForElementToDisplay(taxId);
		gettaxID_PP=taxId.getAttribute("value");
		System.out.println(gettaxID_PP);
		return gettaxID_PP;
		}
	
	public static String fetchPPAddress_PP() {
		waitForElementToDisplay(address1);
		getaddress_PP=address1.getAttribute("value").trim();
		System.out.println(getaddress_PP);
		return getaddress_PP;
		}
	
	public static String fetchPPCity() {
		waitForElementToDisplay(city);
		getcity_PP=city.getAttribute("value");
		return getcity_PP;
		}
	
	public static String fetchPPZipCode() {
		waitForElementToDisplay(zipCode);
		getzipCode_PP=zipCode.getAttribute("value");
		return getzipCode_PP;
		}
	
	public static String fetchPPFirstName() {
		waitForElementToDisplay(firstName);
		getFirstName_PP=firstName.getAttribute("value");
		return getFirstName_PP;
		}
	
	public static String fetchPPPhoneNum() {
		waitForElementToDisplay(phone);
		getPhoneNum_PP=phone.getAttribute("value");
		return getPhoneNum_PP;
		}
	
	public static String fetchPPEmailId() {
		waitForElementToDisplay(email);
		getEmailId_PP=email.getAttribute("value");
		return getEmailId_PP;
		}
	
	
	public static String fetchPPDOC() {
		waitForElementToDisplay(dateOfCovg);
		getdateOfCovg_PP=dateOfCovg.getAttribute("value");
		return getdateOfCovg_PP;
		}
	
	public static String fetchPPStartDate() {
		waitForElementToDisplay(startdate);
		getstartdate_PP=startdate.getText().trim();
		System.out.println(getstartdate_PP);
		return getstartdate_PP;
		
		}
	
	public static String fetchPPEndDate() {
		waitForElementToDisplay(enddate);
		getenddate_PP=enddate.getAttribute("value");
		return getenddate_PP;
		}
	
	public static String fetchPPAvgEmp() {
		waitForElementToDisplay(avgEmployee);
		avgEmp_PP=avgEmployee.getAttribute("value");
		System.out.println(avgEmp_PP);
		return avgEmp_PP;
		}
	
	public static String fetchPPEligiEmp() {
		waitForElementToDisplay(eligiEmployee);
		eligiEmp_PP=eligiEmployee.getAttribute("value");
		return eligiEmp_PP;
		}
	public static String fetchPPEnrollEmp() {
		waitForElementToDisplay(EnrolledEmployee);
		enrollemp1_PP=EnrolledEmployee.getAttribute("value");
		return enrollemp1_PP;
		}
	public static String fetchPPWavier() {
		waitForElementToDisplay(ValidWai);
		validwavier1_PP=ValidWai.getAttribute("value");
		return validwavier1_PP;
		}
	public static String fetchPPCurrentCarrier() {
		waitForElementToDisplay(currentCarrier);
		currentcarrier1_PP=currentCarrier.getAttribute("value");
		return currentcarrier1_PP;
		}
	
	
	


public void verifyLogo() {
	if(driver.findElement(By.xpath("(//img)[1]"))!= null){

		System.out.println("Element is Present");

		}else{

		System.out.println("Element is Absent");

		}
}
}
