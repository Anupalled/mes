package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class ConsultantPortalLoginPage {

WebDriver driver;
@FindBy(id = "password") 
public static WebElement password;

@FindBy(xpath = "//button[@type='submit']")
public static WebElement submitButton;

@FindBy(xpath = "(//div[@class='side-action-item'])[2]")
public static WebElement affinityDashboard;


	
@FindBy(id = "username")
	public static WebElement email;
	
	public ConsultantPortalLoginPage(WebDriver driver) {
	this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void login(String username, String passwordTxt) throws InterruptedException {
		waitForElementToDisplay(email);
		setInput(email,username);
		setInput(password,passwordTxt);
		clickElement(submitButton);

	}
}
