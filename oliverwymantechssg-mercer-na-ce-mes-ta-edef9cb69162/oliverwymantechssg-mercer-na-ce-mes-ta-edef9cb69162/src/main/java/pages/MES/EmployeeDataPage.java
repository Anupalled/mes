package pages.MES;

import static driverfactory.Driver.*;
import static utilities.DatabaseUtils.getResultSet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;
import utilities.DatabaseUtils;
import utilities.ExcelReader;

public class EmployeeDataPage {
WebDriver driver;
	@FindBy(xpath = "//h1[text()=' Employee Data']")
	public static WebElement empHeader;

	@FindBy(xpath = "//input[@type='file']")
	public static WebElement file;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//button[@data-name='Confirm']")
	public static WebElement upload;
	
	@FindBy(xpath = "//div[contains(text(),'There are no HR Administrators indicated on this file. To add HR Administrators, make changes to the file and upload it again.')]")
	public static WebElement errorMsg;
	
	@FindBy(xpath = "//p[text()='Your file successfully uploaded and passed validation with no errors.']")
	public static WebElement successMsg;
	
	
	
	public EmployeeDataPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void uploadFile() throws InterruptedException {
		
		Thread.sleep(5000);
		String uploadfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\EmployeeDataTemplate (2).xlsx";
		file.sendKeys(uploadfile);
		clickElement(upload);
	
		
		
	}
public void uploadFileAffinity() throws InterruptedException {
		
		Thread.sleep(5000);
		String uploadfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\EmployeeDataTemplate (2).xlsx";
		file.sendKeys(uploadfile);
		clickElement(upload);
	
	}
	
	
public void uploadFile1() throws InterruptedException {
		Thread.sleep(5000);
		waitForElementToDisplay(successMsg);
		waitForElementToDisplay(continueButton);
		Thread.sleep(7000);
		clickElementUsingJavaScript(driver,continueButton);
		
	}
	
	
	
public void uploadFile_ahp() throws InterruptedException {
		
		clickElement(continueButton);
		Thread.sleep(5000); 
		waitForElementToDisplay(empHeader);
		String uploadfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\EmployeeDataTemplate (2).xlsx";
		file.sendKeys(uploadfile);
		clickElement(upload);
		
	}

public void uploadInvalidFile() throws InterruptedException {
	
	clickElement(continueButton);
	Thread.sleep(10000);
	waitForElementToDisplay(empHeader);
	String uploadfile = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\EmployeeDataTemplate_Invalid.xlsx";
	file.sendKeys(uploadfile);
	clickElement(upload);
	waitForElementToDisplay(errorMsg);
	
}

	
	/*public static void setOrgNameAffinity(String Index) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/EmployeeDataTemplate.xlsx");

		XSSFWorkbook myWorkBook1 = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook1.getSheet("profiledataAffinity");
		Iterator<Row> rowIterator = mySheet.iterator();
		
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			
			// For each row, iterate through all the columns
			java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			
			if (s23.equals(Index)) {
				
				cell = (Cell) coliterator.next();
				String sic1value = cell.getStringCellValue();
				String sic1 = sic1value.trim();
				Driver.selEleByVisbleText(SIC, sic1);
				
				
				clickElement(SIC2);
				cell = (Cell) coliterator.next();
				String sic2value = cell.getStringCellValue();
				String sic22 = sic2value.trim();
				Driver.selEleByVisbleText(SIC2, sic22);

				cell = (Cell) coliterator.next();
				int Exec = (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String taxid = s1s.trim();
				setInput(taxId, taxid);

				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String getaddress = ss1.trim();
				setInput(address1, getaddress);

				cell = (Cell) coliterator.next();
				String cityvalue = cell.getStringCellValue();
				String getCityName1 = cityvalue.trim();
				setInput(city, getCityName1);

				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String getstate = state2value.trim();
				Driver.selEleByVisbleText(state, getstate);

				cell = (Cell) coliterator.next();
				int Exec1 = (int) cell.getNumericCellValue();
				String sss = Integer.toString(Exec1);
				String  getzipCode1= sss.trim();
				setInput(zipCode, getzipCode1);
				
			
			}

		}
	}*/

public void empDataAuthDB() throws ClassNotFoundException, SQLException, InterruptedException {
	
	
	
	Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver","jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
	ResultSet myres = getResultSet(mycon,"select * from comp_ssn where company_id=’39125'");
	System.out.println(myres);
	//ResultSet res2=getResultSet(mycon,"Select * from employee_eff_date  where transaction_id='63fd4873'");
	//System.out.println(res2);
}
}
