package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

public class RequiredDocumentPage {
	WebDriver driver;
	@FindBy(xpath = "//input[@type='file']")
	public static WebElement file;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//h1[text()=' Required Documents']")
	public static WebElement requiredDocHeader;
	
	@FindBy(xpath = "//button[@data-name='Confirm']")
	public static WebElement uploadButton;
	
	@FindBy(xpath = "//button[@data-name='Upload More']")
	public static WebElement uploadMore;
	
	@FindBy(xpath = "(//button[@type='button'])[1]")
	public static WebElement downloadButton;
	
	@FindBy(xpath = "//button[text()='Yes']")
	public static WebElement okButton;
	
	@FindBy(xpath = "(//div[@class='info'])[1]")
	public static WebElement template1;
	
	
	@FindBy(xpath = "(//div[@class='info'])[2]")
	public static WebElement template2;
	
	@FindBy(xpath = "(//div[@class='info'])[3]")
	public static WebElement template3;
	
	@FindBy(xpath = "(//div[@class='info'])[4]")
	public static WebElement template4;
	
	@FindBy(xpath = "(//div[@class='info'])[5]")
	public static WebElement template5;
	
	@FindBy(xpath = "(//button[@class='mos-c-button--sm mos-t-button--info mos-c-button ng-star-inserted'])[1]")
	public static WebElement templateDownload1;
	
	@FindBy(xpath = "(//button[@class='mos-c-button--sm mos-t-button--info mos-c-button ng-star-inserted'])[2]")
	public static WebElement templateDownload2;
	
	@FindBy(xpath = "(//button[@class='mos-c-button--sm mos-t-button--info mos-c-button ng-star-inserted'])[3]")
	public static WebElement templateDownload3;
	
	@FindBy(xpath = "(//button[@class='mos-c-button--sm mos-t-button--info mos-c-button ng-star-inserted'])[4]")
	public static WebElement templateDownload4;
	
	
	public RequiredDocumentPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void uploadFile() throws InterruptedException {
	
		String uploadfile1 = System.getProperty("user.dir") + "\\src\\main\\resources\\WindowScreens\\Desert_200x80.jpg";
		file.sendKeys(uploadfile1);
		waitForElementToDisplay(uploadButton);
		Thread.sleep(3000);
		clickElement(uploadButton);
		
		
		Thread.sleep(5000);
		clickElement(continueButton);
		okButton.click();
		Thread.sleep(5000);
	
		
	}
	
	public void downloadFiles() throws InterruptedException, FindFailed {
	
		for(int i=1;i<5;i++) {
		WebElement e=driver.findElement(By.xpath("(//button[@type='button'])["+i+"]"));
		clickElement(e);
		Screen s=new Screen();
		String file1=System.getProperty("user.dir") + "\\src\\main\\resources\\WindowScreens\\Savebuuton.PNG";
    	s.find(file1);
    	s.click(file1);
	}
	}
}
