package pages.MES;

import static driverfactory.Driver.*;
import static driverfactory.Driver.clickElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUpPage {
	WebDriver driver;
	@FindBy(xpath = "//h4[text()='Medical']")
	public static WebElement medicalTitle;
			
	@FindBy(xpath = "(//div[@class='benefit-details'])[1]")
	public static WebElement MedibenefitDetails;	
	
	@FindBy(xpath = "//h4[text()='Dental']")
	public static WebElement dentalTitle;
	
	@FindBy(xpath = "(//div[@class='benefit-details'])[2]")
	public static WebElement dentalbenefitDetails;
	
	@FindBy(xpath = "//h4[text()='Vision']")
	public static WebElement visionTitle;
	
	@FindBy(xpath = "(//div[@class='benefit-details'])[3]")
	public static WebElement visionbenefitDetails;
	
	@FindBy(xpath = "//h4[text()='Life']")
	public static WebElement lifeTitle;
	
	@FindBy(xpath = "(//div[@class='benefit-details'])[4]")
	public static WebElement lifebenefitDetails;
	
	@FindBy(xpath = "//button[text()[normalize-space()='Checkout']]")
	public static WebElement checkout;
	
	@FindBy(xpath = "//button[contains(string(),' Continue')]")
	public static WebElement continueButton;
	
	
	@FindBy(xpath = "//a[text()='Sign Up']")
	public static WebElement signup;
	
	@FindBy(xpath = "//h1[text()=' Sign Up']")
	public static WebElement signupHeader;
	
	@FindBy(xpath = "//h4[text()='Medical']")
	public static WebElement medicalPlan;
	
	@FindBy(xpath = "(//button[@data-name='edit'])[1]")
	public static WebElement editButton;
	
	
	@FindBy(xpath = "//h1[text()=' Quote Review & Plan Selection']")
	public static WebElement planSelectionTitle;
	
	            
	
			
	public SignUpPage(WebDriver driver) {
				this.driver=driver;
				PageFactory.initElements(driver, this);
			}
	
	
	public void getPlanDetails() throws InterruptedException {
		waitForElementToDisplay(medicalTitle);
		String mediben=MedibenefitDetails.getText();
		System.out.println(mediben);
		
		waitForElementToDisplay(dentalTitle);
		String dentalben=dentalbenefitDetails.getText();
		System.out.println(dentalben);
		
		waitForElementToDisplay(visionTitle);
		String visionben=visionbenefitDetails.getText();
		System.out.println(mediben);
		
		waitForElementToDisplay(lifeTitle);
		String lifeben=lifebenefitDetails.getText();
		System.out.println(lifeben);
		
		clickElement(signup);
		Thread.sleep(20000);
		clickElement(checkout);
		Thread.sleep(20000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
		
		
	}
	
	public void getMedicaPlanDetails() throws InterruptedException {
		waitForElementToDisplay(medicalTitle);
		String mediben=MedibenefitDetails.getText();
		System.out.println(mediben);
		clickElement(checkout);
		Thread.sleep(20000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
		
		
	}
	
	public void planDetailsAffinity() throws InterruptedException {
	
		String mediben=MedibenefitDetails.getText();
		System.out.println(mediben);
		waitForElementToDisplay(editButton);
		clickElement(editButton);
		Thread.sleep(3000);
		waitForElementToDisplay(planSelectionTitle);
		clickElement(continueButton);
		Thread.sleep(20000);
		clickElement(checkout);
		Thread.sleep(30000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
		
	}
	
}
