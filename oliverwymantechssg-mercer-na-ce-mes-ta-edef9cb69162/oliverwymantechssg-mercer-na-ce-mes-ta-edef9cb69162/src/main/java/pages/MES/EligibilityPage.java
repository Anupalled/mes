package pages.MES;

import static driverfactory.Driver.*;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;

public class EligibilityPage {
	WebDriver driver;
	
	@FindBy(xpath = "//h1[text()=' Eligibility']")
	public static WebElement eligibilityHeader;
	
	@FindBy(xpath = "//label[@class='choice-option choice-no']")
	public static WebElement choiceNoOption;
			
	@FindBy(xpath = "(//div[@data-name='group selection'])[1]")
	public static WebElement groupSelection;
	
	@FindBy(xpath = "//select[@data-name='eligibility-modal-dropdown']") 
	public static WebElement createGroup;
	
	@FindBy(xpath = "//select[@data-name='first criteria']")  
	public static WebElement optStatus;		
			
	@FindBy(xpath = "//div[@class='mos-c-multi-select__toggle']")
	public static WebElement selectToggle;	
			
	@FindBy(xpath = "//label[text()[normalize-space()='Full-Time']]")
	public static WebElement fullTimeCheckbox;		
			 
	@FindBy(xpath = "//select[@data-name='second criteria']")
	public static WebElement secondCriteria;	
	
	@FindBy(xpath = "//input[@data-name='group name']")
	public static WebElement groupName;	
	
	@FindBy(xpath = "//button[@data-name='save']")
	public static WebElement saveButton;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//h3[text()='Who should be eligible for this plan?']")
	public static WebElement planHeader;
	
	public static String grpname;
	
		
	public EligibilityPage(WebDriver driver) {
				this.driver=driver;
				PageFactory.initElements(driver, this);
			}
	
	public void createGroups() throws InterruptedException {
		
		clickElement(choiceNoOption);
		clickElement(groupSelection);
		waitForElementToDisplay(planHeader);
		selEleByVisbleText(createGroup, "Create New Group");
		selEleByVisbleText(optStatus, "Status");
		clickElement(selectToggle);
		clickElement(fullTimeCheckbox);
		selEleByVisbleText(secondCriteria, "I'm Done");
		getAlphaNumericString();
		Thread.sleep(10000);
		clickElement(saveButton);
		Thread.sleep(10000);
		clickElement(saveButton);
		Thread.sleep(10000);
		clickElement(continueButton);
		Thread.sleep(10000);
		
	}
	
	public static void  getAlphaNumericString() throws InterruptedException 
    { 
  
		 grpname =RandomStringUtils.randomAlphabetic(3).toLowerCase();
		System.out.println(grpname);
		    setInput(groupName, grpname);
        
    }
	
	public void eligibility() throws InterruptedException {
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
	}
	
}
