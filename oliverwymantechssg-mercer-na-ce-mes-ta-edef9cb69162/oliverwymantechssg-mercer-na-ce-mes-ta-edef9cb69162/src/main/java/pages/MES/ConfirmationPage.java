package pages.MES;

import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import static driverfactory.Driver.scrollToElement;

import driverfactory.Driver;

public class ConfirmationPage {
	WebDriver driver;
	@FindBy(xpath = "//h1[text()=' Terms and conditions']")
	public static WebElement termsConditionHeader;
	
	@FindBy(xpath = "//strong[text()='I agree']")
	public static WebElement acceptButton;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//button[@data-name='I Confirm and Want to Proceed']")
	public static WebElement confirm;
	
	@FindBy(xpath = "//h3[text()='Congratulations!']")
	public static WebElement congratsMsg;
	
	@FindBy(xpath = "//h3[text()='Your setup has been finalized and submitted.']")
	public static WebElement submittedMsg;
	
	

	
	
	public ConfirmationPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void acceptTermsCondition() throws InterruptedException {
		Thread.sleep(5000);
		scrollToElement(driver,acceptButton);
		Thread.sleep(5000);
		acceptButton.click();
		clickElementUsingJavaScript(driver,continueButton);
		Thread.sleep(3000);
		clickElementUsingJavaScript(driver,confirm);
		waitForPageLoad(driver);
		waitForElementToDisplay(congratsMsg);
		waitForElementToDisplay(submittedMsg);
	}
	
	
}
