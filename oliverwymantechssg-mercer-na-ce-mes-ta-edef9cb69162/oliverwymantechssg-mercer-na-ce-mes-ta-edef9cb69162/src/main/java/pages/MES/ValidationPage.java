package pages.MES;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class ValidationPage {
WebDriver driver;
	
	@FindBy(xpath = "//h1[text()=' Validation']")
	public static WebElement validationHeader ;
	
	@FindBy(xpath = "//button[text()='Access Testing Portal']")
	public static WebElement testingPortal ;
	
	
	@FindBy(xpath = "//input[@type='text']")
	public static WebElement testEmployee ;
	
	@FindBy(xpath = "//a[text()='Validation']")
	public static WebElement validationTab ;
	
	@FindBy(xpath = "//a[text()='Quote Review']")
	public static WebElement quoteReviewTab ;
	
	@FindBy(xpath = "(//label[@data-name='plan'])[1]//strong")
	public static WebElement visionplan1 ;
	
	@FindBy(xpath = "(//label[@data-name='plan'])[2]//strong")
	public static WebElement visionplan2 ;
	
	
	@FindBy(xpath = "//div[@data-name='VISION']")
	public static WebElement visionTab;
			
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//button[@data-name='Save Selected']")
	public static WebElement saveSelected;
	
	@FindBy(xpath = "//button[@data-name='I’m Done Making Changes']")
	public static WebElement changesDone;
	
	
	
	
	
	public static String company_id;
	public static String signup_url;
	
	
	public ValidationPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void proxyUser() throws InterruptedException {
		
		clickElement(QuoteReviewPage.completeSetUp);
		
	}
	public void getURL() throws InterruptedException {
		signup_url=driver.getCurrentUrl();
		System.out.println(signup_url);
		company_id=signup_url.substring(57, 62);
		System.out.println("company id is"+company_id);
		Thread.sleep(3000);
		
	
	}
	
	public void resetHandling() throws InterruptedException {
		Thread.sleep(3000);
		waitForElementToDisplay(quoteReviewTab);
		clickElement(quoteReviewTab);
		
		waitForElementToDisplay(visionTab);
		clickElement(visionTab);
		
		clickElement(visionplan1);
		clickElement(visionplan2);
		
		clickElement(saveSelected);
		
		Thread.sleep(6000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
		Thread.sleep(6000);
		
	}
	
	public void clickDoneButton() throws InterruptedException {
		clickElement(changesDone);
	}
	
}
