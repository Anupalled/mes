package pages.MES;

import static driverfactory.Driver.*;
import static utilities.DatabaseUtils.getResultSet;
import static pages.MES.ProfilePage1.value;
import static pages.MES.ValidationPage.company_id;

import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import org.apache.bcel.generic.Select;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.DatabaseUtils;
import utilities.ExcelReader;

public class AssociationPage {
	static WebDriver driver;
	String currentURL=null;
	public static String name;
	@FindBy(xpath = "//a[text()='16']")
	public static WebElement pagination;
	
	@FindBy(xpath = "//a[text()='ABC Association']")
	public static WebElement abc_assoc;
	
	@FindBy(xpath = "//a[text()='!HSA-GK']")
	public static WebElement assoc;
	
	@FindBy(xpath = "//a[text()='25']")
	public static WebElement page25;
	
	@FindBy(xpath = "//a[text()='23']")
	public static WebElement page23;
	
	@FindBy(xpath = "//a[text()='21']")
	public static WebElement page20;
	
	//navigate to page 18-sb
	@FindBy(xpath = "//a[text()='27']")
	public static WebElement page27;
	
	@FindBy(xpath = "//a[text()='21']")
	public static WebElement page21;
	
	@FindBy(xpath = "//a[text()='19']")
	public static WebElement page19;
	
	@FindBy(xpath = "//a[text()='»']")
	public static WebElement lastPage;
	
	@FindBy(xpath = "//a[text()=\"QA's UHC Association\"]")
	public static WebElement assocNew;
	
	@FindBy(xpath = "//a[text()='!Samrin-UHC setup']")
	public static WebElement ActiveOffsystem;
	
	@FindBy(xpath = "//a[text()=\"QA's Medica Association\"]")
	public static WebElement medicaAssoc;
	
	@FindBy(xpath = "//a[text()=\"!!!QA NAM (Headhunters)!!!!\"]")
	public static WebElement qaHeadHunter;
	
	@FindBy(xpath = "(//div[@class='side-action-item'])[2]")
	public static WebElement affinityDashboard;
	
	
	@FindBy(xpath = "//a[text()=\".000Medica.-QA\"]")
	public static WebElement medica;
	
	@FindBy(xpath = "//h1[text()='MY DASHBOARD']")
	public static WebElement myDashboard;
	
	@FindBy(xpath = "//a[text()='View All Employees']")
	public static WebElement viewEmp;
	
	@FindBy(xpath = "//a[text()='Shyaam']/../..//a[@class='evo-icon-site-view-coverage ng-scope']")
	public static WebElement lastName;
	
	
	
	
	@FindBy(xpath = "//a[text()='Member Companies']")
	public static WebElement memebercompy;
	
	@FindBy(xpath = "//input[@data-e2e-name='searchInput']")
	public static WebElement searchInput;
	
	@FindBy(xpath = "//a[@data-e2e-name='searchButton']")
	public static WebElement searchButton;
	
	@FindBy(xpath = "(//a[@data-e2e-name='company name'])[1]")
	public static WebElement companyName;
	
	@FindBy(xpath = "(//a[text()='Profile'])[2]")
	public static WebElement profile1;
	
	@FindBy(xpath = "//h1[text()='Association Details']")
	public static WebElement assocDetails;
	
	@FindBy(xpath = "//h1[text()='associations']")
	public static WebElement associationHeader;
	
	
	@FindBy(xpath = "//i[@class='evo-icon-site-pen']")
	public static WebElement editIcon;
	
	@FindBy(xpath = "(//button[@class='ams_btn'])[2]")
	public static WebElement industrycategory;
	
	@FindBy(xpath = "(//button[@class='ams_btn'])[3]")
	public static WebElement industrysubcategory;
	
	@FindBy(xpath = "//select[@name='associationOffering']//option[text()='Both']")
	public static WebElement offeringType_Both;
	
	@FindBy(xpath = "//th[text()='AHP Supported States']")
	public static WebElement AHPSupported;
	
	@FindBy(xpath = "//th[text()='Affinity Supported States']")
	public static WebElement AffinitySupported;
	
	@FindBy(xpath = "//select[@name='associationOffering']")
	public static WebElement associationOffering;
	
	@FindBy(xpath = "//select[@name='associationOffering']//option[text()='Affinity only']")
	public static WebElement offeringType_Affinityonly;
	
	@FindBy(xpath = "//select[@name='associationOffering']//option[text()='AHP only']")
	public static WebElement offeringType_AHPonly;
	
	@FindBy(xpath = "//a[text()='Save']")
	public static WebElement saveButton;
	
	@FindBy(xpath = "//h3[text()='Manage Client']")
	public static WebElement manageClient;
	
	@FindBy(xpath = "//a[text()='Edit Account Structure']")
	public static WebElement editAccStru;
	
	@FindBy(xpath = "//h1[contains(text(),'Account Structure')]")
	public static WebElement AccountStruTitle;
	
	@FindBy(xpath = "(//input[@placeholder='Please provide value'])[1]")
	public static WebElement customerNum;
	
	@FindBy(xpath = "(//input[@placeholder='Please provide value'])[2]")
	public static WebElement policyNum;
	
	@FindBy(xpath = "//a[text()='Return To Dashboard']")
	public static WebElement returnDashboard;
	
	@FindBy(xpath = "//button[@data-name='cancel']")
	public static WebElement cancel;
	
	@FindBy(xpath = "//button[@data-name='Save']")
	public static WebElement save;
	
	
	@FindBy(xpath = "(//div[@class='action ng-scope'])[2]//a")
	public static WebElement mercerOps;
	
	@FindBy(xpath = "(//div[@class='action ng-scope'])[1]//a")
	public static WebElement mercerOps1;
	
	@FindBy(xpath = "//h1[text()='Employees']")
	public static WebElement employeeHeader;
	
	@FindBy(xpath = "//h3[text()='Active (Off System)']")
	public static WebElement activeoffSystemTitle;
	
	
	@FindBy(xpath = "//span[text()='Active (Off System)']")
	public static WebElement activeoffSystemStatus;
	
	@FindBy(xpath = "//i[@data-tooltip='In Renewal']")
	public static WebElement activeoffSystemIcon;
	
	
	@FindBy(xpath = "//h1[text()=' Affinity 365+ Dashboard ']")
	public static WebElement affinityDashboardTitle;
	
	
	@FindBy(xpath = "//a[text()=' Employers ']")
	public static WebElement employerMenu;
	
	@FindBy(xpath = "//a[text()='Active Employers']")
	public static WebElement activeEmp;
	
	@FindBy(xpath = "//a[text()='Renewal Employers']")
	public static WebElement renewalemp;
	
	@FindBy(xpath = "//h1[text()=' Active Employers ']")
	public static WebElement activeEmpTitle;
	
	@FindBy(xpath = "//mercer-icon[@icon='rotate_90_degrees_ccw']")
	public static WebElement icon;
	
	@FindBy(xpath = "//div[@class='mos-c-card__content mos-t-card__content-hover--undefined']/mercer-card-content")
	public static WebElement activeQAHeadHunterText;
	
	@FindBy(xpath = "//h1[text()=' Renewal Employers ']")
	public static WebElement renewalEmpTitle;
	
	@FindBy(xpath = "//div[@class='mos-c-card__content mos-t-card__content-hover--undefined']/mercer-card-content")
	public static WebElement renewalQAHeadHunterText;
	
	
	@FindBy(xpath = "//div[@class='row-status']")
	public static List<WebElement> getAssociationName;
	
	
	

	
	
	public AssociationPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public static void selectAssociation(WebElement e) throws InterruptedException {
		Thread.sleep(5000);
		scrollToElement(driver,e);
		e.click();
		//clickElement(e);
		
	}
	public static void searchCompany(String companyname) throws IOException, InterruptedException {
	
					setInput(searchInput, companyname);
					clickElement(searchButton);
				//	waitForPageLoad();
					Thread.sleep(7000);
					System.out.println(companyName);
					clickElement(companyName);
		
	}
	public static void searchMembers(String memberCompany) throws InterruptedException {
		setInput(searchInput, memberCompany);
		clickElement(searchButton);
	}
	
	
	public void fetchListOfIndustryCategory() throws InterruptedException {
		clickElement(editIcon);
		
		clickElement(industrycategory);
		
		List<WebElement> industrycategory=driver.findElements(By.xpath("//div[@class='ams_layer']//div[@class='ams_item ng-scope disabled ams_selected']//div"));
		int count_sic1=industrycategory.size();
		 System.out.println(count_sic1);
		for (WebElement webElement : industrycategory) {
			   String name = webElement.getText();
	            System.out.println("industryCategory_SIC1 :"+name);
		 }
		/*Thread.sleep(3000);
		industrysubcategory.click();
		Thread.sleep(8000);
		List<WebElement> industrysubcategory=driver.findElements(By.xpath("//div[@class='ams_layer']//div[@class='ams_item ng-scope disabled ams_selected']//div"));
		int count_sic2=industrysubcategory.size();
		 System.out.println(count_sic2);
		for (WebElement webElement : industrysubcategory) {
			 String name = webElement.getText();
	            System.out.println("industrySubCategory_SIC2 :"+name);
		 }
		*/
	}
	
	public void offeringType() throws InterruptedException {
		
		clickElement(associationOffering);
		clickElement(offeringType_Affinityonly);
		
		if(offeringType_Affinityonly.isEnabled()) {
			
			waitForElementToDisplay(AffinitySupported);
			System.out.println("pass");
		}
		
		
		clickElement(associationOffering);
		clickElement(offeringType_AHPonly);
		
		if(offeringType_AHPonly.isEnabled()) {
			
			waitForElementToDisplay(AHPSupported);
			System.out.println("pass");
		}
		
		
		clickElement(associationOffering);
		clickElement(offeringType_Both);
		
		if(offeringType_Both.isEnabled())
		{
			waitForElementToDisplay(AHPSupported);
			waitForElementToDisplay(AffinitySupported);
			System.out.println("pass");
			
		}
		
	}
	 /*public void offeringType_AHP() throws InterruptedException {
		 clickElement(associationOffering);
			clickElement(offeringType_AHPonly);
			
			if(offeringType_AHPonly.isEnabled()) {
				
				waitForElementToDisplay(AHPSupported);
				System.out.println("pass");
			}
			 clickElement(saveButton);
			
	 }
	 
	 public void offeringType_Affinity() throws InterruptedException {
		 clickElement(associationOffering);
		 clickElement(offeringType_Affinityonly);
			
			if(offeringType_Affinityonly.isEnabled()) {
				
				waitForElementToDisplay(AffinitySupported);
				System.out.println("pass");
			}
			 clickElement(saveButton);
			 Thread.sleep(7000);
			
	 }
	*/
	public void vaildateOfferingType() throws ClassNotFoundException, SQLException {
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
		ResultSet myres = getResultSet(mycon, "select  company_id,MC_ASSOC_OFFERING from company_detail where company_id='39356'");
		while (myres.next()) {
			for (int i = 1; i <= 2; i++) {
				String t = myres.getString(i);
				System.out.println(t);
			}
		}
	}
	
	public void pagination_assoc() throws InterruptedException {
		 Thread.sleep(5000);
		clickElement(lastPage);
		clickElement(page27);
		clickElement(page25);
		clickElement(page23);
		clickElement(page21);
		clickElement(page19);
				
		
	}
	public void viewAllEmp() throws InterruptedException
	{
		Thread.sleep(10000);
		waitForElementToDisplay(viewEmp);
		clickElement(viewEmp);
		waitForElementToDisplay(employeeHeader);
		clickElement(lastName);
		switchToWindowByTitle("Working...- Google Chrome",driver);
		switchToWindowWithURL("https://qaf-member.mercermarketplace365plus.com/dashboard",driver);
		System.out.println("working");
		
	}
	
	public void activeStateQAHeadhunter() throws InterruptedException
	{
		
		switchToWindowByTitle("Working...- Google Chrome",driver);
		switchToWindowWithURL("https://dashboard-qa.merceraffinity.com/dashboard",driver);
		System.out.println("working");
		waitForElementToDisplay(affinityDashboardTitle);
		clickElement(employerMenu);
		clickElement(activeEmp);
		waitForElementToDisplay(activeEmpTitle);
		
	}
	
	public void renewalStateQAHeadhunter() throws InterruptedException
	{
		
		
		clickElement(employerMenu);
		clickElement(renewalemp);
		waitForElementToDisplay(renewalEmpTitle);
		
	}
	
	
	
	public void clickMOP_AccountStru() throws InterruptedException {
		Thread.sleep(5000);
		clickElement(mercerOps1);
	}
	public void clickEyeIcon() {
		WebElement ele=driver.findElement(By.xpath("//i[@class='evo-icon-site-view-coverage']"));
		ele.click();
		
		System.out.println(driver.getWindowHandles().size());
		
	/*	String url1="https://hr-qaf.mercermarketplace365plus.com/HrPortal/#/hr/";
		String url2="/2021/promotion/dashboard";
		String del_url=url1.concat(company_id).concat(url2);*/
		switchToWindowWithURL("https://hr-qaf.mercermarketplace365plus.com/HrPortal#/hr/",driver);
	}
	
	public void AccountStru() throws InterruptedException {
		switchToWindowByTitle("Working...- Google Chrome",driver);
		switchToWindowByTitle("Mercer Associations - Google Chrome",driver);
		waitForElementToDisplay(manageClient);
		clickElement(editAccStru);
		waitForElementToDisplay(AccountStruTitle);
		Thread.sleep(5000);
		setInput(customerNum, "abed23");
		/*for(int i=1;i<11;i++) {
			WebElement poliynum=driver.findElement(By.xpath("(//input[@placeholder='Please provide value'])["+i+"]"));
			setInput(poliynum, "12345678");
		}*/
		WebElement poliynum=driver.findElement(By.xpath("(//input[@placeholder='Please provide value'])[2]"));
		setInput(poliynum, "12345678");
		Thread.sleep(5000);
		clickElement(save);
	
	}
	public void activerOffSystemIcon() {
		boolean icon=activeoffSystemIcon.isDisplayed();
		if(icon==true)
		{
			System.out.println("symbol exists");
		}
		else {
			System.out.println("symbol not exists");
		}
	}
	
	public void fetchUser_ID() throws ClassNotFoundException, SQLException {
		Statement smt;
		int id;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA15", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
		smt = mycon.createStatement();
		ResultSet rs=smt.executeQuery("select field_source from CARRIER_CLIENT_CONFIG_DATA where company_id = '62953' "
				+ "and field_id ='7' and plan_id in (select Plan_id from ASSOCIATION_CARRIER_PLAN_MAPPING where External_plan_id = '$500 Deductible 75% PPO-(Medica Choice PP)') "
				+ "order by update_logdate desc");
		while (rs.next()) {
			
				
				System.out.println(rs.getString(1));
			
}
	}
	
	public void fetchUser() throws ClassNotFoundException, SQLException {
		Statement s;
		
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//usfkl23db39v-sca.mrshmc.com:1530/BWQA15", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
		s = mycon.createStatement();
		ResultSet rs=s.executeQuery("select field_source from CARRIER_CLIENT_CONFIG_DATA where company_id = '63088' and Field_id= '4' and Benefit_id='MEDICAL' order by update_logdate desc");
	
		while(rs.next()) {
			System.out.println(rs.getString(1));
			//27762
		}
	}
	
	public void assocName_allsavers() throws InterruptedException {
		int count=getAssociationName.size();
		int flag=0;
		for(int j=1;j<=19;j++) {
		    for(int i=1;i<count;i++) {
		    	
		    if (getAssociationName.get(i).getText().equals("QA's UHC Association")){
		    	System.out.println(getAssociationName.get(i).getText());
		    	WebElement ele=driver.findElement(By.xpath("//a[contains(text(),'UHC Association')]"));
		    	ele.click();
		    	flag=1;
		    	break;
		    
		    }
		    else {
		    	 WebElement ele=driver.findElement(By.xpath("//a[text()='"+j+"']"));
			   ele.click();
			   
		    }
		    }if(flag==1) break;
		    
		    }
			
}
	
	public void assocName_medica() throws InterruptedException {
		int count=getAssociationName.size();
		int flag=0;
		for(int j=1;j<=19;j++) {
		    for(int i=1;i<count;i++) {
		    	
		    if (getAssociationName.get(i).getText().equals("QA's Medica Association")){
		    	System.out.println(getAssociationName.get(i).getText());
		    	WebElement ele=driver.findElement(By.xpath("//a[contains(text(),'Medica Association')]"));
		    	ele.click();
		    	flag=1;
		    	break;
		    
		    }
		    else {
		    	 WebElement ele=driver.findElement(By.xpath("//a[text()='"+j+"']"));
			   ele.click();
			   
		    }
		    }if(flag==1) break;
		    
		    }
			
}
	public void assocName_Ahp() throws InterruptedException {
		int count=getAssociationName.size();
		int flag=0;
		for(int j=1;j<=23;j++) {
			System.out.println("page no "+ j);		
		    for(int i=1;i<count;i++) {

		    if (getAssociationName.get(i).getText().equals("QA's UHC Association")){
		    	System.out.println(getAssociationName.get(i).getText());
		    	WebElement ele=driver.findElement(By.xpath("//a[contains(text(),'UHC Association')]"));
		    	ele.click();
		    	flag=1;
		    	break;
		    
		    }
		    else {
		    	 WebElement ele=driver.findElement(By.xpath("//a[text()='"+j+"']"));
			   ele.click();
			   
		    }
		    } if (flag==1) break;
		    
		    }
			
}
	public void waitForPageLoad() {
		System.out.println("Waiting for page to load..");		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
		System.out.println("Page Loaded..");
	}
}
	

