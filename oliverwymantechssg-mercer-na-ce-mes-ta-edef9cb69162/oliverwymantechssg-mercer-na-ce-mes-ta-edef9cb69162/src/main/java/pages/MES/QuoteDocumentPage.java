package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QuoteDocumentPage {
	WebDriver driver;
	@FindBy(xpath = "//h1[text()=' Quote Documents']")
	public static WebElement quoteDoc;
	
	@FindBy(xpath = "(//div[@class='info'])[1]")
	public static WebElement medicaAHP;
	
	@FindBy(xpath = "(//div[@class='info'])[2]")
	public static WebElement currentSummary;
	
	@FindBy(xpath = "(//div[@class='info'])[3]")
	public static WebElement claimData;
	
	@FindBy(xpath = "(//div[@class='info'])[4]")
	public static WebElement insuredRates;
	
	
	@FindBy(xpath = "//button[@class='mos-c-button--sm mos-t-button--info mos-c-button ng-star-inserted']")
	public static WebElement downloadTemplate;
	
	@FindBy(xpath = "//input[@type='file']")
	public static WebElement fileUpload;
	
	@FindBy(xpath = "//button[@data-name='Confirm']")
	public static WebElement uploadButton;
	
	@FindBy(xpath = "//div[contains(text(),'You have successfully uploaded 1 files!')]")
	public static WebElement confirmationMsg;
	
	@FindBy(xpath = "//button[text()='Yes']")
	public static WebElement yesbutton;
	
	@FindBy(xpath = "//a[text()='SampeUpload.zip']")
	public static WebElement sampleuploadfile;
	
	public static String getFileName;
	
	public QuoteDocumentPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public static String fetchFilename() {
		getFileName=sampleuploadfile.getText();
		System.out.println(getFileName);
		return getFileName;
	}
	
	public void uploadFile() throws InterruptedException {
		Thread.sleep(5000);
		String uploadfile1 = System.getProperty("user.dir") + "\\src\\main\\resources\\testdata\\\\SampeUpload.zip";
		fileUpload.sendKeys(uploadfile1);
		Thread.sleep(3000);
		clickElement(uploadButton);
	}
	
	public static void downloadFile(String path,WebElement ele) throws InterruptedException {
		
		clickElement(ele);
  	 
  	
  	  Thread.sleep(7000);
  	try {
  	  setClipboardData(path);
	      
  	  Robot robot = new Robot();
	      
	      robot.keyPress(KeyEvent.VK_CONTROL);
	      robot.keyPress(KeyEvent.VK_L);
	      Thread.sleep(2000);
	      robot.keyRelease(KeyEvent.VK_L);
	      robot.keyRelease(KeyEvent.VK_CONTROL);
	      Thread.sleep(2000);
	      
	      robot.keyPress(KeyEvent.VK_CONTROL);
	      robot.keyPress(KeyEvent.VK_V);
	      Thread.sleep(2000);
	      robot.keyRelease(KeyEvent.VK_V);
	      robot.keyRelease(KeyEvent.VK_CONTROL);
	      Thread.sleep(4000);
	     
	      robot.keyPress(KeyEvent.VK_ENTER);
	      robot.keyRelease(KeyEvent.VK_ENTER);
	      Thread.sleep(4000);
	      
	      robot.keyPress(KeyEvent.VK_ENTER);
	      robot.keyRelease(KeyEvent.VK_ENTER);
	      Thread.sleep(4000);
	      
	      robot.keyPress(KeyEvent.VK_ENTER);
	      robot.keyRelease(KeyEvent.VK_ENTER);
	      Thread.sleep(4000);
	      
	      robot.keyPress(KeyEvent.VK_ENTER);
	      robot.keyRelease(KeyEvent.VK_ENTER);
	      Thread.sleep(4000);
	     

	      robot.keyPress(KeyEvent.VK_ENTER);
	      robot.keyRelease(KeyEvent.VK_ENTER);

	      
	  } catch (Exception exp) {
	       exp.printStackTrace();
	  }

	}

public static void setClipboardData(String string) {
	//StringSelection is a class that can be used for copy and paste operations.
	StringSelection stringSelection = new StringSelection(string);
	Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
	}

public static void deleteFilesInPath(String downloadPath) {
	

    File dir = new File(downloadPath);
    File[] dirContents = dir.listFiles();

    for (int i = 0; i < dirContents.length; i++) {

          dirContents[i].delete();

    }

}

public static boolean isFileDownloaded(String Path, String fileName) {
    File dir = new File(Path);
    File[] dirContents = dir.listFiles();

    for (int i = 0; i < dirContents.length; i++) {
          
        if (dirContents[i].getName().contains(fileName)) {
            // File has been found, it can now be deleted:
          System.out.println("File found "+dirContents[i].getName());
        System.out.println("Downloaded File name--- >> "+dirContents[i].getName());
        
            return true;
        }
            }
        return false;
    }

}
