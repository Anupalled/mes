package pages.MES;

import static driverfactory.Driver.*;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QuoteReviewPage {
	WebDriver driver;
public static String ACOContent;
public static String NOPContent;
	@FindBy(xpath = "//a[text()='Show Me My Quote']")
	public static WebElement myQuotes;

	@FindBy(xpath = "//a[text()='Complete Your Set Up']")
	public static WebElement completeSetUp;

	@FindBy(xpath = "//h1[text()=' Quote Review & Plan Selection']")
	public static WebElement planSelectionTitle;

	@FindBy(xpath = "//div[@data-name='DENTAL']")
    public static WebElement dentalTab;

	
//	@FindBy(xpath = "(//div[@class='nav-benefit-item '])[1]")
//  public static WebElement dentalTab;

	@FindBy(xpath = "(//label[@data-name='plan'])[1]//strong")
	public static WebElement optPlans;

	@FindBy(xpath = "//button[@data-name='Save Selected']")
	public static WebElement saveSelected;

	@FindBy(xpath = "//div[@data-name='VISION']")
	public static WebElement visionTab;

	@FindBy(xpath = "//div[@data-name='BLIFE']")
	public static WebElement lifeTab;

	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//a[@data-name = 'Download PDF Principal Proposal']")
	public static WebElement PrintableProposalQRPDF;
	
	@FindBy(xpath = "//a[@data-name = 'Download Principal Proposal']")
	public static WebElement RatesSheetQRPDF;
	
	@FindBy(xpath = "//h1[text()=' Sign Up']")
	public static WebElement signUpTitle;

	@FindBy(xpath = "//li[@data-id='Signup']")
	public static WebElement signUp;

	@FindBy(xpath = "//a[text()='Sign Up']")
	public static WebElement signup;

	@FindBy(xpath = "//div[@data-name='MEDICAL']")
	public static WebElement medicalTab;

	@FindBy(xpath = "(//label[@data-name='plan'])[1]//strong")
	public static WebElement selectPlan;

	@FindBy(xpath = "//button[@data-name='Review Request']")
	public static WebElement reviewButton;

	@FindBy(xpath = "//h4[@data-name='request-popup']")
	public static WebElement reviewRequestHeader;

	@FindBy(xpath = "//a[text()[normalize-space()='View Rate Sheet']]")
	public static WebElement rateButton;

	@FindBy(xpath = "//h4[text()='Core PPO HDHP Bronze']")
	public static WebElement planHeader;

	@FindBy(xpath = "//button[@class='mos-c-modal__close']")
	public static WebElement closeButton;

	@FindBy(xpath = "(//button[@data-name='expandPlan'])[1]")
	public static WebElement expandPlan1;

	@FindBy(xpath = "(//mercer-icon[@class='mos-c-table--skip-click shrink ng-star-inserted'])[1]")
	public static WebElement expandEmp1;

	@FindBy(xpath = "(//mercer-icon[@class='mos-c-table--skip-click shrink ng-star-inserted'])[2]")
	public static WebElement expandEmp2;

	@FindBy(xpath = "(//tr[@class='mos-c-tree-table__row--link ng-star-inserted'])[1]//td[2]")
	public static WebElement fetchEmpAge;

	@FindBy(xpath = "(//tr[@class='mos-c-tree-table__row--link ng-star-inserted'])[2]//td[2]")
	public static WebElement fetchSpouseAge;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[2]")
	public static int ageBand2;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[3]")
	public static int ageBand3;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[4]")
	public static int ageBand4;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[5]")
	public static int ageBand5;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[6]")
	public static int ageBand6;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[7]")
	public static int ageBand7;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[8]")
	public static int ageBand8;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[9]")
	public static int ageBand9;

	@FindBy(xpath = "(//td[@data-mercer-table-th='ageBand'])[10]")
	public static int ageBand10;

	@FindBy(xpath = "//button[@class='mos-c-modal__close']")
	public static WebElement closeRatesInfo;
	
	@FindBy(xpath = "((//table[@class='mos-c-table mos-u-table--bordered mos-u-table--dense ng-star-inserted'])[1]//td[3])[1]")
	public static WebElement costOfCovg;
	
	@FindBy(xpath = "(//button[@data-name='expandPlan'])[1]")
	public static WebElement expandPlan;
	
	@FindBy(xpath = "//div[text()='Your final quote is ready for review!']")
	public static WebElement FinalQuote;
	
	@FindBy(xpath = "//a[text()='Learn more about the assumptions rates below are based on.']")
	public static WebElement assumptionRateLink;
	
	@FindBy(xpath = "//div[@class='assumptions-content']//ul//li[1]")
	public static WebElement assumptionRateDescp;
	
	@FindBy(xpath = "//button[@class='mos-c-modal__close']//mercer-icon")
	public static WebElement closeAssumptionButton;
	
	@FindBy(xpath = "//div[text()='This quote is valid for 120 days from the date you requested a quote.']")
	public static WebElement quoteRequestText;
	
	@FindBy(xpath = "//a[text()='Click here for more information on available Medica providers.']")
	public static WebElement DoctorsLink;
	
	
	@FindBy(xpath = "//div[text()='Minnesota State Bar Association']")
	public static WebElement DoctorsLinkHeader;
	
	@FindBy(xpath = "//h5[text()='Learn More About Accountable Care Organizations or ACO']")
	public static WebElement learnACOTitle;
	
	@FindBy(xpath = "//h5[text()='Learn More About Limitation to the Number of Plans']")
	public static WebElement LearnNOPTitle;
	
	@FindBy(xpath = "(//button[@class='mos-c-icon-button--xxlg mos-c-icon-button'])[2]")
	public static WebElement arrowDownButton1;
	
	@FindBy(xpath = "(//button[@class='mos-c-icon-button--xxlg mos-c-icon-button'])[3]")
	public static WebElement arrowDownButton2;
	
	@FindBy(xpath = "(//div[@class='info-box-content']//p)[1]")
	public static WebElement LearnACOContent;
	
	@FindBy(xpath = "(//div[@class='info-box-content'])[2]//p")
	public static WebElement LearnNOPContent;
	
	
	@FindBy(xpath = "(//mercer-icon[@class='mos-c-tooltip--host'])[1]")
	public static WebElement compareToolTip;
	
	@FindBy(xpath = "//button[@data-name='Compare Selected' and @disabled='false']")
	public static WebElement compareEnabled;
	
	@FindBy(xpath = "//button[@data-name='Compare Selected' and @disabled='true']")
	public static WebElement compareDisabled;
	
	@FindBy(xpath = "(//mercer-icon[@ng-reflect-icon='keyboard_arrow_down'])[3]")
	public static WebElement expandArrow;
	
	@FindBy(xpath = "//a[text()='Learn more about the assumptions rates below are based on.']")
	public static WebElement learnAssumption;
	
	@FindBy(xpath = "(//div[@class='assumptions-content']//li)[1]")
	public static WebElement learnAssumptionContent;
	
	@FindBy(xpath = "//div[@class='mos-c-alert__content']")
	public static WebElement content90days;
	
	@FindBy(xpath = "//button[@data-name='Review Preliminary Quote Request']")
	public static WebElement preliminaryButton;
	
	@FindBy(xpath = "//h4[text()='Preliminary Quote']")
	public static WebElement preliminaryQuoteTitle;
	
	@FindBy(xpath = "//div[@class='mos-c-modal__close-wrapper--circle ng-tns-c30-7 ng-star-inserted']")
	public static WebElement closePreliminary;
	
	@FindBy(xpath = "//div[@class='mos-u-footnote']")
	public static WebElement footerContent;
	
	
	
	
	
	
	public String ages = "";
	public String spouseage = "";
	public String expectedSpouseValue ="";
	public String expectedEmpValue="";
	public static String assumpContent;
	public static String quoteValid;

	public QuoteReviewPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void selectPlans() throws InterruptedException {
		clickElement(myQuotes);
		waitForElementToDisplay(medicalTab);
		Thread.sleep(10000);
		clickElement(medicalTab);
		waitForElementToDisplay(optPlans);
		clickElement(optPlans);
		clickElement(saveSelected);
		Thread.sleep(20000);
		waitForPageLoad(driver);
		Thread.sleep(20000);
		waitForElementToDisplay(dentalTab);
		
		clickElement(dentalTab);
		
		clickElement(optPlans);
		
		clickElement(saveSelected);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(visionTab);
	
		clickElement(visionTab);
	
		clickElement(optPlans);
		
		clickElement(saveSelected);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(lifeTab);
	
		clickElement(lifeTab);
		
		clickElement(optPlans);
		
		clickElement(saveSelected);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(signUpTitle);

	}
	
	public void selectPlans_AllSaver() throws InterruptedException {
		
		waitForElementToDisplay(medicalTab);
		Thread.sleep(10000);
		clickElement(medicalTab);
		waitForElementToDisplay(optPlans);
		clickElement(optPlans);
		clickElement(saveSelected);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(dentalTab);
		
		clickElement(dentalTab);
		
		clickElement(optPlans);
		
		clickElement(saveSelected);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(visionTab);
	
		clickElement(visionTab);
	
		clickElement(optPlans);
		
		clickElement(saveSelected);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(lifeTab);
	
		clickElement(lifeTab);
		
		clickElement(optPlans);
		
		clickElement(saveSelected);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(signUpTitle);

	}
	
	
	public void selectMedicaPlans() throws InterruptedException {
		
		waitForElementToDisplay(medicalTab);
		clickElement(medicalTab);
		clickElement(expandArrow);
		
		waitForElementToDisplay(optPlans);
		clickElement(optPlans);
		waitForPageLoad(driver);
		Thread.sleep(10000);
		waitForElementToDisplay(continueButton);
		clickElementUsingJavaScript(driver,continueButton);
	}
	public void myQuotes() throws InterruptedException {
		clickElement(myQuotes);
	}

	public void selectPlansAffinity_assoc() throws InterruptedException {
		Thread.sleep(10000);
		waitForElementToDisplay(medicalTab);
		
		clickElement(medicalTab);
		waitForElementToDisplay(selectPlan);
		clickElementUsingJavaScript(driver,selectPlan);
		Thread.sleep(10000);
		clickElementUsingJavaScript(driver,saveSelected);
		
		waitForPageLoad(driver);
		    Thread.sleep(30000);
			waitForElementToDisplay(dentalTab);
			Thread.sleep(10000);
			clickElement(dentalTab);
			Thread.sleep(10000);
			waitForElementToDisplay(optPlans);
			clickElementUsingJavaScript(driver,optPlans);
			Thread.sleep(10000);
			clickElementUsingJavaScript(driver,saveSelected);

		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(visionTab);
		clickElement(visionTab);
		waitForElementToDisplay(optPlans);
		clickElementUsingJavaScript(driver,optPlans);
		Thread.sleep(10000);
		clickElementUsingJavaScript(driver,saveSelected);

		waitForPageLoad(driver);
		Thread.sleep(30000);
		waitForElementToDisplay(lifeTab);
		clickElement(lifeTab);
		waitForElementToDisplay(optPlans);
		clickElementUsingJavaScript(driver,optPlans);
		Thread.sleep(10000);
		clickElementUsingJavaScript(driver,saveSelected);
		
		
		
		Thread.sleep(5000);
		waitForElementToDisplay(continueButton);
		clickElementUsingJavaScript(driver,continueButton);
		waitForElementToDisplay(signUpTitle);

	}
	public void selectPlansAffinity_hsagk() throws InterruptedException {
		

		Thread.sleep(5000);
		waitForElementToDisplay(visionTab);
		clickElement(visionTab);
		Thread.sleep(5000);
		waitForElementToDisplay(optPlans);
		Thread.sleep(5000);
		clickElementUsingJavaScript(driver,optPlans);
		clickElementUsingJavaScript(driver,saveSelected);

		Thread.sleep(5000);
		waitForElementToDisplay(lifeTab);
		clickElement(lifeTab);
		waitForElementToDisplay(optPlans);
		Thread.sleep(5000);
		clickElementUsingJavaScript(driver,optPlans);
		clickElementUsingJavaScript(driver,saveSelected);

		
		Thread.sleep(5000);
		waitForElementToDisplay(continueButton);
		clickElement(continueButton);
		waitForElementToDisplay(signUpTitle);

	}

	public void calculateEmpCovg() throws InterruptedException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy");
		LocalDateTime now = LocalDateTime.now();
		int currentyear = now.getYear();
		System.out.println(currentyear);
		waitForElementToDisplay(reviewButton);
		clickElement(reviewButton);
		waitForElementToDisplay(reviewRequestHeader);
		clickElement(expandEmp1);
		String age = fetchEmpAge.getText().substring(6);
		int EmpAge1 = Integer.parseInt(age);
		System.out.println(EmpAge1);
		clickElement(closeButton);

		int actualAge1 = currentyear - EmpAge1;

		ages = String.valueOf(actualAge1);
		System.out.println(ages);

	}

	@SuppressWarnings("unlikely-arg-type")
	public void fetchRates() throws InterruptedException {

		clickElement(rateButton);

		Thread.sleep(8000);

		WebElement table = driver.findElement(By.xpath(
				"(//table[@class='mos-c-table mos-t-table--accent2 mos-u-table--bordered mos-u-table--column-bordered mos-u-table--dense ng-star-inserted'])[1]"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> column = table.findElements(By.tagName("td"));

		List<String> value = new ArrayList<String>();

		int j = 0;
		int i = 1;
		while (i < 6) {
			for (j = 0; j < column.size(); j++) {
				System.out.println(column.get(j).getText());
				value.add(column.get(j).getText());

			}
			if (value.contains(ages)) {
				WebElement fetchValue = driver.findElement(By.xpath(" //td[text()=" + ages + "]/..//td[2]"));
				expectedEmpValue = fetchValue.getText().substring(1);
				System.out.println(expectedEmpValue);
				break;

			}

			else {
				Thread.sleep(3000);
				WebElement pagination = driver.findElement(By.xpath("(//li[@class='ng-star-inserted'])[" + i + "]//a"));
				clickElement(pagination);
				i++;

			}

		}
		clickElement(closeRatesInfo);

	}

	public void calculateSpouseCovg() throws InterruptedException {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy");
		LocalDateTime now = LocalDateTime.now();
		int currentyear = now.getYear();
		System.out.println(currentyear);

		clickElement(reviewButton);
		waitForElementToDisplay(reviewRequestHeader);
		clickElement(expandEmp1);
		String age = fetchSpouseAge.getText().substring(6);
		int EmpAge1 = Integer.parseInt(age);
		System.out.println(EmpAge1);
		clickElement(closeButton);

		int actualAge1 = currentyear - EmpAge1;

		spouseage = String.valueOf(actualAge1);
		System.out.println(spouseage);

	}

	public void fetchRates2() throws InterruptedException {

		clickElement(rateButton);

		Thread.sleep(15000);

		WebElement table = driver.findElement(By.xpath("(//table[@class='mos-c-table mos-t-table--accent2 mos-u-table--bordered mos-u-table--column-bordered mos-u-table--dense ng-star-inserted'])[1]"));
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		List<WebElement> column = table.findElements(By.tagName("td"));

		List<String> value = new ArrayList<String>();

		int j = 0;
		int i = 1;
		while (i < 6) {
			for (j = 0; j < column.size(); j++) {
				System.out.println(column.get(j).getText());
				value.add(column.get(j).getText());

			}
			if (value.contains(ages)) {
				WebElement fetchValue = driver.findElement(By.xpath(" //td[text()=" + spouseage + "]/..//td[2]"));
				expectedSpouseValue = fetchValue.getText().substring(1);
				System.out.println(expectedSpouseValue);
				break;

			}

			else {
				Thread.sleep(5000);
				WebElement pagination = driver.findElement(By.xpath("(//li[@class='ng-star-inserted'])[" + i + "]//a"));
				clickElement(pagination);
				i++;
				
			}

		}
		clickElement(closeRatesInfo);

	}
public void calVal() throws InterruptedException {
	
		float emp = Float.parseFloat(expectedEmpValue);
		float spouse = Float.parseFloat(expectedSpouseValue);
		float resultVal = emp + spouse;
		System.out.println(resultVal);
		
		clickElement(expandPlan);
		String covg=costOfCovg.getText().substring(1);
		String val=String.valueOf(resultVal);
		if(val.equals(covg)) {
			System.out.println(covg);
		}
		
		
		
}

public void clickbutton(WebElement e) throws InterruptedException {
	clickElement(e);
}

public void verifyLink() throws InterruptedException {
	clickElement(DoctorsLink);
	switchToWindowByTitle("Minnesota State Bar Association",driver);
	
}
public void dwnldPP() throws InterruptedException, AWTException {
	clickElement(PrintableProposalQRPDF);
	Robot robot = new Robot();
	 robot.keyPress(KeyEvent.VK_TAB);	
    Thread.sleep(2000);	
    robot.keyPress(KeyEvent.VK_TAB);	
    Thread.sleep(2000);	
    robot.keyPress(KeyEvent.VK_TAB);	
    Thread.sleep(2000);	
    robot.keyPress(KeyEvent.VK_ENTER);
    System.out.println("working");
//	switchToWindowByTitle("printableproposal",driver);
	
}
public void dwnldRS() throws InterruptedException, AWTException {
	clickElement(RatesSheetQRPDF);
	Robot robot = new Robot();
	 robot.keyPress(KeyEvent.VK_TAB);	
    Thread.sleep(2000);	
    robot.keyPress(KeyEvent.VK_TAB);	
    Thread.sleep(2000);	
    robot.keyPress(KeyEvent.VK_TAB);	
    Thread.sleep(2000);	
    robot.keyPress(KeyEvent.VK_ENTER);
    System.out.println("working");
//	switchToWindowByTitle("printableproposal",driver);
	
}




}
