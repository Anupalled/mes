package pages.MES;

import static driverfactory.Driver.clickElement;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PreliminaryQuotes {
	WebDriver driver;
	@FindBy(xpath = "//button[@class='mos-c-button--md mos-c-tooltip--host mos-t-button--info mos-c-button']")
	public static WebElement downloadButton;
	
	@FindBy(xpath = "//label[@data-name='Agree']")
	public static WebElement checkBox;
	
	@FindBy(xpath = "//a[@data-name = 'Download PDF Principal Proposal']")
	public static WebElement PrintableProposalQRPDF;
	
	@FindBy(xpath = "//a[@data-name = 'Download Principal Proposal']")
	public static WebElement RatesSheetQRPDF;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement saveButton;
	
	@FindBy(xpath = "//h1[text()=' Preliminary Quote Review']")
	public static WebElement preliminaryHeader;
	
	@FindBy(xpath = "(//label[@data-name='plan'])[1]")
	public static WebElement chkbox;
	
	@FindBy(xpath = "//strong[text()='AllSavers Plans']")
	public static WebElement allSaversTitle;
	
	@FindBy(xpath = "//strong[text()='Affinity Plans']")
	public static WebElement affinityPlanTitle;
	
	@FindBy(xpath = "//label[@data-id='AllSavers']")
	public static WebElement allSaverCheckBox;
	
	@FindBy(xpath = "//label[@data-id='Affinity']")
	public static WebElement affinityCheckBox;
	
	@FindBy(xpath = "//div[@class='mos-u-footnote']")
	public static WebElement footContent;
	
	@FindBy(xpath = "//div[@data-name='DENTAL']")
    public static WebElement dentalTab;


	@FindBy(xpath = "//div[@data-name='VISION']")
	public static WebElement visionTab;

	@FindBy(xpath = "//div[@data-name='BLIFE']")
	public static WebElement lifeTab;
	
	@FindBy(xpath = "//div[@data-name='MEDICAL']")
	public static WebElement medicalTab;
	
	public PreliminaryQuotes(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void reviewQuotes() throws InterruptedException {
		Thread.sleep(5000);
		clickElement(checkBox);
		clickElement(saveButton);
	}
	
	public void reviewQuotes_AllSavers() throws InterruptedException {
		/*Thread.sleep(5000);
		clickElement(dentalTab);
		Thread.sleep(5000);
		clickElement(visionTab);
		Thread.sleep(5000);
		clickElement(lifeTab);
		Thread.sleep(5000);
		clickElement(medicalTab);*/
		Thread.sleep(5000);
		clickElement(allSaverCheckBox);
		
		
	}
	public void dwnldPP() throws InterruptedException, AWTException {
		clickElement(PrintableProposalQRPDF);
		Robot robot = new Robot();
		 robot.keyPress(KeyEvent.VK_TAB);	
	    Thread.sleep(2000);	
	    robot.keyPress(KeyEvent.VK_TAB);	
	    Thread.sleep(2000);	
	    robot.keyPress(KeyEvent.VK_TAB);	
	    Thread.sleep(2000);	
	    robot.keyPress(KeyEvent.VK_ENTER);
	    System.out.println("working");
//		switchToWindowByTitle("printableproposal",driver);
		
	}
	public void dwnldRS() throws InterruptedException, AWTException {
		clickElement(RatesSheetQRPDF);
		Robot robot = new Robot();
		 robot.keyPress(KeyEvent.VK_TAB);	
	    Thread.sleep(2000);	
	    robot.keyPress(KeyEvent.VK_TAB);	
	    Thread.sleep(2000);	
	    robot.keyPress(KeyEvent.VK_TAB);	
	    Thread.sleep(2000);	
	    robot.keyPress(KeyEvent.VK_ENTER);
	    System.out.println("working");
//		switchToWindowByTitle("printableproposal",driver);
		
	}
	
	
}
