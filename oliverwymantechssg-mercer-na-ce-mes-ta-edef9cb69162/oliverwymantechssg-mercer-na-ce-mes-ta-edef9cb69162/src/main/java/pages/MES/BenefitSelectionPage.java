package pages.MES;

import static driverfactory.Driver.*;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.scrollToElement;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.ExcelReader;

public class BenefitSelectionPage {
	WebDriver driver;
	@FindBy(xpath = "//label[@for='DENTAL-on']")
	public static WebElement dental;
	
	@FindBy(xpath = "//label[@for='VISION-on']")
	public static WebElement vision;
	
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//h1[text()=' Benefit Selections']")
	public static WebElement benefitSelectionHeader;
	
	@FindBy(xpath = "(//input[contains(@class,'switch-input ng-untouched')]/following-sibling::label)[4]")
	public static WebElement dentalToggle_off;
	
	@FindBy(xpath = "//label[@for='VISION-off']")
	public static WebElement visionToggle_off;
	
	@FindBy(xpath = "//label[@for='BLIFE-off']")
	public static WebElement lifeToggle_off;
	
	@FindBy(xpath = "(//div[@class='section ng-star-inserted'])[2]")
	public static WebElement benefitfooter;
	
	@FindBy(xpath = "//h3[text()='Coming Soon']")
	public static WebElement comingSoonTitle;
	
	@FindBy(xpath = "//p[text()='Additional benefits coming soon. We will contact you as benefits are added.']")
	public static WebElement additionalBenefit;
	
	
	public BenefitSelectionPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public void setBenefits() throws InterruptedException {
		Thread.sleep(5000);
		scrollToElement(driver,continueButton);
		continueButton.click();
	}
	
	public void setBenefitsAffinity() throws InterruptedException {
	Thread.sleep(10000);
	//scrollToElement(driver,dentalToggle_off);
		//clickElementUsingJavaScript(driver,dentalToggle_off);
		clickElement(continueButton);
	}
}
