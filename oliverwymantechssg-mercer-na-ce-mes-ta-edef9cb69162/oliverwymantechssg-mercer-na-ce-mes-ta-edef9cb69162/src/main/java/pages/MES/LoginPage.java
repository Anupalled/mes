package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {
	WebDriver driver;
	@FindBy(xpath = "//input[@class='mas-form-control mas-value-lower ng-untouched ng-pristine ng-invalid']")
	public static WebElement email;
	
	@FindBy(id = "password") 
	public static WebElement password;
	
	@FindBy(xpath = "//button[@type='submit']")
	public static WebElement submitButton;
	
	
	@FindBy(xpath = "//input[@id='username']")
	public static WebElement username;
	
	//label[text()='Email sh*****@mmc.com ']
	
	//button[text()='Continue']
	
	public LoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void login(String username, String passwordTxt) throws InterruptedException {
		waitForElementToDisplay(email);
		setInput(email,username);
		setInput(password,passwordTxt);
		clickElement(submitButton);

	}
	public void re_login() throws InterruptedException{
		waitForElementToDisplay(email);
		email.sendKeys("palledananya+517@gmail.com");
		password.sendKeys("Welcome1@");
		clickElement(submitButton);
	}
	
	public void loginAHPCIT(String user, String passwordTxt) throws InterruptedException{
		waitForElementToDisplay(username);
		setInput(username,user);
		setInput(password,passwordTxt);
		clickElement(submitButton);
		System.out.println("Waiting for page to load...");
		waitForElementToDisappear(By.cssSelector("div[class='mas-loading-indicator']"));		
		
	}
	
	
}
