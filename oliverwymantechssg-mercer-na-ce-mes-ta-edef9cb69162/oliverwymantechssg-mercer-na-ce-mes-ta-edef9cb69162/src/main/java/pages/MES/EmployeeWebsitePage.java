package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EmployeeWebsitePage {
	
	WebDriver driver;
	
	@FindBy(xpath = "//label[@class='choice-option choice-yes']")
	public static WebElement selectOptionYes ;
	
	@FindBy(xpath = "//button[text()='Remove']")
	public static WebElement removeImg ;
	
	
	@FindBy(xpath = "//input[@type='file']")
	public static WebElement file;
	
	@FindBy(xpath = "//h1[text()=' Website Logo']")
	public static WebElement header;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	public EmployeeWebsitePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void uploadLogo() throws InterruptedException {
		/*waitForElementToDisplay(selectOptionYes);
		clickElement(selectOptionYes);
		String uploadfile1 = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" +
				File.separator + "WindowScreens" + File.separator + "Desert_200x80.jpg";
		file.sendKeys(uploadfile1);
		clickElement(removeImg);
		
		String uploadfile2 = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" +
				File.separator + "WindowScreens" + File.separator + "Desert_200x80.jpg";
		file.sendKeys(uploadfile2);*/
		clickElement(continueButton);
		
		
	}
	
	
}
