package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QuestionnariePage {
	WebDriver driver;
	@FindBy(xpath = "//h1[text()=' Questionnaire']")
	public static WebElement questionnaireTitle;
	
	@FindBy(xpath = "//label[@class='choice-option choice-yes']")
	public static WebElement choiceYes;
	
	@FindBy(xpath = "//span[text()='Medical']")
	public static WebElement medicalCheckbox;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//span[text()='Status']")
	public static WebElement statusCheckbox;
	
	@FindBy(xpath = "//a[text()='Worksheet']")
	public static WebElement worksheet;
	
	
	public QuestionnariePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void questionnarie() throws InterruptedException {
		waitForElementToDisplay(questionnaireTitle);
		clickElement(choiceYes);
		clickElement(medicalCheckbox);
		clickElement(continueButton);
		Thread.sleep(10000);
		clickElement(statusCheckbox);
		clickElement(continueButton);
		Thread.sleep(10000);
		/*clickElement(worksheet);
		System.out.println("Clicked on worskheet");*/
		
		
	}
	
}
