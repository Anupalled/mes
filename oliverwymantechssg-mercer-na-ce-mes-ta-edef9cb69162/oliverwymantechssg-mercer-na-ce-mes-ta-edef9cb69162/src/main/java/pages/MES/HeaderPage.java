package pages.MES;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeaderPage {
WebDriver driver;
	
	@FindBy(xpath = "//a[text()='Associations']")
	public static WebElement associationTab;
	
	@FindBy(xpath = "//h1[text()='My Dashboard']")
	public static WebElement dashboard;
	
	
	
	
	
	
	public HeaderPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectHeaders(WebElement e) throws InterruptedException {
		waitForElementToDisplay(dashboard);
		clickElement(e);
	}
}
