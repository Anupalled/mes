package pages.MES;

import static utilities.DatabaseUtils.getResultSet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.DatabaseUtils;


public class SelfRegPage {
	WebDriver driver;
	public static int id;
	public static String Company_id;
	@FindBy(xpath = "//h3[text()='Congratulations! Your set-up is now complete!']")
	public static WebElement setUpComplete;
	
	public SelfRegPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	public void selfRegisteration() throws ClassNotFoundException, SQLException {
		Statement stmt;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14", "MUSE_SMARKT", "testmuse_smarkttest");
		stmt = mycon.createStatement();
		ResultSet rs=stmt.executeQuery("insert into users (email,role)" + 
    		"values ('palledananya+555@gmail.com','E')") ;
	
}
	
	public void fetchUser_ID() throws ClassNotFoundException, SQLException {
		Statement stmt;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14", "MUSE_SMARKT", "testmuse_smarkttest");
		stmt = mycon.createStatement();
		ResultSet rs=stmt.executeQuery("select user_id from users where email = 'palledananya+555@gmail.com'") ;
		while (rs.next()) {
			
				 id = rs.getInt(1);
				System.out.println(id);
			
}
	}
	
	public void user_application() throws ClassNotFoundException, SQLException, InterruptedException {
		Statement stmt;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14", "MUSE_SMARKT", "testmuse_smarkttest");
		stmt = mycon.createStatement();
		ResultSet rs=stmt.executeQuery("insert 	into user_applications (user_id, application_id , role)" +
                "values ('"+id+"' , '365P' ,'Q') ") ;
		ResultSet rs1=stmt.executeQuery("insert into user_claim (user_id, claim, claim_value)" +
                "values('"+id+"', 'QuotingTool.User.AssociationId', 52386) ");
		
		//62291	-medica
		//52386-ahp
		Thread.sleep(5000);
		mycon.setAutoCommit(false);
		mycon.commit();
		
		
}
	
	public void getCompanyID() throws InterruptedException {
		Thread.sleep(5000);
		String url = driver.getCurrentUrl();
		System.out.println(url);
		 Company_id=url.substring(50,55);
		System.out.println(Company_id);
	}
	public void getCompanyIDs() throws InterruptedException {
		Thread.sleep(5000);
		String url = driver.getCurrentUrl();
		System.out.println(url);
		 Company_id=url.substring(50,55);
		System.out.println(Company_id);
	}
	

	public void authDelQuery() throws ClassNotFoundException, SQLException {
		
		
		Statement s;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA15", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
		s = mycon.createStatement();
		
		String sql1="DELETE FROM HP_RATES_STEPS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql2="DELETE FROM COMPANY_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql3="DELETE FROM ASSOCIATION_DETAILS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		
		String sql5="DELETE FROM COMPANY_BENEFIT_CONTROL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
	
		String sql7="DELETE FROM BKWIP_CLIENT_WIZARD_LOG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql8="DELETE FROM BKWIP_CLIENT_WIZARD WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		String sql11="DELETE FROM COMPANY_STATUS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql14="DELETE FROM ELIG_GROUP_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql15="DELETE FROM COMPANY_CARRIER_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql16="DELETE FROM COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		String sql17="DELETE FROM AMC_COST_REQUIREMENTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql18="DELETE FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql19="DELETE FROM COMPANY_PAYROLL_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql20="DELETE FROM EMPLOYEE_EFF_DATE WHERE SSN IN (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID = '"+Company_id+"')";
		
		String sql21="DELETE FROM COMPANY_PAYROLL_CONFIG_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql22="DELETE FROM HRI_FILE_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
		String sql23="DELETE FROM HRI_FILE_ERROR_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
		String sql24="DELETE FROM CONTACT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		String sql25="DELETE FROM CONTACT_ADDRESS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql26="DELETE FROM ASSOC_GROUPS_COMPANY_ANSWER WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql27="DELETE FROM ASSOC_COMPANY_FILE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql28="DELETE FROM ASSOC_QUOTE_EMPLOYEE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		String sql29="DELETE FROM ASSOCIATION_AFFINITY_COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql30="DELETE FROM ASSOCIATION_AFFINITY_COSTS_AGE_BAND WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		String sql31="DELETE FROM ASSOC_REQUESTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql32="DELETE FROM COST_FACTORS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql33="DELETE FROM COST_FACTOR_BENEFIT_VALUE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql34="DELETE FROM COMPANY_BENEFIT_LIFEEVENTS  WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql6="DELETE FROM COMPANY_PLAN_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql13="DELETE FROM ELIG_GROUP WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql36 ="DELETE FROM COBRA_CONFIG WHERE CONTROL_ID='SMLMKT' AND COMPANY_ID='"+Company_id+"'";
		String sql10="DELETE FROM COMPANY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql35 ="DELETE FROM COMP_SSN WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		int r1=s.executeUpdate(sql1);
		System.out.println(r1);
		int r2=s.executeUpdate(sql2); 
		System.out.println(r2);
		int r3=s.executeUpdate(sql3);
		System.out.println(r3);
		
		int r5=s.executeUpdate(sql5);
		System.out.println(r5);
		
		int r7=s.executeUpdate(sql7);
		System.out.println(r7);
		int r8=s.executeUpdate(sql8);
		System.out.println(r8);
		
		
		
		int r11=s.executeUpdate(sql11);
		System.out.println(r11);
		
		
		int r14=s.executeUpdate(sql14);
		System.out.println(r14);
		int r15=s.executeUpdate(sql15);
		System.out.println(r15);
		int r16=s.executeUpdate(sql16);
		System.out.println(r16);
		int r17=s.executeUpdate(sql17);
		System.out.println(r17);
		int r18=s.executeUpdate(sql18);
		System.out.println(r18);
		int r19=s.executeUpdate(sql19);
		System.out.println(r19);
		int r20=s.executeUpdate(sql20);
		System.out.println(r20);
		
		int r21=s.executeUpdate(sql21);
		System.out.println(r21);
		int r22=s.executeUpdate(sql22); 
		System.out.println(r22);
		int r23=s.executeUpdate(sql23);
		System.out.println(r23);
		int r24=s.executeUpdate(sql24);
		System.out.println(r24);
		int r25=s.executeUpdate(sql25);
		System.out.println(r25);
		int r26=s.executeUpdate(sql26);
		System.out.println(r26);
		int r27=s.executeUpdate(sql27);
		System.out.println(r27);
		int r28=s.executeUpdate(sql28);
		System.out.println(r28);
		int r29=s.executeUpdate(sql29);
		System.out.println(r29);
		int r30=s.executeUpdate(sql30);
		System.out.println(r30);
		
		int r31=s.executeUpdate(sql31);
		System.out.println(r31);
		int r32=s.executeUpdate(sql32);
		System.out.println(r32);
		int r33=s.executeUpdate(sql33);
		System.out.println(r33);
		int r34=s.executeUpdate(sql34);
		System.out.println(r34);
		int r6=s.executeUpdate(sql6);
		System.out.println(r6);
		int r13=s.executeUpdate(sql13);
		System.out.println(r13);
		int r36=s.executeUpdate(sql36);
		System.out.println(r36);
		int r10=s.executeUpdate(sql10);
		System.out.println(r10);
		int r35=s.executeUpdate(sql35);
		System.out.println(r35);
		mycon.setAutoCommit(false);
		mycon.commit();
		
	}
	
	
	
	public void authDelQuery12() throws ClassNotFoundException, SQLException {
		Statement s;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA15", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
		s = mycon.createStatement();
		
		String sql12="DELETE FROM QUOTE_EMPL_VALRESULT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		int r12=s.executeUpdate(sql12); 
		System.out.println(r12);
		mycon.setAutoCommit(false);
		mycon.commit();
		
	}
	public void deliveryDB() throws ClassNotFoundException, SQLException {
		Statement s;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14", "Z_MM_MT1_D", "MM_MT1_D_zqaFri2019");
		s = mycon.createStatement();
		//"+Company_id+"
		String sql1="DELETE FROM COMPANY_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql2="DELETE FROM COMPANY_BENEFIT_CONTROL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql9 ="DELETE FROM COMPANY_CARRIER_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql14 ="DELETE FROM COMPANY_PAYROLL_CONFIG_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql11="DELETE FROM COMPANY_PAYROLL_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'"; 
		String sql3="DELETE FROM COMPANY_PLAN_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql4="DELETE FROM BKWIP_CLIENT_WIZARD_LOG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql5 ="DELETE FROM BKWIP_CLIENT_WIZARD WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql7 ="DELETE FROM ELIG_GROUP WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql8 ="DELETE FROM ELIG_GROUP_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql6 ="DELETE FROM COMPANY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
		
		String sql10 ="DELETE FROM COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
		String sql12="DELETE FROM EMPLOYEE_EFF_DATE WHERE SSN IN (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID = '"+Company_id+"')";
		String sql13="DELETE FROM COMP_SSN WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		String sql15 ="DELETE FROM HRI_FILE_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
		String sql16 ="DELETE FROM HRI_FILE_ERROR_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
		String sql17 ="DELETE FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql18 ="DELETE FROM CONTACT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql19 ="DELETE FROM CONTACT_ADDRESS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql20 ="DELETE FROM ASSOCIATION_AFFINITY_COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql21 ="DELETE FROM COST_FACTORS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql22 ="DELETE FROM COST_FACTOR_BENEFIT_VALUE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql23 ="DELETE FROM COMPANY_BENEFIT_LIFEEVENTS  WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
		int r1=s.executeUpdate(sql1);
		System.out.println(r1);
		int r2=s.executeUpdate(sql2); 
		System.out.println(r2);
		int r9=s.executeUpdate(sql9);
		System.out.println(r9);
		int r14=s.executeUpdate(sql14);
		System.out.println(r14);
		int r11=s.executeUpdate(sql11);
		System.out.println(r11);
		int r3=s.executeUpdate(sql3);
		System.out.println(r3);
		int r4=s.executeUpdate(sql4);
		System.out.println(r4);
		int r5=s.executeUpdate(sql5);
		System.out.println(r5);
		int r7=s.executeUpdate(sql7);
		System.out.println(r7);
		int r8=s.executeUpdate(sql8);
		System.out.println(r8);
		int r6=s.executeUpdate(sql6);
		System.out.println(r6);
		
		
		int r10=s.executeUpdate(sql10);
		System.out.println(r10);
		
		
		int r12=s.executeUpdate(sql12);
		System.out.println(r12);
		int r13=s.executeUpdate(sql13);
		System.out.println(r13);
		
		int r15=s.executeUpdate(sql15);
		System.out.println(r15);
		int r16=s.executeUpdate(sql16);
		System.out.println(r16);
		int r17=s.executeUpdate(sql17);
		System.out.println(r17);
		int r18=s.executeUpdate(sql18);
		System.out.println(r18);
		int r19=s.executeUpdate(sql19);
		System.out.println(r19);
		int r20=s.executeUpdate(sql20);
		System.out.println(r20);
		
		int r21=s.executeUpdate(sql21);
		System.out.println(r21);
		int r22=s.executeUpdate(sql22); 
		System.out.println(r22);
		int r23=s.executeUpdate(sql23);
		System.out.println(r23);
		
		
		mycon.setAutoCommit(false);
		mycon.commit();
		
		
	}
	
public void authDelQuery_affinity() throws ClassNotFoundException, SQLException {
		
		
		Statement s;
		java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
				"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA15", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
		s = mycon.createStatement();
		
		String sql1="DELETE FROM HP_RATES_STEPS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql2="DELETE FROM COMPANY_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql3="DELETE FROM ASSOCIATION_DETAILS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql5="DELETE FROM COMPANY_BENEFIT_CONTROL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql7="DELETE FROM BKWIP_CLIENT_WIZARD_LOG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql8="DELETE FROM BKWIP_CLIENT_WIZARD WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql14="DELETE FROM ELIG_GROUP_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql15="DELETE FROM COMPANY_CARRIER_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql16="DELETE FROM COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql17="DELETE FROM AMC_COST_REQUIREMENTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql18="DELETE FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql19="DELETE FROM COMPANY_PAYROLL_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql20="DELETE FROM EMPLOYEE_EFF_DATE WHERE SSN IN (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID = '"+Company_id+"')";
		String sql21="DELETE FROM COMPANY_PAYROLL_CONFIG_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql22="DELETE FROM HRI_FILE_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
		String sql23="DELETE FROM HRI_FILE_ERROR_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
		String sql24="DELETE FROM CONTACT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql25="DELETE FROM CONTACT_ADDRESS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql26="DELETE FROM ASSOC_GROUPS_COMPANY_ANSWER WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql27="DELETE FROM ASSOC_COMPANY_FILE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql28="DELETE FROM ASSOC_QUOTE_EMPLOYEE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql29="DELETE FROM ASSOCIATION_AFFINITY_COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql30="DELETE FROM ASSOCIATION_AFFINITY_COSTS_AGE_BAND WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql31="DELETE FROM ASSOC_REQUESTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql32="DELETE FROM COST_FACTORS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql33="DELETE FROM COST_FACTOR_BENEFIT_VALUE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql34="DELETE FROM COMPANY_BENEFIT_LIFEEVENTS  WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql6="DELETE FROM COMPANY_PLAN_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql13="DELETE FROM ELIG_GROUP WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql11="DELETE FROM COMPANY_STATUS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql36 ="DELETE FROM ASSOC_COMPANY_OFFERING WHERE CONTROL_ID='SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql10="DELETE FROM COMPANY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		String sql35 ="DELETE FROM COMP_SSN WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
		
				
		
		int r1=s.executeUpdate(sql1);
		System.out.println(r1);
		int r2=s.executeUpdate(sql2); 
		System.out.println(r2);
		int r3=s.executeUpdate(sql3);
		System.out.println(r3);
		
		int r5=s.executeUpdate(sql5);
		System.out.println(r5);
		
		int r7=s.executeUpdate(sql7);
		System.out.println(r7);
		int r8=s.executeUpdate(sql8);
		System.out.println(r8);
		
		
		
		
		
		int r14=s.executeUpdate(sql14);
		System.out.println(r14);
		int r15=s.executeUpdate(sql15);
		System.out.println(r15);
		int r16=s.executeUpdate(sql16);
		System.out.println(r16);
		int r17=s.executeUpdate(sql17);
		System.out.println(r17);
		int r18=s.executeUpdate(sql18);
		System.out.println(r18);
		int r19=s.executeUpdate(sql19);
		System.out.println(r19);
		int r20=s.executeUpdate(sql20);
		System.out.println(r20);
		
		int r21=s.executeUpdate(sql21);
		System.out.println(r21);
		int r22=s.executeUpdate(sql22); 
		System.out.println(r22);
		int r23=s.executeUpdate(sql23);
		System.out.println(r23);
		int r24=s.executeUpdate(sql24);
		System.out.println(r24);
		int r25=s.executeUpdate(sql25);
		System.out.println(r25);
		int r26=s.executeUpdate(sql26);
		System.out.println(r26);
		int r27=s.executeUpdate(sql27);
		System.out.println(r27);
		int r28=s.executeUpdate(sql28);
		System.out.println(r28);
		int r29=s.executeUpdate(sql29);
		System.out.println(r29);
		int r30=s.executeUpdate(sql30);
		System.out.println(r30);
		
		int r31=s.executeUpdate(sql31);
		System.out.println(r31);
		int r32=s.executeUpdate(sql32);
		System.out.println(r32);
		int r33=s.executeUpdate(sql33);
		System.out.println(r33);
		int r34=s.executeUpdate(sql34);
		System.out.println(r34);
		int r6=s.executeUpdate(sql6);
		System.out.println(r6);
		int r13=s.executeUpdate(sql13);
		System.out.println(r13);
		int r11=s.executeUpdate(sql11);
		System.out.println(r11);
		int r36=s.executeUpdate(sql36);
		System.out.println(r36);
		int r10=s.executeUpdate(sql10);
		System.out.println(r10);
		int r35=s.executeUpdate(sql35);
		System.out.println(r35);
		mycon.setAutoCommit(false);
		mycon.commit();
		
		
		//"+Company_id+"
		
	}


public void deliveryDBOLD_affinity() throws ClassNotFoundException, SQLException {
	Statement s;
	java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
			"jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14", "Z_MM_MT1_D", "MM_MT1_D_zqaFri2019");
	s = mycon.createStatement();
	//"+Company_id+"

	String sql1="DELETE FROM COMPANY_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql2="DELETE FROM COMPANY_BENEFIT_CONTROL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql14 ="DELETE FROM COMPANY_PAYROLL_CONFIG_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql9 ="DELETE FROM COMPANY_CARRIER_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql20 ="DELETE FROM ASSOCIATION_AFFINITY_COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql7="DELETE FROM ELIG_GROUP WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'"; 
	String sql11 ="DELETE FROM COMPANY_PAYROLL_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql3="DELETE FROM COMPANY_PLAN_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	String sql4 ="DELETE FROM BKWIP_CLIENT_WIZARD_LOG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	String sql5 ="DELETE FROM BKWIP_CLIENT_WIZARD WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql25 ="DELETE COMP_COMMS_ACTIVE_TEMPLATES  WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID ='"+Company_id+"'";
	
	String sql6 ="DELETE FROM COMPANY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	String sql8="DELETE FROM ELIG_GROUP_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	String sql10="DELETE FROM COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	
	String sql12 ="DELETE FROM EMPLOYEE_EFF_DATE WHERE SSN IN (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID = '"+Company_id+"')";
	String sql13 ="DELETE FROM COMP_SSN WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	
	String sql15="DELETE FROM HRI_FILE_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
	String sql16 ="DELETE FROM HRI_FILE_ERROR_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
	String sql17 ="DELETE FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql18 ="DELETE FROM CONTACT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql19 ="DELETE FROM CONTACT_ADDRESS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	

	
	String sql22 ="DELETE FROM COST_FACTORS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql23 ="DELETE FROM COST_FACTOR_BENEFIT_VALUE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String sql24 ="DELETE FROM COMPANY_BENEFIT_LIFEEVENTS  WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	
	int r1=s.executeUpdate(sql1);
	System.out.println(r1);
	int r2=s.executeUpdate(sql2); 
	System.out.println(r2);
	int r14=s.executeUpdate(sql14);
	System.out.println(r14);
	int r9=s.executeUpdate(sql9);
	System.out.println(r9);
	int r20=s.executeUpdate(sql20); 
	System.out.println(r20);
	int r7=s.executeUpdate(sql7);
	System.out.println(r7);
	int r11=s.executeUpdate(sql11);
	System.out.println(r11);
	int r3=s.executeUpdate(sql3);
	System.out.println(r3);
	int r4=s.executeUpdate(sql4);
	System.out.println(r4);
	int r5=s.executeUpdate(sql5);
	System.out.println(r5);
	
	int r25=s.executeUpdate(sql25);
	System.out.println(r25);
	
	int r6=s.executeUpdate(sql6);
	System.out.println(r6);
	
	int r8=s.executeUpdate(sql8);
	System.out.println(r8);
	
	int r10=s.executeUpdate(sql10);
	System.out.println(r10);
	
	int r12=s.executeUpdate(sql12);
	System.out.println(r12);
	int r13=s.executeUpdate(sql13);
	System.out.println(r13);
	
	int r15=s.executeUpdate(sql15);
	System.out.println(r15);
	int r16=s.executeUpdate(sql16);
	System.out.println(r16);
	int r17=s.executeUpdate(sql17);
	System.out.println(r17);
	int r18=s.executeUpdate(sql18);
	System.out.println(r18);
	int r19=s.executeUpdate(sql19);
	System.out.println(r19);
	
	int r22=s.executeUpdate(sql22); 
	System.out.println(r22);
	int r23=s.executeUpdate(sql23);
	System.out.println(r23);
	int r24=s.executeUpdate(sql24);
	System.out.println(r24);
	
	mycon.setAutoCommit(false);
	mycon.commit();
	
	
}
public void musedeletion() throws ClassNotFoundException, SQLException {
	Statement s;
	java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
			"jdbc:oracle:thin:@//usfkl23db39v-sca.mrshmc.com:1530/BWQA14", "MUSE_SMARKT", "testmuse_smarkttest");
	s = mycon.createStatement();
/*	String sql1="delete from user_claim where user_id='47170' and claim = 'BrokerPortal.User.Clients'";
	String sql2="delete from user_claim where user_id='47170' and claim = 'QuotingTool.User.CompanyId'";*/
	
	String sql3 ="delete from User_claim where claim in ('BrokerPortal.User.Clients','QuotingTool.User.CompanyId') and user_id = '47170'";
	
	int r3=s.executeUpdate(sql3);
	System.out.println(r3);
	
	//userid552-47170
	//userid553-47208
	//554-47248
	mycon.setAutoCommit(false);
	mycon.commit();

	
	}

public void museMedicadeletion() throws ClassNotFoundException, SQLException {
	Statement s;
	java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
			"jdbc:oracle:thin:@//usfkl23db39v-sca.mrshmc.com:1530/BWQA14", "MUSE_SMARKT", "testmuse_smarkttest");
	s = mycon.createStatement();
/*	String sql1="delete from user_claim where user_id='47170' and claim = 'BrokerPortal.User.Clients'";
	String sql2="delete from user_claim where user_id='47170' and claim = 'QuotingTool.User.CompanyId'";*/
	
	String sql3 ="delete from User_claim where claim in ('BrokerPortal.User.Clients','QuotingTool.User.CompanyId') and user_id = '52468'";
	
	int r3=s.executeUpdate(sql3);
	System.out.println(r3);
	
	//userid552-47170
	//userid553-47208
	//554-47248
	mycon.setAutoCommit(false);
	mycon.commit();

	
	}
public void authQuery() throws ClassNotFoundException, SQLException {
	Statement s;
	java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
			
			"jdbc:oracle:thin:@//usfkl23db39v-sca.mrshmc.com:1530/BWQA15", "Z_MM_MT1_A", "MM_MT1_A_zqaFri2019");
	s = mycon.createStatement();
	
String s1="DELETE FROM COMPANY_STATUS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s2="DELETE FROM QUOTE_EMPL_VALRESULT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s3="DELETE FROM AMC_COST_REQUIREMENTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s4="DELETE FROM ASSOC_GROUPS_COMPANY_ANSWER WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s5="DELETE FROM ASSOC_COMPANY_FILE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s6="DELETE FROM ASSOC_QUOTE_EMPLOYEE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s7="DELETE FROM ASSOC_REQUESTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s8="DELETE FROM ELIG_FACTOR_COMP_NICKNAMES WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s9="DELETE FROM HP_RATES_STEPS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s10="DELETE FROM COMPANY_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s11="DELETE FROM COMPANY_BENEFIT_CONTROL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s12="DELETE FROM COMPANY_PAYROLL_CONFIG_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s13="DELETE FROM COMPANY_CARRIER_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s14="DELETE FROM ASSOCIATION_AFFINITY_COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s15="DELETE FROM ASSOCIATION_AFFINITY_COSTS_AGE_BAND WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s22="DELETE FROM ELIG_GROUP_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s16="DELETE FROM ELIG_GROUP WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s17="DELETE FROM COMPANY_PAYROLL_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s18="DELETE FROM COMPANY_PLAN_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s19="DELETE FROM BKWIP_CLIENT_WIZARD_LOG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s20="DELETE FROM BKWIP_CLIENT_WIZARD WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s23="DELETE FROM COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s24="DELETE FROM EMPLOYEE_EFF_DATE WHERE SSN IN (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID = '"+Company_id+"')";
String s25="DELETE FROM COMP_SSN WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s26="DELETE FROM HRI_FILE_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
String s27="DELETE FROM HRI_FILE_ERROR_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
String s28="DELETE FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s29="DELETE FROM CONTACT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s30="DELETE FROM CONTACT_ADDRESS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s32="DELETE FROM COST_FACTOR_BENEFIT_VALUE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s31="DELETE FROM COST_FACTORS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s33="DELETE FROM COMPANY_BENEFIT_LIFEEVENTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s34="DELETE FROM COBRA_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
String s21="DELETE FROM COMPANY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";


	int r1=s.executeUpdate(s1);
	System.out.println(r1);
	int r2=s.executeUpdate(s2); 
	System.out.println(r2);
	int r3=s.executeUpdate(s3);
	System.out.println(r3);
	
	int r4=s.executeUpdate(s4);
	System.out.println(r4);
	int r5=s.executeUpdate(s5); 
	System.out.println(r5);
	int r6=s.executeUpdate(s6);
	System.out.println(r6);
	int r7=s.executeUpdate(s7);
	System.out.println(r7);
	int r8=s.executeUpdate(s8);
	System.out.println(r8);
	int r9=s.executeUpdate(s9);
	System.out.println(r9);
	int r10=s.executeUpdate(s10);
	System.out.println(r10);
	
	int r11=s.executeUpdate(s11);
	System.out.println(r11);
	
	int r12=s.executeUpdate(s12);
	System.out.println(r12);
	
	int r13=s.executeUpdate(s13);
	System.out.println(r13);
	
	int r14=s.executeUpdate(s14);
	System.out.println(r14);
	
	int r15=s.executeUpdate(s15);
	System.out.println(r15);
	int r22=s.executeUpdate(s22); 
	System.out.println(r22);
	int r16=s.executeUpdate(s16);
	System.out.println(r16);
	int r17=s.executeUpdate(s17);
	System.out.println(r17);
	int r18=s.executeUpdate(s18);
	System.out.println(r18);
	int r19=s.executeUpdate(s19);
	System.out.println(r19);
	int r20=s.executeUpdate(s20);
	System.out.println(r20);
	int r23=s.executeUpdate(s23);
	System.out.println(r23);
	
	int r24=s.executeUpdate(s24);
	System.out.println(r24);
	
	
	int r25=s.executeUpdate(s25);
	System.out.println(r25);
	
	int r26=s.executeUpdate(s26);
	System.out.println(r26);
	
	int r27=s.executeUpdate(s27);
	System.out.println(r27);
	
	int r28=s.executeUpdate(s28);
	System.out.println(r28);
	
	int r29=s.executeUpdate(s29);
	System.out.println(r29);
	
	int r30=s.executeUpdate(s30);
	System.out.println(r30);
	
	int r32=s.executeUpdate(s32);
	System.out.println(r32);
	
	int r31=s.executeUpdate(s31);
	System.out.println(r31);
	
	
	int r33=s.executeUpdate(s33);
	System.out.println(r33);
	int r34=s.executeUpdate(s34);
	System.out.println(r34);
	int r21=s.executeUpdate(s21);
	System.out.println(r21);
	
	mycon.setAutoCommit(false);
	mycon.commit();
	
	
}
public void deliveryQuery() throws ClassNotFoundException, SQLException {

	Statement s;
	java.sql.Connection mycon = DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver",
			"jdbc:oracle:thin:@//usfkl23db39v-sca.mrshmc.com:1530/BWQA14", "Z_MM_MT1_D", "MM_MT1_D_zqaFri2019");
	s = mycon.createStatement();

	String s1="DELETE FROM COMPANY_STATUS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s2="DELETE FROM QUOTE_EMPL_VALRESULT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s3="DELETE FROM AMC_COST_REQUIREMENTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s4="DELETE FROM ASSOC_GROUPS_COMPANY_ANSWER WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s5="DELETE FROM ASSOC_COMPANY_FILE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s6="DELETE FROM ELIG_FACTOR_COMP_NICKNAMES WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s7="DELETE FROM COMPANY_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s8="DELETE FROM COMPANY_BENEFIT_CONTROL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s9="DELETE FROM COMPANY_PAYROLL_CONFIG_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s10="DELETE FROM COMPANY_CARRIER_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s11="DELETE FROM ASSOCIATION_AFFINITY_COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s12="DELETE FROM ELIG_GROUP WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s13="DELETE FROM COMPANY_PAYROLL_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s14="DELETE FROM COMPANY_PLAN_DETAIL WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s15="DELETE FROM BKWIP_CLIENT_WIZARD_LOG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s16="DELETE FROM BKWIP_CLIENT_WIZARD WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	String s18="DELETE FROM ELIG_GROUP_BP_ASSOC WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s19="DELETE FROM COSTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s20="DELETE FROM EMPLOYEE_EFF_DATE WHERE SSN IN (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID = '"+Company_id+"')";
	String s21="DELETE FROM COMP_SSN WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s22="DELETE FROM HRI_FILE_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
	String s23="DELETE FROM HRI_FILE_ERROR_DETAIL WHERE TRANSACTION_ID IN (SELECT TRANSACTION_ID FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"')";
	String s24="DELETE FROM HRI_FILE_HISTORY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s25="DELETE FROM CONTACT WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s26="DELETE FROM CONTACT_ADDRESS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	String s28="DELETE FROM COST_FACTOR_BENEFIT_VALUE WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s27="DELETE FROM COST_FACTORS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s29="DELETE FROM COMPANY_BENEFIT_LIFEEVENTS WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s30="DELETE FROM COBRA_CONFIG WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s31="DELETE FROM COMP_COMMS_ACTIVE_TEMPLATES WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	String s17="DELETE FROM COMPANY WHERE CONTROL_ID = 'SMLMKT' AND COMPANY_ID = '"+Company_id+"'";
	
	
	
	
	int r1=s.executeUpdate(s1);
	System.out.println(r1);
	int r2=s.executeUpdate(s2); 
	System.out.println(r2);
	int r3=s.executeUpdate(s3);
	System.out.println(r3);
	int r4=s.executeUpdate(s4);
	System.out.println(r4);
	int r5=s.executeUpdate(s5); 
	System.out.println(r5);
	int r6=s.executeUpdate(s6);
	System.out.println(r6);
	int r7=s.executeUpdate(s7);
	System.out.println(r7);
	int r8=s.executeUpdate(s8);
	System.out.println(r8);
	int r9=s.executeUpdate(s9);
	System.out.println(r9);
	int r10=s.executeUpdate(s10);
	System.out.println(r10);
	
	int r11=s.executeUpdate(s11);
	System.out.println(r11);
	
	int r12=s.executeUpdate(s12);
	System.out.println(r12);
	
	int r13=s.executeUpdate(s13);
	System.out.println(r13);
	
	int r14=s.executeUpdate(s14);
	System.out.println(r14);
	
	int r15=s.executeUpdate(s15);
	System.out.println(r15);
	int r16=s.executeUpdate(s16);
	System.out.println(r16);
	
	
	int r18=s.executeUpdate(s18);
	System.out.println(r18);
	int r19=s.executeUpdate(s19);
	System.out.println(r19);
	int r20=s.executeUpdate(s20);
	System.out.println(r20);
	int r21=s.executeUpdate(s21);
	System.out.println(r21);
	
	int r22=s.executeUpdate(s22); 
	System.out.println(r22);
	int r23=s.executeUpdate(s23);
	System.out.println(r23);
	int r24=s.executeUpdate(s24);
	System.out.println(r24);
	int r25=s.executeUpdate(s25);
	System.out.println(r25);
	
	int r26=s.executeUpdate(s26);
	System.out.println(r26);
	
	
	
	int r28=s.executeUpdate(s28);
	System.out.println(r28);
	
	int r27=s.executeUpdate(s27);
	System.out.println(r27);
	int r29=s.executeUpdate(s29);
	System.out.println(r29);
	int r30=s.executeUpdate(s30);
	System.out.println(r30);
	int r31=s.executeUpdate(s31);
	System.out.println(r31);
	int r17=s.executeUpdate(s17);
	System.out.println(r17);
	mycon.setAutoCommit(false);
	mycon.commit();

}

}
	
	
	
