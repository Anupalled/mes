package pages.MES;

import static driverfactory.Driver.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.ListIterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.ExcelReader;

public class MemberRates {
	
static WebDriver driver;

@FindBy(xpath = "//h2[text()='Select an option to begin entering rates.']")
public static WebElement memberRatesHeader;

//a[text()='Rates Upload']

@FindBy(xpath = "//button[text()='Save']")
public static WebElement saveButton;

@FindBy(xpath = "//button[text()='Save & Continue']")
public static WebElement saveContinueButton;

@FindBy(xpath = "(//div[@ng-click='setActiveTab(category.id)'])[2]")
public static WebElement dentalTab;

@FindBy(xpath = "(//div[@ng-click='setActiveTab(category.id)'])[3]")
public static WebElement visionTab;

@FindBy(xpath = "(//div[@ng-click='setActiveTab(category.id)'])[4]")
public static WebElement lifeTab;

@FindBy(xpath = "//a[text()='Upload Rates']")
public static WebElement uploadRates;

@FindBy(xpath = "//a[text()='Continue']")
public static WebElement continuepopup;

@FindBy(xpath = "//a[text()='Close']")
public static WebElement closeButton;


@FindBy(xpath = "//input[@type='file']")
public static WebElement file;

@FindBy(xpath = "(//button[@class='button radius evo-button ng-binding'])[4]")
public static WebElement uploadRatesButton;

@FindBy(xpath = "//a[text()='Rates Upload']")
public static WebElement Rates;

@FindBy(xpath = "(//input[@type='text'])[1]")
public static WebElement value1;

@FindBy(xpath = "(//input[@type='text'])[2]")
public static WebElement value2;
//div[@class='large-3 medium-3 small-12 columns ng-scope'][4]

	public MemberRates(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	
	
	public static void setData() throws IOException, InterruptedException {
		Thread.sleep(10000);
		waitForElementToDisplay(memberRatesHeader);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
		
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("memberrates");
	
	
		int i,j,numberOfBoxes=1,colsize;
		int rowsize = mySheet.getPhysicalNumberOfRows();
		
		for(i=0;i<rowsize;i++){
			Row row=mySheet.getRow(i);
			colsize = row.getPhysicalNumberOfCells();
			for(j=0;j<colsize;j++) {
				Cell value =row.getCell(j);
				int cellValue=(int) value.getNumericCellValue();
				String zipcode1 = Integer.toString(cellValue);
				String s23 = zipcode1.trim();
				WebElement box= driver.findElement(By.xpath("(//input[@type='text'])[" + numberOfBoxes + "]"));
				numberOfBoxes++;
				Thread.sleep(2000);
				setInput(box, s23);
			}
		}
		
}
	public static void setData_AllSavers() throws IOException, InterruptedException {
		Thread.sleep(5000);
		clickElement(Rates);
		Thread.sleep(3000);
		waitForElementToDisplay(memberRatesHeader);
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/QuotingTool.xlsx");
		
	
		XSSFWorkbook myWorkBook = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook.getSheet("rates");
	
	
		int i,j,numberOfBoxes=1,colsize;
		int rowsize = mySheet.getPhysicalNumberOfRows();
		
		for(i=0;i<rowsize;i++){
			Row row=mySheet.getRow(i);
			colsize = row.getPhysicalNumberOfCells();
			for(j=0;j<colsize;j++) {
				Cell value =row.getCell(j);
				int cellValue=(int) value.getNumericCellValue();
				String zipcode1 = Integer.toString(cellValue);
				String s23 = zipcode1.trim();
				WebElement box= driver.findElement(By.xpath("(//input[@type='text'])[" + numberOfBoxes + "]"));
				numberOfBoxes++;
				Thread.sleep(2000);
				setInput(box, s23);
			}
		}
		
}
	
public static void selectTabs(WebElement e) throws InterruptedException {
	waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	Thread.sleep(7000);
	clickElement(e);
}
public static void setLifeData() throws IOException, InterruptedException {
	waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	Thread.sleep(15000);
	waitForElementToDisplay(value1);
			setInput(value1,"100");
			setInput(value2,"10");
		}
	
	
	

public void clickContinue(WebElement e) throws InterruptedException {
	Thread.sleep(5000);
	clickElement(e);
	Thread.sleep(5000);
}
public void submitQuote() throws InterruptedException {
	clickElement(saveContinueButton);
	clickElement(continuepopup);
	waitForElementToDisplay(MemberRates.closeButton);
	/*Thread.sleep(7000);
	closeButton.click();
	Thread.sleep(6000);
	waitForElementToDisplay(AssociationPage.assocDetails);*/
}
public void submitFinalQuote() throws InterruptedException {
	clickElement(saveContinueButton);
	clickElement(continuepopup);
	waitForElementToDisplay(MemberRates.closeButton);
	Thread.sleep(5000);
/*	clickElement(closeButton);
	Thread.sleep(6000);
	waitForElementToDisplay(AssociationPage.assocDetails);*/
}

}

