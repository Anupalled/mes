package pages.MES;

import static driverfactory.Driver.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PayrollPage {
	WebDriver driver;
	@FindBy(xpath = "//h1[contains(text(),' Payroll')]")
	public static WebElement payrollHeader;
	
	@FindBy(xpath = "(//strong[@class='ng-star-inserted'])[4]")
	public static WebElement monthlyCheckbox;
	
	@FindBy(xpath = "//mercer-icon[@icon='date_range']")
	public static WebElement datePicker;
	
	@FindBy(xpath = "(//td[@class='ng-star-inserted'])[10]//a")
	public static WebElement date;
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "//h1[text()=' Employee Data']")
	public static WebElement empHeader;
	
	
	public PayrollPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void selectPayroll() throws InterruptedException {
		
		clickElement(monthlyCheckbox);
		clickElement(datePicker);
		clickElement(date);
		waitForPageLoad(driver);
		Thread.sleep(6000);
		clickElement(continueButton);
		Thread.sleep(6000);
		
	}
}
