package pages.MES;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.waitForElementToDisplay;

import java.io.File;
import java.util.Set;
public class SubmitFinalQuote {
	
	WebDriver driver;
	
	@FindBy(xpath = "//input[@type='file']")
	public static WebElement file;
	
	@FindBy(xpath = "//h1[text()=' Submit Final Quote Request']")
	public static WebElement finalQuoteHeader;
	
	@FindBy(xpath = "//button[@data-name='Confirm']")
	public static WebElement upload;
	
	
	
	@FindBy(xpath = "//button[@data-name='Save and Continue']")
	public static WebElement saveButton;
	
	@FindBy(xpath = "//h1[text()=' Request Complete ']")
	public static WebElement requestComplete;
	
	@FindBy(xpath = "//button[text()='Yes']")
	public static WebElement okButton;
	
	@FindBy(xpath = "//div[contains(text(),'Employee Application')]")
	public static WebElement EmpApplication;
	
	
	public SubmitFinalQuote(WebDriver driver) {
		
		PageFactory.initElements(driver, this);
	}
	
	public void draganddrop() throws InterruptedException {
		    Thread.sleep(10000);
			waitForElementToDisplay(finalQuoteHeader);

			
			
			String screenshot1 = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" +
					File.separator + "WindowScreens" + File.separator + "CancelButton.png";
			
		
			file.sendKeys(screenshot1);
			clickElement(upload);
			Thread.sleep(4000);
			clickElement(saveButton);
			Thread.sleep(4000);
			okButton.click();
			Thread.sleep(20000);
			
	}
}
