package pages.censusTool;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.clickElementUsingJavaScript;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;
import utilities.ExcelReader;

public class ProfilePage {
	static WebDriver driver;
	@FindBy(xpath = "(//h1[text()='Profile & Employees'])[1]")
	public static WebElement profileHeader;
	
	@FindBy(xpath = "//input[@name='name']")
	public static WebElement nameTextbox;
	
	@FindBy(xpath = "//input[@name='phone']")
	public static WebElement phoneTestbox;
	
	@FindBy(xpath = "//input[@name='email']")
	public static WebElement emailTextbox;
	
	@FindBy(xpath = "//input[@name='member_number']")
	public static WebElement memberNumber;
	
	@FindBy(xpath = "//input[@name='association_number']")
	public static WebElement associationNumber;
	
	
	
	@FindBy(xpath = "//select[@name='assoc_offer_a_plan']")
	public static WebElement healthPlan;
	
	
			
	@FindBy(xpath = "//select[@name='assoc_how_long_offer_a_plan']")
	public static WebElement healthPlanPeriod;	
		
			
			
	@FindBy(xpath = "//select[@name='assoc_who_is_your_carrier']")
	public static WebElement currentcarrier;			
			
			
	@FindBy(xpath = "//select[@name='assoc_contr_to_the_plan']")
	public static WebElement contributeThePlaneYes;	
			
			 
	@FindBy(xpath = "//select[@name='assoc_med_prem_percent']")
	public static WebElement currentContribution;	
			 
			
	@FindBy(xpath = "//div[@class='column shrink']")
	public static WebElement datePicker;		
			
			
	@FindBy(xpath = "(//button[@class='mos-c-icon-button--lg mos-c-icon-button'])[2]")
	public static WebElement rightToggle;

	@FindBy(xpath = "//input[@id='datepicker1']")
	public static WebElement inputDate;


	
	@FindBy(xpath = "//a[text()='20']")
	public static WebElement selectDate;			
			
	@FindBy(xpath = "(//div[@class='large-6 large-centered columns'])[13]//select")
	public static WebElement nmmaPlan;	
			
	@FindBy(xpath = "(//div[@class='large-6 large-centered columns'])[14]//select")
	public static WebElement voluntaryBenefits;
	
	@FindBy(xpath = "(//div[@class='large-6 large-centered columns'])[15]//select")
	public static WebElement marineIndustry;
	
	@FindBy(xpath = "//button[@type='submit']")
	public static WebElement nextButton;
	
	@FindBy(xpath = "//button[text()='Yes']")
	public static WebElement continueButton;
	
	@FindBy(xpath = "(//div[@class='large-6 large-centered columns'])[8]//select")
	public static WebElement plan1;
	
	@FindBy(xpath = "(//div[@class='large-6 large-centered columns'])[9]//select")
	public static WebElement plan2;
	
	@FindBy(xpath = "(//div[@class='large-6 large-centered columns'])[10]//select")
	public static WebElement plan3;
	


	
	public ProfilePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
					
	public static void setProfileDetails_No(String Index) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/CensusToolData.xlsx");
	
		XSSFWorkbook myWorkBook1 = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook1.getSheet("DataWithNo");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(Index)) {
				
					
					cell = (Cell) coliterator.next();
					String sic1value = cell.getStringCellValue();
					String sic1 = sic1value.trim();
					setInput(nameTextbox, sic1);
					
					
				cell = (Cell) coliterator.next();
				int Exec= (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String phone = s1s.trim();
				setInput(phoneTestbox, phone);
				
				
				
				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String ss2 = ss1.trim();
				setInput(emailTextbox, ss2);
				
				cell = (Cell) coliterator.next();
				int number= (int) cell.getNumericCellValue();
				String memberNum = Integer.toString(number);
				String memberNums = memberNum.trim();
				setInput(memberNumber, memberNums);
				
				waitForElementToDisplay(associationNumber);
				cell = (Cell) coliterator.next();
				String ss3 = cell.getStringCellValue();
				String ss4 = ss3.trim();
				setInput(associationNumber, ss4);
				
				
				waitForElementToDisplay(healthPlan);
				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String state2 = state2value.trim();
				Driver.selEleByVisbleText(healthPlan, state2);
				
				waitForElementToDisplay(plan1);
				cell = (Cell) coliterator.next();
				String state3value = cell.getStringCellValue();
				String state3 = state3value.trim();
				Driver.selEleByVisbleText(plan1, state3);
				
				waitForElementToDisplay(plan2);
				cell = (Cell) coliterator.next();
				String state4value = cell.getStringCellValue();
				String state4 = state4value.trim();
				Driver.selEleByVisbleText(plan2, state4);
				
				
				waitForElementToDisplay(plan3);
				cell = (Cell) coliterator.next();
				String state5value = cell.getStringCellValue();
				String state5 = state5value.trim();
				Driver.selEleByVisbleText(plan3, state5);
				
				clickElement(nextButton);
				clickElement(continueButton);

				
			}
			
         
        }
	}
	
	public static void setProfileDetails_Yes(String Index) throws IOException, InterruptedException {
		ClassLoader loader = new ExcelReader().getClass().getClassLoader();
		InputStream input = loader.getResourceAsStream("testdata/CensusToolData.xlsx");
	
		XSSFWorkbook myWorkBook1 = new XSSFWorkbook(input);
		XSSFSheet mySheet = myWorkBook1.getSheet("DataWithYes");
		Iterator<Row> rowIterator = mySheet.iterator();
		
        while(rowIterator.hasNext()){
        	Row row = rowIterator.next();
            //For each row, iterate through all the columns
        	java.util.Iterator<Cell> cellIterator = row.cellIterator();
			java.util.Iterator<Cell> coliterator = cellIterator;
			Cell cell = (Cell) coliterator.next();
			String Exec_indicator1 = cell.getStringCellValue();
			String s23 = Exec_indicator1.trim();
			System.out.println(s23);
			if (s23.equals(Index)) {
				
					
					cell = (Cell) coliterator.next();
					String sic1value = cell.getStringCellValue();
					String sic1 = sic1value.trim();
					setInput(nameTextbox, sic1);
					

					
				cell = (Cell) coliterator.next();
				int Exec= (int) cell.getNumericCellValue();
				String s1s = Integer.toString(Exec);
				String phone = s1s.trim();
				setInput(phoneTestbox, phone);
				
				
				
				cell = (Cell) coliterator.next();
				String ss1 = cell.getStringCellValue();
				String ss2 = ss1.trim();
				setInput(emailTextbox, ss2);
				
				cell = (Cell) coliterator.next();
				int number= (int) cell.getNumericCellValue();
				String memberNum = Integer.toString(number);
				String memberNums = memberNum.trim();
				setInput(memberNumber, memberNums);
				
				waitForElementToDisplay(associationNumber);
				cell = (Cell) coliterator.next();
				String ss3 = cell.getStringCellValue();
				String ss4 = ss3.trim();
				setInput(associationNumber, ss4);
				
				waitForElementToDisplay(healthPlan);
				cell = (Cell) coliterator.next();
				String state2value = cell.getStringCellValue();
				String state2 = state2value.trim();
				Driver.selEleByVisbleText(healthPlan, state2);
				
				waitForElementToDisplay(healthPlanPeriod);
				cell = (Cell) coliterator.next();
				String value1 = cell.getStringCellValue();
				String  planPeriod= value1.trim();
				Driver.selEleByVisbleText(healthPlanPeriod, planPeriod);
				
				waitForElementToDisplay(currentcarrier);
				cell = (Cell) coliterator.next();
				String value2 = cell.getStringCellValue();
				String currentCarrier = value2.trim();
				Driver.selEleByVisbleText(currentcarrier, currentCarrier);
				
				waitForElementToDisplay(contributeThePlaneYes);
				cell = (Cell) coliterator.next();
				String value3 = cell.getStringCellValue();
				String contribute = value3.trim();
				Driver.selEleByVisbleText(contributeThePlaneYes, contribute);
				
				
				Driver.selEleByVisbleText(currentContribution, "10%");

				
				clickElement(datePicker);
				clickElement(rightToggle);
				clickElement(selectDate);
				
				
				Driver.selEleByVisbleText(nmmaPlan,"Yes");
				
				
				Driver.selEleByVisbleText(voluntaryBenefits,"Yes");
				
				
				Driver.selEleByVisbleText(marineIndustry,"Yes");
				
				clickElement(nextButton);
				clickElement(continueButton);
				
			}
			
         
        }
	}
						
							
}
