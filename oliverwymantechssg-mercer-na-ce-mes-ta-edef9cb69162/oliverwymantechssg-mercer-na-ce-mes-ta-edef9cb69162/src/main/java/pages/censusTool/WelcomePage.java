package pages.censusTool;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import static utilities.DatabaseUtils.getResultSet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.mongodb.connection.Connection;

import utilities.DatabaseUtils;

public class WelcomePage {
	
	@FindBy(xpath = "//img[@src='/assets/images/clients_logo/NMMA.png']")
	public static WebElement nmmaIcon;
	
	@FindBy(xpath = "//img[@src='/assets/images/mercer.png']")
	public static WebElement mercerIcon;
	
	@FindBy(xpath = "//h1[contains(text(),'Welcome')]")
	public static WebElement welcomeHeader;
	
	@FindBy(xpath = "//h1[contains(text(),'Thank You for Your Interest')]")
	public static WebElement thankYouTitle;
	
	@FindBy(xpath = "//span[@class='endDates']")
	public static WebElement endDate;
	
	@FindBy(xpath = "//a[text()='Continue']")
	public static WebElement continueButton;
	
	
	public WelcomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	public  void clickContinue() throws InterruptedException {
		waitForElementToDisplay(nmmaIcon);
		waitForElementToDisplay(mercerIcon);
		waitForElementToDisplay(welcomeHeader);
		waitForElementToDisplay(endDate);
		clickElement(continueButton);

	}
	public void verifydata() throws ClassNotFoundException, SQLException {
	
	java.sql.Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", "jdbc:oracle:thin:@//10.2.189.146:1530/BWQA14","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
	ResultSet myres= getResultSet(mycon, "select ASSOCIATION_ID,end_date from census_association where association_id='NMMA'");
	while (myres.next()){
	String t1 = myres.getString(2);
		System.out.println(t1); 
	
	}
	}
	
	
	
	
	
	
	
	
	
}
