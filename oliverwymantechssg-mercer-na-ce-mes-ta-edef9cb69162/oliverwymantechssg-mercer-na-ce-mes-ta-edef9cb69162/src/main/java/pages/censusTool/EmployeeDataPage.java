package pages.censusTool;

import static driverfactory.Driver.*;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisplay;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import driverfactory.Driver;
import utilities.ExcelReader;

public class EmployeeDataPage {
	static WebDriver driver;
	@FindBy(xpath = "//a[@class='button button-action round evo-button']")
	public static WebElement addEmployeeButton;
	
	@FindBy(xpath = "//div[@class='column shrink ng-star-inserted']")
	public static WebElement datePicker;
	
	@FindBy(xpath = "//td//a[text()='1']")
	public static WebElement date;
	
	@FindBy(xpath = "//select[@name='gender']")
	public static WebElement selectGender;
	
	@FindBy(xpath = "//input[@name='zip_code']")
	public static WebElement zipCode;
	
	@FindBy(xpath = "//h2[text()='Add an Employee']")
	public static WebElement addEmpTitle;
	
	
	@FindBy(xpath = "//select[@name='employment']")
	public static WebElement selectEmployment;
	
	@FindBy(xpath = "//input[@name='salary']")
	public static WebElement salary;
	
	@FindBy(xpath = "//input[@name='total_premium']")
	public static WebElement totalPremium;
	
	
	@FindBy(xpath = "//select[@name='coverage']")
	public static WebElement totalCvg;
	
	@FindBy(xpath = "//button[text()='Save']")
	public static WebElement saveButton;
	
	@FindBy(xpath = "//input[@id='accept-terms']/..//label")
	public static WebElement acceptCheckbox;
	
	@FindBy(xpath = "//a[text()='Submit']")
	public static WebElement submitButton;
	
	@FindBy(xpath = "(//h1[text()='Thank you'])[1]")
	public static WebElement thankYouHeader;
	
	
	
	
	public EmployeeDataPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void setEmployeeData_Yes() throws InterruptedException {
		waitForElementToDisplay(addEmployeeButton);
		clickElement(addEmployeeButton);
		waitForElementToDisplay(addEmpTitle);
		clickElement(datePicker);
		clickElement(date);
		
		Driver.selEleByVisbleText(selectGender, "Female");
		setInput(zipCode, "26587");
		Driver.selEleByVisbleText(selectEmployment, "Full Time");
		setInput(salary, "256");
		setInput(totalPremium, "2582");
		Driver.selEleByVisbleText(totalCvg, "Employee Only");
		clickElement(saveButton);	
		
		clickElementUsingJavaScript(driver,acceptCheckbox);
		clickElement(submitButton);
		
		waitForElementToDisplay(thankYouHeader);
	
	
	}

	public void setEmployeeData_No() throws InterruptedException {
		waitForElementToDisplay(addEmployeeButton);
		clickElement(addEmployeeButton);
		waitForElementToDisplay(addEmpTitle);
		clickElement(datePicker);
		clickElement(date);
		
		Driver.selEleByVisbleText(selectGender, "Female");
		setInput(zipCode, "26587");
		Driver.selEleByVisbleText(selectEmployment, "Full Time");
		setInput(salary, "256");
		Driver.selEleByVisbleText(totalCvg, "Employee Only");
		clickElement(saveButton);	
		
		clickElementUsingJavaScript(driver,acceptCheckbox);
		clickElement(submitButton);
		
		waitForElementToDisplay(thankYouHeader);
	
	
	}
	
}
