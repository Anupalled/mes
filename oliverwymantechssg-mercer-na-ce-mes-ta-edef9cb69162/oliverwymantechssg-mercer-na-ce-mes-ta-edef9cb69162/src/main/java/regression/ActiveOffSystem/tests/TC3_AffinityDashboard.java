package regression.ActiveOffSystem.tests;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyElementIsNotPresent;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

import static pages.MES.ProfilePage1.value;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CompleteRequestPage;
import static pages.MES.CompleteRequestPage.AssociationID;
import pages.MES.ConsultantPortalLoginPage;
import pages.MES.DashboardPage;
import pages.MES.HeaderPage;
import pages.MES.LoginPage;
import pages.MES.MemberRates;
import pages.MES.PlanSelectionPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3_AffinityDashboard extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	public static ExtentTest test = null;

	public TC3_AffinityDashboard(String appName) {
		super(appName);

	}


	@Test(priority = 4, enabled = true)
	public void consultantPortal() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert(); 
		try {

		
			
			Driver driverFact = new Driver();
			TC3_AffinityDashboard mes = new TC3_AffinityDashboard("ConsultantPortal");
			
			test = reports.createTest("consultantportal");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);

			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			header.selectHeaders(HeaderPage.associationTab);

			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
			
			waitForPageLoad(driver);
			association.selectAssociation(AssociationPage.ActiveOffsystem);
			Thread.sleep(10000);

			
			//waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			association.selectAssociation(AssociationPage.memebercompy);
			waitForElementToDisplay(AssociationPage.assocDetails);
			
			AssociationPage.searchMembers("ABCD_1");
			verifyElementTextContains(driver,AssociationPage.activeoffSystemStatus,"Active (Off System) ","status exists","status does not exists", test,softAssert);
			
			association.selectAssociation(AssociationPage.mercerOps);
			switchToWindowByTitle("Mercer Associations - Google Chrome",driver);
			waitForElementToDisplay(AssociationPage.activeoffSystemTitle);
			DashboardPage dashboard = new DashboardPage(driver);
			verifyElementTextContains(driver,DashboardPage.workTimings,"8 AM to 6 PM ET Monday-Friday ","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.msgVerify,"Please contact us or UHC with any questions regarding ongoing changes.","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.editAccountStructure,"Edit Account Structure","link pass","link fail", test,softAssert);
			
			dashboard.submitQuoteRequest(DashboardPage.viewCompanyButton);
			dashboard.verifyButton();
			
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
	
	@Test(priority = 5, enabled = true)
	public void ActiveRenewalQAHeadhunter() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert(); 
		try {
			Driver driverFact = new Driver();
			TC3_AffinityDashboard mes = new TC3_AffinityDashboard("ConsultantPortal");
			
			test = reports.createTest("ActiveRenewalQAHeadhunter");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			

			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			header.selectHeaders(HeaderPage.associationTab);

			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
			
			waitForPageLoad(driver);
			AssociationPage.selectAssociation(AssociationPage.qaHeadHunter);
			Thread.sleep(10000);
			waitForPageLoad(driver);
			AssociationPage.selectAssociation(AssociationPage.affinityDashboard);
			association.activeStateQAHeadhunter();
			waitForElementToDisplay(AssociationPage.activeQAHeadHunterText);
			verifyElementTextContains(driver,AssociationPage.activeQAHeadHunterText, "Some companies have been renewed with UHC directly and are not being managed on this platform","activeemp pass","activeemp fail", test,softAssert);
			
			association.renewalStateQAHeadhunter();
			waitForElementToDisplay(AssociationPage.renewalQAHeadHunterText);
			verifyElementTextContains(driver,AssociationPage.renewalQAHeadHunterText, "Some renewals for your association will be handled directly with UHC and will not appear on the renewal dashboard","renewalemp pass","renewalemp fail", test,softAssert);
			
		

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
	
	@Test(priority = 6, enabled = true)
	public void ActiveRenewalUHCAssoc() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert(); 
		try {
			Driver driverFact = new Driver();
			TC3_AffinityDashboard mes = new TC3_AffinityDashboard("ConsultantPortal");
			
			test = reports.createTest("ActiveRenewalUHCAssoc");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			

			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			header.selectHeaders(HeaderPage.associationTab);

			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
			association.pagination_assoc();
			waitForPageLoad(driver);
			AssociationPage.selectAssociation(AssociationPage.assocNew);
			Thread.sleep(5000);
			waitForPageLoad(driver);
			AssociationPage.selectAssociation(AssociationPage.affinityDashboard);
			association.activeStateQAHeadhunter();
			Thread.sleep(5000);
			verifyElementIsNotPresent(driver,AssociationPage.activeQAHeadHunterText, "Some companies have been renewed with UHC directly and are not being managed on this platform","activeemp not present", test,softAssert);
			
			association.renewalStateQAHeadhunter();
			Thread.sleep(5000);
			verifyElementIsNotPresent(driver,AssociationPage.renewalQAHeadHunterText, "Some renewals for your association will be handled directly with UHC and will not appear on the renewal dashboard","renewalemp not present", test,softAssert);
			
		

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
	
	
}