package regression.ActiveOffSystem.tests;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

import static pages.MES.ProfilePage1.value;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CompleteRequestPage;
import static pages.MES.CompleteRequestPage.AssociationID;
import pages.MES.ConsultantPortalLoginPage;
import pages.MES.DashboardPage;
import pages.MES.HeaderPage;
import pages.MES.LoginPage;
import pages.MES.MemberRates;
import pages.MES.PlanSelectionPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_ConsultantPortal extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC1_ConsultantPortal(String appName) {
		super(appName);

	}

	@BeforeClass
	public void beforeClass() throws Exception {

		TC1_ConsultantPortal portal = new TC1_ConsultantPortal("ConsultantPortal");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");

	}

	@Test(priority = 1, enabled = true)
	public void consultantPortal() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert(); 
		try {

			test = reports.createTest("consultantportal");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			header.selectHeaders(HeaderPage.associationTab);

			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
			
			waitForPageLoad(driver);
			association.selectAssociation(AssociationPage.ActiveOffsystem);
			Thread.sleep(10000);

			
			
			association.selectAssociation(AssociationPage.memebercompy);
			waitForElementToDisplay(AssociationPage.assocDetails);
			
			AssociationPage.searchMembers("samrin-affinity- anupalled%2b21@gmail.com");
			association.activerOffSystemIcon();
			
			
			association.selectAssociation(AssociationPage.mercerOps1);
			switchToWindowByTitle("Mercer Associations - Google Chrome",driver);
			waitForElementToDisplay(AssociationPage.activeoffSystemTitle);
			DashboardPage dashboard = new DashboardPage(driver);
			verifyElementTextContains(driver,DashboardPage.workTimings,"8 AM to 6 PM ET Monday-Friday ","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.msgVerifyPY,"Please contact us to begin the process.","link pass","link fail", test,softAssert);
			
			
			dashboard.submitQuoteRequest(DashboardPage.editClient);
			dashboard.verifyButton();
			
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
	
}