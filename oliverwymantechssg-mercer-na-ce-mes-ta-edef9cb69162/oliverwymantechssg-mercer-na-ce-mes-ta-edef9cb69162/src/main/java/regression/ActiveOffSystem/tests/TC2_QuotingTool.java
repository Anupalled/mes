package regression.ActiveOffSystem.tests;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyContains;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;
import static verify.SoftAssertions.verifyNotEquals;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import pages.MES.CensusData;
import static pages.MES.CensusData.*;
import pages.MES.CompleteRequestPage;
import pages.MES.ConsultantPortalLoginPage;

import static pages.MES.CompleteRequestPage.*;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CensusData;
import pages.MES.CompleteRequestPage;

import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisplay;
import static pages.MES.CompleteRequestPage.AssociationID;
import static pages.MES.CompleteRequestPage.getAddress1;
import static pages.MES.CompleteRequestPage.getAvgEmployee;
import static pages.MES.CompleteRequestPage.getCity1;
import static pages.MES.CompleteRequestPage.getEligibleEmp;
import static pages.MES.CompleteRequestPage.getEmailId;
import static pages.MES.CompleteRequestPage.getName;
import static pages.MES.CompleteRequestPage.getOrgName1;
import static pages.MES.CompleteRequestPage.getZipCode1;
import static pages.MES.CompleteRequestPage.getphoneNum;
import static pages.MES.CompleteRequestPage.getsic2;
import static pages.MES.CompleteRequestPage.getstartDate;
import static pages.MES.CompleteRequestPage.sicvalue;
import static pages.MES.CompleteRequestPage.taxidval;
import static pages.MES.ProfilePage1.avgEmp_PP;
import static pages.MES.ProfilePage1.eligiEmp_PP;
import static pages.MES.ProfilePage1.getEmailId_PP;
import static pages.MES.ProfilePage1.getFirstName_PP;
import static pages.MES.ProfilePage1.getOrgName_PP;
import static pages.MES.ProfilePage1.getPhoneNum_PP;
import static pages.MES.ProfilePage1.getSIC2_PP;
import static pages.MES.ProfilePage1.getSIC_PP;
import static pages.MES.ProfilePage1.getaddress_PP;
import static pages.MES.ProfilePage1.getcity_PP;
import static pages.MES.ProfilePage1.getstartdate_PP;
import static pages.MES.ProfilePage1.gettaxID_PP;
import static pages.MES.ProfilePage1.getzipCode_PP;

import pages.MES.DashboardPage;
import pages.MES.LoginPage;
import pages.MES.ProfilePage1;
import pages.MES.SelfRegPage;
import smoke.mes.tests.TC1_QuotingTool;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC2_QuotingTool extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC2_QuotingTool(String appName) {				
		
		super(appName);

	}

	@Test(priority = 2, enabled = true)
	public void MESlogin() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			Driver driverFact = new Driver();
			TC2_QuotingTool mes = new TC2_QuotingTool("PYQTDevDoor");
			
			test = reports.createTest("LoginQTST");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			
			
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(DashboardPage.activeoffSystemTitle);
			verifyElementTextContains(driver,DashboardPage.workTimings,"8 AM to 6 PM ET Monday-Friday ","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.planYearFinishedMsg_QT,"is up for renewal with UHC. Please contact us to begin the process.","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.benefitOffering_Button,"View Your Benefits Offering","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.contactUsTitle,"Contact Us","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.benefitOffering_Button,"View Your Benefits Offering","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.FAQ,"Tips to obtain a quote","link pass","link fail", test,softAssert);
			
			verifyElementTextContains(driver,DashboardPage.quotingProcess,"Initiate Quoting Process","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.quoteRequest,"Submit Initial Quote Request","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.reviewQuote,"Review Quote","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.signUp,"Sign Up!","link pass","link fail", test,softAssert);
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	@Test(priority = 3, enabled = true)
	public void MES() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			Driver driverFact = new Driver();
			TC2_QuotingTool mes = new TC2_QuotingTool("PNYQTDevDoor");
			
			test = reports.createTest("LoginQTST");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			
			
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(DashboardPage.activeoffSystemTitle);
			verifyElementTextContains(driver,DashboardPage.workTimings,"8 AM to 6 PM ET Monday-Friday ","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.planNotFinished_QT,"directly with UHC. Please contact us or UHC with any questions regarding ongoing changes.","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.contactUsTitle,"Contact Us","link pass","link fail", test,softAssert);
			verifyElementTextContains(driver,DashboardPage.FAQ,"Tips to obtain a quote","link pass","link fail", test,softAssert);
			
			
			//
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
}