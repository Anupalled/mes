package regression.AllSavers_AHP.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static pages.MES.ProfilePage1.value;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CensusData;
import pages.MES.CompleteRequestPage;
import pages.MES.ConfirmationPage;
import pages.MES.ConsultantPortalLoginPage;
import pages.MES.DashboardPage;
import pages.MES.EligibilityPage;
import pages.MES.EmployeeDataPage;
import pages.MES.EmployeeWebsitePage;
import pages.MES.HeaderPage;
import pages.MES.LoginPage;
import pages.MES.MemberRates;
import pages.MES.PayrollPage;
import pages.MES.PreliminaryQuotes;
import pages.MES.ProfilePage1;
import pages.MES.ProxyPage;
import pages.MES.QuestionnariePage;
import pages.MES.QuoteReviewPage;
import pages.MES.RequiredDocumentPage;
import pages.MES.SelfRegPage;
import pages.MES.SignUpPage;
import pages.MES.SubmitFinalQuote;
import pages.MES.ValidationPage;
import pages.MES.WorkSheetPage;
import regression.Medica_ahp.tests.TC3_ConsultantPortal;
import regression.mes_affinity.tests.TC2_QuotingTool;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC6_SignUpTool extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC6_SignUpTool(String appName) {
		super(appName);

	}

	
	@Test(priority = 12, enabled = true)
	public void quoteReviewPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			Driver driverFact = new Driver();
			TC2_QuotingTool mes = new TC2_QuotingTool("AllSavers");
			
			test = reports.createTest("signUpTool");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			
			System.out.println("BaseURL is: " + BASEURL);
			
			QuoteReviewPage quotereview = new QuoteReviewPage(driver);
			Thread.sleep(10000);
			waitForElementToDisplay(QuoteReviewPage.FinalQuote);
			verifyElementTextContains(driver,QuoteReviewPage.FinalQuote, "Your final quote is ready for review!","final quote pass","final quote fail", test,softAssert);
			clickElement(QuoteReviewPage.myQuotes);
			
			clickElement(QuoteReviewPage.learnAssumption);
			verifyElementTextContains(driver,QuoteReviewPage.learnAssumptionContent, "Rates and fees are subject to final underwriting terms and conditions. ","final quote pass","final quote fail", test,softAssert);
			quotereview.clickbutton(QuoteReviewPage.closeAssumptionButton);
			verifyElementTextContains(driver,QuoteReviewPage.content90days, "This quote is valid for 90 days from the date you requested a quote.","final quote pass","final quote fail", test,softAssert);
			
			
			quotereview.clickbutton(QuoteReviewPage.arrowDownButton1);
			quotereview.clickbutton(QuoteReviewPage.arrowDownButton1);
			
			quotereview.clickbutton(QuoteReviewPage.preliminaryButton);
			verifyElementTextContains(driver,QuoteReviewPage.preliminaryQuoteTitle, "Preliminary Quote","ACO pass","ACO fail", test,softAssert);
			quotereview.clickbutton(QuoteReviewPage.closePreliminary);
			verifyElementTextContains(driver,QuoteReviewPage.footerContent, "Mercer will receive a service provider fee","ACO pass","ACO fail", test,softAssert);
			
			quotereview.selectPlans_AllSaver();
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			//driver.close();
		}
	}
	
@Test(priority = 13, enabled = true)
	public void signUpPage() throws Exception {
	ExtentTest test=null;
	//ExtentTest test=null;
	 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("signuptool");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);

			SignUpPage signup = new SignUpPage(driver);
			waitForElementToDisplay(SignUpPage.signupHeader);
			verifyElementTextContains(driver,SignUpPage.signupHeader, "Sign Up","signupHeader pass","signupHeader fail", test,softAssert);
			signup.getPlanDetails();


			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			//driver.close();
		}
	}
	
	@Test(priority = 14, enabled = true)
	public void eligibilityPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("eligibility");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			
			EligibilityPage eligi = new EligibilityPage(driver);
			waitForElementToDisplay(EligibilityPage.eligibilityHeader);
			verifyElementTextContains(driver,EligibilityPage.eligibilityHeader, "Eligibility","eligibilityHeader pass","eligibilityHeader fail", test,softAssert);
			eligi.createGroups();

			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	@Test(priority = 15, enabled = true)
	public void contributionPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("contribution");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			
			QuestionnariePage question = new QuestionnariePage(driver);
			waitForElementToDisplay(QuestionnariePage.questionnaireTitle);
			verifyElementTextContains(driver,QuestionnariePage.questionnaireTitle, "Questionnaire","questionnaireTitle pass","questionnaireTitle fail", test,softAssert);
			question.questionnarie();

			WorkSheetPage comparevalue = new WorkSheetPage(driver);
			waitForElementToDisplay(WorkSheetPage.workSheetHeader);
			verifyElementTextContains(driver,WorkSheetPage.workSheetHeader, "Worksheet","workSheetHeader pass","workSheetHeader fail", test,softAssert);
			comparevalue.worksheetAffinity();

			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			//driver.close();
		}
	}
	
	@Test(priority = 16, enabled = true)
	public void payrollPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("payroll");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			
			PayrollPage payroll = new PayrollPage(driver);
			waitForElementToDisplay(PayrollPage.payrollHeader);
			verifyElementTextContains(driver,PayrollPage.payrollHeader, "Payroll","payrollHeader pass","payrollHeader fail", test,softAssert);
			payroll.selectPayroll();

			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			//driver.close();
		}
	}
	
	@Test(priority = 17, enabled = true)
	public void employeeDataPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			
			test = reports.createTest("employeedata");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
		
		
			EmployeeDataPage empdata = new EmployeeDataPage(driver);
			  waitForElementToDisplay(EmployeeDataPage.empHeader);
			  verifyElementTextContains(driver,EmployeeDataPage.empHeader, "Employee Data","empHeader pass","empHeader fail", test,softAssert);
			  empdata.uploadFile_ahp();
			  empdata.uploadFile1();
			
			  
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			//driver.close();
		}
	}
	
	
	
	@Test(priority = 19, enabled = true)
	public void employeeWebsitePage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("employeewebsite");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
 
			EmployeeWebsitePage website = new EmployeeWebsitePage(driver);
			  waitForElementToDisplay(EmployeeWebsitePage.header);
			  verifyElementTextContains(driver,EmployeeWebsitePage.header, "Website Logo","header pass","header fail", test,softAssert);
			  website.uploadLogo();
			  
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			//driver.close();
		}	}
	
	@Test(priority = 20, enabled = true)
	public void validationPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("validation");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			ValidationPage validate = new ValidationPage(driver);
			waitForElementToDisplay(ValidationPage.validationHeader);
			verifyElementTextContains(driver,ValidationPage.validationHeader, "Validation","validationHeader pass","validationHeader fail", test,softAssert);

			
			WorkSheetPage comparevalue = new WorkSheetPage(driver);
			comparevalue.clickSave();

			RequiredDocumentPage requireddoc = new RequiredDocumentPage(driver);
			waitForElementToDisplay(RequiredDocumentPage.requiredDocHeader);
			verifyElementTextContains(driver,RequiredDocumentPage.requiredDocHeader, "Required Documents","requiredDocHeader pass","requiredDocHeader fail", test,softAssert);
			requireddoc.uploadFile();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
		}
	}

	@Test(priority = 21, enabled = true)
	public void confirmationPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("confirmation page");
			test.assignCategory("regression");
			driver = driverFact.getEventDriver(webdriver, test);

			ConfirmationPage confirm = new ConfirmationPage(driver);
			Thread.sleep(3000);
			waitForElementToDisplay(ConfirmationPage.termsConditionHeader);
			verifyElementTextContains(driver,ConfirmationPage.termsConditionHeader, "Terms and conditions","termsConditionHeader pass","termsConditionHeader fail", test,softAssert);
			confirm.acceptTermsCondition();
			
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
	@Test(priority = 22, enabled = false)
	public void proxyMBC() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert(); 
		try {

			test = reports.createTest("proxy MBC");
			test.assignCategory("smoke");
			Driver driverFact = new Driver();
			
			TC3_ConsultantPortal portal = new TC3_ConsultantPortal("ConsultantPortal");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);

			
			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);
			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			header.selectHeaders(HeaderPage.associationTab);

			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
			association.pagination_assoc();
			waitForPageLoad(driver);
			association.selectAssociation(AssociationPage.assocNew);
			Thread.sleep(20000);

			
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			association.selectAssociation(AssociationPage.memebercompy);
			waitForElementToDisplay(AssociationPage.assocDetails);
			AssociationPage.searchMembers(value);
			association.clickEyeIcon();
			
			
			waitForElementToDisplay(AssociationPage.myDashboard);
			association.viewAllEmp();
			
			ProxyPage proxy=new ProxyPage(driver);
			proxy.proxyMBC();
			proxy.chooseBenefits();
			proxy.yourCart();
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
		@Test(priority = 23, enabled = true)
		public void reloginConfirmation() throws Exception {
			ExtentTest test=null;
	    	//ExtentTest test=null;
			 SoftAssert softAssert=new SoftAssert();
			try {

				Driver driverFact = new Driver();
				TC2_QuotingTool mes = new TC2_QuotingTool("AllSavers");
				
				test = reports.createTest("Delete Auth and Delivery entries");
				test.assignCategory("smoke");
				webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
				driver = driverFact.getEventDriver(webdriver, test);
				
				DashboardPage dashboard = new DashboardPage(driver);
				waitForElementToDisplay(DashboardPage.setupCompletionMsg);
				verifyElementTextContains(driver,DashboardPage.setupCompletionMsg, "Congratulations! Your set-up is now complete!","setupCompletionMsg pass","setupCompletionMsg fail", test,softAssert);
			
				
				SelfRegPage self=new SelfRegPage(driver);
				self.getCompanyID();
				self.authQuery();
				self.deliveryQuery();
				
				

			}catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
				ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				
				}
				catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
				ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				


			}
			finally
			{
				reports.flush();
				System.out.println("before soft assert all");
				softAssert.assertAll();
				driver.close();
			}
			}
		
	
}