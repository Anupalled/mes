package regression.affinity_broker.tests;
import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.LoginPage;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.ConsultantPortal;
import pages.affinityDashboard.MfaPage;
import utilities.InitTests;
import verify.SoftAssertions;
public class VerifyActivePendingEmpBroker extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	public static ExtentTest test;
	String companyId;

	public VerifyActivePendingEmpBroker(String appName) {
		super(appName);	
	}

@Test(enabled=true , priority= 2)
	public void verifyDocUpload() throws Exception	{		
		new VerifyActivePendingEmpBroker("MESBroker");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Search and Export Functioanlity Broker user");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFABroker(driver, outlookDetails);

			
			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);
			
			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}

			
			affinityPage.verifyActiveEmpNameBroker();
			verifyElementTextContains(driver,affinityPage.activeEmpDetails, "Affinity bbrkr", "activeEmpDetails pass","activeEmpDetails fail",test,softAssert);
			
			/*String downloadPath1 =  "c:/users/temp/downloads";
					//System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator + "downloadFiles";
			
			//Verify Export file
			AffinityDashboard.deleteFilesInPath(downloadPath1);
			affinityPage.downloadFile(downloadPath1);
			AffinityDashboard.isFileDownloaded(downloadPath1, "!!!QA NAM (Headhunters)!!! Active Employers 7_16_2020.xlsx");	
			List<String> employerName = new ArrayList<>();
			employerName =affinityPage.readEmployeDetailsActive(affinityPage.getFileInDirectory(downloadPath1));	
			verifyElementTextContains(affinityPage.activeEmpDetails, employerName.get(0), test);
			AffinityDashboard.deleteFilesInPath(downloadPath1);
			*/
			
			
			affinityPage.verifyPendingEmpNameBroker();
			verifyElementTextContains(driver,affinityPage.activeEmpDetails, "!BROKER API UHC!","activeEmpDetails pass","activeEmpDetails fail", test,softAssert);
			
			/*String downloadPath =  "c:/users/temp/downloads";
					//System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator + "downloadFiles";
			AffinityDashboard.deleteFilesInPath(downloadPath);				
			affinityPage.downloadFile(downloadPath);
			AffinityDashboard.isFileDownloaded(downloadPath, "!!!QA NAM (Headhunters)!!! Pending Employers 7_16_2020.xlsx");
			//Verify Export File
			employerName = new ArrayList<>();
			employerName =affinityPage.readEmployeDetailsPending(affinityPage.getFileInDirectory(downloadPath1));	
			verifyElementTextContains(affinityPage.activeEmpDetails, employerName.get(0), test);
			AffinityDashboard.deleteFilesInPath(downloadPath);			
			
*/
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
		//	driver.close();
		}

	}

@Test(enabled=false , priority= 2)
public void verifyExport() throws Exception	{		
	new VerifyActivePendingEmpBroker("MESBroker");	
	ExtentTest test=null;
	//ExtentTest test=null;
	 SoftAssert softAssert=new SoftAssert();
	try {
		webdriver = driverFact.initWebDriver(BASEURL,"CHROME","local","");
		test = reports.createTest("Verify Export Functioanlity Broker user");
		test.assignCategory("Regression");
		driver=driverFact.getEventDriver(webdriver,test);

		LoginPage login =  new LoginPage(driver);
		login.login(USERNAME,PASSWORD);	

		String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
		MfaPage mfa = new MfaPage(driver);			
		mfa.NavigateToOutlookAndGetMFABroker(driver, outlookDetails);

		
		AffinityDashboard affinityPage = new AffinityDashboard(driver);
		waitForElementToDisplay(affinityPage.affinityDashboardHeader);
		
		if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
			verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
			test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

		}else {
			test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
			System.out.println(affinityPage.affinityDashboardHeader.getText());
		}
		
		affinityPage.navigateToPendingEmployers();
		String downloadPath1 =  "c:/users/temp/downloads";
		
		AffinityDashboard.deleteFilesInPath(downloadPath1);
		waitForElementToDisplay(affinityPage.exportAllButton);
		clickElement(affinityPage.exportAllButton);
		System.out.println("waiting for file download");
		Thread.sleep(4000);
		List<String> employerName = new ArrayList<>();
		employerName =affinityPage.readEmployeDetailsPending(affinityPage.getFileInDirectory(downloadPath1));
		for(String name:employerName){
			System.out.println(name);
		}
		Thread.sleep(4000);
		System.out.println(affinityPage.employerTableItems.size());
			int j=0;
		for(int i=0;i<affinityPage.employerTableItems.size();i=i+4) {
			System.out.print(affinityPage.employerTableItems.get(i).getText());
			System.out.print("="+employerName.get(j)+"\n");
			if(employerName.contains(affinityPage.employerTableItems.get(i).getText())){
				test.log(Status.PASS, "Value matched "+affinityPage.employerTableItems.get(i).getText()+" " +affinityPage.employerTableItems.get(i).getText());
			}
			System.out.println(employerName.contains(affinityPage.employerTableItems.get(i).getText()));
			j++;
		}
		
		
		
		//
		
		

		
		
	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		
		}
		catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		


	}
	finally
	{
		reports.flush();
		System.out.println("before soft assert all");
		softAssert.assertAll();
		driver.close();
	}
}


}
