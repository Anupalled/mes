package regression.affinity_broker.tests;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.LoginPage;
import regression.affinity.tests.VerifyAddAndDeleteUser;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.ConsultantPortal;
import pages.affinityDashboard.MfaPage;
import utilities.InitTests;
import verify.SoftAssertions;
public class VerifyBrokerLoginAndProxy extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	public static ExtentTest test;
	String companyId;
	public static String downloadPath1;
	public VerifyBrokerLoginAndProxy(String appName) {
		super(appName);	
	}
//

@Test(enabled=true , priority= 3)
public void verifyUserLogin() throws Exception	{		
	new VerifyAddAndDeleteUser("MESBroker");
	ExtentTest test=null;
	//ExtentTest test=null;
	 SoftAssert softAssert=new SoftAssert();
	try {
		webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
		test = reports.createTest("Verify user Login to Affinity Dashboard Proxy");
		test.assignCategory("Regression");
		driver=driverFact.getEventDriver(webdriver,test);

		LoginPage login =  new LoginPage(driver);
		login.login(USERNAME,PASSWORD);	

		String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
		MfaPage mfa = new MfaPage(driver);			
		mfa.NavigateToOutlookAndGetMFABroker(driver, outlookDetails);

		
		AffinityDashboard affinityPage = new AffinityDashboard(driver);
		waitForElementToDisplay(affinityPage.affinityDashboardHeader);
		
		if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
			verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
			test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

		}else {
			test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
			System.out.println(affinityPage.affinityDashboardHeader.getText());
		}

		affinityPage.companyButtonEnabled();
		verifyElementTextContains(driver,AffinityDashboard.memberCompanyButton, "Create New Member Company","memberCompanyButton pass","memberCompanyButton fail", test,softAssert);
		affinityPage.verifyActiveEmpProxy(test);
		affinityPage.verifyHRProxy(test);
		
		


	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		
		}
		catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		


	}
	finally
	{
		reports.flush();
		System.out.println("before soft assert all");
		softAssert.assertAll();
		//driver.close();
	}

}
}
