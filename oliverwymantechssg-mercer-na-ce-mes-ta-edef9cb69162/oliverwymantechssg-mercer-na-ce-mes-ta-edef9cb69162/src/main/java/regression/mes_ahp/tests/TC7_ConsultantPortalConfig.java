package regression.mes_ahp.tests;

	import static driverfactory.Driver.waitForElementToDisplay;
	import static pages.MES.ProfilePage1.value;
	import static utilities.MyExtentReports.reports;
	import static verify.SoftAssertions.verifyElementTextContains;

	import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
	import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

	import atu.testng.reports.ATUReports;
	import atu.testng.reports.logging.LogAs;
	import atu.testng.selenium.reports.CaptureScreen;
	import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
	import driverfactory.Driver;
	import pages.MES.AssociationPage;
	import pages.MES.CompleteRequestPage;
	import pages.MES.ConsultantPortalLoginPage;
	import pages.MES.HeaderPage;
import pages.MES.SelfRegPage;
import utilities.InitTests;
	import verify.SoftAssertions;

	public class TC7_ConsultantPortalConfig extends InitTests {
		Driver driverFact = new Driver();
		WebDriver driver = null;
		WebDriver webdriver = null;
		ExtentTest test = null;

		public TC7_ConsultantPortalConfig(String appName) {
			super(appName);

	}
		
		
		
		@BeforeClass
		public void beforeClass() throws Exception {

			TC7_ConsultantPortalConfig portal = new TC7_ConsultantPortalConfig("ConsultantPortal");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");

		}

		
		@Test(priority =1, enabled = false)
		public void AssociationSetup_CP() throws Exception {
			ExtentTest test=null;
	    	//ExtentTest test=null;
			 SoftAssert softAssert=new SoftAssert();
			try {

				test = reports.createTest("consultantportal");
				test.assignCategory("smoke");
				driver = driverFact.getEventDriver(webdriver, test);

				ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);
				
				

				loginpage.login(USERNAME, PASSWORD);

				HeaderPage header = new HeaderPage(driver);
				waitForElementToDisplay(HeaderPage.dashboard);
				verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
				header.selectHeaders(HeaderPage.associationTab);
				
				
				AssociationPage association = new AssociationPage(driver);
				waitForElementToDisplay(AssociationPage.associationHeader);
				verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
				//association.pagination_assoc();
				association.selectAssociation(AssociationPage.assoc);
				waitForElementToDisplay(AssociationPage.profile1);
				
				association.fetchListOfIndustryCategory();
				association.offeringType();
			
				
				
			} catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
				ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				
				}
				catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
				ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				


			}
			finally
			{
				reports.flush();
				System.out.println("before soft assert all");
				softAssert.assertAll();
				driver.close();
			}
		}
		
	}


