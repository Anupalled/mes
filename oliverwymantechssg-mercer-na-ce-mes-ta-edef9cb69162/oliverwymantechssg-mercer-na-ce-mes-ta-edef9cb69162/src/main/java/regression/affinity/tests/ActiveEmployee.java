package regression.affinity.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;
import org.openqa.selenium.WebDriver;
import static verify.SoftAssertions.assertNotNull;

import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.MfaPage;
import pages.hri.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class ActiveEmployee extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;

	public ActiveEmployee(String appName) {
		super(appName);	
	}

	@Test(enabled=true , priority= 1)
	public void verifyPromotedMemberCompanyList() throws Exception	{		
		new ActiveEmployee("MESDB");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Promoted Member Company With OE Details");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);

			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}	
			
			affinityPage.navigateToActiveEmployers();
			waitForElementToDisplay(affinityPage.ActiveEmployerHeader);
			verifyElementContains(affinityPage.ActiveEmployerHeader, "ACTIVE EMPLOYERS", "Navigated to Active Employers Page", test,softAssert);
			String oedate;
			String employeerName;
			for(int i=2;i<35;i=i+5) {
				oedate = affinityPage.employerTableItems.get(i).getText();
				employeerName = affinityPage.employerTableItems.get(i-2).getText();
				assertNotNull(oedate, " OE date for employeer "+employeerName+" is Not Null", test);
				System.out.println(employeerName);
			}
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
			
			//driver.close();
		}
}

	
	
	@Test(enabled=false , priority= 2)
	public void verifyTilesFlip() throws Exception	{		
		new VerifyDashboards("MESDB");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify different Tiles Flip");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);

			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			affinityPage.navigateToActiveEmployers();
			verifyElementContains(affinityPage.ActiveEmployerHeader, "ACTIVE EMPLOYERS", "Navigated to Pending Employers Page", test,softAssert);
			verifyElementContains(affinityPage.totalEmployers, "TOTAL EMPLOYERS", "Total Employers Tile", test,softAssert);
			verifyElementContains(affinityPage.EnrolledEmployees, "ENROLLED EMPLOYEES", "Enrolled Employees tile", test,softAssert);
			verifyElementContains(affinityPage.totalPremium, "TOTAL PREMIUM", "Total premium tile", test,softAssert);
			
			affinityPage.flipTiles();
			
			verifyElementContains(affinityPage.OENotStarted, "OE Not Started", "Total Employers Tile Flipped", test,softAssert);
			/*verifyElementContains(affinityPage.medical2ndTile, "Medical", "Enrolled Employees tile Flipped", test);
			verifyElementContains(affinityPage.medical3rdTile, "Medical", "Total premium tile Flipped", test);*/			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
		
	}
	
	
	
}
