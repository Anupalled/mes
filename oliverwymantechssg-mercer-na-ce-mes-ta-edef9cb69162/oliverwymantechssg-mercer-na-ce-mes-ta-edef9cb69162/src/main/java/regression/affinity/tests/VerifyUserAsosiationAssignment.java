package regression.affinity.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;
import static verify.SoftAssertions.verifyElementIsPresent;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.ConsultantPortal;
import pages.affinityDashboard.MfaPage;
import pages.hri.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class VerifyUserAsosiationAssignment extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;

	public VerifyUserAsosiationAssignment(String appName) {
		super(appName);	
	}


	@Test(enabled=true , priority= 1)
	public void userRegistrationWithAssosiaionAssigmed() throws Exception	{		
		new VerifyUserAsosiationAssignment("ConsultantPortal");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("verify the MES user registration with assigning association");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			String assosiationName = "!HSA-GK";

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			ConsultantPortal cp = new ConsultantPortal(driver);
			waitForElementToDisplay(cp.homeIcon);
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon",softAssert);
			cp.navigateToMesUsers(driver);

			if(cp.mesUsersHeader.getText().equalsIgnoreCase("MES Users")) {
				test.log(Status.PASS, "MES User Page Loaded");
				verifyElementIsPresent(cp.addNewUser, test, "Add new user",softAssert);
			}else {
				test.log(Status.FAIL, "MES User Page Loading Failed");
			}

			
			cp.searchUser(driver, "Shreyy K");
			
			cp.removeAssosiation(driver);
			/*Thread.sleep(5000);
			if(cp.removeUserAssosiationMessage.getText().equalsIgnoreCase("Association assignment for the user has been removed")) {
			
				test.log(Status.PASS, "Association Removed..");
			}
			else {
				test.log(Status.FAIL, "Association Removal Failed..");
			}
			*/
			/*driver.get(props.getProperty("MES_baseurl"));
			//LoginPage login =  new LoginPage(driver);
			login.login(props.getProperty("MES_username"),props.getProperty("MES_password"));	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);

			
			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);
			waitForElementToDisplay(affinityPage.affinityDashboardCompanyHeader);
			
			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			System.out.println(affinityPage.affinityDashboardCompanyHeader.getText());
			if(affinityPage.affinityDashboardCompanyHeader.getText().equalsIgnoreCase(assosiationName)) {
				verifyElementContains(affinityPage.affinityDashboardCompanyHeader, assosiationName, " Assosiation matched ", test);
				test.log(Status.PASS, "User Assosiation Assignment Success");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}*/

			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
		//	driver.close();
		}
}
	
	
	@Test(enabled=true , priority= 2)
	public void userRegistrationWithoutAssosiaionAssigned() throws Exception	{		
		new VerifyUserAsosiationAssignment("ConsultantPortal");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("verify user association assignment");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			String assosiationName = " !HSA-GK ";

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			ConsultantPortal cp = new ConsultantPortal(driver);
			waitForElementToDisplay(cp.homeIcon);
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon",softAssert);
			cp.navigateToMesUsers(driver);

			if(cp.mesUsersHeader.getText().equalsIgnoreCase("MES Users")) {
				test.log(Status.PASS, "MES User Page Loaded");
				verifyElementIsPresent(cp.addNewUser, test, "Add new user",softAssert);
			}else {
				test.log(Status.FAIL, "MES User Page Loading Failed");
			}

			
			cp.searchUser(driver, "Shreyy K");
			
			cp.assignAssosiation(driver, assosiationName);
			
			
			driver.get(props.getProperty("MESDB_baseurl"));
			//LoginPage login =  new LoginPage(driver);
			login.login(props.getProperty("MESDB_username"),props.getProperty("MESDB_password"));	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);

			
			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);
			waitForElementToDisplay(affinityPage.affinityDashboardCompanyHeader);
			
			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			System.out.println(affinityPage.affinityDashboardCompanyHeader.getText());
			if(affinityPage.affinityDashboardCompanyHeader.getText().equalsIgnoreCase(assosiationName)) {
				verifyElementContains(affinityPage.affinityDashboardCompanyHeader, assosiationName, " Assosiation matched ", test,softAssert);
				test.log(Status.PASS, "User Assosiation Assignment Success");

			}else {
				test.log(Status.FAIL, "Login Failed or Association Name Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}

			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
}
	
}
