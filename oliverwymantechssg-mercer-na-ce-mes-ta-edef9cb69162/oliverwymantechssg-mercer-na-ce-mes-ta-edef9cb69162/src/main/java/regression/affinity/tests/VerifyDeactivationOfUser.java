package regression.affinity.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.ConsultantPortal;
import pages.hri.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class VerifyDeactivationOfUser extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;

	public VerifyDeactivationOfUser(String appName) {
		super(appName);	
	}


	@Test(enabled=true, priority= 1)
	public void deactivateAffinityAndQtUser() throws Exception	{		
		new VerifyDeactivationOfUser("HRI");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify deactivation of user registered in Affinity dashboard");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			String userToSearch = "DEMO1 DEMO2";

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			ConsultantPortal cp = new ConsultantPortal(driver);
			cp.waitForConsultantPortalHome();	
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon");
			cp.navigateToMesUsers(driver);

			if(cp.mesUsersHeader.getText().equalsIgnoreCase("MES Users")) {
				test.log(Status.PASS, "MES User Page Loaded");
				verifyElementIsPresent(cp.addNewUser, test, "Add new user");
			}else {
				test.log(Status.FAIL, "MES User Page Loading Failed");
			}

			cp.searchUser(driver, userToSearch);

			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(userToSearch)&&cp.userStatusActive.getText().equalsIgnoreCase("Active")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusActive, "Active","userStatusActive pass","userStatusActive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Active");
			}else {
				test.log(Status.FAIL, "User search failed or user is not active");
			}

			cp.deactivateOrReactivateUser(driver);
			verifyElementTextContains(driver,cp.deleteMesage, "You have successfully deactivated the selected user.","deleteMesage pass","deleteMesage fail", test,softAssert);
			clickElement(cp.confirmButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			cp.waitForPageLoad();


			cp.searchUser(driver, userToSearch);

			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(userToSearch)&&cp.userStatusInactive.getText().equalsIgnoreCase("INACTIVE")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusInactive, "INACTIVE","userStatusInactive pass","userStatusInactive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Inactive");
			}else {
				test.log(Status.FAIL, "User search failed or user is active");
			}


		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
	//		driver.close();
		}
}
	@Test(enabled=true, priority= 2)
	public void deactivateQtUser() throws Exception	{		
		new VerifyDeactivationOfUser("HRI");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify deactivation of user registered in QT/ST and Affinity dashboard");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			String userToSearch = "Shreyy K";

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			ConsultantPortal cp = new ConsultantPortal(driver);
			cp.waitForConsultantPortalHome();	
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon",softAssert);
			cp.navigateToMesUsers(driver);

			if(cp.mesUsersHeader.getText().equalsIgnoreCase("MES Users")) {
				test.log(Status.PASS, "MES User Page Loaded");
				verifyElementIsPresent(cp.addNewUser, test, "Add new user",softAssert);
			}else {
				test.log(Status.FAIL, "MES User Page Loading Failed");
			}

			cp.searchUser(driver, userToSearch);

			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(userToSearch)&&cp.userStatusActive.getText().equalsIgnoreCase("Active")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusActive, "Active", "userStatusActive pass","userStatusActive fail",test,softAssert);
				test.log(Status.PASS, "The status of current user is Active");
			}else {
				test.log(Status.FAIL, "User search failed or user is not active");
			}

			cp.deactivateOrReactivateUser(driver);
			verifyElementTextContains(driver,cp.deleteMesage, "You have successfully deactivated the selected user.","deleteMesage pass","deleteMesage fail", test,softAssert);
			clickElement(cp.confirmButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			cp.waitForPageLoad();


			cp.searchUser(driver,userToSearch );

			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(userToSearch)&&cp.userStatusInactive.getText().equalsIgnoreCase("INACTIVE")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusInactive, "INACTIVE","userStatusInactive pass","userStatusInactive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Inactive");
			}else {
				test.log(Status.FAIL, "User search failed or user is active");
			}


		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
	//		driver.close();
		}
}
	@Test(enabled=true, priority= 3)
	public void deactivateUserUsingEditButton() throws Exception	{		
		new VerifyDeactivationOfUser("HRI");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify deactivation of user registered using Edit Button");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			String userToSearch = "Broker Name Broker";

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			ConsultantPortal cp = new ConsultantPortal(driver);
			cp.waitForConsultantPortalHome();	
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon",softAssert);
			waitForElementToDisappear(By.cssSelector("div[class='blocking-overlay']"));	
			cp.navigateToMesUsers(driver);

			if(cp.mesUsersHeader.getText().equalsIgnoreCase("MES Users")) {
				test.log(Status.PASS, "MES User Page Loaded");
				verifyElementIsPresent(cp.addNewUser, test, "Add new user",softAssert);
			}else {
				test.log(Status.FAIL, "MES User Page Loading Failed");
			}

			cp.searchUser(driver, userToSearch);


			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(userToSearch)&&cp.userStatusActive.getText().equalsIgnoreCase("Active")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusActive, "Active","userStatusActive pass","userStatusActive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Active");
			}else {
				test.log(Status.FAIL, "User search failed or user is not active");
			}

			cp.deactivateUserusingEditButton(driver);
			System.out.println(cp.deleteMesage.getText());
			verifyElementTextContains(driver,cp.deleteMesage, "You have successfully deactivated the selected user.","deleteMesage pass","deleteMesage fail", test,softAssert);
			clickElement(cp.confirmButton);			
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			cp.waitForPageLoad();
			cp.searchUser(driver,userToSearch );

			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(userToSearch)&&cp.userStatusInactive.getText().equalsIgnoreCase("INACTIVE")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusInactive, "INACTIVE","userStatusInactive pass","userStatusInactive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Inactive");
			}else {
				test.log(Status.FAIL, "User search failed or user is active");
			}
			
			cp.deactivateOrReactivateUser(driver);
			clickElement(cp.confirmButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			cp.waitForPageLoad();			


		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
	//		driver.close();
		}
}



	@Test(enabled=true, priority= 4)
	public void reactivateUser() throws Exception	{		
		new VerifyDeactivationOfUser("HRI");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify deactivation of user registered in Affinity Dashboard and QT/ST");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			String affintyUserToSearch="DEMO1 DEMO2";			
			String AffinityAndQtUserToSearch ="Shreyy K";

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			ConsultantPortal cp = new ConsultantPortal(driver);
			cp.waitForConsultantPortalHome();	
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon",softAssert);
			cp.navigateToMesUsers(driver);

			if(cp.mesUsersHeader.getText().equalsIgnoreCase("MES Users")) {
				test.log(Status.PASS, "MES User Page Loaded");
				verifyElementIsPresent(cp.addNewUser, test, "Add new user",softAssert);
			}else {
				test.log(Status.FAIL, "MES User Page Loading Failed");
			}


			//--Reactivate Affinity and QT user
			cp.searchUser(driver, AffinityAndQtUserToSearch);
			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(AffinityAndQtUserToSearch)&&cp.userStatusInactive.getText().equalsIgnoreCase("Inactive")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusInactive, "INACTIVE","userStatusInactive pass","userStatusInactive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Inactive");
			}else {
				test.log(Status.FAIL, "User search failed or user is active");
				System.out.println(cp.firstUserSearchResult.getText());
			}

			cp.deactivateOrReactivateUser(driver);
			verifyElementTextContains(driver,cp.deleteMesage, "You have successfully reactivated the selected user.","deleteMesage pass","deleteMesage fail", test,softAssert);
			clickElement(cp.confirmButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			cp.waitForPageLoad();			
			cp.searchUser(driver, AffinityAndQtUserToSearch);

			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(AffinityAndQtUserToSearch)&&cp.userStatusActive.getText().equalsIgnoreCase("ACTIVE")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusActive, "ACTIVE","userStatusActive pass","userStatusActive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Active");
			}else {
				test.log(Status.FAIL, "User search failed or user is not active");
			}

			//--Reactivate QT User
			cp.searchUser(driver, affintyUserToSearch);
			if(cp.userStatusInactive.getText().equalsIgnoreCase("Inactive")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusInactive, "INACTIVE","userStatusInactive pass","userStatusInactive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Inactive");
			}else {
				test.log(Status.FAIL, "User search failed or user is active");
				System.out.println(cp.firstUserSearchResult.getText());
			}

			cp.deactivateOrReactivateUser(driver);
			verifyElementTextContains(driver,cp.deleteMesage, "You have successfully reactivated the selected user.","deleteMesagepass","deleteMesage fail", test,softAssert);
			clickElement(cp.confirmButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			cp.waitForPageLoad();			
			cp.searchUser(driver, affintyUserToSearch);

			if(cp.firstUserSearchResult.getText().equalsIgnoreCase(affintyUserToSearch)&&cp.userStatusActive.getText().equalsIgnoreCase("ACTIVE")) {				
				verifyElementIsPresent(cp.firstUserSearchResult, test, "The user "+cp.firstUserSearchResult.getText()+" is Displayed",softAssert);
				verifyElementTextContains(driver,cp.userStatusActive, "ACTIVE","userStatusActive pass","userStatusActive fail", test,softAssert);
				test.log(Status.PASS, "The status of current user is Active");
			}else {
				test.log(Status.FAIL, "User search failed or user is not active");
			}



		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
}
	
}
