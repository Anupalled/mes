package regression.affinity.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;
import static verify.SoftAssertions.verifyElementIsPresent;

import java.sql.Connection;
import java.sql.ResultSet;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.ConsultantPortal;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;
import static verify.SoftAssertions.verifyEquals;

public class VerifyTilesFlip extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;

	public VerifyTilesFlip(String appName) {
		super(appName);	
	}

	
	
	@Test(enabled=true , priority= 2)
	public void verifyTilesFlip() throws Exception	{		
		new VerifyDashboards("HRI");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Tiles Flip Db Verification");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			
			ConsultantPortal cp = new ConsultantPortal(driver);
			cp.waitForConsultantPortalHome();	
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon",softAssert);
			cp.proxyToAffinityDashboard();


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);

			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			affinityPage.navigateToActiveEmployers();
			verifyElementContains(affinityPage.ActiveEmployerHeader, "ACTIVE EMPLOYERS", "Navigated to Pending Employers Page", test,softAssert);
			verifyElementContains(affinityPage.totalEmployers, "TOTAL EMPLOYERS", "Total Employers Tile", test,softAssert);
			verifyElementContains(affinityPage.EnrolledEmployees, "ENROLLED EMPLOYEES", "Enrolled Employees tile", test,softAssert);
			verifyElementContains(affinityPage.totalPremium, "TOTAL PREMIUM", "Total premium tile", test,softAssert);
			
			affinityPage.flipTilesHead();
			waitForElementToDisplay(affinityPage.medicalValue);
			System.out.println(affinityPage.medicalValue.getText().substring(1));
			System.out.println(affinityPage.dentalValue.getText());
			System.out.println(affinityPage.hsaValue.getText());
			System.out.println(affinityPage.basicValue.getText());
			System.out.println(affinityPage.totalPremiumValue.getText());
			
			long medicalValue = Math.round(Float.parseFloat(affinityPage.medicalValue.getText().substring(1)));
			long dentalValue = Math.round(Float.parseFloat(affinityPage.dentalValue.getText().substring(1)));
			long visionValue = Math.round(Float.parseFloat(affinityPage.visionValue.getText().substring(1)));
			long hsaValue = Math.round(Float.parseFloat(affinityPage.hsaValue.getText().substring(1)));
			long basicValue = Math.round(Float.parseFloat(affinityPage.basicValue.getText().substring(1)));
			long totalPremiumValue = Math.round(Float.parseFloat(affinityPage.totalPremiumValue.getText().substring(1)));
			
			System.out.println(medicalValue);
			System.out.println(dentalValue);
			System.out.println(visionValue);
			System.out.println(hsaValue);
			System.out.println(basicValue);
			System.out.println(totalPremiumValue);
			
			 Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_D","MM_MT1_D_zqaFri2019");
			 
			 String query = "SELECT "+
					   "B.short_desc, EBE.premium_Annual "+
					   "FROM COMP_SSN CS "+
					   "JOIN EMP_BEN_ELECTS EBE ON "+
					   "CS.CONTROL_ID = EBE.CONTROL_ID AND "+
					   "CS.SSN = EBE.SSN "+
					   "JOIN BENEFIT B ON "+
					   "B.CONTROL_ID = EBE.CONTROL_ID "+
					   "AND EBE.BENEFIT_ID = B.BENEFIT_ID "+
					   "JOIN COMPANY C ON "+
					   "C.CONTROL_ID = CS.CONTROL_ID AND " +
					   "C.COMPANY_ID = CS.COMPANY_ID "+
					   "JOIN COMPANY C2 ON "+
					   "C.CONTROL_ID = C2.CONTROL_ID AND "+
					   "C.ASSOCIATION_ID = C2.COMPANY_ID "+
					   "WHERE "+ 
					   "EBE.PLAN_ID <> 'NOCOV' AND "+
					   "EBE.DATE_TERMINATED IS NULL AND "+
					   "EBE.CONTROL_ID = 'SMLMKT' AND "+
					   "C.COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE ASSOCIATION_ID= '26582') "+
					   "ORDER BY "+
					   "B.BP_DISPLAY_ORDER";
			 
			 
			 
			 String query2 ="SELECT " + 
			 		"B.short_desc, EBE.premium_Annual " + 
			 		"FROM COMP_SSN CS " + 
			 		"JOIN EMP_BEN_ELECTS EBE ON " + 
			 		"CS.CONTROL_ID = EBE.CONTROL_ID AND " + 
			 		"CS.SSN = EBE.SSN " + 
			 		"JOIN BENEFIT B ON " + 
			 		"B.CONTROL_ID = EBE.CONTROL_ID " + 
			 		"AND EBE.BENEFIT_ID = B.BENEFIT_ID " + 
			 		"JOIN COMPANY C ON " + 
			 		"C.CONTROL_ID = CS.CONTROL_ID AND " + 
			 		"C.COMPANY_ID = CS.COMPANY_ID " + 
			 		"JOIN COMPANY C2 ON " + 
			 		"C.CONTROL_ID = C2.CONTROL_ID AND " + 
			 		"C.ASSOCIATION_ID = C2.COMPANY_ID " + 
			 		"WHERE " + 
			 		"EBE.PLAN_ID <> 'NOCOV' AND " + 
			 		"EBE.DATE_TERMINATED IS NULL AND " + 
			 		"EBE.CONTROL_ID = 'SMLMKT' AND " + 
			 		"C.COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY_PLAN_DETAIL WHERE Status = 'Live' AND Company_id in (select Company_id from Company where ASSOCIATION_ID= '26582')) " + 
			 		"ORDER BY " + 
			 		"B.BP_DISPLAY_ORDER ";
			 
			 
			 
			 
			 float medical= 0;
			 float dental= 0;
			 float vision= 0;
			 float hsa= 0;
			 float basicLife= 0;
			 
			    ResultSet query1= getResultSet(mycon, query2);
			    
			    while(query1.next()) {	
			    	
			    	if(query1.getString(1).equalsIgnoreCase("Medical")) {
			    		medical= medical+query1.getFloat(2);
			    	}else if (query1.getString(1).equalsIgnoreCase("Dental")) {
						dental = dental+query1.getFloat(2);
					}else if (query1.getString(1).equalsIgnoreCase("Vision")) {
						vision = vision+query1.getFloat(2);
					}else if (query1.getString(1).equalsIgnoreCase("HSA")) {
						hsa = hsa+query1.getFloat(2);
					}else if (query1.getString(1).equalsIgnoreCase("Basic Life / AD&D")) {
						basicLife = basicLife+query1.getFloat(2);
					}
				    }	 
			
			
			    long medicalValueDb =Math.round(medical);
				long dentalValueDb =Math.round(dental);
				long visionValueDb =Math.round(vision);
				long hsaValeuDb =Math.round(hsa);
				long basicValueDb =Math.round(basicLife);
				long totalValueDb =medicalValueDb+visionValueDb+hsaValeuDb+basicValueDb+dentalValueDb;
			
			if(medicalValue==medicalValueDb) {
				test.log(Status.PASS, "Medical value Matched from DB Actual: "+medicalValue+" Expected: "+medicalValueDb);
			}else {
				test.log(Status.FAIL, "Medical value Match Failed from DB Actual: "+medicalValue+" Expected: "+medicalValueDb);
			}
			
			if(visionValue==visionValueDb) {
				test.log(Status.PASS, "Vision value Matched from DB Actual: "+visionValue+" Expected: "+visionValueDb);
			}else {
				test.log(Status.FAIL, "Vision value Match Failed from DB Actual: "+visionValue+" Expected: "+visionValueDb);
			}
			
			if(dentalValue==dentalValueDb) {
				test.log(Status.PASS, "Dental value Matched from DB Actual: "+dentalValue+" Expected: "+dentalValueDb);
			}else {
				test.log(Status.FAIL, "Dental value Match Failed from DB Actual: "+dentalValueDb+" Expected: "+dentalValueDb);
			}
			
			if(hsaValue==hsaValeuDb) {
				test.log(Status.PASS, "HSA value Matched from DB Actual: "+hsaValue+" Expected: "+hsaValeuDb);
			}else {
				test.log(Status.FAIL, "HSA value Match Failed from DB Actual: "+hsaValue+" Expected: "+hsaValeuDb);
			}
			
			if(basicValue==basicValueDb) {
				test.log(Status.PASS, "Basic value Matched from DB Actual: "+basicValue+" Expected: "+basicValueDb);
			}else {
				test.log(Status.FAIL, "Baic value Match Failed from DB Actual: "+basicValue+" Expected: "+basicValueDb);
			}
			
			if(totalPremiumValue==totalValueDb) {
				test.log(Status.PASS, "Total Premium value Matched from DB Actual: "+totalPremiumValue+" Expected: "+totalValueDb);
			}else {
				test.log(Status.FAIL, "Total Premium value Match Failed from DB Actual: "+totalPremiumValue+" Expected: "+totalValueDb);
			}

			
			
			
			
			
			
			
					
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
}
	
	
}
