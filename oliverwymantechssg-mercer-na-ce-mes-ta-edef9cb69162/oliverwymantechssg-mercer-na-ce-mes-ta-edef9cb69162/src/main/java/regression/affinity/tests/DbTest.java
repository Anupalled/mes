package regression.affinity.tests;

import static utilities.DatabaseUtils.getResultSet;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import utilities.DatabaseUtils;
public class DbTest {
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		 Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
				  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_D","MM_MT1_D_zqaFri2019");
		 
		 String query = "SELECT "+
				   "B.short_desc, EBE.premium_Annual "+
				   "FROM COMP_SSN CS "+
				   "JOIN EMP_BEN_ELECTS EBE ON "+
				   "CS.CONTROL_ID = EBE.CONTROL_ID AND "+
				   "CS.SSN = EBE.SSN "+
				   "JOIN BENEFIT B ON "+
				   "B.CONTROL_ID = EBE.CONTROL_ID "+
				   "AND EBE.BENEFIT_ID = B.BENEFIT_ID "+
				   "JOIN COMPANY C ON "+
				   "C.CONTROL_ID = CS.CONTROL_ID AND " +
				   "C.COMPANY_ID = CS.COMPANY_ID "+
				   "JOIN COMPANY C2 ON "+
				   "C.CONTROL_ID = C2.CONTROL_ID AND "+
				   "C.ASSOCIATION_ID = C2.COMPANY_ID "+
				   "WHERE "+ 
				   "EBE.PLAN_ID <> 'NOCOV' AND "+
				   "EBE.DATE_TERMINATED IS NULL AND "+
				   "EBE.CONTROL_ID = 'SMLMKT' AND "+
				   "C.COMPANY_ID IN (SELECT COMPANY_ID FROM COMPANY WHERE ASSOCIATION_ID= '26582') "+
				   "ORDER BY "+
				   "B.BP_DISPLAY_ORDER";
		 
		 float medical= 0;
		 float dental= 0;
		 float vision= 0;
		 float hsa= 0;
		 float basicLife= 0;
		 
		    ResultSet query1= getResultSet(mycon, query);
		    
		    while(query1.next()) {	
		    	
		    	if(query1.getString(1).equalsIgnoreCase("Medical")) {
		    		medical= medical+query1.getFloat(2);
		    	}else if (query1.getString(1).equalsIgnoreCase("Dental")) {
					dental = dental+query1.getFloat(2);
				}else if (query1.getString(1).equalsIgnoreCase("Vision")) {
					vision = vision+query1.getFloat(2);
				}else if (query1.getString(1).equalsIgnoreCase("HSA")) {
					hsa = hsa+query1.getFloat(2);
				}else if (query1.getString(1).equalsIgnoreCase("Basic Life / AD&D")) {
					basicLife = basicLife+query1.getFloat(2);
				}
			    }	 
		
		
		long medicalValueDb =Math.round(medical);
		long dentalValueDb =Math.round(dental);
		long visionValueDb =Math.round(vision);
		long hsaValeuDb =Math.round(hsa);
		long basicValueDb =Math.round(basicLife);
		long totalValueDb =medicalValueDb+visionValueDb+hsaValeuDb+basicValueDb+dentalValueDb;

		System.out.println(totalValueDb);
		
		
		
		
		 
		 
	}
	
}
