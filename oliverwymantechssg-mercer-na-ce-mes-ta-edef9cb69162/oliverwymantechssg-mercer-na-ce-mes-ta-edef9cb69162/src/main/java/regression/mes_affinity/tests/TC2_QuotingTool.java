package regression.mes_affinity.tests;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.*;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CensusData;
import pages.MES.CompleteRequestPage;
import pages.MES.ConfirmationPage;

import static pages.MES.ProfilePage1.*;
import static pages.MES.CompleteRequestPage.*;
import static driverfactory.Driver.waitForElementToDisplay;
import pages.MES.DashboardPage;
import pages.MES.EligibilityPage;
import pages.MES.EmployeeDataPage;
import pages.MES.EmployeeWebsitePage;
import pages.MES.LoginPage;
import pages.MES.PayrollPage;
import pages.MES.ProfilePage1;
import pages.MES.ProxyPage;
import pages.MES.QuestionnariePage;
import pages.MES.QuoteReviewPage;
import pages.MES.RequiredDocumentPage;
import pages.MES.SelfRegPage;
import pages.MES.SignUpPage;
import pages.MES.ValidationPage;
import pages.MES.WorkSheetPage;
import smoke.mes.tests.TC1_QuotingTool;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC2_QuotingTool extends InitTests {
	
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC2_QuotingTool(String appName) {
		super(appName);
	}
	@Test(priority = 1, enabled = true)
	public void MESlogin() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			Driver driverFact = new Driver();
			TC1_QuotingTool mes = new TC1_QuotingTool("DEVDOOR");
			
			test = reports.createTest("LoginQTST");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			
			 SelfRegPage self=new SelfRegPage(driver);
				Thread.sleep(5000);
				self.musedeletion();
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
				
					@Test(priority = 2, enabled = true)
					public void dashboardPage() throws Exception {
						ExtentTest test=null;
				    	//ExtentTest test=null;
						 SoftAssert softAssert=new SoftAssert();
						try {
							
							Driver driverFact = new Driver();
							TC2_QuotingTool mes = new TC2_QuotingTool("DEVDOOR");
							test = reports.createTest("profilePage1");
							test.assignCategory("regression");
							webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
							driver = driverFact.getEventDriver(webdriver, test);
							
							DashboardPage dashboard = new DashboardPage(driver);
							
							dashboard.requestQuote();
							
							ProfilePage1 profile = new ProfilePage1(driver);
							waitForElementToDisplay(ProfilePage1.profileHeader1);
							//verifyElementTextContains(ProfilePage1.profileHeader1, "Profile", test);
							verifyElementTextContains(driver,ProfilePage1.profileHeader1, "Profile","profileHeader1 pass","profileHeader1 fail", test,softAssert);
							profile.selectContinue(); // negative flow
						
							profile.negativeFlowPP1();
				
							profile.getOrganisationName();// positive flow
							ProfilePage1.setOrgNameAffinity("StateAffinity");
							profile.selectContinue();
				
						}  catch (Error e) {
							e.printStackTrace();
							SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
							ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
							
							}
							catch (Exception e) {
							e.printStackTrace();
							SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
							ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
							


						}
						finally
						{
							reports.flush();
							System.out.println("before soft assert all");
							softAssert.assertAll();
							driver.close();
						}
					}
				
					@Test(priority = 3, enabled = true)
					public void profilePage() throws Exception {
						ExtentTest test=null;
				    	//ExtentTest test=null;
						 SoftAssert softAssert=new SoftAssert();
						try {
							Driver driverFact = new Driver();
							TC2_QuotingTool mes = new TC2_QuotingTool("DEVDOOR");
							test = reports.createTest("profilePage1");
							test.assignCategory("smoke");
							webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
							driver = driverFact.getEventDriver(webdriver, test);
							
							
							System.out.println("BaseURL is: " + BASEURL);
							
							DashboardPage dashboard = new DashboardPage(driver);
							dashboard.submitQuoteRequest(DashboardPage.continueButton);
							
							ProfilePage1 profile = new ProfilePage1(driver);
							waitForElementToDisplay(ProfilePage1.profileHeader1);
							//verifyElementTextContains(ProfilePage1.profileHeader1, "Profile", test);
							verifyElementTextContains(driver,ProfilePage1.profileHeader1, "Profile","profileHeader1 pass","profileHeader1 fail", test,softAssert);
							
							
							profile.getOrganisationName();// positive flow
							ProfilePage1.setOrgNameAffinity("StateAffinity");
							profile.selectContinue();
				
						}  catch (Error e) {
							e.printStackTrace();
							SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
							ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
							
							}
							catch (Exception e) {
							e.printStackTrace();
							SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
							ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
							


						}
						finally
						{
							reports.flush();
							System.out.println("before soft assert all");
							softAssert.assertAll();
							
						}
					}
				
					@Test(priority = 4, enabled = true)
					public void profilePage2() throws Exception {
						ExtentTest test=null;
				    	//ExtentTest test=null;
						 SoftAssert softAssert=new SoftAssert();
						try {
				
							test = reports.createTest("profilepage2");
							test.assignCategory("smoke");
							driver = driverFact.getEventDriver(webdriver, test);
				
							ProfilePage1 profile = new ProfilePage1(driver);
							DashboardPage dashboard = new DashboardPage(driver);
				
							waitForElementToDisplay(ProfilePage1.profileHeader2);
							verifyElementTextContains(driver,ProfilePage1.profileHeader2, "Tell us about yourself","profileHeader2 pass","profileHeader2 fail", test,softAssert);
							AssociationPage association = new AssociationPage(driver);
							
							
							ProfilePage1.setProfileData("profile2");
							profile.checkbox();
							ProfilePage1.sameOrg("uncheckorg");
							profile.selectContinue();
				
							/*dashboard.clickOnDashboard();
							waitForElementToDisplay(DashboardPage.quotingEngineTitle);
							verifyElementTextContains(driver,DashboardPage.quotingEngineTitle, "Quoting Engine","quotingEngineTitle pass","quotingEngineTitle fail", test,softAssert);
							dashboard.submitQuoteRequest(DashboardPage.continueButton);
							profile.selectContinue();
							profile.selectContinue();*/
				
							
						}  catch (Error e) {
							e.printStackTrace();
							SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
							ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
							
							}
							catch (Exception e) {
							e.printStackTrace();
							SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
							ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
							


						}
						finally
						{
							reports.flush();
							System.out.println("before soft assert all");
							softAssert.assertAll();
							
						}
	}

	
	@Test(priority = 5, enabled = true)
	public void profilePage4_QA() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("profilepage4");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			ProfilePage1 profile = new ProfilePage1(driver);
			DashboardPage dashboard = new DashboardPage(driver);

			waitForElementToDisplay(ProfilePage1.profileHeader4);
			verifyElementTextContains(driver,ProfilePage1.profileHeader4,"We have a few questions about your group and existing coverage.","profileHeader4 pass","profileHeader4 fail", test,softAssert);
			
			ProfilePage1.setCoveragePosFlow("Affinity_profile4posflow");
			profile.selectContinue();
			
			waitForElementToDisplay(ProfilePage1.profileHeader3);
			verifyElementTextContains(driver,ProfilePage1.profileHeader3, "Tell us when you want coverage","profileHeader3 pass","profileHeader3 fail", test,softAssert);
			profile.setCoverage();
			profile.selectContinue();
			
			BenefitSelectionPage benefits = new BenefitSelectionPage(driver);
			waitForElementToDisplay(BenefitSelectionPage.benefitSelectionHeader);
			verifyElementTextContains(driver,BenefitSelectionPage.benefitSelectionHeader, "Benefit Selections","benefitSelectionHeader pass","benefitSelectionHeader fail", test,softAssert);
			benefits.setBenefitsAffinity();
			

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	
	@Test(priority = 6, enabled = true)
	public void censusData() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("censusdata");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			ProfilePage1 profile = new ProfilePage1(driver);
			

			CensusData censusdata = new CensusData(driver);
			waitForElementToDisplay(CensusData.censusdataHeader);
			verifyElementTextContains(driver,CensusData.censusdataHeader, "Census Data","censusdataHeader pass","censusdataHeader fail", test,softAssert);
		

			CensusData.affinitySetCensusData("affinitycensus");
			
			//CensusData.selectDependents("AddDependents");
			
			/*censusdata.invalidData();
			waitForElementToDisplay(CensusData.alertMsg);
			verifyElementTextContains(CensusData.alertMsg, "Employee’s coverage level does not match the list of dependents provided.", test);
			censusdata.uploadInvalidFiles();*/
			
			censusdata.uploadValidFiles();
			profile.selectContinue();

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}

	@Test(priority = 7, enabled = true)
	public void reviewRequest() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("reviewrequest");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			ProfilePage1 profile = new ProfilePage1(driver);
			DashboardPage dashboard = new DashboardPage(driver);
			CompleteRequestPage request = new CompleteRequestPage(driver);

			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);
			verifyElementTextContains(driver,CompleteRequestPage.reviewQuoteHeader, "Review Quote Request","reviewQuoteHeader pass","reviewQuoteHeader fail", test,softAssert);

			CompleteRequestPage.fetchOrgData();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPOrg();
			verifyContains(getOrgName_PP, getOrgName1, "org verified", test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			/*CompleteRequestPage.fetchsic();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPsic();
			verifyContains(getSIC_PP, sicvalue, "sic verified", test);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchsic2();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPsic2();
			verifyContains(getSIC2_PP, getsic2, "sic2 verified", test);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);*/

			CompleteRequestPage.fetchTaxId();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPTaxID();
			verifyContains(gettaxID_PP, taxidval, "taxid verified", test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchAddress();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPAddress_PP();
			verifyEquals(getaddress_PP, getAddress1, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchCityName();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPCity();
		//	verifyEquals(getcity_PP, getCity1, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchZipCode();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPZipCode();
			verifyEquals(getzipCode_PP, getZipCode1, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

		/*	CompleteRequestPage.fetchName();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			profile.selectContinue();
			Thread.sleep(2000);
			ProfilePage1.fetchPPFirstName();
			// verifyNotEquals(getFirstName_PP, getName, test);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);*/

			/*CompleteRequestPage.fetchPhoneNum();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			profile.selectContinue();
			Thread.sleep(2000);
			ProfilePage1.fetchPPPhoneNum();
			verifyEquals(getPhoneNum_PP, getphoneNum, test);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			

			CompleteRequestPage.fetchAddressLine();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			profile.selectContinue();
			Thread.sleep(2000);
			ProfilePage1.fetchPPAddress_PP();
			verifyEquals(getaddress_PP, getAddressLine, test);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchCity();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			profile.selectContinue();
			Thread.sleep(2000);
			ProfilePage1.fetchPPCity();
			verifyEquals(getcity_PP, getCityName, test);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

		*/
			

			request.completeRequestAffinity();
			waitForElementToDisplay(ProfilePage1.planSelectionTitle);

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}

	@Test(priority = 8, enabled = true)
	public void quoteReviewPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("signuptool");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			QuoteReviewPage quotereview = new QuoteReviewPage(driver);
			waitForElementToDisplay(QuoteReviewPage.planSelectionTitle);
			verifyElementTextContains(driver,QuoteReviewPage.planSelectionTitle, "Quote Review & Plan Selection","planSelectionTitle pass","planSelectionTitle fail", test,softAssert);
			quotereview.calculateEmpCovg();
			quotereview.fetchRates();
			quotereview.calculateSpouseCovg();
			quotereview.fetchRates2();
			quotereview.calVal();
			quotereview.selectPlansAffinity_assoc();
			

			SignUpPage signup = new SignUpPage(driver);
			waitForElementToDisplay(SignUpPage.medicalTitle);
			signup.planDetailsAffinity();

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}

	@Test(priority = 9, enabled = true)
	public void eligibilityPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("eligibility");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			EligibilityPage eligi = new EligibilityPage(driver);
			waitForElementToDisplay(EligibilityPage.eligibilityHeader);
			verifyElementTextContains(driver,EligibilityPage.eligibilityHeader, "Eligibility", "eligibilityHeader pass","eligibilityHeader fail",test,softAssert);
			eligi.createGroups();

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}

	@Test(priority = 10, enabled = true)
	public void contibutionPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("contibution");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			WorkSheetPage comparevalue = new WorkSheetPage(driver);
			waitForElementToDisplay(WorkSheetPage.workSheetHeader);
			verifyElementTextContains(driver,WorkSheetPage.workSheetHeader, "Worksheet","workSheetHeader pass","workSheetHeader fail", test,softAssert);
			comparevalue.worksheetAffinity();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}

	@Test(priority = 11, enabled = true)
	public void payrollPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("payroll");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			PayrollPage payroll = new PayrollPage(driver);
			waitForElementToDisplay(PayrollPage.payrollHeader);
			verifyElementTextContains(driver,PayrollPage.payrollHeader, "Payroll","payrollHeader pass","payrollHeader fail", test,softAssert);
			payroll.selectPayroll();

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	@Test(priority = 12, enabled = true)
	public void employeedataPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			
			test = reports.createTest("employeedata");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			EmployeeDataPage empdata = new EmployeeDataPage(driver);
			waitForElementToDisplay(EmployeeDataPage.empHeader);
			verifyElementTextContains(driver,EmployeeDataPage.empHeader, "Employee Data","empHeader pass","empHeader fail", test,softAssert);
			
			waitForElementToDisplay(EmployeeDataPage.empHeader);
			empdata.uploadInvalidFile();
			verifyElementTextContains(driver,EmployeeDataPage.errorMsg, "There are no HR Administrators indicated on this file. To add HR Administrators, make changes to the file and upload it again.","errorMsg pass","errorMsg fail", test,softAssert);
			
			waitForElementToDisplay(EmployeeDataPage.empHeader);
			empdata.uploadFileAffinity();
			empdata.uploadFile1();
			  
			

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	
	
	@Test(priority = 13, enabled = true)
	public void employeeWebsitePage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("employeewebsite");
			test.assignCategory("regression");
			driver = driverFact.getEventDriver(webdriver, test);

			EmployeeWebsitePage website = new EmployeeWebsitePage(driver);
			waitForElementToDisplay(EmployeeWebsitePage.header);
			verifyElementTextContains(driver,EmployeeWebsitePage.header, "Website Logo","header pass","header fail", test,softAssert);
			website.uploadLogo();

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}

	@Test(priority = 14, enabled = true)
	public void validationPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("validation");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			ValidationPage validate = new ValidationPage(driver);
			waitForElementToDisplay(ValidationPage.validationHeader);
			verifyElementTextContains(driver,ValidationPage.validationHeader, "Validation","validationHeader pass","validationHeader fail", test,softAssert);

			/*validate.resetHandling();

			SignUpPage signup = new SignUpPage(driver);
			waitForElementToDisplay(SignUpPage.medicalTitle);
			verifyElementTextContains(SignUpPage.medicalTitle, "Medical", test);
			signup.planDetailsAffinity();

			EligibilityPage eligi = new EligibilityPage(driver);
			waitForElementToDisplay(EligibilityPage.eligibilityHeader);
			verifyElementTextContains(EligibilityPage.eligibilityHeader, "Eligibility", test);
			eligi.eligibility();

			WorkSheetPage comparevalue = new WorkSheetPage(driver);
			waitForElementToDisplay(WorkSheetPage.workSheetHeader);
			verifyElementTextContains(WorkSheetPage.workSheetHeader, "Worksheet", test);
			comparevalue.empContribution();
			comparevalue.clickSave();

			PayrollPage payroll = new PayrollPage(driver);
			waitForElementToDisplay(PayrollPage.payrollHeader);
			verifyElementTextContains(PayrollPage.payrollHeader, "Payroll", test);
			comparevalue.clickSave();

			EmployeeDataPage empdata = new EmployeeDataPage(driver);
			waitForElementToDisplay(EmployeeDataPage.empHeader);
			verifyElementTextContains(EmployeeDataPage.empHeader, "Employee Data", test);
			comparevalue.clickSave();

			EmployeeWebsitePage website = new EmployeeWebsitePage(driver);
			waitForElementToDisplay(EmployeeWebsitePage.header);
			verifyElementTextContains(EmployeeWebsitePage.header, "Website Logo", test);
			comparevalue.clickSave();
			
		//	validate.clickDoneButton();

			validate.getURL();

			ProxyPage proxy = new ProxyPage(driver);
			proxy.proxyUser1(); 
			proxy.clickEmp();*/
			WorkSheetPage comparevalue = new WorkSheetPage(driver);
			comparevalue.clickSave();

			RequiredDocumentPage requireddoc = new RequiredDocumentPage(driver);
			waitForElementToDisplay(RequiredDocumentPage.requiredDocHeader);
			verifyElementTextContains(driver,RequiredDocumentPage.requiredDocHeader, "Required Documents","requiredDocHeader pass","requiredDocHeader fail", test,softAssert);
			requireddoc.uploadFile();
			
			

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}

	@Test(priority = 15, enabled = true)
	public void confirmationPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("confirmation page");
			test.assignCategory("regression");
			driver = driverFact.getEventDriver(webdriver, test);

			ConfirmationPage confirm = new ConfirmationPage(driver);
			waitForElementToDisplay(ConfirmationPage.termsConditionHeader);
			verifyElementTextContains(driver,ConfirmationPage.termsConditionHeader, "Terms and conditions","termsConditionHeader pass","termsConditionHeader fail", test,softAssert);
			
			confirm.acceptTermsCondition();
			
			

		}  catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
		@Test(priority = 16, enabled = true)
		public void finalPage() throws Exception {
			ExtentTest test=null;
	    	//ExtentTest test=null;
			 SoftAssert softAssert=new SoftAssert();
			try {
				Driver driverFact = new Driver();
				TC2_QuotingTool mes = new TC2_QuotingTool("DEVDOOR");
				
				test = reports.createTest("Deleting auth and delivery entries");
				test.assignCategory("regression");
				webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
				driver = driverFact.getEventDriver(webdriver, test);
				

				
				DashboardPage dashboard = new DashboardPage(driver);
				waitForElementToDisplay(DashboardPage.setupCompletionMsg);
				verifyElementTextContains(driver,DashboardPage.setupCompletionMsg, "Congratulations! Your set-up is now complete!","setupCompletionMsg fail","setupCompletionMsg fail", test,softAssert);
			
				
				
				SelfRegPage self=new SelfRegPage(driver);
				self.getCompanyID();
				self.authQuery();
				self.deliveryQuery();


			
			}  catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
				ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				
				}
				catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
				ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				


			}
			finally
			{
				reports.flush();
				System.out.println("before soft assert all");
				softAssert.assertAll();
				driver.quit();
			}
}
		
}