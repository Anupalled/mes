package regression.mes_affinity.tests;




import static driverfactory.Driver.waitForElementToDisplay;
import static pages.MES.ProfilePage1.value;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.CompleteRequestPage;
import pages.MES.ConsultantPortalLoginPage;
import pages.MES.HeaderPage;
import pages.MES.SelfRegPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_SelfRegistration extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC1_SelfRegistration(String appName) {
		super(appName);

}
	@BeforeClass
	public void beforeClass() throws Exception {

		TC1_SelfRegistration portal = new TC1_SelfRegistration("ConsultantPortal");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");

	}
	

	@Test(priority = 1, enabled = false)
	public void AssociationSetup_CP() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("consultantportal");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);
			
			
			
			SelfRegPage self=new SelfRegPage(driver);
			self.selfRegisteration();
			self.fetchUser_ID();
			self.user_application();

		
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
}
	
}


