package regression.mes_affinity.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static pages.MES.ProfilePage1.value;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.CompleteRequestPage;
import pages.MES.ConsultantPortalLoginPage;
import pages.MES.HeaderPage;
import pages.MES.LoginPage;
import pages.MES.ProfilePage1;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3_ConsultantPortal extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC3_ConsultantPortal(String appName) {
		super(appName);

}
	

	@Test(priority = 19, enabled = false)
	public void consultantPortalPage_login() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			Driver driverFact = new Driver();
			TC3_ConsultantPortal portal = new TC3_ConsultantPortal("ConsultantPortal");
			
			test = reports.createTest("Dashboard confirmation page");
			test.assignCategory("regression");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
		
			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			driver.navigate().refresh();
			header.selectHeaders(HeaderPage.associationTab);
			
			
			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
			association.selectAssociation(AssociationPage.assoc);
			waitForElementToDisplay(AssociationPage.profile1);
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			//driver.close();
		}
	}

	@Test(priority = 20, enabled = false)
	public void verfiyProxy() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			Driver driverFact = new Driver();
			TC3_ConsultantPortal portal = new TC3_ConsultantPortal("ConsultantPortal");
			
			test = reports.createTest("Dashboard confirmation page");
			test.assignCategory("regression");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			
			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			driver.navigate().refresh();
			Thread.sleep(10000);
			header.selectHeaders(HeaderPage.associationTab);
			
			
			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			association.selectAssociation(AssociationPage.assoc);
			Thread.sleep(10000);
			waitForElementToDisplay(AssociationPage.profile1);
			
			
			association.selectAssociation(AssociationPage.memebercompy);
			waitForElementToDisplay(AssociationPage.assocDetails);
			AssociationPage.searchMembers(value);
			
			CompleteRequestPage request = new CompleteRequestPage(driver);
			request.getCurrentURL();
			request.proxyHRPortal();
			
			
			request.proxyMemberCompany();
			driver.close();
			
			request.proxyAffinityDashboard();
			waitForElementToDisplay(CompleteRequestPage.affinityDashboardHeader);
			verifyElementTextContains(driver,CompleteRequestPage.affinityDashboardHeader, " Affinity 365+ Dashboard ","affinityDashboardHeader pass","affinityDashboardHeader fail", test,softAssert);
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
		
	
	}
	
}
