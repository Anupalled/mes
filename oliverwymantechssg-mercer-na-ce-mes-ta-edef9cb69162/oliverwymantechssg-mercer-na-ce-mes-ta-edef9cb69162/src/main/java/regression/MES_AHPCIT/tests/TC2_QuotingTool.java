package regression.MES_AHPCIT.tests;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyContains;
import static verify.SoftAssertions.verifyElementTextContains;
import static verify.SoftAssertions.verifyEquals;
import static verify.SoftAssertions.verifyNotEquals;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import pages.MES.CensusData;
import static pages.MES.CensusData.*;
import pages.MES.CompleteRequestPage;
import pages.MES.ConsultantPortalLoginPage;

import static pages.MES.CompleteRequestPage.*;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CensusData;
import pages.MES.CompleteRequestPage;

import static driverfactory.Driver.waitForElementToDisplay;
import static pages.MES.CompleteRequestPage.AssociationID;
import static pages.MES.CompleteRequestPage.getAddress1;
import static pages.MES.CompleteRequestPage.getAvgEmployee;
import static pages.MES.CompleteRequestPage.getCity1;
import static pages.MES.CompleteRequestPage.getEligibleEmp;
import static pages.MES.CompleteRequestPage.getEmailId;
import static pages.MES.CompleteRequestPage.getName;
import static pages.MES.CompleteRequestPage.getOrgName1;
import static pages.MES.CompleteRequestPage.getZipCode1;
import static pages.MES.CompleteRequestPage.getphoneNum;
import static pages.MES.CompleteRequestPage.getsic2;
import static pages.MES.CompleteRequestPage.getstartDate;
import static pages.MES.CompleteRequestPage.sicvalue;
import static pages.MES.CompleteRequestPage.taxidval;
import static pages.MES.ProfilePage1.avgEmp_PP;
import static pages.MES.ProfilePage1.eligiEmp_PP;
import static pages.MES.ProfilePage1.getEmailId_PP;
import static pages.MES.ProfilePage1.getFirstName_PP;
import static pages.MES.ProfilePage1.getOrgName_PP;
import static pages.MES.ProfilePage1.getPhoneNum_PP;
import static pages.MES.ProfilePage1.getSIC2_PP;
import static pages.MES.ProfilePage1.getSIC_PP;
import static pages.MES.ProfilePage1.getaddress_PP;
import static pages.MES.ProfilePage1.getcity_PP;
import static pages.MES.ProfilePage1.getstartdate_PP;
import static pages.MES.ProfilePage1.gettaxID_PP;
import static pages.MES.ProfilePage1.getzipCode_PP;

import pages.MES.DashboardPage;
import pages.MES.LoginPage;
import pages.MES.ProfilePage1;
import pages.MES.SelfRegPage;
import pages.affinityDashboard.MfaPage;
import smoke.mes.tests.TC1_QuotingTool;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC2_QuotingTool extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public static String username="";
	public static String password="";

	public TC2_QuotingTool(String appName) {				
		
		super(appName);

	}
	

	@Test(priority = 1, enabled = false)
	public void MESlogin() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			
			
			Driver driverFact = new Driver();
			TC2_QuotingTool mes = new TC2_QuotingTool("AHPCIT");
			test = reports.createTest("LoginQTST");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			

			SelfRegPage self=new SelfRegPage(driver);
			self.musedeletion();
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	@Test(priority = 2, enabled = true)
	public void dashboardPage() throws Exception {
		ExtentTest test=null;
    	
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			
			Driver driverFact = new Driver();
			citProps.load(input);
			username = citProps.getProperty("AHPCIT_username");
			password = citProps.getProperty("AHPCIT_password");
			TC2_QuotingTool mes = new TC2_QuotingTool("AHPCIT");
			
			test = reports.createTest("dashboardPage");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			
			System.out.println("BaseURL is: " + BASEURL);
			
			LoginPage login=new LoginPage(driver);
			login.loginAHPCIT(USERNAME, PASSWORD);
			
			String[] outlookDetails = {props.getProperty("outlook1_url"),props.getProperty("outlook1_username"),props.getProperty("outlook1_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFACIT(driver, outlookDetails);
			
			DashboardPage dashboard = new DashboardPage(driver);
			
			dashboard.submitQuoteRequest(DashboardPage.continueButton);
			
			ProfilePage1 profile = new ProfilePage1(driver);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			verifyElementTextContains(driver,ProfilePage1.profileHeader1, "Profile","profileHeader1 pass","profileHeader1 fail", test,softAssert);
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}


	@Test(priority = 3, enabled = true)
	public void profilepage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("quotingtool");
			test.assignCategory("smoke");
			
			driver = driverFact.getEventDriver(webdriver, test);
			
			System.out.println("BaseURL is: " + BASEURL);
		
			
			

			ProfilePage1 profile = new ProfilePage1(driver);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			verifyElementTextContains(driver,ProfilePage1.profileHeader1, "Profile","profileHeader1 pass","profileHeader1 fail", test,softAssert);
			
			profile.getOrganisationName();
			ProfilePage1.setOrgNameCIT("StateAHPCIT");

			profile.selectContinue();
		
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	@Test(priority = 4, enabled = true)
	public void profilepage2() throws Exception {
		ExtentTest test=null;
    	
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("profilepage2");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			DashboardPage dashboard = new DashboardPage(driver);
			ProfilePage1 profile = new ProfilePage1(driver);
			
			
			waitForElementToDisplay(ProfilePage1.profileHeader2);
			verifyElementTextContains(driver,ProfilePage1.profileHeader2, "Tell us about yourself","profileHeader2 pass","profileHeader2 fail", test,softAssert);
			ProfilePage1.setProfileData("profile2");
			profile.checkbox();
			ProfilePage1.sameOrg("uncheckorg");
			profile.selectContinue();



		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	
	@Test(priority = 5, enabled = true)
	public void profilepage4() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("profilepage4");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			DashboardPage dashboard = new DashboardPage(driver);
			ProfilePage1 profile = new ProfilePage1(driver);
			
			waitForElementToDisplay(ProfilePage1.profileHeader4);
			verifyElementTextContains(driver,ProfilePage1.profileHeader4,"We have a few questions about your group and existing coverage.","profileHeader4 pass","profileHeader4 fail", test,softAssert);
			ProfilePage1.setCoverage("AHP_profile4");
			profile.selectContinue();

			


		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	
	@Test(priority = 6, enabled = true)
	public void profilepage3() throws Exception {
		ExtentTest test=null;
    	
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("profilepage3");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			DashboardPage dashboard = new DashboardPage(driver);
			ProfilePage1 profile = new ProfilePage1(driver);
			
			waitForElementToDisplay(ProfilePage1.profileHeader3);
			verifyElementTextContains(driver,ProfilePage1.profileHeader3, "Tell us when you want coverage","profileHeader3 pass","profileHeader3 fail", test,softAssert);
			profile.setCoverage();
			profile.selectContinue();
			
			
			BenefitSelectionPage benefits = new BenefitSelectionPage(driver);
			waitForElementToDisplay(BenefitSelectionPage.benefitSelectionHeader);
			verifyElementTextContains(driver,BenefitSelectionPage.benefitSelectionHeader, "Benefit Selections","benefitSelectionHeader pass","benefitSelectionHeader fail", test,softAssert);
			benefits.setBenefits();


		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			
		}
	}
	
	@Test(priority = 7, enabled = true)
	public void censusdata() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("censusdata");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			DashboardPage dashboard = new DashboardPage(driver);
			ProfilePage1 profile = new ProfilePage1(driver);
			
			CensusData censusdata = new CensusData(driver);
			Thread.sleep(5000);
			waitForElementToDisplay(CensusData.censusdataHeader);
			verifyElementTextContains(driver,CensusData.censusdataHeader, "Census Data","censusdataHeader pass","censusdataHeader fail", test,softAssert);
			//CensusData.downloadFile1();
			CensusData.setCensusData("census");
			CensusData.selectDependents_ahp("AddDependents_ahp");
			censusdata.uploadInvalidFiles_ahp();
			censusdata.uploadValidFiles_ahp();
			profile.selectContinue();
			
			

		
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
		
		}
	}
	
	@Test(priority = 8, enabled = true)
	public void reviewrequest() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("reviewrequest");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			
			DashboardPage dashboard = new DashboardPage(driver);
			ProfilePage1 profile = new ProfilePage1(driver);
			
			CompleteRequestPage request = new CompleteRequestPage(driver);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);
			verifyElementTextContains(driver,CompleteRequestPage.reviewQuoteHeader, "Review Quote Request","reviewQuoteHeader pass","reviewQuoteHeader fail", test,softAssert);

			CompleteRequestPage.fetchOrgData();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPOrg();
			verifyContains(getOrgName_PP, getOrgName1, "org verified", test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchsic();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPsic();
			verifyContains(getSIC_PP, sicvalue, "sic verified", test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchsic2();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPsic2();
			verifyContains(getSIC2_PP, getsic2, "sic2 verified", test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchTaxId();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPTaxID();
			verifyContains(gettaxID_PP, taxidval, "taxid verified", test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchAddress();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPAddress_PP();
			verifyEquals(getaddress_PP, getAddress1, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			
			CompleteRequestPage.fetchCityName();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPCity();
			verifyEquals(getcity_PP, getCity1, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchZipCode();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			ProfilePage1.fetchPPZipCode();
			verifyEquals(getzipCode_PP, getZipCode1, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			/*CompleteRequestPage.fetchCity();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			profile.selectContinue();
			Thread.sleep(5000);
			ProfilePage1.fetchPPCity();
		//	verifyEquals(getcity_PP, getCityName, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			CompleteRequestPage.fetchZipCode1();
			ProfilePage1.switchProfile1(ProfilePage1.profile);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			profile.selectContinue();
			Thread.sleep(5000);
			ProfilePage1.fetchPPZipCode();*/
			//verifyEquals(getzipCode_PP, getZipCode, test,softAssert);
			request.switchRequestPage(CompleteRequestPage.completeRequestTab);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteHeader);

			
			
			request.completeRequest();
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
	
}