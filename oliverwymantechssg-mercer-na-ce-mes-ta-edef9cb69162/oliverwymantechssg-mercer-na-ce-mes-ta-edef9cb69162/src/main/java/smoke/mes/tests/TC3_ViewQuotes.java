package smoke.mes.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static driverfactory.Driver.waitForElementToDisappear;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.BenefitSelectionPage;
import pages.MES.CensusData;
import pages.MES.CompleteRequestPage;
import pages.MES.DashboardPage;
import pages.MES.LoginPage;
import pages.MES.MemberRates;
import pages.MES.PreliminaryQuotes;
import pages.MES.ProfilePage1;
import pages.MES.SubmitFinalQuote;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC3_ViewQuotes extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC3_ViewQuotes(String appName) {
		super(appName);

	}

	@BeforeClass
	public void beforeClass() throws Exception {

		TC3_ViewQuotes mes = new TC3_ViewQuotes("DEVDOOR");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");

	}

	@Test(priority = 5, enabled = true)
	public void viewquotes() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("viewquotes");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			

			DashboardPage dashboard = new DashboardPage(driver);
			
			//waitForElementToDisappear(By.cssSelector("div[class='mos-app-loader']"));
			waitForElementToDisplay(DashboardPage.reviewQuoteHeader);
			verifyElementTextContains(driver,DashboardPage.reviewQuoteHeader, "Continue / Review Quote","reviewQuoteHeader pass","reviewQuoteHeader fail", test,softAssert);
			dashboard.myQuotes();

			PreliminaryQuotes prelimquotes = new PreliminaryQuotes(driver);
			waitForElementToDisplay(PreliminaryQuotes.preliminaryHeader);
			verifyElementTextContains(driver,PreliminaryQuotes.preliminaryHeader, "Preliminary Quote Review","preliminaryHeader pass","preliminaryHeader fail", test,softAssert);
			prelimquotes.reviewQuotes();

			SubmitFinalQuote finalquote = new SubmitFinalQuote(driver);
			waitForElementToDisplay(SubmitFinalQuote.finalQuoteHeader);
			verifyElementTextContains(driver,SubmitFinalQuote.finalQuoteHeader, "Submit Final Quote Request","finalQuoteHeader pass","finalQuoteHeader fail", test,softAssert);
			finalquote.draganddrop();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
}
