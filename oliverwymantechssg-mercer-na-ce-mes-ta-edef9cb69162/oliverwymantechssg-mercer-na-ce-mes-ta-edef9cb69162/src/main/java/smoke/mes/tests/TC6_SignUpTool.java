package smoke.mes.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.BenefitSelectionPage;
import pages.MES.CensusData;
import pages.MES.CompleteRequestPage;
import pages.MES.ConfirmationPage;
import pages.MES.DashboardPage;
import pages.MES.EligibilityPage;
import pages.MES.EmployeeDataPage;
import pages.MES.EmployeeWebsitePage;
import pages.MES.LoginPage;
import pages.MES.MemberRates;
import pages.MES.PayrollPage;
import pages.MES.PreliminaryQuotes;
import pages.MES.ProfilePage1;
import pages.MES.QuestionnariePage;
import pages.MES.QuoteReviewPage;
import pages.MES.RequiredDocumentPage;
import pages.MES.SelfRegPage;
import pages.MES.SignUpPage;
import pages.MES.SubmitFinalQuote;
import pages.MES.ValidationPage;
import pages.MES.WorkSheetPage;
import regression.mes_affinity.tests.TC2_QuotingTool;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC6_SignUpTool extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC6_SignUpTool(String appName) {
		super(appName);

	}

	
	@Test(priority = 8, enabled = true)
	public void loginsignuptool() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("signuptool");
			test.assignCategory("smoke");
			
			TC6_SignUpTool mes = new TC6_SignUpTool("DEVDOOR");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			
			QuoteReviewPage quotereview = new QuoteReviewPage(driver);
			quotereview.selectPlans();

			SignUpPage signup = new SignUpPage(driver);
			waitForElementToDisplay(SignUpPage.signupHeader);
			verifyElementTextContains(driver,SignUpPage.signupHeader, "Sign Up","signupHeader pass","signupHeader fail",test,softAssert);
			signup.getPlanDetails();

			EligibilityPage eligi = new EligibilityPage(driver);
			waitForElementToDisplay(EligibilityPage.eligibilityHeader);
			verifyElementTextContains(driver,EligibilityPage.eligibilityHeader, "Eligibility","eligibilityHeader pass","eligibilityHeader fail", test,softAssert);
			eligi.createGroups();

			QuestionnariePage question = new QuestionnariePage(driver);
			waitForElementToDisplay(QuestionnariePage.questionnaireTitle);
			verifyElementTextContains(driver,QuestionnariePage.questionnaireTitle, "Questionnaire","questionnaireTitle pass","questionnaireTitle fail", test,softAssert);
			question.questionnarie();

			WorkSheetPage comparevalue = new WorkSheetPage(driver);
			waitForElementToDisplay(WorkSheetPage.workSheetHeader);
			verifyElementTextContains(driver,WorkSheetPage.workSheetHeader, "Worksheet","workSheetHeader pass","workSheetHeader fail", test,softAssert);
			comparevalue.workSheet();

			PayrollPage payroll = new PayrollPage(driver);
			waitForElementToDisplay(PayrollPage.payrollHeader);
			verifyElementTextContains(driver,PayrollPage.payrollHeader, "Payroll","payrollHeader pass","payrollHeader  fail", test,softAssert);
			payroll.selectPayroll();
			
			EmployeeDataPage empdata = new EmployeeDataPage(driver);
			  waitForElementToDisplay(EmployeeDataPage.empHeader);
			  verifyElementTextContains(driver,EmployeeDataPage.empHeader, "Employee Data","empHeader pass","empHeader fail", test,softAssert);
			  empdata.uploadFile_ahp();
			  empdata.uploadFile1();
			  
			  EmployeeWebsitePage website = new EmployeeWebsitePage(driver);
			  waitForElementToDisplay(EmployeeWebsitePage.header);
			  verifyElementTextContains(driver,EmployeeWebsitePage.header, "Website Logo", "header pass","header fail",test,softAssert);
			  website.uploadLogo();
			  
			  ValidationPage validate = new ValidationPage(driver);
				waitForElementToDisplay(ValidationPage.validationHeader);
				verifyElementTextContains(driver,ValidationPage.validationHeader, "Validation","validationHeader pass","validationHeader fail", test,softAssert);
				comparevalue.clickSave();

				RequiredDocumentPage requireddoc = new RequiredDocumentPage(driver);
				waitForElementToDisplay(RequiredDocumentPage.requiredDocHeader);
				verifyElementTextContains(driver,RequiredDocumentPage.requiredDocHeader, "Required Documents","requiredDocHeader pass","requiredDocHeader fail", test,softAssert);
				requireddoc.uploadFile();
				
				
				ConfirmationPage confirm = new ConfirmationPage(driver);
				Thread.sleep(3000);
				waitForElementToDisplay(ConfirmationPage.termsConditionHeader);
				verifyElementTextContains(driver,ConfirmationPage.termsConditionHeader, "Terms and conditions","termsConditionHeader pass","termsConditionHeader fail", test,softAssert);
				confirm.acceptTermsCondition();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	
	}
	
	@Test(priority = 9, enabled = true)
	public void entryDeletion() throws Exception {
		ExtentTest test=null;
    	
		 SoftAssert softAssert=new SoftAssert();
		try {

			Driver driverFact = new Driver();
			TC2_QuotingTool mes = new TC2_QuotingTool("DEVDOOR");
			
			test = reports.createTest("Delete Auth and Delivery entries");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			
			System.out.println("BaseURL is: " + BASEURL);
			DashboardPage dashboard = new DashboardPage(driver);
			waitForElementToDisplay(DashboardPage.setupCompletionMsg);
			verifyElementTextContains(driver,DashboardPage.setupCompletionMsg, "Congratulations! Your set-up is now complete!","setupCompletionMsg pass","setupCompletionMsg fail", test,softAssert);
		
			
			SelfRegPage self=new SelfRegPage(driver);
			self.getCompanyID();
			self.authQuery();
			self.deliveryQuery();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
}
