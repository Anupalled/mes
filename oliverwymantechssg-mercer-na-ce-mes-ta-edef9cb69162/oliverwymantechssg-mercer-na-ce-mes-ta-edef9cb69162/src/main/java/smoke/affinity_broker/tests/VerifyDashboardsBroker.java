package smoke.affinity_broker.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyEquals;
import static driverfactory.Driver.clickElement;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementContains;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.MfaPage;
import pages.hri.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class VerifyDashboardsBroker extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;

	public VerifyDashboardsBroker(String appName) {
		super(appName);	
	}

	@Test(enabled=false , priority= 1)
	public void verifyViewDocumentsSection() throws Exception	{		
		new VerifyDashboardsBroker("MESBroker");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify View Documents Section");
			test.assignCategory("Smoke");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFABroker(driver, outlookDetails);


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);

			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			affinityPage.navigateToViewDocuments(driver);
			
			if(affinityPage.documentsHeader.getText().equalsIgnoreCase("DOCUMENTS")) {
				test.log(Status.PASS, "Navigated to documentsPage");
				verifyElementContains(affinityPage.documentsHeader, "DOCUMENTS", " Header text matched ", test,softAssert);
				
			}else {
				System.out.println(affinityPage.documentsHeader.getText());
			}
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
	}
	
	@Test(enabled=true , priority= 2)
	public void verifyMemberCompanyStatus() throws Exception	{		
		new VerifyDashboardsBroker("MESBroker");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Member Company Status Broker");
			test.assignCategory("Smoke");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFABroker(driver, outlookDetails);


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);
			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			affinityPage.navigateToPendingEmployers();
			waitForElementToDisplay(affinityPage.PendingEmployerHeader);
			verifyElementContains(affinityPage.PendingEmployerHeader, "Pending Employers", "Navigated to Pending Employers Page", test,softAssert);
			affinityPage.filterMemberCompanies("INITIATED");
			if(affinityPage.statusInitiated.size()==0) {
				test.log(Status.PASS, "The Initiated filter has 0 MC");
			}
			for(WebElement e : affinityPage.statusInitiated) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(206, 61, 149, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in Initiated State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in Initiated State");
					System.out.println(e.getCssValue("color")+"IniTiated");
				}				
			}
			
			affinityPage.filterMemberCompanies("KNOCKED OUT");
			if(affinityPage.statusKnockedOutBroker.size()==0) {
				test.log(Status.PASS, "The Knocked out filter has 0 MC");
			}
			
			for(WebElement e : affinityPage.statusKnockedOutBroker) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(0, 168, 200, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in KNOCKED OUT State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in KNOCKED OUT State");
					System.out.println(e.getCssValue("color")+"Knocked Out");
				}				
			}
			
			affinityPage.filterMemberCompanies("IN QUOTING");
			if(affinityPage.statusInQuotingBroker.size()==0) {
				test.log(Status.PASS, "The In Quoting filter has 0 MC");
			}
			
			for(WebElement e : affinityPage.statusInQuotingBroker) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(244, 129, 50, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in IN QUOTING State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in IN QUOTING State");
					System.out.println(e.getCssValue("color")+"Quoting");
				}				
			}
			
			affinityPage.filterMemberCompanies("COMPLETING SETUP");			
			if(affinityPage.statusCompletingSetupBroker.size()==0) {
				test.log(Status.PASS, "The Completing Setup filter has 0 MC");
			}
			
			for(WebElement e : affinityPage.statusCompletingSetupBroker) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(15, 182, 148, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in COMPLETING SETUP State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in COMPLETING SETUP State");
					System.out.println(e.getCssValue("color")+"Completing");
				}				
			}
			affinityPage.filterMemberCompanies("CLOSED LOST");
			if(affinityPage.statusClosed.size()==0) {
				test.log(Status.PASS, "The closed Lost filter has 0 MC");
			}
			
			for(WebElement e : affinityPage.statusClosed) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(100, 112, 128, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in CLOSED LOST State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in CLOSED LOST State");
					System.out.println(e.getCssValue("color")+"Closed");
				}				
			}			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
//			driver.close();
		}

	}
	
	
	
	@Test(enabled=false , priority= 4)
	public void verifyKnockedOutMemberCompanyList() throws Exception	{		
		new VerifyDashboardsBroker("MESDB");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Knocked Out Member Company List");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);
			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			affinityPage.navigateToPendingEmployers();
			waitForElementToDisplay(affinityPage.PendingEmployerHeader);
			verifyElementContains(affinityPage.PendingEmployerHeader, "Pending Employers", "Navigated to Pending Employers Page", test,softAssert);
			
			
			affinityPage.filterMemberCompanies("KNOCKED OUT");
			for(WebElement e : affinityPage.statusKnockedOut) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(0, 168, 200, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in KNOCKED OUT State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in KNOCKED OUT State");
					System.out.println(e.getCssValue("color")+"Knocked Out");
				}				
			}
			String knockedOutNum = affinityPage.knockedOutHeaderNumber.getText();
			String footerNum = affinityPage.footerShowingResult.getText().substring(19, 22);
			//verifyEquals(knockedOutNum, footerNum, test);
			
					
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
	//		driver.close();
		}

	}
	
	@Test(enabled=false , priority= 5)
	public void verifyAffiliateIcon() throws Exception	{		
		new VerifyDashboardsBroker("MESDB");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Knocked Out Member Company List");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);
			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			affinityPage.navigateToActiveEmployers();
			waitForElementToDisplay(affinityPage.ActiveEmployerHeader);
			verifyElementContains(affinityPage.ActiveEmployerHeader, "ACTIVE EMPLOYERS", "Navigated to Active Employers Page", test,softAssert);
			waitForElementToDisplay(affinityPage.affiliateIcon);
			verifyElementIsPresent(affinityPage.affiliateIcon, test, "Affiliate Icon",softAssert);
			verifyElementIsPresent(affinityPage.affiliateEmployer, test, "Affiliate Employer",softAssert);
			verifyElementIsPresent(affinityPage.nonAffiliateEmployer, test, "NON Affiliate Employer",softAssert);
			//hoverOverElement(driver, affinityPage.affiliateIcon);
			if(affinityPage.affiliateEmployer.isEnabled()) {
				test.log(Status.PASS, "Affiliate Employer is not clickable");
			}else {
				test.log(Status.FAIL, "Affiliate Employer is  clickable");
			}
			
			if(affinityPage.nonAffiliateEmployer.isEnabled()) {
				test.log(Status.PASS, "Non Affiliate Employer is clickable");
			}else {
				test.log(Status.FAIL, "Non Affiliate Employer is not clickable");
			}
			
			clickElement(affinityPage.nonAffiliateEmployer);
			waitForElementToDisplay(affinityPage.EmployerProfileHeader);
			verifyElementContains(affinityPage.EmployerProfileHeader, "EMPLOYER PROFILE", " Navigated to employer profile page", test,softAssert);
			waitForElementToDisplay(affinityPage.nonAffiliateOrganisationName);
			verifyElementContains(affinityPage.nonAffiliateOrganisationName, "!HSAGk5!", "Organisation Name is displayed", test,softAssert);	
			
			
			
					
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
}
