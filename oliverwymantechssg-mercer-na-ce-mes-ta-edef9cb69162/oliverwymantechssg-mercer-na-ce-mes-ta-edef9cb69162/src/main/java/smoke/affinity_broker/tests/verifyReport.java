package smoke.affinity_broker.tests;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.MfaPage;
import pages.hri.LoginPage;
import regression.affinity.tests.VerifyAddAndDeleteUser;
import utilities.InitTests;
import verify.SoftAssertions;
public class verifyReport extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	public static ExtentTest test;
	String companyId;
	public static String downloadPath1;
	public verifyReport(String appName) {
		super(appName);	
	}


@Test(enabled=false , priority= 3)
public void verifyUserLogin() throws Exception	{		
	new VerifyAddAndDeleteUser("MESBroker");
	ExtentTest test=null;
	//ExtentTest test=null;
	 SoftAssert softAssert=new SoftAssert();
	try {
		webdriver = driverFact.initWebDriver(BASEURL,"CHROME","local","");
		test = reports.createTest("Verify Report");
		test.assignCategory("Regression");
		driver=driverFact.getEventDriver(webdriver,test);

		LoginPage login =  new LoginPage(driver);
		login.login(USERNAME,PASSWORD);	

		String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
		MfaPage mfa = new MfaPage(driver);			
		mfa.NavigateToOutlookAndGetMFABroker(driver, outlookDetails);

		
		AffinityDashboard affinityPage = new AffinityDashboard(driver);
		waitForElementToDisplay(affinityPage.affinityDashboardHeader);
		
		if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
			verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
			test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

		}else {
			test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
			System.out.println(affinityPage.affinityDashboardHeader.getText());
		}

		affinityPage.companyButtonEnabled();
		 downloadPath1 = "c:/users/temp/downloads";
				 //System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator + "downloadFiles";
		 AffinityDashboard.deleteFilesInPath(downloadPath1);
		
		affinityPage.reporting();
		
		//affinityPage.downloadReportFile(downloadPath1);
		affinityPage.isfiledownloaded();
		AffinityDashboard.setData();
		
		affinityPage.navigateToPendingEmployers();
		Thread.sleep(8000);
		for(int i=0;i<24;i=i+4) {
			System.out.print(affinityPage.employerTableItems.get(i).getText());
			System.out.print("="+affinityPage.empNames.get(i)+"\n");
			if(affinityPage.empNames.contains(affinityPage.employerTableItems.get(i).getText())){
				test.log(Status.PASS, "Value matched "+affinityPage.employerTableItems.get(i).getText()+" " +affinityPage.employerTableItems.get(i).getText());
			}
			System.out.println(affinityPage.empNames.contains(affinityPage.employerTableItems.get(i).getText()));
			
		}
		
		
		//affinityPage.pendingMCName();
		AffinityDashboard.deleteFilesInPath(downloadPath1);


	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		
		}
		catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		


	}
	finally
	{
		reports.flush();
		System.out.println("before soft assert all");
		softAssert.assertAll();
		//driver.close();
	}

}
}


