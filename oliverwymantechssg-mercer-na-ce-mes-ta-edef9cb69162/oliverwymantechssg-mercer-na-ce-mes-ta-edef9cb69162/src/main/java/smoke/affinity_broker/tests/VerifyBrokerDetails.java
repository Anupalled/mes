package smoke.affinity_broker.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;
import static verify.SoftAssertions.verifyElementIsPresent;

import java.util.Hashtable;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.ConsultantPortal;
import pages.affinityDashboard.MfaPage;
import pages.hri.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class VerifyBrokerDetails extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;

	public VerifyBrokerDetails(String appName) {
		super(appName);	
	}

	@Test(enabled=true , priority= 1)
	public void verifyUserLogin() throws Exception	{		
		new VerifyBrokerDetails("HRI");	
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify broker details");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	
			
			ConsultantPortal cp = new ConsultantPortal(driver);
			cp.waitForConsultantPortalHome();	
			verifyElementIsPresent(cp.homeIcon, test, "Consultant portal homeIcon",softAssert);
			cp.navigateToMesUsers(driver);

			if(cp.mesUsersHeader.getText().equalsIgnoreCase("MES Users")) {
				test.log(Status.PASS, "MES User Page Loaded");
				verifyElementIsPresent(cp.addNewUser, test, "Add new user");
			}else {
				test.log(Status.FAIL, "MES User Page Loading Failed");
			}			
			cp.searchUser(driver, "shretzz+broker@gmail.com");
			
			if(cp.searchResultBroker.getText().equalsIgnoreCase("Shreyash Kapadi")) {
				test.log(Status.PASS, "User added succesfully");
				verifyElementIsPresent(cp.searchResultBroker, test, "User Aurojit Das",softAssert);
			}else {
				test.log(Status.FAIL, "User creation failed");
			}
			
			clickElement(cp.editUserButton);
			System.out.println("clicking edit button");
			cp.waitForPageLoad();
			
			Hashtable<String, String> brokerDetails = new Hashtable<String, String>();
			brokerDetails.put("firstName", cp.firstName.getAttribute("value"));
			brokerDetails.put("lastName", cp.lastName.getAttribute("value"));
			brokerDetails.put("addressLine1", cp.addressLine1.getAttribute("value"));
			brokerDetails.put("addressLine2", cp.addressLine2.getAttribute("value"));
			brokerDetails.put("zip", cp.zip.getAttribute("value"));
			brokerDetails.put("phone", cp.phone.getAttribute("value"));
			brokerDetails.put("email", cp.emailAddress.getAttribute("value"));
			
			System.out.println(brokerDetails.get("firstName"));
			System.out.println(brokerDetails.get("lastName"));
			System.out.println(brokerDetails.get("addressLine1"));
			System.out.println(brokerDetails.get("addressLine2"));
			System.out.println(brokerDetails.get("zip"));
			System.out.println(brokerDetails.get("phone"));
			System.out.println(brokerDetails.get("email"));
			
			driver.get(props.getProperty("MESBroker_baseurl"));
			
			login.login(props.getProperty("MESBroker_username"), props.getProperty("MESBroker_password"));
			
			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFABroker(driver, outlookDetails);
			
			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);
			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}
			
			affinityPage.navigateToPendingEmployers();
			waitForElementToDisplay(affinityPage.PendingEmployerHeader);
			verifyElementContains(affinityPage.PendingEmployerHeader, "Pending Employers", "Navigated to Pending Employers Page", test,softAssert);
			
			affinityPage.navigateToMcDetails();
			verifyElementContains(affinityPage.memberCompanyDetailsHeader, "!BROKER AFF UHC!", "Navigated to !AHP_Brokr!222 Details", test,softAssert);
			
			verifyElementContains(affinityPage.brokerName,brokerDetails.get("firstName") , "First name Matched", test,softAssert);
			verifyElementContains(affinityPage.brokerName,brokerDetails.get("lastName") , "First name Matched", test,softAssert);
			verifyElementContains(affinityPage.brokerAddressLine1,brokerDetails.get("addressLine1") , "Address Line 1 Matched", test,softAssert);
			verifyElementContains(affinityPage.brokerAddressLine2,brokerDetails.get("addressLine2") , "Address Line 1 Matched", test,softAssert);
			verifyElementContains(affinityPage.brokerZip,brokerDetails.get("zip") , " Zip matched", test,softAssert);
			//verifyElementContains(affinityPage.brokerPhone,brokerDetails.get("phone") , "Phone Matched", test);
			verifyElementContains(affinityPage.brokerEmail,brokerDetails.get("email") , "Email matched", test,softAssert);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
}
