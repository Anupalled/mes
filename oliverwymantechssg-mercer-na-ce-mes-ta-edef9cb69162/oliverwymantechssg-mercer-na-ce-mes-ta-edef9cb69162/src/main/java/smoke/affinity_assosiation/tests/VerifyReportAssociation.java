package smoke.affinity_assosiation.tests;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.MfaPage;
import pages.hri.LoginPage;
import regression.affinity.tests.VerifyAddAndDeleteUser;
import utilities.InitTests;
import verify.SoftAssertions;
public class VerifyReportAssociation extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	public static ExtentTest test;
	String companyId;
	public static String downloadPath1;
	public VerifyReportAssociation(String appName) {
		super(appName);	
	}


@Test(enabled=false , priority= 3)
public void verifyUserLogin() throws Exception	{		
	new VerifyAddAndDeleteUser("MESDB");
	ExtentTest test=null;
	//ExtentTest test=null;
	 SoftAssert softAssert=new SoftAssert();
	try {
		webdriver = driverFact.initWebDriver(BASEURL,"CHROME","local","");
		test = reports.createTest("Verify Reports");
		test.assignCategory("Regression");		
		driver=driverFact.getEventDriver(webdriver,test);
		
		/*Dimension d = new Dimension(1600,900);
        //Resize the current window to the given dimension
        driver.manage().window().setSize(d);*/
		driver.get(BASEURL);
		LoginPage login =  new LoginPage(driver);
		login.login(USERNAME,PASSWORD);	

		String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
		MfaPage mfa = new MfaPage(driver);			
		mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);

		
		AffinityDashboard affinityPage = new AffinityDashboard(driver);
		waitForElementToDisplay(affinityPage.affinityDashboardHeader);
		
		if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
			verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
			test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

		}else {
			test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
			System.out.println(affinityPage.affinityDashboardHeader.getText());
		}

		String jenkinsDownloadPath = "c:/users/temp/downloads";
		//affinityPage.companyButtonEnabled();
		 downloadPath1 = System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator + "downloadFiles";
		 System.out.println(jenkinsDownloadPath);
		 
		 AffinityDashboard.deleteFilesInPath(jenkinsDownloadPath);
		
		affinityPage.reporting();
		
		//affinityPage.downloadReportFile(downloadPath1);
		affinityPage.isfiledownloaded();
		AffinityDashboard.setData();
		
		affinityPage.navigateToPendingEmployers();
		Thread.sleep(8000);
		for(int i=0;i<40;i=i+4) {
			System.out.print(affinityPage.employerTableItems.get(i).getText());
			System.out.print("="+affinityPage.empNames.get(i)+"\n");
			if(affinityPage.empNames.contains(affinityPage.employerTableItems.get(i).getText())){
				test.log(Status.PASS, "Value matched "+affinityPage.employerTableItems.get(i).getText()+" " +affinityPage.employerTableItems.get(i).getText());
			}
			System.out.println(affinityPage.empNames.contains(affinityPage.employerTableItems.get(i).getText()));
			
		}
		AffinityDashboard.deleteFilesInPath(jenkinsDownloadPath);


	} catch (Error e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		
		}
		catch (Exception e) {
		e.printStackTrace();
		SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
		ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		


	}
	finally
	{
		reports.flush();
		System.out.println("before soft assert all");
		softAssert.assertAll();
		//driver.close();
	}
}

}

