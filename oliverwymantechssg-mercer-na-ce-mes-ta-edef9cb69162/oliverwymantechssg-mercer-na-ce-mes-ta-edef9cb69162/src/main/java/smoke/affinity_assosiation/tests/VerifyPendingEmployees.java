package smoke.affinity_assosiation.tests;

import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementContains;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.affinityDashboard.AffinityDashboard;
import pages.affinityDashboard.MfaPage;
import pages.hri.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class VerifyPendingEmployees extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;

	public VerifyPendingEmployees(String appName) {
		super(appName);	
	}

	
	@Test(enabled=true , priority=1)
	public void verifyEmailUpdate() throws Exception	{		
		new VerifyPendingEmployees("MESDB");
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Each company Status and its Next Step");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);

			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);	

			String[] outlookDetails = {props.getProperty("outlook_url"),props.getProperty("outlook_username"),props.getProperty("outlook_password")};
			MfaPage mfa = new MfaPage(driver);			
			mfa.NavigateToOutlookAndGetMFA(driver, outlookDetails);


			AffinityDashboard affinityPage = new AffinityDashboard(driver);
			waitForElementToDisplay(affinityPage.affinityDashboardHeader);

			if(affinityPage.affinityDashboardHeader.getText().equalsIgnoreCase("Affinity 365+ Dashboard")) {
				verifyElementContains(affinityPage.affinityDashboardHeader, "AFFINITY 365+ DASHBOARD", " Header text matched ", test,softAssert);
				test.log(Status.PASS, "Succesfully Logged into Affinity Dashboard");

			}else {
				test.log(Status.FAIL, "Login Failed or Header Text Mismatch");
				System.out.println(affinityPage.affinityDashboardHeader.getText());
			}

			affinityPage.navigateToPendingEmployers();
			waitForElementToDisplay(affinityPage.PendingEmployerHeader);
			verifyElementContains(affinityPage.PendingEmployerHeader, "Pending Employers", "Navigated to Pending Employers Page", test,softAssert);
			
			String employerName;
			String nextStep;
			String daysToExpire;
			
			
			affinityPage.filterMemberCompanies("INITIATED");
			//waitForElementToDisplay(affinityPage.statusInitiated.get(0));
			for(WebElement e : affinityPage.statusInitiated) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(206, 61, 149, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in Initiated State");								
					
				}else {
					test.log(Status.FAIL, "The Member Company is not in Initiated State");
					System.out.println(e.getCssValue("color")+"IniTiated");
				}				
			}
			
			for(int i=0;i<affinityPage.employerTableItems.size();i=i+4) {
				employerName = affinityPage.employerTableItems.get(i).getText();
				daysToExpire = affinityPage.employerTableItems.get(i+2).getText();
				nextStep =  affinityPage.employerTableItems.get(i+3).getText();
				System.out.println(employerName+" "+daysToExpire+" "+nextStep);	
				if(nextStep.equalsIgnoreCase("Census Data")||nextStep.contains("Preliminary Quote Review")||nextStep.equalsIgnoreCase("Review Quote Request")||nextStep.equalsIgnoreCase("")||nextStep.equalsIgnoreCase("Benefit Selections")||nextStep.equalsIgnoreCase("Quote Review")||nextStep.equalsIgnoreCase("Profile")||nextStep.equalsIgnoreCase("Employee Data")||nextStep.equalsIgnoreCase("Employee Website")) {
					test.log(Status.PASS, "The employeer "+employerName+" contains NextStep "+nextStep);
				}else {
					test.log(Status.FAIL, "The employeer "+employerName+" contains wrong NextStep "+nextStep);
				}				
			}
			
			affinityPage.filterMemberCompanies("KNOCKED OUT");
			
			for(WebElement e : affinityPage.statusKnockedOut) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(0, 168, 200, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in KNOCKED OUT State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in KNOCKED OUT State");
					System.out.println(e.getCssValue("color")+"Knocked Out");
				}				
			}
			
			for(int i=0;i<affinityPage.employerTableItems.size();i=i+4) {
				employerName = affinityPage.employerTableItems.get(i).getText();
				daysToExpire = affinityPage.employerTableItems.get(i+2).getText();
				nextStep =  affinityPage.employerTableItems.get(i+3).getText();
				System.out.println(employerName+" "+daysToExpire+" "+nextStep);	
				if(nextStep.equalsIgnoreCase("")) {
					test.log(Status.PASS, "The employeer "+employerName+" contains NextStep as Null");
				}else {
					//test.log(Status.FAIL, "The employeer "+employerName+" contains wrong NextStep "+nextStep);
				}				
			}
			
			
			affinityPage.filterMemberCompanies("IN QUOTING");
			
			for(WebElement e : affinityPage.statusInQuoting) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(244, 129, 50, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in IN QUOTING State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in IN QUOTING State");
					System.out.println(e.getCssValue("color")+"Quoting");
				}				
			}
			
			for(int i=0;i<affinityPage.employerTableItems.size();i=i+4) {
				employerName = affinityPage.employerTableItems.get(i).getText();
				daysToExpire = affinityPage.employerTableItems.get(i+2).getText();
				nextStep =  affinityPage.employerTableItems.get(i+3).getText();
				System.out.println(employerName+" "+daysToExpire+" "+nextStep);	
				if(nextStep.equalsIgnoreCase("Census Data")||nextStep.equalsIgnoreCase("Preliminary Quote Review")||nextStep.equalsIgnoreCase("Profile")||nextStep.equalsIgnoreCase("Benefit Selections")||nextStep.equalsIgnoreCase("Profile")||nextStep.equalsIgnoreCase("Preliminary Quote Review")||nextStep.equalsIgnoreCase("Sign Up")||nextStep.equalsIgnoreCase("Submit Final Quote Request")||nextStep.equalsIgnoreCase("Quote Review")||nextStep.equalsIgnoreCase("Review Quote Request")||nextStep.equalsIgnoreCase("Preliminary Quote Review")||nextStep.equalsIgnoreCase("")) {
					test.log(Status.PASS, "The employeer "+employerName+" contains NextStep "+nextStep);
				}else {
					test.log(Status.FAIL, "The employeer "+employerName+" contains wrong NextStep "+nextStep);
				}				
			}
			
			affinityPage.filterMemberCompanies("COMPLETING SETUP");
			
			for(WebElement e : affinityPage.statusCompletingSetup) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(15, 182, 148, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in COMPLETING SETUP State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in COMPLETING SETUP State");
					System.out.println(e.getCssValue("color")+"Completing");
				}				
			}
			
			for(int i=0;i<affinityPage.employerTableItems.size();i=i+4) {
				employerName = affinityPage.employerTableItems.get(i).getText();
				daysToExpire = affinityPage.employerTableItems.get(i+2).getText();
				nextStep =  affinityPage.employerTableItems.get(i+3).getText();
				System.out.println(employerName+" "+daysToExpire+" "+nextStep);	
				if(nextStep.equalsIgnoreCase("Contributions")||nextStep.equalsIgnoreCase("Quote Review")||nextStep.equalsIgnoreCase("Eligibility")||nextStep.equalsIgnoreCase("Profile")||nextStep.equalsIgnoreCase("Submit Final Quote Request")||nextStep.equalsIgnoreCase("Review Quote Request")||nextStep.equalsIgnoreCase("Benefit Selections")||nextStep.equalsIgnoreCase("Employee Data")||nextStep.equalsIgnoreCase("Employee Website")||nextStep.equalsIgnoreCase("Preliminary Quote Review")||nextStep.equalsIgnoreCase("")) {
					test.log(Status.PASS, "The employeer "+employerName+" contains NextStep "+nextStep);
				}else {
					test.log(Status.FAIL, "The employeer "+employerName+" contains wrong NextStep "+nextStep);
				}				
			}
			
			affinityPage.filterMemberCompanies("CLOSED LOST");
			
			for(WebElement e : affinityPage.statusClosed) {
				if(e.getCssValue("color").equalsIgnoreCase("rgba(100, 112, 128, 1)")) {
					System.out.println(e.getCssValue("color"));
					test.log(Status.PASS, "The Member Company is in CLOSED LOST State");
				}else {
					test.log(Status.FAIL, "The Member Company is not in CLOSED LOST State");
					System.out.println(e.getCssValue("color")+"Closed");
				}				
			}	
			
			for(int i=0;i<4;i=i+4) {
				employerName = affinityPage.employerTableItems.get(i).getText();
				daysToExpire = affinityPage.employerTableItems.get(i+2).getText();
				nextStep =  affinityPage.employerTableItems.get(i+3).getText();
				System.out.println(employerName+" "+daysToExpire+" "+nextStep);	
				if(nextStep.equalsIgnoreCase("Profile")||nextStep.equalsIgnoreCase("Quote Review")||nextStep.equalsIgnoreCase("Employee Data")||nextStep.equalsIgnoreCase("Census Data")||nextStep.equalsIgnoreCase("Preliminary Quote Review")||nextStep.equalsIgnoreCase("")) {
					test.log(Status.PASS, "The employeer "+employerName+" contains NextStep "+nextStep);
				}else {
					test.log(Status.FAIL, "The employeer "+employerName+" contains wrong NextStep "+nextStep);
				}				
			}			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}
}
	
}
