
package smoke.censustool.tests;

//import static org.junit.Assert.assertTrue;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.censusTool.EmployeeDataPage;
import pages.censusTool.ProfilePage;
import pages.censusTool.WelcomePage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_LoginToCensusTool extends InitTests{
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;


	public TC1_LoginToCensusTool(String appName) {
		super(appName);

	}

	@Test(priority = 1, enabled = true)
	public void MESlogin() throws Exception {
		try {

			test = reports.createTest("censustool");
			test.assignCategory("smoke");
			TC1_LoginToCensusTool census =new TC1_LoginToCensusTool("CensusTool");
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);

			
			WelcomePage welcomepage=new WelcomePage(driver);
			welcomepage.verifydata();
			welcomepage.clickContinue();

			ProfilePage profile =new ProfilePage(driver);
			ProfilePage.setProfileDetails_Yes("aa");

			EmployeeDataPage empdata=new EmployeeDataPage(driver);
			empdata.setEmployeeData_Yes();




		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{

			reports.flush();
			driver.close();



		}
	}
	@Test(priority = 2, enabled = true)
	public void censusTool() throws Exception {
		try {

			test = reports.createTest("censustool");
			test.assignCategory("smoke");
			TC1_LoginToCensusTool census =new TC1_LoginToCensusTool("CensusTool");
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);


			WelcomePage welcomepage=new WelcomePage(driver);
			welcomepage.clickContinue();

			ProfilePage profile =new ProfilePage(driver);
			ProfilePage.setProfileDetails_No("bb");

			EmployeeDataPage empdata=new EmployeeDataPage(driver);
			empdata.setEmployeeData_No();




		} catch (Error e) {
			e.printStackTrace();

			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));

		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("Login failed", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		finally
		{

			reports.flush();
			driver.close();
			


		}
	}
}

