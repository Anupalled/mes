package verify;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
//import com.epam.reportportal.message.ReportPortalMessage;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import utilities.InitTests;
import static utilities.InitTests.dir_path;
import static utilities.InitTests.retryCount;
import static utilities.InitTests.EXECUTION_ENV;
import static utilities.MyExtentReports.reports;
import static listeners.InitReports.logger; 



/**
 * @author YugandharReddyGorrep
 *
 */
public class SoftAssertions
{
	  public static String prevTest="";
	  public static String currTest;


	// publpublic static SoftAssert softAssert;ic static SoftAssert softAssert;
	public static void verifyEquals(String actual,String expected,ExtentTest test)
	{
		
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyEquals() "+"actual " + actual + " expected " + expected );
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.PASSED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() "+"actual " + actual + " but " + " expected " + expected);
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.FAILED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
	}
	public static void verifyEquals(String actual,String expected,ExtentTest test,SoftAssert softassert)
	{
		boolean condition = actual.equals(expected);
		if (condition)
		{
			test.log(Status.PASS, "verifyEquals() "+"actual " + actual + " expected " + expected );
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.PASSED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() "+"actual " + actual + " but " + " expected " + expected);
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.FAILED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		softassert.assertTrue(condition);
	}
	
	public static void verifyEquals(String actual,String expected,String messge,ExtentTest test)
	{
		
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyEquals() "+ messge+" " + expected );
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.PASSED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() "+"actual " + actual + " but " + " expected " + expected);
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.FAILED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
	}
	public static void verifyEquals(String actual,String fieldName,String expected,String message,ExtentTest test)
	{
		
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyEquals() "+fieldName+" " + expected+" "+message );
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.PASSED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() " +fieldName+" actual " + actual + " but " + " expected " + expected);
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.FAILED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
	}
	
	public static void verifyEquals(WebDriver driver,String actual,String fieldName,String expected,String message,ExtentTest test) throws IOException
	{
		
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyEquals() "+fieldName+" " + expected+" "+message );
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.PASSED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() " +fieldName+" actual " + actual + " but " + " expected " + expected);
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
		}
	}
	
	public static void verifyEquals(WebDriver driver,String actual,String fieldName,String expected,String passmessage,String failMessage,ExtentTest test) throws IOException
	{
		
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyEquals() "+fieldName+" " + expected+" "+passmessage );
		//ATUReports.add("verifyEquals() ", "Locator--" , expected, actual, LogAs.PASSED,
				//new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		else
		{
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());

			test.log(Status.FAIL, "verifyEquals() " +fieldName+" actual " + actual + " but " + " expected " + expected +failMessage);
			
			
			//test.log(Status.FAIL, "fail()-Exception"+test.addScreenCaptureFromPath(screen));
			
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());


		}
	}
	public static void verifyNotEquals(String actual,String expected,ExtentTest test)
	{
		
		if (!actual.equals(expected))
		{
			test.log(Status.PASS, "verifyNotEquals()--"+
					"actual " + actual + " expected " + expected );
			ATUReports.add("verifyNotEquals() ", "Locator--" , expected, actual, LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.valueOf(actual)));
		}
		else
		{
			test.log(Status.FAIL, "verifyNotEquals()--"+"actual " + actual + "but" + " expected " + expected);
			ATUReports.add("verifyNotEquals() ", "Locator--" , expected, actual, LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.valueOf(actual)));
			
		}
	}
	

	public static void verifyEquals(int act, int exp, ExtentTest test) {
		// TODO Auto-generated method stub
		if (act==exp)
		{
			test.log(Status.PASS, "verifyEquals() "+"actual " + act + " expected " + exp );
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.PASSED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() "+"actual " +act +" but " + " expected " + exp);
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.FAILED,
					//new CaptureScreen(ScreenshotOf.valueOf(new Integer(act).toString())));
		}
	}
	public static void verifyNotEquals(String valueName,int act, int exp,String message, ExtentTest test) {
		// TODO Auto-generated method stub
		if (act!=exp)
		{
			test.log(Status.PASS, "verifyNotEquals() "+valueName +" is not equal to " + exp +" "+message);
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.PASSED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyNotEquals() "+ valueName+" has actual value " +act) ;
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.FAILED,
					//new CaptureScreen(ScreenshotOf.valueOf(new Integer(act).toString())));
			
		}
	}
	
	public static void verifyNotEquals(WebDriver driver,String valueName,int act, int exp,String message, ExtentTest test) throws IOException {
		// TODO Auto-generated method stub
		if (act!=exp)
		{
			test.log(Status.PASS, "verifyNotEquals() "+valueName +" is not equal to " + exp +" "+message);
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.PASSED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyNotEquals() "+ valueName+" has actual value " +act) ;
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
			
		}
	}
	
	public static void verifyEquals(String valueName,int act, int exp, String passedMessage,String failedMessage,ExtentTest test) {
		// TODO Auto-generated method stub
		if (act==exp)
		{
			test.log(Status.PASS, "verifyEquals()"+ valueName+": "+ act + " "+passedMessage);
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.PASSED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() "+valueName+" "+"actual " +act +" but " + " expected " + exp+" "+failedMessage );
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.FAILED,
					//new CaptureScreen(ScreenshotOf.valueOf(new Integer(act).toString())));
		}
	}

	
	public static void verifyEquals(WebDriver driver,String valueName,int act, int exp, String passedMessage,String failedMessage,ExtentTest test) throws IOException {
		// TODO Auto-generated method stub
		if (act==exp)
		{
			test.log(Status.PASS, "verifyEquals()"+ valueName+": "+ act + " "+passedMessage);
			//ATUReports.add("verifyEquals() ", "Locator--" , new Integer(exp).toString(), new Integer(act).toString(), LogAs.PASSED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() "+valueName+" "+"actual " +act +" but " + " expected " + exp+" "+failedMessage );
			
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());


		}
	}
	public static void verifyContains(String act, String exp, String message, ExtentTest test) {
		// TODO Auto-generated method stub
		if (act.toLowerCase().contains(exp.toLowerCase()) || exp.toLowerCase().contains(act.toLowerCase()))
		{
			test.log(Status.PASS, "verifyContains() "+ message +" "+ exp );
			//ATUReports.add("verifyContains() ", "Locator--" , exp, act, LogAs.PASSED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyContains() "+"actual " +act +" but " + " expected " + exp);
			//ATUReports.add("verifyContains() ", "Locator--" , exp, act, LogAs.FAILED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	public static void verifyContains(String act, String exp, String message, ExtentTest test,SoftAssert softassert) {
		// TODO Auto-generated method stub
		if (act.toLowerCase().contains(exp.toLowerCase()) || exp.toLowerCase().contains(act.toLowerCase()))
		{
			test.log(Status.PASS, "verifyContains() "+ message +" "+ exp );
			//ATUReports.add("verifyContains() ", "Locator--" , exp, act, LogAs.PASSED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyContains() "+"actual " +act +" but " + " expected " + exp);
			//ATUReports.add("verifyContains() ", "Locator--" , exp, act, LogAs.FAILED,
					//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		softassert.assertTrue(act.toLowerCase().contains(exp.toLowerCase()) || exp.toLowerCase().contains(act.toLowerCase()));
	}
		public static void verifyContains(String fieldName,String act, String exp, String message,String failed, ExtentTest test) {
			// TODO Auto-generated method stub
			if (act.toLowerCase().contains(exp.toLowerCase()) || exp.toLowerCase().contains(act.toLowerCase()))
			{
				test.log(Status.PASS, "verifyContains() "+fieldName+" "+ exp+" "+ message  );
				//ATUReports.add("verifyContains() ", "Locator--" , exp, act, LogAs.PASSED,
						//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
			else
			{
				test.log(Status.FAIL, "verifyContains() "+"actual " +act +" but " + " expected " + exp);
				//ATUReports.add("verifyContains() ", "Locator--" , exp, act, LogAs.FAILED,
						//new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
	}
	public static void verifyEquals(Boolean act, Boolean exp, String message, ExtentTest test) {
		// TODO Auto-generated method stub
		if (act==exp)
		{
			test.log(Status.PASS, "verifyEquals() "+"actual " + act + " expected " + exp );
			ATUReports.add("verifyEquals() ", "Locator--" ,  LogAs.PASSED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
		else
		{
			test.log(Status.FAIL, "verifyEquals() "+"actual " +act +" but " + " expected " + exp);
			ATUReports.add("verifyEquals() ", "Locator--" ,  LogAs.FAILED,
					new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
		}
	}
	
	
	public static void verifyElementTextContains(WebElement e, String expected,String passMesg,String failMsg,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value").trim();
		else
			actual = e.getText().trim();
		if (actual.toUpperCase().contains(expected.trim().toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementTextContains()"+ e.getAttribute("outerHTML") + " "+ passMesg);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementTextContains() "+e.getAttribute("outerHTML") + " has actual value " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
			
		}
	}
	
	
	
	public static void verifyElementTextContains(WebElement e, String expected,ExtentTest test,SoftAssert softassert)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value").trim();
		else
			actual = e.getText().trim();
		if (actual.toUpperCase().contains(expected.trim().toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementContains()"+ e.getAttribute("outerHTML") + " contains text as " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementContains() "+e.getAttribute("outerHTML") + " has actual value " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	public static void verifyElementTextContains(WebElement e, String expected,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value").trim();
		else
			actual = e.getText().trim();
		if (actual.toUpperCase().contains(expected.trim().toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementContains()"+ e.getAttribute("outerHTML") + " contains text as " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementContains() "+e.getAttribute("outerHTML") + " has actual value " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	
	public  static void verifyElementTextContains(WebDriver driver,WebElement e, String expected,String passMesg,String failMsg,ExtentTest test) throws IOException
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value").trim();
		else
			actual = e.getText().trim();
		if (actual.toUpperCase().contains(expected.trim().toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementTextContains()"+ e.getAttribute("outerHTML") + " "+ passMesg);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementTextContains() "+e.getAttribute("outerHTML") + " has actual value " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
			
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());

			
		}
	}
	public static void verifyElementText(WebElement e, String expected,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyElementText() "+
					"Webelement " + e.getAttribute("outerHTML") + " as " + expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementText() "+
					e.getAttribute("outerHTML") + " has actual " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	public static void verifyElementTextIgnoreCase(WebElement e, String expected,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText().trim();
		if (actual.toUpperCase().equals(expected.toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementTextIgnoreCase() "+
					"Webelement " + e.getAttribute("outerHTML") + " contains text as " + expected);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementTextIgnoreCase() "+
					e.getAttribute("outerHTML") + " has actual " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	public static void verifyElementHyperLink(WebElement e,ExtentTest test)
	{
		if (e.getTagName().contains("a"))
		{
			test.log(Status.PASS, "verifyElementHyperLink()"+
					"Webelement " + e.getAttribute("outerHTML") + " contains hyperlink");
		} else
		{
			test.log(Status.FAIL, "verifyElementHyperLink() "+ "Webelement " + e.getAttribute("outerHTML")
					+ " have actual " + e.getTagName() + " but " + " expected " + "a");
		}
	}
	
	public static void verifyElementContains(WebElement e, String expected, String message,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.toUpperCase().contains(expected.toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementContains() "+
					"Webelement " + e.getAttribute("outerHTML") + " contains text as " + expected + message);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementContains() "+
					e.getAttribute("outerHTML") + "has actual " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	public static void verifyElementContains(WebElement e, String expected, String message,ExtentTest test,SoftAssert softassert)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		boolean condition=actual.toUpperCase().contains(expected.toUpperCase());
		if (condition)
		{
			test.log(Status.PASS, "verifyElementContains() "+
					"Webelement " + e.getAttribute("outerHTML") + " contains text as " + expected + message);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementContains() "+
					e.getAttribute("outerHTML") + "has actual " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
		softassert.assertTrue(condition);
	}
	public static void verifyElementContains(WebElement e,String fieldName, String expected, String message,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.toUpperCase().contains(expected.toUpperCase()))
		{
			/*test.log(Status.PASS, "verifyElementContains() "+
					fieldName + e.getAttribute("outerHTML") + " contains text as " + expected + message);*/
			test.log(Status.PASS, "verifyElementContains() "+
					fieldName + e.getAttribute("outerHTML") + " contains text as " + expected +" "+ message);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementContains() "+
					e.getAttribute("outerHTML") + "has actual " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	
	public static void verifyElementContains(WebDriver driver,WebElement e,String fieldName, String expected, String message,ExtentTest test) throws IOException
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.toUpperCase().contains(expected.toUpperCase()))
		{
			/*test.log(Status.PASS, "verifyElementContains() "+
					fieldName + e.getAttribute("outerHTML") + " contains text as " + expected + message);*/
			test.log(Status.PASS, "verifyElementContains() "+
					fieldName + e.getAttribute("outerHTML") + " contains text as " + expected +" "+ message);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementContains() "+
					e.getAttribute("outerHTML") + "has actual " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
				
		}
	}
	
	public static void verifyElementText(WebElement e, String expected, String message,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyElementText() "+
					"Webelement " + e.getAttribute("outerHTML") + " has " + expected + " "+message);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementText() "+ "actual " + actual + "but" + " expected " + expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	
	public static void verifyElementText(WebDriver driver,WebElement e, String expected, String message,ExtentTest test) throws IOException
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.equals(expected))
		{
			test.log(Status.PASS, "verifyElementText() "+
					"Webelement " + e.getAttribute("outerHTML") + " has " + expected + " "+message);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementText() "+ "actual " + actual + "but" + " expected " + expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
				
		}
	}
	public static void verifyElementText(WebElement e,String fieldname, String expected, String passmessage,String failedmessage,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.equals(expected))
		{
			/*test.log(Status.PASS, "verifyElementText() "+
					fieldname+" " + e.getAttribute("outerHTML") + " " + expected + " "+passmessage);*/
			test.log(Status.PASS, "verifyElementText() "+
					fieldname+" : "  + expected + " "+passmessage);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			//test.log(Status.FAIL, "verifyElementText() "+ fieldname+" "+"actual " + actual + "but" + " expected " +failedmessage+ expected);
			test.log(Status.FAIL, "verifyElementText() "+ fieldname+" : " +"actual " + actual +" "+ "but" + " expected " +expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}	
	
	public static void verifyElementText(WebDriver driver,WebElement e,String fieldname, String expected, String passmessage,String failedmessage,ExtentTest test) throws IOException
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.equals(expected))
		{
			/*test.log(Status.PASS, "verifyElementText() "+
					fieldname+" " + e.getAttribute("outerHTML") + " " + expected + " "+passmessage);*/
			test.log(Status.PASS, "verifyElementText() "+
					fieldname+" : "  + expected + " "+passmessage);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			//test.log(Status.FAIL, "verifyElementText() "+ fieldname+" "+"actual " + actual + "but" + " expected " +failedmessage+ expected);
			test.log(Status.FAIL, "verifyElementText() "+ fieldname+" : " +"actual " + actual +" "+ "but" + " expected " +expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
		}
	}
	public  static void verifyElementTextContains(WebDriver driver,WebElement e, String expected,String passMesg,String failMsg,ExtentTest test,SoftAssert softassert) throws IOException
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value").trim();
		else
			actual = e.getText().trim();
		if (actual.toUpperCase().contains(expected.trim().toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementTextContains()"+ e.getAttribute("outerHTML") + " "+ passMesg);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementTextContains() "+e.getAttribute("outerHTML") + " has actual value " + actual + " but" + " expected " + expected);
			ATUReports.add("verifyElementTextContains() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
			
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());

			
		}
		softassert.assertTrue(actual.toUpperCase().contains(expected.trim().toUpperCase()),failMsg +"--act val "+actual.toUpperCase());
	}
	
	
	
	public static void verifyElementTextNotEquals(WebElement e,String fieldname, String expected, String passmessage,String failedmessage,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (!(actual.equals(expected)))
		{
			/*test.log(Status.PASS, "verifyElementText() "+
					fieldname+" " + e.getAttribute("outerHTML") + " " + expected + " "+passmessage);*/
			test.log(Status.PASS, "verifyElementTextNotEquals() "+
					fieldname+" : "  + actual + " "+passmessage);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			//test.log(Status.FAIL, "verifyElementText() "+ fieldname+" "+"actual " + actual + "but" + " expected " +failedmessage+ expected);
			test.log(Status.FAIL, "verifyElementTextNotEquals() "+ fieldname+" : " +"actual " + actual +" "+ "but" + " expected " +expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	
	public static void verifyElementTextNotEquals(WebDriver driver,WebElement e,String fieldname, String expected, String passmessage,String failedmessage,ExtentTest test) throws IOException
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (!(actual.equals(expected)))
		{
			/*test.log(Status.PASS, "verifyElementText() "+
					fieldname+" " + e.getAttribute("outerHTML") + " " + expected + " "+passmessage);*/
			test.log(Status.PASS, "verifyElementTextNotEquals() "+
					fieldname+" : "  + actual + " "+passmessage);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			//test.log(Status.FAIL, "verifyElementText() "+ fieldname+" "+"actual " + actual + "but" + " expected " +failedmessage+ expected);
			test.log(Status.FAIL, "verifyElementTextNotEquals() "+ fieldname+" : " +"actual " + actual +" "+ "but" + " expected " +expected);
			ATUReports.add("verifyElementText() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
				
		}
	}
	
	public static void verifyElementTextIgnoreCase(WebElement e, String expected, String message,ExtentTest test)
	{
		String actual;
		if (e.getText().isEmpty())
			actual = e.getAttribute("value");
		else
			actual = e.getText();
		if (actual.toUpperCase().equals(expected.toUpperCase()))
		{
			test.log(Status.PASS, "verifyElementTextIgnoreCase() "+
					"Webelement " + e.getAttribute("outerHTML") + " contains text as " + expected + message);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementTextIgnoreCase() "+
					"actual " + actual + "but" + " expected " + expected);
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), expected, actual, LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	public static void verifyElementLink(WebElement e, String message,ExtentTest test)
	{
		if (e.getTagName().contains("a"))
		{
			test.log(Status.PASS, "verifyElementLink()"+
					"Webelement " + e.getAttribute("outerHTML") + " contains hyperlink" + message);
			ATUReports.add("verifyElementLink() ", "Locator--" + e.toString(), "a", e.getTagName(), LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementLink() "+"actual " + e.getTagName() + "but" + " expected " + "a");
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), "a",  e.getTagName(), LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	public static void verifyElementLink(WebElement e,String linkName, String message,ExtentTest test)
	{
		if (e.getTagName().contains("a"))
		{
			test.log(Status.PASS, "verifyElementLink()"+
					linkName+" "+ e.getAttribute("outerHTML") + " " + message);
			ATUReports.add("verifyElementLink() ", "Locator--" + e.toString(), "a", e.getTagName(), LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementLink() "+"actual " + e.getTagName() + "but" + " expected " + "a");
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), "a",  e.getTagName(), LogAs.FAILED,
					new CaptureScreen(e));
		}
	}
	
	public static void verifyElementLink(WebDriver driver,WebElement e,String linkName, String message,ExtentTest test) throws IOException
	{
		if (e.getTagName().contains("a"))
		{
			test.log(Status.PASS, "verifyElementLink()"+
					linkName+" "+ e.getAttribute("outerHTML") + " " + message);
			ATUReports.add("verifyElementLink() ", "Locator--" + e.toString(), "a", e.getTagName(), LogAs.PASSED,
					new CaptureScreen(e));
		} else
		{
			test.log(Status.FAIL, "verifyElementLink() "+"actual " + e.getTagName() + "but" + " expected " + "a");
			ATUReports.add("verifyElementTextIgnoreCase() ", "Locator--" + e.toString(), "a",  e.getTagName(), LogAs.FAILED,
					new CaptureScreen(e));
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
				
		}
	}
	public static void assertTrue(WebDriver driver,boolean condition, String message,ExtentTest test,SoftAssert soft) throws IOException
	{
		if (condition == true)
		{
			test.log(Status.PASS, "assertTrue()"+message);
			logger.info("assertTrue()--"+condition+"-"+message);
		
					
		} else
		{
			test.log(Status.FAIL, "assertTrue()"+ condition);
			logger.info("assertTrue()"+condition);
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
		}
		soft.assertTrue(condition);
		
	}
	public static void assertTrue(WebDriver driver,boolean condition, String message,ExtentTest test) throws IOException
	{
		if (condition == true)
		{
			test.log(Status.PASS, "assertTrue()"+message);
		
					
		} else
		{
			test.log(Status.FAIL, "assertTrue()"+ condition);
			String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
			test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
		}
		
	}
	public static void assertTrue(boolean condition, String message,ExtentTest test)
	{
		if (condition == true)
		{
			test.log(Status.PASS, "assertTrue()"+message);
					
		} else
		{
			test.log(Status.FAIL, "assertTrue()"+ condition);
		}
	}
	public static void assertFalse(boolean condition, String message,ExtentTest test)
	{
		if (condition == false)
		{
			test.log(Status.PASS, "assertFalse()"+ message);
		} else
		{
			test.log(Status.FAIL, "assertFalse()");
		}
	}
	public static void assertNull(Object obj, String message,ExtentTest test)
	{
		if (obj == null)
		{
			test.log(Status.PASS, "assertNull()" + message);
		} else
		{
			test.log(Status.FAIL, "assertNull()");
		}
	}
	public static void assertNotNull(Object obj, String message,ExtentTest test)
	{
		if (obj != null)
		{
			test.log(Status.PASS, "assertNotNull()"+obj.toString() + message);
		} else
		{
			test.log(Status.FAIL, "assertNotNull()");
		}
	}
	public  static void fail(Throwable e, String pathFrScreenshot,ExtentTest test, SoftAssert softAssert ) throws IOException
	{
		
			
		
		
		if(pathFrScreenshot.isEmpty())
		{
			//System.out.println("in fail dd");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			test.log(Status.FAIL, "fail()-Exception"+errors.toString());
		}
		else
		{
		String excFile="/src/main/WebContent/extentReports/exception.txt";
		String path="/src/main/WebContent/extentReports";
		createExceptionFile(excFile,e);
		String content = new String(
				Files.readAllBytes(Paths.get(dir_path + excFile)));
		int len=(content.length())/4;
		content=content.substring(0, len);
		test.log(Status.FAIL, "fail()-Exception"+content);
		softAssert.fail();
		if (InitTests.CaptureScreenshotOnFail.equalsIgnoreCase("true"))
		{
			test.log(Status.INFO, "fail()-Exception"+test.addScreenCaptureFromPath(pathFrScreenshot));
			System.out.println("before"+pathFrScreenshot);
			pathFrScreenshot=pathFrScreenshot.replaceFirst(".", path);
			System.out.println("aft"+pathFrScreenshot);
		//	ReportPortalMessage message = new ReportPortalMessage(new File(dir_path+pathFrScreenshot), content);
			//logger.error(message);
			ATUReports.add("fail() ", "Locator--" ,"","", LogAs.FAILED,new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		
		
		//System.out.println("retryCount"+retryCount);
		//System.out.println("runCount"+runCount);
		
		currTest=test.getModel().getName();

		
		if(retryCount>0&& !(currTest.equals(prevTest)))
		{
			if(EXECUTION_ENV.contains("local"))
			{
			System.out.println("in remove test"+EXECUTION_ENV);
			reports.removeTest(test);
			System.out.println("Due to retry test "+currTest+" is removed from report");
		}
		}
		prevTest=currTest;
		}
		//return softAssert;
	
	}
	
	
	public  static void fail(Throwable e, String pathFrScreenshot,ExtentTest test  ) throws IOException
	{
		
			
		
		
		if(pathFrScreenshot.isEmpty())
		{
			//System.out.println("in fail dd");
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			test.log(Status.FAIL, "fail()-Exception"+errors.toString());
		}
		else
		{
		String excFile="/src/main/WebContent/extentReports/exception.txt";
		createExceptionFile(excFile,e);
		String content = new String(
				Files.readAllBytes(Paths.get(dir_path + excFile)));
		int len=(content.length())/4;
		content=content.substring(0, len);
		test.log(Status.FAIL, "fail()-Exception"+content);
		if (InitTests.CaptureScreenshotOnFail.equalsIgnoreCase("true"))
		{
			test.log(Status.INFO, "fail()-Exception"+test.addScreenCaptureFromPath(pathFrScreenshot));
			ATUReports.add("fail() ", "Locator--" ,"","", LogAs.FAILED,new CaptureScreen(ScreenshotOf.DESKTOP));
		}
		
		
		//System.out.println("retryCount"+retryCount);
		//System.out.println("runCount"+runCount);
		
		currTest=test.getModel().getName();

		
		if(retryCount>0&& !(currTest.equals(prevTest)))
		{
			if(EXECUTION_ENV.contains("local"))
			{
			System.out.println("in remove test"+EXECUTION_ENV);
			reports.removeTest(test);
			System.out.println("Due to retry test "+currTest+" is removed from report");
		}
		}
		prevTest=currTest;
		}
	
	}
	private static void createExceptionFile(String path,Throwable e) throws IOException
	{
		File f = new File(dir_path + path);
		if (!f.exists())
			f.createNewFile();
		PrintWriter p = new PrintWriter(dir_path + path);
		e.printStackTrace(p);
		p.close();
	}
	

	public static void verifyElementIsPresent(WebElement e,ExtentTest test, String element)
	{
		if(e.isDisplayed()) {
			test.log(Status.PASS, "verifyElementIsPresent()"+
					" Webelement " + e.getAttribute("outerHTML") + element +" is Displayed");

		} else	
		{
			test.log(Status.FAIL, "verifyElementIsPresent() "+ " Webelement " + e.getAttribute("outerHTML")
			+ " have actual ");			
		}
	}
	public static void verifyElementIsPresent(WebElement e,ExtentTest test, String element,SoftAssert softassert)
	{
		boolean condition = e.isDisplayed();
		if(condition) {
			test.log(Status.PASS, "verifyElementIsPresent()"+
					" Webelement " + e.getAttribute("outerHTML") + element +" is Displayed");

		} else	
		{
			test.log(Status.FAIL, "verifyElementIsPresent() "+ " Webelement " + e.getAttribute("outerHTML")
			+ " have actual ");			
		}
		softassert.assertTrue(condition);
	}
		public static void verifyElementIsPresent(WebElement e,ExtentTest test, String element,String message)
		{
			if(e.isDisplayed()) {
				test.log(Status.PASS, "verifyElementIsPresent()"+
						element +": "  + message);

			} else	
			{
				test.log(Status.FAIL, "verifyElementIsPresent() "+ " Webelement " + e.getAttribute("outerHTML")
				+ " have actual ");			
			}
	}
		
		public static void verifyElementIsPresent(WebDriver driver,WebElement e,ExtentTest test, String element,String message) throws IOException
		{
			if(e.isDisplayed()) {
				test.log(Status.PASS, "verifyElementIsPresent()"+
						element +": "  + message);

			} else	
			{
				test.log(Status.FAIL, "verifyElementIsPresent() "+ " Webelement " + e.getAttribute("outerHTML")
				+ " have actual ");	
				String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
				test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
			}
	}
		public static void verifyElementIsNotPresent(WebDriver driver,WebElement e,String element,String message,ExtentTest test,SoftAssert softassert) throws IOException
		{
			boolean flag=false;
			try{
				if(e.isDisplayed()) {
				flag=false;
			}
			}catch (NoSuchElementException ex)
			{
				flag=true;
			}
			if(flag) {
				test.log(Status.PASS, "verifyElementIsNotPresent()"+
						element +": "  + message);}
			else
			{
				test.log(Status.FAIL, "verifyElementIsNotPresent() "+ " Webelement " + e.getAttribute("outerHTML")
				+ " have actual ");	
				String screen=new Driver().getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName());
				test.fail("details", MediaEntityBuilder.createScreenCaptureFromPath(screen).build());
			}
		}
		
		/*public static void failTest(Throwable e, String pathFrScreenshot,ExtentTest test) throws IOException
		{
			System.out.println("in fail test");
			
			String excFile="/src/main/WebContent/extentReports/exception.txt";
			createExceptionFile(excFile,e);
			String content = new String(
					Files.readAllBytes(Paths.get(dir_path + excFile)));
			int len=content.length();
			content=content.substring(0, len);
			test.log(Status.FAIL, "fail()-Exception  "+pathFrScreenshot +" "+content);
		
			softAssert.fail();
			
		}*/
}
