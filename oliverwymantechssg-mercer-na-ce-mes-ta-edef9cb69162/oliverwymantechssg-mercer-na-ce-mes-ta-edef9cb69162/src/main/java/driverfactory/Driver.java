package driverfactory;


import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
/**
 * @author Yugandhar Reddy 
 *         Utility Class for WebDriver where all common libraries can be defined which can be used
 *         across the test cases.
 * 
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


import static utilities.InitTests.BROWSER_TYPE;
import static utilities.InitTests.EXECUTION_ENV;
import static utilities.InitTests.SECRET_KEY;
import static utilities.InitTests.SECRET_VALUE;
import static utilities.InitTests.dir_path;
import static utilities.MyExtentReports.setPlatformDetails;
import static utilities.InitTests.sauceProps;
import org.apache.commons.io.FileUtils;
/*import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;*/
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverLogLevel;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import atu.testng.reports.ATUReports;
import io.github.bonigarcia.wdm.WebDriverManager;
import listeners.EventListner;
import utilities.Decryption;
import utilities.InitTests;
import static utilities.InitTests.waitTimeout;
import static utilities.InitTests.node_URL;
import static utilities.InitTests.PLATFORM;
import static utilities.InitTests.BROWSER_VERSION;
import static utilities.InitTests.applicationName;;


public class Driver {
	private static final Logger logger = Logger.getLogger("selenium");
	public static String driverpath;
	
	public static WebDriverWait wait;
	 public EventListner event;
	public static SoftAssert softAssert;
	public static int defaultTimeOut = 60;
	public static boolean AtuInitflag=false;
	String platform=PLATFORM;
	String browserVersion=BROWSER_VERSION;
	/**
	 * @author Yugandhar Reddy
	 * 
	 * @description:Instantiate the webdriver based on the browser type and other params
	 * @param url
	 * @param browserType IE/chrome/FF/safari/edge
	 * @param executionEnv local/saucelabs/grid
	 * @param nodeUrl for only Grid

	 * @return WebDriver
	 * @throws Exception
	 */
	public  WebDriver initWebDriver(String url,String browserType,String executionEnv,String nodeUrl) throws Exception {
		WebDriver webdriver = null;
		EventFiringWebDriver driver = null;

		if(url.equals(""))
			url=InitTests.BASEURL;
		String browser=System.getProperty("Browser");
		System.out.println("jenkin browser"+browser);
		if(browser!=null)
		{
			browserType=browser;
		}
		else
		{
		if(browserType.equals(""))
			browserType=InitTests.BROWSER_TYPE;
		System.out.println("Inside initWebDriver : Browser type is :" + browserType);
		browserType=browserType.toUpperCase();
		}
		switch (browserType) {
		case "IE":
			try {
				if (InitTests.OS_VERSION.contains("64")) {
					//driverpath =dir_path + "/src/main/resources/drivers/IEDriverServer.exe";
					WebDriverManager.iedriver().arch64().setup();
 
							
				} else {
					//driverpath =dir_path + "/src/main/resources/drivers/IEDriverServer32.exe";
					WebDriverManager.iedriver().arch32().setup();

				}
			} catch (Exception e) {
				if (driverpath == null) {
					driverpath = "c:\\iedriver\\IEDriverServer.exe";
					File file = new File(driverpath);
					if (!file.exists())
						throw new FileNotFoundException(
								"IE Driver executable not found in resources and " + driverpath);
				}
			}
			System.out.println("\n\t" + "Platform->" + platform);
			System.out.println("\n\t" + "BROWSER_TYPE ->" + browserType);
			System.out.println("\n\t" + "BROWSER_VERSION ->" + browserVersion);
			System.out.println("\n\t" + "BASEURL ->" + url);
			System.out.println("driverpath =" + driverpath);
			//setExecPermsWin(driverpath);
			System.setProperty("webdriver.ie.driver", driverpath);
			InternetExplorerDriverService service = new InternetExplorerDriverService.Builder().usingAnyFreePort()
					.withLogFile(new File(dir_path+"/IE_Driver.log")).withLogLevel(InternetExplorerDriverLogLevel.TRACE).build();
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability("EnableNativeEvents", false);
			ieCapabilities.setCapability("ignoreZoomSetting", true);
			ieCapabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			ieCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			ieCapabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			ieCapabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);

		 ieCapabilities.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,url);
			ieCapabilities.setCapability(CapabilityType.BROWSER_NAME, true);

		if(executionEnv.contains("local"))
		{
			webdriver = new InternetExplorerDriver(service, ieCapabilities);
			setPlatformDetails(browserType,platform,browserVersion,url,"");

		}
		else if(executionEnv.contains("saucelabs"))
		{
			DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
			cap.setBrowserName("internet explorer");
			cap.setCapability("platform", platform);
			cap.setCapability("name", browserType);
			cap.setCapability("screenResolution", "1920x1080");
			cap.setCapability("version", browserVersion);
			cap.setCapability("parentTunnel", InitTests.PARENT_TUNNEL);
			cap.setCapability("tunnel-identifier", InitTests.TUNNEL_IDETIFIER);
			webdriver = new RemoteWebDriver(new URL(InitTests.SAUCE_URL), cap);
			setPlatformDetails(browserType,platform,browserVersion,url,InitTests.SAUCE_URL);

			System.out.println("in sauce"+webdriver);
		}
		else if(executionEnv.toLowerCase().contains("grid"))
		{
			DesiredCapabilities gridCaps=DesiredCapabilities.internetExplorer();
			gridCaps.setCapability("ignoreZoomSetting", true);
			gridCaps.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
			gridCaps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			gridCaps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			gridCaps.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			gridCaps.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);

			 gridCaps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL,"about:blank");
			if(platform.toLowerCase().contains("windows"))
			gridCaps.setPlatform(Platform.WINDOWS);
			if(nodeUrl.equals(""))
			webdriver=new RemoteWebDriver(new URL(node_URL), gridCaps);
			else
				webdriver=new RemoteWebDriver(new URL(nodeUrl), gridCaps);

			setPlatformDetails(browserType,platform,browserVersion,url,nodeUrl);

		}
			if(AtuInitflag==false)
			{
			ATUReports.setWebDriver(webdriver);
			AtuInitflag=true;

			}
			wait = new WebDriverWait(webdriver,waitTimeout);
			if (webdriver == null) {
				System.out.println("Failed to initialize IE webdriver in Utils.initWebDriver() ");
				throw new Exception("Failed to initialize IE webdriver in Utils.initWebDriver() ");
			}
			Capabilities cap = ((RemoteWebDriver) webdriver).getCapabilities();
			String browserName = cap.getBrowserName().toLowerCase();
			System.out.println(browserName);
			Platform os = cap.getPlatform();
			System.out.println(os);
			webdriver.manage().window().maximize();
			webdriver.get(url);
			return webdriver;
			
		case "HEADLESSGC" :

			if (platform.toLowerCase().contains("linux")) {
				if (InitTests.OS_VERSION.contains("32")) {
					driverpath = Driver.class.getClassLoader().getResource("drivers/chromedriver_linux32").getPath();
					System.out.println("Linux 32 bit chrome driver:" + driverpath);
				} else {
					driverpath = Driver.class.getClassLoader().getResource("drivers/chromedriver_linux64").getPath();
					System.out.println("Linux 64 bit chrome driver:" + driverpath);
				}


			} else if (platform.toLowerCase().contains("windows")) {
				//driverpath = dir_path + "/src/main/resources/drivers/chromedriver.exe";
				WebDriverManager.chromedriver().setup();
				//System.out.println("Windows chrome driver:" + driverpath);
			} else if (platform.toLowerCase().contains("mac")) {
				driverpath = dir_path + "/src/main/resources/drivers/chromedriver.exe";
				System.out.println(driverpath);

			}
			try
			{
				System.out.println("\n\t" + "Platform->" + platform);
				System.out.println("\n\t" + "BROWSER_TYPE ->" + browserType);
				System.out.println("\n\t" + "BROWSER_VERSION ->" + browserVersion);
				System.out.println("\n\t" + "BASEURL ->" + url);
				System.out.println("\n\t" + "driverpath for windows->" + driverpath);
				//System.setProperty("webdriver.chrome.driver", driverpath);
				if(executionEnv.contains("local"))
				{
					DesiredCapabilities caps1 = DesiredCapabilities.chrome();
					ChromeOptions chromeOptions = new ChromeOptions();
					chromeOptions.addArguments("--window-size=1920,1080");
					chromeOptions.addArguments("--disable-gpu");
					chromeOptions.addArguments("--disable-extensions");
					chromeOptions.setExperimentalOption("useAutomationExtension", false);
					chromeOptions.addArguments("--proxy-server='direct://'");
					chromeOptions.addArguments("--proxy-bypass-list=*");
					chromeOptions.addArguments("--start-maximized");
					chromeOptions.addArguments("--headless");
					caps1.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
					webdriver = new ChromeDriver(caps1);
					setPlatformDetails(browserType,platform,browserVersion,url,"");
				}

				ATUReports.setWebDriver(webdriver);
				AtuInitflag=true;
				System.out.println("before max");
				//webdriver.manage().window().maximize();
				//webdriver.manage().window().setSize(new Dimension(1600,700));

				wait = new WebDriverWait(webdriver,waitTimeout);
				/*webdriver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
				webdriver.manage().timeouts().setScriptTimeout(15, TimeUnit.SECONDS);*/

				webdriver.get(url);
				System.out.println("after get");
				System.setProperty("webdriver.chrome.logfile",dir_path+"/"+"Chromedriver.log");
				//System.setProperty("webdriver.chrome.verboseLogging", "true");

			}catch (Exception e) {
				System.out.println("got exceptiona after init chrome ");
				e.printStackTrace();
			}
			return webdriver;
		case "CHROME":


			if (platform.toLowerCase().contains("linux")) {
				if (InitTests.OS_VERSION.contains("32")) {
					driverpath = Driver.class.getClassLoader().getResource("drivers/chromedriver_linux32").getPath();
					System.out.println("Linux 32 bit chrome driver:" + driverpath);
				} else {
					driverpath = Driver.class.getClassLoader().getResource("drivers/chromedriver_linux64").getPath();
					System.out.println("Linux 64 bit chrome driver:" + driverpath);
				}


			} else if (platform.toLowerCase().contains("windows")) {
				if(InitTests.runWithCiCd.equalsIgnoreCase("Y"))
					WebDriverManager.chromedriver().setup();
					//driverpath = dir_path + "/src/main/resources/drivers/chromedriver.exe";
				else
				driverpath = dir_path + "/WEB-INF/classes/drivers/chromedriver.exe";
				System.out.println("Windows chrome driver:" + driverpath);
			} else if (platform.toLowerCase().contains("mac")) {
				driverpath = dir_path + "/src/main/resources/drivers/chromedriver.exe";

				//setExecPermsPosix(driverpath);
				System.out.println(driverpath);

			}
			try {
				System.out.println("\n\t" + "Platform->" + platform);
				System.out.println("\n\t" + "BROWSER_TYPE ->" + browserType);
				System.out.println("\n\t" + "BROWSER_VERSION ->" + browserVersion);
				System.out.println("\n\t" + "BASEURL ->" + url);
				System.out.println("\n\t" + "driverpath for windows->" + driverpath);
				if(!(InitTests.runWithCiCd.equalsIgnoreCase("Y")))
				System.setProperty("webdriver.chrome.driver", driverpath);
				if (executionEnv.contains("local")) {
					// Setting new download directory path
					Map<String, Object> prefs = new HashMap<String, Object>();

					// Use File.separator as it will work on any OS
					prefs.put("download.default_directory",
							System.getProperty("user.dir") + File.separator + "externalFiles" + File.separator + "downloadFiles");
					prefs.put("plugins.plugins_disabled", new String[] {"Chrome PDF Viewer"});
					prefs.put("profile.default_content_settings.popups", 0);
					prefs.put("plugins.always_open_pdf_externally", true);
					// Adding cpabilities to ChromeOptions
					
					ChromeOptions options = new ChromeOptions();
					options.setExperimentalOption("prefs", prefs);
                    options.setExperimentalOption("useAutomationExtension", false);
                    
                    webdriver = new ChromeDriver(options); 


					// Printing set download directory
					System.out.println(options.getExperimentalOption("prefs"));

					// Launching browser with desired capabilities

					//webdriver = new ChromeDriver(options);
				} 
				else if(executionEnv.contains("saucelabs"))
				{
					DesiredCapabilities caps = DesiredCapabilities.chrome();

					int v=Integer.parseInt(browserVersion);
					caps.setCapability("platform", platform);
					caps.setCapability("name", browserType);
					caps.setBrowserName("chrome");
					caps.setCapability("screenResolution", "1920x1080");
					caps.setCapability("version", v);
					caps.setCapability("parentTunnel", InitTests.PARENT_TUNNEL);
					caps.setCapability("tunnel-identifier", InitTests.TUNNEL_IDETIFIER);
					webdriver = new RemoteWebDriver(new URL(InitTests.SAUCE_URL), caps);
					System.out.println("in sauce"+webdriver);
				}
				else if(executionEnv.contains("grid"))
				{
					DesiredCapabilities gridCaps=DesiredCapabilities.chrome();
					if(platform.toLowerCase().contains("windows"))
						gridCaps.setPlatform(Platform.WINDOWS);
					if(nodeUrl.equals(""))
						webdriver=new RemoteWebDriver(new URL(node_URL), gridCaps);
					else
						webdriver=new RemoteWebDriver(new URL(nodeUrl), gridCaps); 
				}

				ATUReports.setWebDriver(webdriver);
				AtuInitflag=true;
				setPlatformDetails(browserType,platform,browserVersion,url,"");
				System.out.println("before max");
				webdriver.manage().window().maximize();

				wait = new WebDriverWait(webdriver,waitTimeout);
				webdriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
				webdriver.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);

				webdriver.get(url);
				System.out.println("after get");
				System.setProperty("webdriver.chrome.logfile",dir_path+"/"+"Chromedriver.log");
				//System.setProperty("webdriver.chrome.verboseLogging", "true");
			} catch (Exception e) {
				System.out.println("got exceptiona after init chrome ");
				e.printStackTrace();
			}
			return webdriver;
		case "FF":
			System.out.println("\n\t" + "Platform->" + platform);
			System.out.println("\n\t" + "BROWSER_TYPE ->" + browserType);
			System.out.println("\n\t" + "BROWSER_VERSION ->" + browserVersion);
			System.out.println("\n\t" + "BASEURL ->" + url);
			if (platform.toLowerCase().contains("linux")) {
			} else if (platform.toLowerCase().contains("windows")) {
			} else if (platform.toLowerCase().contains("mac")) {
			}
			logger.info("----- Firefox webdirver -----");
			System.out.println("Checking firefox ");
			if(executionEnv.contains("local"))
			{
				  //System.setProperty("webdriver.gecko.driver",  dir_path + "/src/main/resources/drivers/geckodriver.exe");

				  webdriver = new FirefoxDriver();
					setPlatformDetails(browserType,platform,browserVersion,url,"");

			}
			else if(executionEnv.contains("saucelabs"))
			{
				DesiredCapabilities caps = DesiredCapabilities.firefox();
				caps.setCapability("platform", "win 7");
				caps.setCapability("name", browserType);
				caps.setCapability("browserName", "firefox");
				caps.setCapability("screenResolution", "1920x1080");
				caps.setCapability("version", browserVersion);
				caps.setCapability("parentTunnel", InitTests.PARENT_TUNNEL);
				caps.setCapability("tunnel-identifier", InitTests.TUNNEL_IDETIFIER);
				webdriver = new RemoteWebDriver(new URL(InitTests.SAUCE_URL), caps);
				setPlatformDetails(browserType,platform,browserVersion,url,InitTests.SAUCE_URL);

				System.out.println("in sauce"+webdriver);
			}
			else if(executionEnv.toLowerCase().contains("grid"))
			{
				System.setProperty("webdriver.firefox.bin", "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");

				DesiredCapabilities gridCaps=DesiredCapabilities.firefox();
				
				//gridCaps.setBrowserName(browserType);
				if(platform.toLowerCase().contains("windows"))
				//gridCaps.setPlatform(Platform.WINDOWS);
				System.out.println("in ff grid"+nodeUrl);
				webdriver=new RemoteWebDriver(new URL(nodeUrl), gridCaps);
				setPlatformDetails(browserType,platform,browserVersion,url,nodeUrl);

			}
			if(AtuInitflag==false)
			{
			ATUReports.setWebDriver(webdriver);
			AtuInitflag=true;

			}
		
			wait = new WebDriverWait(webdriver,waitTimeout);

			if (webdriver == null) {
				System.out.println("Failed to initialize FF webdriver in Utils.initWebDriver() ");
				throw new Exception("Failed to initialize FF webdriver in Utils.initWebDriver() ");
			}
			//Capabilities ffCapabilities = ((RemoteWebDriver) webdriver).getCapabilities();
			webdriver.manage().window().maximize();
			wait = new WebDriverWait(webdriver,waitTimeout);

			/*driver = new EventFiringWebDriver(webdriver);
			event = new EventListner(test);
			driver.register(event);*/
			webdriver.get(url);
			waitForPageLoad(webdriver);
			return webdriver;
		case "SAFARI":
			PLATFORM = sauceProps.getProperty("platform");
			System.out.println("\n\t" + "Platform->" + PLATFORM);
			System.out.println("\n\t" + "BROWSER_TYPE ->" + browserType);
			System.out.println("\n\t" + "BROWSER_VERSION ->" + browserVersion);
			System.out.println("\n\t" + "BASEURL ->" + url);
			EXECUTION_ENV="saucelabs";
			System.out.println("\n\t" + "Execution Env ->" + EXECUTION_ENV);
			DesiredCapabilities caps = DesiredCapabilities.safari();
			caps.setBrowserName("safari");
			caps.setCapability("platform", PLATFORM);
			caps.setCapability("name", browserType);
			caps.setCapability("screenResolution", "1920x1440");
			caps.setCapability("version", browserVersion);
			caps.setCapability("parentTunnel", InitTests.PARENT_TUNNEL);
			caps.setCapability("tunnel-identifier", InitTests.TUNNEL_IDETIFIER);
			webdriver = new RemoteWebDriver(new URL(InitTests.SAUCE_URL), caps);
			setPlatformDetails(browserType,PLATFORM, caps.getVersion(),url,InitTests.SAUCE_URL);

			if(AtuInitflag==false)
			{
			ATUReports.setWebDriver(webdriver);
			AtuInitflag=true;

			}
			browserName = caps.getBrowserName().toLowerCase();
			System.out.println(browserName);
			webdriver.manage().window().maximize();
			/*driver = new EventFiringWebDriver(webdriver);
			wait = new WebDriverWait(webdriver,waitTimeout);
			event = new EventListner(test);
			driver.register(event);*/
			//driver.manage().timeouts().setScriptTimeout(defaultTimeOut, TimeUnit.SECONDS);

			wait = new WebDriverWait(webdriver,60);
			webdriver.get(url);
			waitForPageLoad(webdriver);
			return webdriver;
		case "EDGE":
			System.out.println("\n\t" + "Platform->" + platform);
			System.out.println("\n\t" + "BROWSER_TYPE ->" + browserType);
			System.out.println("\n\t" + "BROWSER_VERSION ->" + browserVersion);
			System.out.println("\n\t" + "BASEURL ->" + url);
			 caps = DesiredCapabilities.edge();
			caps.setBrowserName("MicrosoftEdge");
			caps.setCapability("platform","Windows 10");
			caps.setCapability("name", browserType);
			caps.setCapability("screenResolution", "1920x1080");
			caps.setCapability("version", browserVersion);
			caps.setCapability("parentTunnel", InitTests.PARENT_TUNNEL);
			caps.setCapability("tunnel-identifier", InitTests.TUNNEL_IDETIFIER);
			webdriver = new RemoteWebDriver(new URL(InitTests.SAUCE_URL), caps);
			if(AtuInitflag==false)
			{
			ATUReports.setWebDriver(webdriver);
			AtuInitflag=true;

			}
			browserName = caps.getBrowserName().toLowerCase();
			setPlatformDetails(browserName,platform, caps.getVersion(), url,InitTests.SAUCE_URL);
			System.out.println(browserName);
			/*os = caps.getPlatform();
			System.out.println(os);
			version = caps.getVersion().toString();
			logger.info("Browser Name: " + caps.getBrowserName().toLowerCase() + " OS name: "
					+ caps.getPlatform().toString() + " Browser version: " + caps.getVersion().toString());
			setPlatformDetails(browserName, os, version, InitTests.BASEURL);*/
			webdriver.manage().window().maximize();
		/*	driver = new EventFiringWebDriver(webdriver);
			wait = new WebDriverWait(webdriver,waitTimeout);
			event = new EventListner(test);
			driver.register(event);
			driver.manage().timeouts().setScriptTimeout(defaultTimeOut, TimeUnit.SECONDS);*/
			wait = new WebDriverWait(webdriver,waitTimeout);
			webdriver.get(url);
			return webdriver;
			
		case "PHANTOMJS":
            File file = new File(dir_path + "/src/main/resources/drivers/phantomjs.exe");
            System.setProperty("phantomjs.binary.path", file.getAbsolutePath());
            System.out.println("\n\t" + "Platform->" + platform);
            System.out.println("\n\t" + "BROWSER_TYPE ->" + browserType);
            System.out.println("\n\t" + "BROWSER_VERSION ->" + browserVersion);
            System.out.println("\n\t" + "BASEURL ->" + url);
            DesiredCapabilities caps1 = DesiredCapabilities.phantomjs();
            if(applicationName.equalsIgnoreCase("win"))
            caps1.setCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36"); 
            caps1.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
            
            caps1.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);

			caps1.setCapability(CapabilityType.SUPPORTS_ALERTS, "handlesAlerts");
			caps1.setCapability(CapabilityType.SUPPORTS_JAVASCRIPT, "javascriptEnabled");

			caps1.setJavascriptEnabled(true);
			caps1.setCapability(CapabilityType.ELEMENT_SCROLL_BEHAVIOR, true);
			String[] phantomArgs = new String[] { "--webdriver-loglevel=NONE","--web-security=no", "--ignore-ssl-errors=yes"}; 
			 caps1.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, phantomArgs);
            caps1.setBrowserName("phontomJS");
            caps1.setCapability("platform", platform);
            caps1.setCapability("name", browserType);
            webdriver = new PhantomJSDriver(caps1);
            webdriver.manage().window().maximize();
            webdriver.get(url);
			waitForPageLoad(webdriver);
			wait = new WebDriverWait(webdriver,waitTimeout);
			setPlatformDetails(browserType,platform, caps1.getVersion(), url,"");

			return webdriver;
		default:
			//return driver;

			//System.out.println("Not valid Browser");
			throw new RuntimeException("Browser type unsupported");
		}
	}

	public  WebDriver getEventDriver(WebDriver driver,ExtentTest test) {
		EventFiringWebDriver d = new EventFiringWebDriver(driver);
				EventListner event = new EventListner(test);
				d.register(event);
				waitForPageLoad(driver);
				return d;
				}

	/**
	 * Delete cookies 
	 * 
	 */
	public  void deleteCookies(WebDriver driver) {
		driver.manage().deleteAllCookies();
	}

	
	/**
	 * Kill  Driver.exe
	 */
	public static void killBrowserExe(String browser) {
		String browserExe="";
		if(browser.equalsIgnoreCase("chrome"))
	 browserExe="chromedriver.exe";
		if(browser.equalsIgnoreCase("IE"))
			 browserExe="IEDriverServer.exe";
		if(browser.equalsIgnoreCase("FF"))
			 browserExe="firefox.exe";
		if(browser.equalsIgnoreCase("PHANTOMJS"))
			 browserExe="phantomjs.exe";
		try {
			System.out.println("in kill" +browserExe);

			Runtime rt = Runtime.getRuntime();
				rt.exec("taskkill /f /t /im "+browserExe);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	/**
	 * Close All Browsers
	 *//*
	public  void closeBrowser(String brType) {
		logger.info("----- closeBrowser() : Closing the currently opened browser and killing driver " + brType);
		System.out.println("----- closeBrowser() : Closing the currently opened browser and killing driver " + brType);
		if (brType.toLowerCase().contains("ie")) {
			if (getDriver() != null) {
				getDriver().manage().deleteAllCookies();
				getDriver().quit();
			}
			
			deleteCookies();
			//delay(2000);
		} else if (brType.toLowerCase().contains("chrome")) {
			if (getDriver() != null) {
				getDriver().quit();
			}
			//killChromeDriver();
			delay(2000);
		} else if (brType.toLowerCase().contains("ff")) {
			// code for FF
			System.out.println("close broser: kill ff browser");
			if (getDriver() != null) {
				getDriver().quit();
			}
			//killFireFoxDriver();
			//delay(2000);
		}
	}*/

	public static void delay(long milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
	}

	public  void implicitWait(WebDriver driver,long time) {
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}

	//All web element methods
	
	
	public static String getAttributeOfElement(WebElement element, String attr) {
		
		return(element.getAttribute(attr));
	}
	
	public static String GetTextOfElement(WebElement element)
	{
		waitForElementToDisplay(element);
		waitForElementToEnable(element);
		return(element.getText());
	}
	

	/**
	 * @description: WebDriver Wait for element to display by locator
	 * @param WebElement
	 */
	public static void waitForElementToEnable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	/**
	 * @description: WebDriver Wait for element to display by locator
	 * @param WebElement
	 */
	public static void waitForElementToDisplay(WebElement element) {
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	
	/**
	 * @description: WebDriver Wait for element to display by locator
	 * @param WebElement
	 */
	
	public static void waitForElementsToDisplay(List<WebElement> element) {
		wait.until(ExpectedConditions.visibilityOfAllElements(element));
	}
	public static void waitForElementToDisplay(WebElement element,WebDriver driver,int sec) {
		
		WebDriverWait	wait1 = new WebDriverWait(driver,sec);

		wait1.until(ExpectedConditions.visibilityOf(element));
	}
	
	public static void waitForElementToDisplay(By by) {
		try {
			System.out.println("in wait for");

			System.out.println(wait.until(ExpectedConditions.visibilityOfElementLocated(by)));
		} catch (Exception e) {
			System.out.println("in wait for catch");
			e.printStackTrace();
		}
	}
	
	public static void waitForElementStale(WebElement element) {
		wait.until(ExpectedConditions.stalenessOf(element));
	}
	public static void waitForElementToDisappear(By element) {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(element));
	}

	/**
	 * @description: WebDriver Wait for element to display by locator
	 * @param by
	 * @param secs
	 */
	/**
	 * @description: WebDriver Wait for element to display by locator
	 * @param by
	 */
	

	/**
	 * @description: WebDriver Wait for element to display by locator
	 * @param by
	 * @param secs
	 */
	public  void waitForElementToDisplay(WebDriver driver,By by, int secs) {
		wait = new WebDriverWait(driver, secs);
		wait.until(ExpectedConditions.visibilityOfElementLocated(by));
	}

	/**
	 * @description : Waits for the page to load for about 30 seconds.
	 */
	public  static void waitForPageLoad(WebDriver driver) {
		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, defaultTimeOut);
		
			wait.until(expectation);
		
	}


	public static void switchToFrame(WebDriver driver,WebElement frameLocator) {
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator));
		//driver.switchTo().frame(frameLocator);
		System.out.println("navigated inside frame " + driver.getTitle());
	}


	public  static void switchToDefaultContent(WebDriver driver) {
		driver.switchTo().defaultContent();
		
		System.out.println("navigated to default content " + driver.getTitle());
	}

	public  static void navigateBack(WebDriver driver) {
		driver.navigate().back();
	}
	public static boolean isElementExisting(WebDriver driver,WebElement we, int time) {
		try {
			
			// Utils.getDriver().findElement(By.xpath(we.getText()));
			if (we.isDisplayed())
				return true;
			else
				return false;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	public static boolean isElementExisting(WebDriver driver, WebElement we) {
		try {
			wait.until(ExpectedConditions.visibilityOf(we));
			// Utils.getDriver().findElement(By.xpath(we.getText()));
			if (we.isDisplayed())
				return true;
			else
				return false;
		} catch (NoSuchElementException e) {
			return false;
		}
	}
	
	/**
	 * @throws InterruptedException
	 * @description:Sets the input text after the element is displayed
	 */
	public static void setInput(WebElement element, String value)
	{
		waitForElementToDisplay(element);
		if(element.getAttribute("type").contains("password"))
		{
			value=Decryption.getDecryptString(value, SECRET_KEY, SECRET_VALUE);
			//System.out.println("passwrd "+value);
		}
		if (BROWSER_TYPE.equalsIgnoreCase("IE"))
		{
			System.out.println("came in ie block input");
			
			waitForElementToEnable(element);
			element.click(); // This below line
			//new Actions(driver).moveToElement(element).perform();
			element.clear();
			if (value != null)
			{
				element.sendKeys(value);
			}
		} else if(BROWSER_TYPE.equalsIgnoreCase("FF"))
		{
		
			waitForElementToEnable(element);
			try
			{
				Thread.sleep(1000);
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			element.clear();
		//clickElementUsingJavaScript(element);
			element.click();
			//new Actions(driver).moveToElement(element).perform();
			if (value != null)
			{
				element.sendKeys(value);
			}
		}
		else
		{
			
			element.clear();
			//clickElementUsingJavaScript(element);
				element.click();
				//new Actions(driver).moveToElement(element).perform();
				if (value != null)
				{
					element.sendKeys(value);
				}
			
		}
	}
	/**
	 * @throws InterruptedException 
	 * @description:Clicks on a web element after element is displayed
	 */
	public  static void clickElement(WebElement element) throws InterruptedException
	{
		waitForElementToDisplay(element);
		waitForElementToEnable(element);
		Thread.sleep(2000);
		element.click();
	}
	/**
	 * @description:Double clicks on an element
	 */
	public static void doubleClickElement(WebDriver driver,WebElement element)
	{
		Actions action = new Actions(driver);
		action.moveToElement(element).doubleClick(element).build().perform();
	}
	/**
	 * @description:Clicks on a web element using Java Script Executor after
	 *                     element is displayed
	 */
	public static void clickElementUsingJavaScript(WebDriver driver,WebElement element)
	{
		waitForElementToEnable(element);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);
		//test.log(LogStatus.INFO, "clickElementUsingJavaScript() ", "clicked on " + attribute);
	}
	/**
	 * @description: Poll until text display
	 * @param element
	 */
	public  void pollUntilTextDisplay(WebElement element)
	{
		for (int elementDispalyCount = 0; elementDispalyCount < 100; elementDispalyCount++)
		{
			try
			{
				if (element.getText() != null)
				{
					break;
				}
			} catch (Exception e)
			{
				refreshpage();
				try
				{
					Thread.sleep(1000);
				} catch (InterruptedException interuptException)
				{
				}
			}
		}
	}
	/**
	 * @description: refresh the checkout page with robo script
	 */
	public static void refreshpage()
	{
		try
		{
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_F5);
			robot.keyRelease(KeyEvent.VK_F5);
		} catch (AWTException e)
		{
			logger.info("AWTException:Page will not be refereshed");
		}
		// Need time to reload the page
		try
		{
			Thread.sleep(3000);
		} catch (InterruptedException e)
		{
			logger.info("InterruptedException:Wait will not applied");
		}
	}
	/**
	 * @description: refresh the page with Webdriver
	 */
	public  void refreshPageWithWebdriver(WebDriver driver)
	{
		driver.navigate().refresh();
	}
	
	/**
	 * @description:Hovers over parent element and clicks on child element
	 */
	public static void hoverAndClickOnElement(WebDriver driver,WebElement parentElement, WebElement childElement)
	{
		Actions action = new Actions(driver);
		action.moveToElement(parentElement).build().perform();
		delay(2000);
		childElement.click();
	}
	
	
	public static void switchToWindow(String windowName,WebDriver driver)
	{
		driver.switchTo().window(windowName);
		
	}
	
	public static void switchToWindowByTitle(String title,WebDriver driver)
	{
		Set<String> windows=driver.getWindowHandles();
		for(String win:windows)
		{
			
		if(((driver.switchTo().window(win)).getTitle()).contains(title))
		{
			driver.switchTo().window(win);
			break;

		}
		
	}
	}
	
	public static void switchToWindowWithURL(String url,WebDriver driver)
	{
		Set<String> windows=driver.getWindowHandles();
		for(String win:windows)
		{
			
		if(((driver.switchTo().window(win)).getCurrentUrl()).contains(url))
		{
			driver.switchTo().window(win);
			break;

		}
		
	}
	}
	
	public static void selEleByVisbleText(WebElement selectDropDown, String visbleText)
	{
		Select sel = new Select(selectDropDown);
		sel.selectByVisibleText(visbleText);
		
	}
	/**
	 * @description:Hovers over parent element
	 */
	public static void hoverOverElement(WebElement element,WebDriver driver)
	{
		Actions action = new Actions(driver);
		action = action.moveToElement(element);
		action.build().perform();
		delay(2000);
	}
	/**
	 * @description : Gets the page source of the current page.
	 * @return : page source as string.
	 */
	public static String getPageSource(WebDriver driver)
	{
		return driver.getPageSource();
	}
	/**
	 * Scrolls to web element specified
	 *
	 * @param driver
	 * @param element
	 * @throws InterruptedException
	 */
	public static void scrollToElement(WebDriver driver, WebElement element) throws InterruptedException
	{
		Thread.sleep(2000); // we cannot use explicit wait here
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("arguments[0].scrollIntoView(true);", element);
	}
	/**
	 * @description:Hovers over parent element
	 */
	public static void hoverOverElement(WebDriver driver,WebElement element)
	{
		Actions action = new Actions(driver);
		action = action.moveToElement(element);
		action.build().perform();
		delay(2000);
	}
	public static void waitForElementToClickable(WebElement element) {
		wait.until(ExpectedConditions.elementToBeClickable(element));
	} 
	/**
	 * @description:Fetches link of a web element using Java Script Executor
	 *                      after element is displayed
	 *//*
	public static String getHrefFromWebElementUsingJavaScript(WebElement element)
	{
		waitForElementToDisplay(element);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		return executor.executeScript("return arguments[0].getAttribute(\"href\")", element).toString();
	}
	
	*//**
	 * @description:Fetches link of a web element using Java Script Executor
	 *                      after element is displayed
	 *//*
	public static String getTextFromWebElementUsingJavaScript(WebElement element)
	{
		waitForElementToDisplay(element);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		return executor.executeScript("return arguments[0].text", element).toString();
	}
	*//**
	 * @description : Verifies if the checkbox is already selected.
	 * @param driver
	 * @param checkboxpath
	 * @return
	 *//*
	public static boolean verifyCheckBox(WebElement checkBox)
	{
		boolean isChecked = false;
		try
		{
			isChecked = checkBox.isSelected();
		} catch (Exception e)
		{
			logger.info(e.getMessage());
		}
		return isChecked;
	}
	*//**
	 * @description:Fetches link of a web element using Java Script Executor
	 *                      after element is displayed
	 *//*
	public static String getIdFromWebElementUsingJavaScript(WebElement element)
	{
		waitForElementToDisplay(element);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		return executor.executeScript("return arguments[0].id", element).toString();
	}
	*//**
	 * @author 
	 * @description: Equivalent to sendkeys in special cases
	 *//*
	public static void setValueUsingJavaScript(WebElement element, String value)
	{
		waitForElementToDisplay(element);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript(
				"document.getElementById('" + element.getAttribute("id") + "').innerHTML = '" + value + "'", element);
	}
	
	public static WebElement getElementWithLinkText(String locatorText)
	{
		By loc = By.partialLinkText(locatorText);
		waitForElementToDisplay(loc);
		return driver.findElement(loc);
	}
	public static WebElement getElementWithPartialLinkText(String locatorText)
	{
		By loc = By.partialLinkText(locatorText);
		waitForElementToDisplay(loc);
		return driver.findElement(loc);
	}
	public static WebElement getElementWithTitle(String tagName, String tagHasTitle)
	{
		return driver.findElement(By.cssSelector(tagName + "[title='" + tagHasTitle + "']"));
	}
	public static void selEleByVisbleText(WebElement selectDropDown, String visbleText)
	{
		Select sel = new Select(selectDropDown);
		sel.selectByVisibleText(visbleText);
		
	}
	*//**
	 * @param table
	 *//*
	public static List<String> getTableData(WebElement tableLoc)
	{
		List<String> myTabdata = new ArrayList<String>();
		waitForElementToDisplay(tableLoc);
		List<WebElement> trs = tableLoc.findElements(By.cssSelector("tr:not([class*='ng-hide'])"));
		for (WebElement tr : trs)
		{
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			for (WebElement td : tds)
			{
				myTabdata.add(td.getText());
			}
		}
		return myTabdata;
	}
	*//**
	 * @param table,row
	 *            and col index
	 *//*
	public static WebElement getTableColumn(WebElement rowLoc, int colIndex)
	{
		waitForElementToDisplay(rowLoc);
		List<WebElement> tds = rowLoc.findElements(By.tagName("td"));
		return tds.get(colIndex);
	}
	*//**
	 * @param table
	 *//*
	public static int getNoOfRowsInTable(WebElement tableLoc)
	{
		waitForElementToDisplay(tableLoc);
		List<WebElement> trs = tableLoc.findElements(By.cssSelector("tbody>tr:not([class*='ng-hide'])"));
		System.out.println("rows" + trs.size());
		return trs.size();
	}
	*//**
	 * @param table
	 *//*
	public static int getNoOfColInRow(WebElement rowLoc)
	{
		waitForElementToDisplay(rowLoc);
		List<WebElement> tds = rowLoc.findElements(By.tagName("td"));
		return tds.size();
	}
	*//**
	 * @param table
	 *//*
	public static int getSizeOfTable(WebElement tableLoc)
	{
		int c = 0;
		waitForElementToDisplay(tableLoc);
		List<WebElement> trs = tableLoc.findElements(By.cssSelector("tbody>tr:not([class*='ng-hide'])"));
		for (WebElement tr : trs)
		{
			List<WebElement> tds = tr.findElements(By.tagName("td"));
			for (WebElement td : tds)
			{
				c++;
			}
		}
		return c;
	}
	*//**
	 * @param table,row
	 *            and col index
	 *//*
	public static WebElement getTableColumnByIndex(WebElement tableLoc, int rowIndex, int colIndex)
	{
		waitForElementToDisplay(tableLoc);
		List<WebElement> trs = tableLoc.findElements(By.cssSelector("tbody>tr:not([class*='ng-hide']"));
		WebElement row = trs.get(rowIndex);
		List<WebElement> tds = row.findElements(By.tagName("td"));
		return tds.get(colIndex);
	}
	
	*//**
	 * @param table
	 *//*
	public static int getWebElementCount(List<WebElement> element)
	{
		
		return element.size();
	}
	public static List<String> getAllValuesFromDrop(Select drop)
	{
		List<String> values = new ArrayList<String>();
		for (WebElement el : drop.getOptions())
		{
			values.add(el.getText());
		}
		return values;
	}
	

	*//**
	 * @description : Gets the value of "font-family" attribute of a web element
	 *              .
	 * @param webElement
	 * @return : font styles of the web element.
	 *//*
	public static String getFontStyle(WebElement webElement)
	{
		return webElement.getCssValue("font-family");
	}
	
	*//**
	 * @description : Gets the value of "font-size" attribute of a web element .
	 * @param webElement
	 * @return : font size of the web element.
	 *//*
	public static String getFontSize(WebElement webElement)
	{
		return webElement.getCssValue("font-size");
	}
	*//**
	 * @description : Gets the value of "font-weight" attribute of a web element
	 *              .
	 * @param webElement
	 * @return : String,font weight of the web element.
	 *//*
	public static String getFontWeight(WebElement webElement)
	{
		return webElement.getCssValue("font-weight");
	}
	*//**
	 * @param Path
	 *            - File to make it executable
	 * @throws IOException
	 *//*
	public static void setExecPermsWin(String Path) throws Exception {
		File file = new File(Path);
		System.out.println("IE Driver path: " + Path);
		System.out.println("Before set - Is Execute Permission set : " + file.canExecute());
		if (file.exists()) {
			System.out.println("Is Execute Permission set : " + file.canExecute());
			file.setExecutable(true, false);
			file.setReadable(true, false);
			file.setWritable(true, false);
		}
		System.out.println("After - Is Execute allow : " + file.canExecute());
		System.out.println("Afetr - Is Write allow : " + file.canWrite());
		System.out.println("After - Is Read allow : " + file.canRead());
	}

	*//**
	 * @param Path
	 *            - File to make it executable
	 * @throws IOException
	 *//*
	public static void setExecPermsPosix(String Path) throws IOException {
		File file = new File(Path);
		
		 * if(file.exists()){
		 * System.out.println("Before setting perms - Execute allowed : " +
		 * file.canExecute()); file.setExecutable(true, false); file.setReadable(true,
		 * false); file.setWritable(true, false); }
		 
		
		 * System.out.println("Is Execute allow : " + file.canExecute());
		 * System.out.println("Is Write allow : " + file.canWrite());
		 * System.out.println("Is Read allow : " + file.canRead());
		 
		// using PosixFilePermission to set file permissions 777
		Set<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
		// add owners permission
		perms.add(PosixFilePermission.OWNER_READ);
		perms.add(PosixFilePermission.OWNER_WRITE);
		perms.add(PosixFilePermission.OWNER_EXECUTE);
		// add group permissions
		perms.add(PosixFilePermission.GROUP_READ);
		perms.add(PosixFilePermission.GROUP_WRITE);
		perms.add(PosixFilePermission.GROUP_EXECUTE);
		// add others permissions
		perms.add(PosixFilePermission.OTHERS_READ);
		perms.add(PosixFilePermission.OTHERS_WRITE);
		perms.add(PosixFilePermission.OTHERS_EXECUTE);
		Files.setPosixFilePermissions(Paths.get(Path), perms);
	}

	*/
	
	public  String getScreenPath(WebDriver driver,String testScriptName) throws IOException
    {
           String file = null;

           
           String timeStamp = getCurrTimeStamp();
           File ff = new File( dir_path+"/src/main/WebContent/extentReports/Screens");
           if (!ff.exists())
           {
                  ff.mkdir();
           }
           if (InitTests.CaptureScreenshotOnFail.equalsIgnoreCase("true"))
           {
                  String myScreen=testScriptName + "_" + timeStamp + ".png";
                  System.out.println("in flag true");
                  file= dir_path+"/src/main/WebContent/extentReports/Screens/" + myScreen;
                  System.out.println("about to screnn"+file);
                  // TODO Auto-generated method stub
                  File f = ((TakesScreenshot) (driver)).getScreenshotAs(OutputType.FILE);
                  FileUtils.copyFile(f,
                               new File(file));
                  System.out.println("in flag true"+file  );
                  if(InitTests.runWithCiCd.equalsIgnoreCase("y"))
                  {
                        
                               return "./Screens/" + myScreen;
                  }
                  else
                  {
                        return "/UiFramework/extentReports/Screens/" + myScreen;
                  }
           } else
           {
                  return  file ;
           }
           }

		
	/**
	 * @description:Returns the current time stamp
	 * 
	 * 
	 * @return String
	 */
	public static String getCurrTimeStamp()
	{
		LocalDate.now();
		Locale locale = Locale.getDefault();
		TimeZone tz = TimeZone.getDefault();
		Calendar cal = Calendar.getInstance(tz, locale);
		Date d = new Date(System.currentTimeMillis());
		cal.setTime(d);
		int m = cal.get(Calendar.MONTH) + 1;
		int h = cal.get(Calendar.HOUR);
		int mm = cal.get(Calendar.MINUTE);
		int s = cal.get(Calendar.SECOND);
		String timeStamp = cal.get(Calendar.DAY_OF_MONTH) + "_" + m + "_" + cal.get(Calendar.YEAR) + "_" + h + "hh_"
				+ mm + "mm_" + s + "ss";
		return timeStamp;
	}
	
	public static WebElement getEleByXpathContains(String tagName,String txt,WebDriver driver) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//"+tagName+"[contains(text(),'"+txt+"')]")));
		return driver.findElement(By.xpath("//"+tagName+"[contains(text(),'"+txt+"')]"));
		
	}
	
	public static WebElement getEleByXpathTxt(String tagName,String txt,WebDriver driver) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//"+tagName+"[text()='"+txt+"']")));
		return driver.findElement(By.xpath("//"+tagName+"[contains(text(),'"+txt+"')]"));
		
	}
	public static WebElement getEleByCssContains(String tagName,String attribute,String txt,WebDriver driver) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(tagName+"["+attribute+"*='"+txt+"']")));
		return driver.findElement(By.cssSelector(tagName+"["+attribute+"*='"+txt+"']"));
		
	}
	public static void waitForNoOfwindowsTobe(int number)
	{
		wait.until(ExpectedConditions.numberOfWindowsToBe(number));

	}
	public static String getAllEleText(List<WebElement> list)
	{
	String txtList="";
		for(WebElement w:list)
		{
			txtList=txtList+w.getText();
		}
		return txtList;
	}
	/**
	 * Method to send email with attachment
	 * 
	 * @param filename
	 *            filename is HTML report created
	 * @throws MessagingException
	 */
	/*
	 * @SuppressWarnings("unused") public static void sendEmail(String filename)
	 * throws MessagingException { String host = "localhost"; String from =
	 * ".com"; // String to = ""; String to = ".com";
	 * // Get system properties Properties props = System.getProperties(); // Setup
	 * mail server // props.put("smtp.xxxx.com", host);
	 * //MSPMSGCCR000.corp.fairisaac.com // props.put(host, "smtp.xxxx.com");
	 * props.put("mail.smtp.host", "smtp.xxxx.com"); props.put("mail.smtp.port",
	 * "25"); // Get session Session session = Session.getDefaultInstance(props); //
	 * Define message MimeMessage message = new MimeMessage(session);
	 * message.setFrom(new InternetAddress(from));
	 * message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
	 * message.setSubject("OM45 Automation Report"); // Create the message part
	 * BodyPart messageBodyPart = new MimeBodyPart(); // Fill the message
	 * messageBodyPart.
	 * setText("Hi, This is automated email, please do not reply. kindly check the attached report"
	 * ); Multipart multipart = new MimeMultipart();
	 * multipart.addBodyPart(messageBodyPart); // Part two is an attachment //
	 * messageBodyPart = new MimeBodyPart(); // DataSource source = new
	 * FileDataSource(filename); // messageBodyPart.setDataHandler(new
	 * DataHandler(source)); // messageBodyPart.setFileName(filename); //
	 * multipart.addBodyPart(messageBodyPart); // Put parts in message
	 * message.setContent(multipart); // Send message Transport.send(message); }
	 */
}
