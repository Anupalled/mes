package insprintppdownload;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CensusData;
import pages.MES.CompleteRequestPage;
import pages.MES.ConsultantPortalLoginPage;

import static driverfactory.Driver.waitForElementToDisplay;
import static pages.MES.CompleteRequestPage.AssociationID;
import pages.MES.DashboardPage;
import pages.MES.LoginPage;
import pages.MES.ProfilePage1;
import pages.MES.SelfRegPage;
import regression.mes_affinity.tests.TC1_SelfRegistration;
import regression.mes_ahp.tests.TC7_ConsultantPortalConfig;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC1_QuotingTool extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC1_QuotingTool(String appName) {
		super(appName);

	}
	@Test(priority = 1, enabled = true)
	public void MESlogin() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			Driver driverFact = new Driver();
			TC1_QuotingTool mes = new TC1_QuotingTool("DEVDOOR");
			
			test = reports.createTest("LoginQTST");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			

			SelfRegPage self=new SelfRegPage(driver);
			self.musedeletion();
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	@Test(priority = 2, enabled = true)
	public void MESDashboard() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			Driver driverFact = new Driver();
			TC1_QuotingTool mes = new TC1_QuotingTool("DEVDOOR");
			
			test = reports.createTest("LoginQTST");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);
			
			
			DashboardPage dashboard = new DashboardPage(driver);
			//dashboard.submitQuoteRequest(DashboardPage.requestQuoteButton);
			 dashboard.submitQuoteRequest(DashboardPage.continueButton);

			ProfilePage1 profile = new ProfilePage1(driver);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			verifyElementTextContains(driver,ProfilePage1.profileHeader1, "Profile","profileHeader1 pass","profileHeader1 fail", test,softAssert);
			
			profile.getOrganisationName();
			ProfilePage1.setOrgName("StateAHP");
			profile.selectContinue();
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}

	@Test(priority = 3, enabled = true)
	public void loginPage() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {
			
			Driver driverFact = new Driver();
			TC1_QuotingTool mes = new TC1_QuotingTool("DEVDOOR");
			
			test = reports.createTest("DashboardPage");
			test.assignCategory("smoke");
			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");
			driver = driverFact.getEventDriver(webdriver, test);
			System.out.println("BaseURL is: " + BASEURL);

			
			DashboardPage dashboard = new DashboardPage(driver);
			
			 dashboard.submitQuoteRequest(DashboardPage.continueButton);

			ProfilePage1 profile = new ProfilePage1(driver);
			waitForElementToDisplay(ProfilePage1.profileHeader1);
			verifyElementTextContains(driver,ProfilePage1.profileHeader1, "Profile","profileHeader1 pass","profileHeader1 fail", test,softAssert);
			
			
			profile.getOrganisationName();
			ProfilePage1.setOrgName("StateAHP");
			profile.selectContinue();
			
			waitForElementToDisplay(ProfilePage1.profileHeader2);
			verifyElementTextContains(driver,ProfilePage1.profileHeader2, "Tell us about yourself","profileHeader2 pass","profileHeader2 fail", test,softAssert);
			ProfilePage1.setProfileData("profile2");
			profile.selectContinue();
			
			
			waitForElementToDisplay(ProfilePage1.profileHeader4);
			verifyElementTextContains(driver,ProfilePage1.profileHeader4,
					"We have a few questions about your group and existing coverage.", "profileHeader4 pass","profileHeader4 fail",test,softAssert);
			ProfilePage1.setCoverage("AHP_profile4");
			System.out.println("sleeping..");
			profile.selectContinue();
			
			waitForElementToDisplay(ProfilePage1.profileHeader3);
			verifyElementTextContains(driver,ProfilePage1.profileHeader3, "Tell us when you want coverage","profileHeader3 pass","profileHeader3 fail",test,softAssert);
			profile.setCoverage();
			profile.selectContinue();

			BenefitSelectionPage benefits = new BenefitSelectionPage(driver);
			waitForElementToDisplay(BenefitSelectionPage.benefitSelectionHeader);
			verifyElementTextContains(driver,BenefitSelectionPage.benefitSelectionHeader, "Benefit Selections","benefitSelectionHeader pass","benefitSelectionHeader fail", test,softAssert);
			benefits.setBenefits();

			CensusData censusdata = new CensusData(driver);
			CensusData.setCensusData("census");
			profile.selectContinue();

			CompleteRequestPage request = new CompleteRequestPage(driver);
			waitForElementToDisplay(CompleteRequestPage.reviewQuoteRequest_Title);
			request.completeRequest();
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
		
}
