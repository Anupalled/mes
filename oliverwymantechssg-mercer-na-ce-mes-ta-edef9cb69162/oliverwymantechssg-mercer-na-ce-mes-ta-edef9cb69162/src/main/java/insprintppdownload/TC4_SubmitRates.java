
package insprintppdownload;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static pages.MES.ProfilePage1.value;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.CompleteRequestPage;
import static pages.MES.CompleteRequestPage.AssociationID;
import pages.MES.ConsultantPortalLoginPage;
import pages.MES.HeaderPage;
import pages.MES.LoginPage;
import pages.MES.MemberRates;
import pages.MES.PlanSelectionPage;
import pages.MES.SubmitFinalQuote;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC4_SubmitRates extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC4_SubmitRates(String appName) {
		super(appName);

	}

	@BeforeClass
	public void beforeClass() throws Exception {

		TC4_SubmitRates portal = new TC4_SubmitRates("ConsultantPortal");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");

	}

	@Test(priority =6, enabled = true)
	public void ConsultantPortal() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("submitRates");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard", "dashboard pass","dashboard fail",test,softAssert);
			header.selectHeaders(HeaderPage.associationTab);

			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations","associationHeader pass","associationHeader fail", test,softAssert);
			
			
			association.pagination_assoc();
			waitForPageLoad(driver);
			association.selectAssociation(AssociationPage.assocNew);

			
			Thread.sleep(3000);
			waitForElementToDisplay(AssociationPage.memebercompy);
			association.selectAssociation(AssociationPage.memebercompy);
			waitForElementToDisplay(AssociationPage.assocDetails);
			AssociationPage.searchCompany(value);

			MemberRates rates = new MemberRates(driver);
			waitForElementToDisplay(MemberRates.memberRatesHeader);
			verifyElementTextContains(driver,MemberRates.memberRatesHeader, "Select an option to begin entering rates.","memberRatesHeader pass","memberRatesHeader fail", test,softAssert);
			Thread.sleep(5000);
			rates.submitFinalQuote();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
	
}
