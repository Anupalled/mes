package insprintppdownload;

import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;
import static driverfactory.Driver.waitForElementToDisplay;
import static driverfactory.Driver.waitForPageLoad;
import static driverfactory.Driver.waitForElementToDisappear;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

import static pages.MES.ProfilePage1.value;
import driverfactory.Driver;
import pages.MES.AssociationPage;
import pages.MES.BenefitSelectionPage;
import pages.MES.CompleteRequestPage;
import static pages.MES.CompleteRequestPage.AssociationID;
import pages.MES.ConsultantPortalLoginPage;
import pages.MES.HeaderPage;
import pages.MES.LoginPage;
import pages.MES.MemberRates;
import pages.MES.PlanSelectionPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class TC2_ConsultantPortal extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public TC2_ConsultantPortal(String appName) {
		super(appName);

	}

	@BeforeClass
	public void beforeClass() throws Exception {

		TC2_ConsultantPortal portal = new TC2_ConsultantPortal("ConsultantPortal");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, EXECUTION_ENV, "");

	}

	@Test(priority = 4, enabled = true)
	public void ConsultantPortal() throws Exception {
		ExtentTest test=null;
    	//ExtentTest test=null;
		 SoftAssert softAssert=new SoftAssert();
		try {

			test = reports.createTest("consultantportal");
			test.assignCategory("smoke");
			driver = driverFact.getEventDriver(webdriver, test);

			ConsultantPortalLoginPage loginpage = new ConsultantPortalLoginPage(driver);

			loginpage.login(USERNAME, PASSWORD);

			HeaderPage header = new HeaderPage(driver);
			waitForElementToDisplay(HeaderPage.dashboard);
			verifyElementTextContains(driver,HeaderPage.dashboard, "My Dashboard","dashboard pass","dashboard fail", test,softAssert);
			header.selectHeaders(HeaderPage.associationTab);

			AssociationPage association = new AssociationPage(driver);
			waitForElementToDisplay(AssociationPage.associationHeader);
			verifyElementTextContains(driver,AssociationPage.associationHeader, "associations", "associationHeader pass","associationHeader fail",test,softAssert);
			
			
			association.pagination_assoc();
			waitForPageLoad(driver);
			association.selectAssociation(AssociationPage.assocNew);
			waitForElementToDisplay(AssociationPage.profile1);

			
			Thread.sleep(3000);
			waitForElementToDisplay(AssociationPage.memebercompy);
			association.selectAssociation(AssociationPage.memebercompy);
			waitForElementToDisplay(AssociationPage.assocDetails);
			AssociationPage.searchCompany(value);

			PlanSelectionPage plans = new PlanSelectionPage(driver);
			waitForElementToDisplay(PlanSelectionPage.MedicalPlan);
			verifyElementTextContains(driver,PlanSelectionPage.MedicalPlan, "Medical","medicalOption pass","medicalOption fail", test,softAssert);
			plans.selectPlans();

			MemberRates rates = new MemberRates(driver);
			waitForElementToDisplay(MemberRates.memberRatesHeader);
			verifyElementTextContains(driver,MemberRates.memberRatesHeader, "Select an option to begin entering rates.","memberRatesHeader pass","memberRatesHeader fail", test,softAssert);

			MemberRates.setData();
			//waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			rates.clickContinue(MemberRates.saveButton);

			Thread.sleep(8000);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			MemberRates.selectTabs(MemberRates.dentalTab);
			MemberRates.setData();
			rates.clickContinue(MemberRates.saveButton);

			Thread.sleep(5000);
			MemberRates.selectTabs(MemberRates.visionTab);
			MemberRates.setData();
			rates.clickContinue(MemberRates.saveButton);

			Thread.sleep(5000);
			MemberRates.selectTabs(MemberRates.lifeTab);
			MemberRates.setLifeData();
			
			rates.submitQuote();
			

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			
			}
			catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test,softAssert);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			


		}
		finally
		{
			reports.flush();
			System.out.println("before soft assert all");
			softAssert.assertAll();
			driver.close();
		}

	}
	
}
