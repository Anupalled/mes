package utilities;

import java.io.InputStream;
import java.util.Properties;

import org.testng.asserts.SoftAssert;

/**
 * @author YugandharReddy
 */
public class InitTests {
	public static SoftAssert softAssert = new SoftAssert();

	public static Properties props = new Properties();
	public static Properties sauceProps = new Properties();
	public static Properties prodProps= new Properties();
	public static Properties stagProps = new Properties();
	public static Properties citProps =new Properties();
	public static Properties csoProps =new Properties();


	/**
	 * @description:Initialization OS,version,browser and url details.
	 * 
	 * @throws Exception
	 */
	public static String OS_VERSION = "";
	public static String ENDPOINT_LOGIN = "";
	public static String CaptureScreenshotOnFail = "";
	public static String OS_NAME = "";
	public static String BROWSER_TYPE = "";
	public static String BASEURL = "";

	public static String USERNAME = "";
	public static String PASSWORD = "";
	public static int retryCount;

	public static String dir_path;
	public static String REST_URL_PURCHASE_JOBS = "";

	public static String PROJ_CONFIG_Path = "";

	public static final String TestCases = "";
	public static String BROWSER_VERSION = "";
	public static String PLATFORM = "";
	public static String SAUCE_USERNAME = "";
	public static String SAUCE_ACCESSKEY = "";
	public static String SAUCE_URL = "";
	public static String PARENT_TUNNEL = "";
	public static String TUNNEL_IDETIFIER = "";
	public static String RESOLUTION = "";
	public static String EXECUTION_ENV = "";
	public static String node_URL;
	public static String runWithCiCd;
	public static String SECRET_KEY;
	public static String SECRET_VALUE;
	public static int waitTimeout;
public static InputStream input;
public static String applicationName;
	public InitTests(String appName) {
		try {
			applicationName=appName;
			System.out.println("Getting config files..");
			 input = null;
			System.out.println("Config read successfully");
			ClassLoader loader = this.getClass().getClassLoader();
			input = loader.getResourceAsStream("config/testdata.properties");

			props.load(input);
			
			retryCount=Integer.parseInt(props.getProperty("Retry_test_count"));
			System.out.println("retry count is" +retryCount);

			ClassLoader loader1 = this.getClass().getClassLoader();
			InputStream sinput = loader1.getResourceAsStream("config/saucelab.properties");
			sauceProps.load(sinput);
			runWithCiCd=props.getProperty("runWithCICD");
			runWithCiCd=runWithCiCd.toLowerCase();
			System.out.println("run CI/CD flag " +runWithCiCd);
			if (runWithCiCd.equals("y")) {
				System.out.println("flag y");
				dir_path = System.getProperty("user.dir");
			} else {

				dir_path = props.getProperty("userdir");
				// System.out.println("dir path .. "+dir_path);

			}
			BASEURL = props.getProperty(appName + "_baseurl");
			System.out.println("base url in test data"+BASEURL);
			SECRET_KEY = props.getProperty("secretKey");
			SECRET_VALUE = props.getProperty("secretValue");
			USERNAME = props.getProperty(appName + "_username");
			PASSWORD = props.getProperty(appName + "_password");

			System.out.println("dir path.." + dir_path);
			PROJ_CONFIG_Path = "//resources//config//Project_Config.properties";

			REST_URL_PURCHASE_JOBS = props.getProperty("removePurchaseJobsRestUrl");

			waitTimeout = Integer.parseInt(props.getProperty("explicitWaitInSec"));

			if (props.getProperty("execution_env").equalsIgnoreCase("local")) {
				System.out.println("browser before reading test data " + BROWSER_TYPE);

				if (BROWSER_TYPE.isEmpty()) {
					BROWSER_TYPE = props.getProperty("browser");
				}
			}

			OS_VERSION = props.getProperty("os_version");
			OS_NAME = props.getProperty("os_name");
			CaptureScreenshotOnFail = props.getProperty("CaptureScreenshotOnFail");
			SAUCE_URL = sauceProps.getProperty("sauce_url");
			PARENT_TUNNEL = sauceProps.getProperty("parent_tunnel");
			TUNNEL_IDETIFIER = sauceProps.getProperty("tunnel_identifier");
			RESOLUTION = sauceProps.getProperty("resolution");
			BROWSER_VERSION = sauceProps.getProperty("browser_version");
			EXECUTION_ENV = props.getProperty("execution_env");
			node_URL = props.getProperty("node_url");
			if (PLATFORM.isEmpty())
				PLATFORM = props.getProperty("platform");

			if (props.getProperty("execution_env").equalsIgnoreCase("saucelabs")) {
				System.out.println("browser before reading test data " + BROWSER_TYPE);

				if (BROWSER_TYPE.isEmpty()) {
					BROWSER_TYPE = sauceProps.getProperty("browser");

				}
				PLATFORM = sauceProps.getProperty("platform");

			}
			if (runWithCiCd.equals("y")) {
				try
				{
			if (System.getProperty("env").equalsIgnoreCase("Prod")|| System.getProperty("env").equalsIgnoreCase("Production")) {
				
				

				System.out.println("Environmnet prod");
				input = loader.getResourceAsStream("config/prod.properties");
				prodProps.load(input);
				BASEURL = prodProps.getProperty(appName + "_baseurl");

				USERNAME = prodProps.getProperty(appName + "_username");
				PASSWORD = prodProps.getProperty(appName + "_password");

			} 
			if (System.getProperty("env").equalsIgnoreCase("Staging")) {
				System.out.println("Environmnet staging");

				input = loader.getResourceAsStream("config/staging.properties");
				stagProps.load(input);
				BASEURL = stagProps.getProperty(appName + "_baseurl");
				USERNAME = stagProps.getProperty(appName + "_username");
				PASSWORD = stagProps.getProperty(appName + "_password");

			}
			if (System.getProperty("env").equalsIgnoreCase("CIT")) {
				System.out.println("Environmnet cit");
				
				
				input = loader.getResourceAsStream("config/cit.properties");
				citProps.load(input);
				BASEURL = citProps.getProperty(appName + "_baseurl");
				USERNAME = citProps.getProperty(appName + "_username");
				PASSWORD = citProps.getProperty(appName + "_password");

			}
			if (System.getProperty("env").equalsIgnoreCase("CSO")) {
				System.out.println("Environmnet cso");
				
				
				input = loader.getResourceAsStream("config/cso.properties");
				csoProps.load(input);
				BASEURL = csoProps.getProperty(appName + "_baseurl");
				USERNAME = csoProps.getProperty(appName + "_username");
				PASSWORD = csoProps.getProperty(appName + "_password");

			}
				}
				catch(NullPointerException e)
				{
				System.out.println("env is not passed, default QA is considered");
				}

			}
			

		} catch (Exception ex) {
			ex.printStackTrace();
		} catch (Error ex) {
			ex.printStackTrace();
		}
	}

	public InitTests() {

	}

	
	public static String getPropValue(String key) {
		return props.getProperty(key);
	}
}
