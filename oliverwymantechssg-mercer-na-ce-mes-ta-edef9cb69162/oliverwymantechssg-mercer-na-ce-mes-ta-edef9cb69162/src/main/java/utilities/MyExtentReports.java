package utilities;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.epam.ta.reportportal.log4j.appender.ReportPortalAppender;
import static listeners.InitReports.logger;

/**
 * @author YugandharReddyGorrep
 *
 */
public class MyExtentReports
{
	/**
	 * Setting up the platform details
	 * 
	 * @param browserType
	 * @param url
	 * @param platform
	 * @param version
	 * @throws Exception
	 */
	public static String  timeStampFrJsp;
	public static ExtentReports reports;

	public static void setPlatformDetails(String browserType, String platform, String version, String url,String sauceOrNode)
			throws IOException
	{
	
	reports.setSystemInfo("User", System.getProperty("user.name"));
		reports.setSystemInfo("Browser", browserType);
		reports.setSystemInfo("Browser Version", version);
		 reports.setSystemInfo("platform", platform.toString());
		reports.setSystemInfo("URL", url);
		if(!sauceOrNode.isEmpty())
		reports.setSystemInfo("Node", sauceOrNode);

	}
	/**
	 * Initialization of extent reports with time stamp
	 * 
	 */
	
	public void initExtentReports() throws IOException
	{
		try
		{
		Properties props = new Properties();

	String reportLoc="/src/main/WebContent/extentReports";

			ClassLoader loader = this.getClass().getClassLoader();
			 InputStream input = loader.getResourceAsStream("config/testdata.properties");
			props.load(input);
			String dir_path;
			String runwithCI=props.getProperty("runWithCICD");
			runwithCI=runwithCI.toLowerCase();
			if(runwithCI.equals("y"))
				 dir_path = System.getProperty("user.dir");

			else
				 dir_path = props.getProperty("userdir");
			System.out.println("dir path in extent reports init "+dir_path);

			System.setProperty("atu.reporter.config", dir_path+"/src/main/resources/config/atu.properties");
			
			String environment = props.getProperty("Environment");
			String browser = props.getProperty("browser");
			//if(timeStamp==null)
				String timeStamp=new DateUtils().getCurrTimeStamp();
			System.out.println("extent report time stamp "+timeStamp);

		File f = new File(dir_path + reportLoc);
		if (!f.exists())
		{			f.mkdir();
		}
		String reportFilePath;
		if(runwithCI.equals("y"))
		{
			String reportName=System.getProperty("suiteXmlFile");
			System.out.println("report name "+reportName);
			reportName=reportName.replace(".xml", ".html");
			f = new File(dir_path + reportLoc+"/" + reportName );
			f.createNewFile();
			 reportFilePath = dir_path + reportLoc+"/" + reportName ;		
			 }
			else
			{		
		f = new File(dir_path + reportLoc+"/" + timeStamp + ".html");
		f.createNewFile();
		 reportFilePath = dir_path + reportLoc+"/" + timeStamp + ".html";
			}
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(reportFilePath);
		reports = new ExtentReports();
		logger.addAppender(new ReportPortalAppender());
		reports.attachReporter(htmlReporter);
		// reports.addSystemInfo("Selenium Version", "2.53.1");
		//reports.addSystemInfo("Browser",browser);
		try
		{
			String env=System.getProperty("env");
		reports.setSystemInfo("Environment",env);
		if(env==null)
			reports.setSystemInfo("Environment","QA");
		System.out.println("env for reports"+env);
		}
		catch(NullPointerException e)
		{
			System.out.println("env for reports default to QA");
			reports.setSystemInfo("Environment","QA");

		}
		
	}
	
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	
	}
	}
